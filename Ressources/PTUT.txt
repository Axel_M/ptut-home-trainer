Sub Create_Tables()

DoCmd.RunSQL "CREATE TABLE Exercice(" & _
   "Id_Exercice COUNTER," & _
   "Nom VARCHAR(25)," & _
   "Description TEXT," & _
   "image ," & _
   "video VARCHAR(50)," & _
   "PRIMARY KEY(Id_Exercice)" & _
");"   

DoCmd.RunSQL "CREATE TABLE Compte_Client(" & _
   "Id_Compte_Client COUNTER," & _
   "nom VARCHAR(50)," & _
   "prenom VARCHAR(50)," & _
   "pma BYTE," & _
   "fcm BYTE," & _
   "PRIMARY KEY(Id_Compte_Client)" & _
");"   

DoCmd.RunSQL "CREATE TABLE Programme(" & _
   "Id_Programme COUNTER," & _
   "Nom VARCHAR(50) NOT NULL," & _
   "Difficulte VARCHAR(15)," & _
   "Id_Compte_Client INT NOT NULL," & _
   "PRIMARY KEY(Id_Programme)," & _
   "FOREIGN KEY(Id_Compte_Client) REFERENCES Compte_Client(Id_Compte_Client)" & _
");"   

DoCmd.RunSQL "CREATE TABLE contenir(" & _
   "Id_Exercice INT," & _
   "Id_Programme INT," & _
   "place BYTE," & _
   "nb_rep BYTE," & _
   "temps SMALLINT," & _
   "repos VARCHAR(50)," & _
   "pourcentage_fcm DECIMAL(3,2)," & _
   "puissance SMALLINT," & _
   "cadence SMALLINT," & _
   "PRIMARY KEY(Id_Exercice, Id_Programme, place)," & _
   "FOREIGN KEY(Id_Exercice) REFERENCES Exercice(Id_Exercice)," & _
   "FOREIGN KEY(Id_Programme) REFERENCES Programme(Id_Programme)" & _
");"   

DoCmd.RunSQL "CREATE TABLE utiliser(" & _
   "Id_Compte_Client INT," & _
   "Id_Programme INT," & _
   "DateUtilisation DATETIME," & _
   "PRIMARY KEY(Id_Compte_Client, Id_Programme, DateUtilisation)," & _
   "FOREIGN KEY(Id_Compte_Client) REFERENCES Compte_Client(Id_Compte_Client)," & _
   "FOREIGN KEY(Id_Programme) REFERENCES Programme(Id_Programme)" & _
");"   

End Sub