/**
 * Code généré par WINDEV Mobile - NE PAS MODIFIER !
 * Objet WINDEV Mobile : Fenêtre
 * Classe Android : FEN_Programm_Preregister3
 * Date : 02/04/2021 14:52:48
 * Version de wdjava.dll  : 25.0.221.6
 */


package com.logicorp.home_biking.wdgen;


import com.logicorp.home_biking.*;
import fr.pcsoft.wdjava.core.types.*;
import fr.pcsoft.wdjava.core.*;
import fr.pcsoft.wdjava.ui.champs.fenetre.*;
import fr.pcsoft.wdjava.ui.champs.zr.*;
import fr.pcsoft.wdjava.ui.champs.saisie.*;
import fr.pcsoft.wdjava.ui.cadre.*;
import fr.pcsoft.wdjava.ui.champs.libelle.*;
import fr.pcsoft.wdjava.ui.champs.image.*;
import fr.pcsoft.wdjava.api.*;
import fr.pcsoft.wdjava.core.parcours.*;
import fr.pcsoft.wdjava.core.parcours.champ.*;
import fr.pcsoft.wdjava.ui.champs.combo.*;
import fr.pcsoft.wdjava.ui.champs.groupeoptions.*;
import fr.pcsoft.wdjava.core.application.*;
import fr.pcsoft.wdjava.ui.activite.*;
/*Imports trouvés dans le code WL*/
/*Fin Imports trouvés dans le code WL*/



public class GWDFFEN_Programm_Preregister3 extends WDFenetre
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs de FEN_Programm_Preregister3
////////////////////////////////////////////////////////////////////////////

/**
 * ZR_ProgrammePreenregistre
 */
class GWDZR_ProgrammePreenregistre extends WDZoneRepeteeFichierEnMemoire
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FEN_Programm_Preregister3.ZR_ProgrammePreenregistre
////////////////////////////////////////////////////////////////////////////

/**
 * ATT_Nom
 */
class GWDATT_Nom extends WDAttributZR
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FEN_Programm_Preregister3.ZR_ProgrammePreenregistre.ATT_Nom
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setNom("ATT_Nom");

super.setLiaisonFichier("req_recherche_programmepreenregistre", "nom");

super.setChampAssocie(mWD_Nom);

super.setProprieteAssocie(EWDPropriete.PROP_VALEUR);

activerEcoute();
super.terminerInitialisation();
}
// Pas de traitement pour le champ FEN_Programm_Preregister3.ZR_ProgrammePreenregistre.ATT_Nom

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDATT_Nom mWD_ATT_Nom = new GWDATT_Nom();

/**
 * ATT_Duree
 */
class GWDATT_Duree extends WDAttributZR
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°2 de FEN_Programm_Preregister3.ZR_ProgrammePreenregistre.ATT_Duree
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setNom("ATT_Duree");

super.setProprieteAssocie(EWDPropriete.PROP_VALEUR);

activerEcoute();
super.terminerInitialisation();
}
// Pas de traitement pour le champ FEN_Programm_Preregister3.ZR_ProgrammePreenregistre.ATT_Duree

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDATT_Duree mWD_ATT_Duree = new GWDATT_Duree();

/**
 * ATT_Difficulte
 */
class GWDATT_Difficulte extends WDAttributZR
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°3 de FEN_Programm_Preregister3.ZR_ProgrammePreenregistre.ATT_Difficulte
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setNom("ATT_Difficulte");

super.setLiaisonFichier("req_recherche_programmepreenregistre", "difficulte");

super.setChampAssocie(mWD_Difficulte);

super.setProprieteAssocie(EWDPropriete.PROP_VALEUR);

activerEcoute();
super.terminerInitialisation();
}
// Pas de traitement pour le champ FEN_Programm_Preregister3.ZR_ProgrammePreenregistre.ATT_Difficulte

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDATT_Difficulte mWD_ATT_Difficulte = new GWDATT_Difficulte();

/**
 * ATT_NbExo
 */
class GWDATT_NbExo extends WDAttributZR
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°4 de FEN_Programm_Preregister3.ZR_ProgrammePreenregistre.ATT_NbExo
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setNom("ATT_NbExo");

super.setProprieteAssocie(EWDPropriete.PROP_VALEUR);

activerEcoute();
super.terminerInitialisation();
}
// Pas de traitement pour le champ FEN_Programm_Preregister3.ZR_ProgrammePreenregistre.ATT_NbExo

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDATT_NbExo mWD_ATT_NbExo = new GWDATT_NbExo();

/**
 * Id_Programme
 */
class GWDId_Programme extends WDChampSaisieSimple
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°5 de FEN_Programm_Preregister3.ZR_ProgrammePreenregistre.Id_Programme
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setRectLibelle(0,2,128,40);
super.setRectCompPrincipal(128,2,78,40);
super.setQuid(3448017831620369788l);

super.setChecksum("1087137916");

super.setNom("Id_Programme");

super.setType(20004);

super.setBulle("");

super.setLibelle("Id_Programme");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setTaille(0);

super.setNavigable(false);

super.setEtatInitial(1);

super.setPositionInitiale(65, 68);

super.setTailleInitiale(206, 44);

super.setValeurInitiale("");

super.setPlan(0);

super.setCadrageHorizontal(2);

super.setMotDePasse(false);

super.setLiaisonFichier("req_recherche_programmepreenregistre", "id_programme");

super.setTypeSaisie(1);

super.setMasqueSaisie(new WDChaineU("+9 999 999 999"));

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(false);

super.setAltitude(1);

super.setAncrageInitial(8, 1000, 1000, 1000, 1000, 0);

super.setEllipse(0);

super.setIndication("");

super.setNumTab(-1);

super.setModeAscenseur(2, 2);

super.setEffacementAutomatique(true);

super.setFinSaisieAutomatique(false);

super.setLettreAppel(65535);

super.setSelectionEnAffichage(true);

super.setPersistant(true);

super.setClavierEnSaisie(true);

super.setMasqueAffichage(new WDChaineU(""));

super.setParamBtnActionClavier(0, "");

super.setRetraitGauche(2);

super.setMiseABlancSiZero(true);

super.setVerifieOrthographe(true);

super.setTauxParallaxe(0, 0);

super.setBoutonSuppression(0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -2, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN("C:\\Users\\axelm\\Desktop\\COURS IUT INFO\\Semestre 3\\M3302 - Projet tutore - Mise en situation professionnelle\\ptut-home-trainer\\Home_Biking\\Gabarits\\WM\\210 Material Design Orange\\Material Design Orange_Edt@dpi1x.png?E5_3NP_8_8_8_8", new int[] {1,4,1,2,2,2,1,4,1}, new int[] {8, 8, 8, 8}, 0xFFFFFFFF, 1, 5));

super.setStyleSaisie(0x222222, creerPolice_GEN("Roboto", -8.000000, 0));

super.setStyleTexteIndication(0x8E8E8F, creerPolice_GEN("Roboto", -8.000000, 0), 0);

super.setStyleJeton(WDCadreFactory.creerCadre_GEN(2, 0x98FF, 0x187F, 0x98FF, 4.000000, 4.000000, 1, 1), 0xFFFFFF, "", 1);

activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
public int getModeSaisieAssistee()
{
return 1;
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDId_Programme mWD_Id_Programme = new GWDId_Programme();

/**
 * Difficulte
 */
class GWDDifficulte extends WDChampSaisieSimple
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°6 de FEN_Programm_Preregister3.ZR_ProgrammePreenregistre.Difficulte
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setRectCompPrincipal(22,2,148,44);
super.setQuid(3448017831620435324l);

super.setChecksum("1087156991");

super.setNom("Difficulte");

super.setType(20001);

super.setBulle("");

super.setLibelle("");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setTaille(15);

super.setNavigable(true);

super.setEtatInitial(1);

super.setPositionInitiale(8, 66);

super.setTailleInitiale(170, 48);

super.setValeurInitiale("");

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setMotDePasse(false);

super.setLiaisonFichier("req_recherche_programmepreenregistre", "difficulte");

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(false);

super.setAltitude(2);

super.setAncrageInitial(8, 1000, 1000, 1000, 1000, 0);

super.setEllipse(0);

super.setIndication("");

super.setNumTab(2);

super.setModeAscenseur(2, 2);

super.setEffacementAutomatique(true);

super.setFinSaisieAutomatique(false);

super.setLettreAppel(65535);

super.setSelectionEnAffichage(true);

super.setPersistant(true);

super.setClavierEnSaisie(true);

super.setMasqueAffichage(new WDChaineU(""));

super.setParamBtnActionClavier(0, "");

super.setRetraitGauche(2);

super.setMiseABlancSiZero(true);

super.setVerifieOrthographe(true);

super.setTauxParallaxe(0, 0);

super.setBoutonSuppression(0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -1, 0, 0xFFFFFF);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN("C:\\Users\\axelm\\Desktop\\COURS IUT INFO\\Semestre 3\\M3302 - Projet tutore - Mise en situation professionnelle\\ptut-home-trainer\\Home_Biking\\Gabarits\\WM\\210 Material Design Orange\\Material Design Orange_Edt@dpi1x.png?E5_3NP_8_8_8_8", new int[] {1,4,1,2,2,2,1,4,1}, new int[] {8, 8, 8, 8}, 0xFFFFFFFF, 1, 5));

super.setStyleSaisie(0x222222, creerPolice_GEN("Roboto", -8.000000, 0));

super.setStyleTexteIndication(0x8E8E8F, creerPolice_GEN("Roboto", -8.000000, 0), 0);

super.setStyleJeton(WDCadreFactory.creerCadre_GEN(2, 0x98FF, 0x187F, 0x98FF, 4.000000, 4.000000, 1, 1), 0xFFFFFF, "", 1);

activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
public int getModeSaisieAssistee()
{
return 1;
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDDifficulte mWD_Difficulte = new GWDDifficulte();

/**
 * Id_Compte_Client
 */
class GWDId_Compte_Client extends WDChampSaisieSimple
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°7 de FEN_Programm_Preregister3.ZR_ProgrammePreenregistre.Id_Compte_Client
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setRectLibelle(0,2,128,38);
super.setRectCompPrincipal(128,2,74,38);
super.setQuid(3448017831620500860l);

super.setChecksum("1087268988");

super.setNom("Id_Compte_Client");

super.setType(20004);

super.setBulle("");

super.setLibelle("Id_Compte_Client");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setTaille(0);

super.setNavigable(false);

super.setEtatInitial(1);

super.setPositionInitiale(531, 72);

super.setTailleInitiale(202, 42);

super.setValeurInitiale("");

super.setPlan(0);

super.setCadrageHorizontal(2);

super.setMotDePasse(false);

super.setLiaisonFichier("req_recherche_programmepreenregistre", "id_compte_client");

super.setTypeSaisie(1);

super.setMasqueSaisie(new WDChaineU("+9 999 999 999"));

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(false);

super.setAltitude(3);

super.setAncrageInitial(8, 1000, 1000, 1000, 1000, 0);

super.setEllipse(0);

super.setIndication("");

super.setNumTab(-1);

super.setModeAscenseur(2, 2);

super.setEffacementAutomatique(true);

super.setFinSaisieAutomatique(false);

super.setLettreAppel(65535);

super.setSelectionEnAffichage(true);

super.setPersistant(false);

super.setClavierEnSaisie(true);

super.setMasqueAffichage(new WDChaineU(""));

super.setParamBtnActionClavier(0, "");

super.setRetraitGauche(2);

super.setMiseABlancSiZero(true);

super.setVerifieOrthographe(true);

super.setTauxParallaxe(0, 0);

super.setBoutonSuppression(0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -2, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN("C:\\Users\\axelm\\Desktop\\COURS IUT INFO\\Semestre 3\\M3302 - Projet tutore - Mise en situation professionnelle\\ptut-home-trainer\\Home_Biking\\Gabarits\\WM\\210 Material Design Orange\\Material Design Orange_Edt@dpi1x.png?E5_3NP_8_8_8_8", new int[] {1,4,1,2,2,2,1,4,1}, new int[] {8, 8, 8, 8}, 0xFFFFFFFF, 1, 5));

super.setStyleSaisie(0x222222, creerPolice_GEN("Roboto", -8.000000, 0));

super.setStyleTexteIndication(0x8E8E8F, creerPolice_GEN("Roboto", -8.000000, 0), 0);

super.setStyleJeton(WDCadreFactory.creerCadre_GEN(2, 0x98FF, 0x187F, 0x98FF, 4.000000, 4.000000, 1, 1), 0xFFFFFF, "", 1);

activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
public int getModeSaisieAssistee()
{
return 1;
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDId_Compte_Client mWD_Id_Compte_Client = new GWDId_Compte_Client();

/**
 * LIB_Temps
 */
class GWDLIB_Temps extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°8 de FEN_Programm_Preregister3.ZR_ProgrammePreenregistre.LIB_Temps
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3448017831620566396l);

super.setChecksum("1087288519");

super.setNom("LIB_Temps");

super.setType(3);

super.setBulle("");

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("Durée : ");

super.setNote("", "");

super.setCurseurSouris(0);

super.setEtatInitial(0);

super.setPositionInitiale(2, 118);

super.setTailleInitiale(196, 19);

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(4);

super.setAncrageInitial(1, 1000, 1000, 1000, 1000, 0);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, 0xFFFFFFFF, creerPolice_GEN("Roboto", -8.000000, 0), 3, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_Temps mWD_LIB_Temps = new GWDLIB_Temps();

/**
 * LIB_NB_Exercice
 */
class GWDLIB_NB_Exercice extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°9 de FEN_Programm_Preregister3.ZR_ProgrammePreenregistre.LIB_NB_Exercice
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3448017831620631932l);

super.setChecksum("1087354055");

super.setNom("LIB_NB_Exercice");

super.setType(3);

super.setBulle("");

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("NB Exercices :");

super.setNote("", "");

super.setCurseurSouris(0);

super.setEtatInitial(0);

super.setPositionInitiale(2, 45);

super.setTailleInitiale(228, 19);

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(5);

super.setAncrageInitial(0, 1000, 1000, 1000, 1000, 0);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, 0xFFFFFFFF, creerPolice_GEN("Roboto", -8.000000, 0), 3, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_NB_Exercice mWD_LIB_NB_Exercice = new GWDLIB_NB_Exercice();

/**
 * Nom
 */
class GWDNom extends WDChampSaisieSimple
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°10 de FEN_Programm_Preregister3.ZR_ProgrammePreenregistre.Nom
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setRectCompPrincipal(0,2,234,31);
super.setQuid(3448017831620697468l);

super.setChecksum("1087419135");

super.setNom("Nom");

super.setType(20001);

super.setBulle("");

super.setLibelle("Nom Prog");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setTaille(50);

super.setNavigable(true);

super.setEtatInitial(1);

super.setPositionInitiale(8, 6);

super.setTailleInitiale(234, 35);

super.setValeurInitiale("");

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setMotDePasse(false);

super.setLiaisonFichier("req_recherche_programmepreenregistre", "nom");

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(6);

super.setAncrageInitial(8, 1000, 1000, 1000, 1000, 0);

super.setEllipse(0);

super.setIndication("");

super.setNumTab(1);

super.setModeAscenseur(2, 2);

super.setEffacementAutomatique(true);

super.setFinSaisieAutomatique(false);

super.setLettreAppel(65535);

super.setSelectionEnAffichage(true);

super.setPersistant(false);

super.setClavierEnSaisie(true);

super.setMasqueAffichage(new WDChaineU(""));

super.setParamBtnActionClavier(0, "");

super.setRetraitGauche(2);

super.setMiseABlancSiZero(true);

super.setVerifieOrthographe(false);

super.setTauxParallaxe(0, 0);

super.setBoutonSuppression(0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -2, 0, 0xF1000000);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0x0, 0x0, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN(1, 0xF4800000, 0xBFBFBF, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1));

super.setStyleSaisie(0x222222, creerPolice_GEN("Roboto", -8.000000, 0));

super.setStyleTexteIndication(0x8E8E8F, creerPolice_GEN("Roboto", -8.000000, 0), 0);

super.setStyleJeton(WDCadreFactory.creerCadre_GEN(2, 0x98FF, 0x187F, 0x98FF, 4.000000, 4.000000, 1, 1), 0xFFFFFF, "", 1);

activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
public int getModeSaisieAssistee()
{
return 1;
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDNom mWD_Nom = new GWDNom();

/**
 * LIB_Container
 */
class GWDLIB_Container extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°11 de FEN_Programm_Preregister3.ZR_ProgrammePreenregistre.LIB_Container
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3448017831620763004l);

super.setChecksum("1087485127");

super.setNom("LIB_Container");

super.setType(3);

super.setBulle("");

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("");

super.setNote("", "");

super.setCurseurSouris(0);

super.setEtatInitial(0);

super.setPositionInitiale(0, 41);

super.setTailleInitiale(295, 99);

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(7);

super.setAncrageInitial(10, 1000, 1000, 1000, 1000, 0);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0xF1000000, 0xFFFFFFFF, creerPolice_GEN("MS Shell Dlg", -12.000000, 0), 3, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(2, 0xE0E0E0, 0x808080, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_Container mWD_LIB_Container = new GWDLIB_Container();

/**
 * IMG_Diffi
 */
class GWDIMG_Diffi extends WDChampImage
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°12 de FEN_Programm_Preregister3.ZR_ProgrammePreenregistre.IMG_Diffi
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3448017831620828540l);

super.setChecksum("1087552943");

super.setNom("IMG_Diffi");

super.setType(30001);

super.setBulle("");

super.setLibelle("");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setNavigable(false);

super.setEtatInitial(0);

super.setPositionInitiale(241, 46);

super.setTailleInitiale(44, 44);

super.setValeurInitiale("");

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(8);

super.setAncrageInitial(4, 1000, 1000, 1000, 1000, 0);

super.setTransparence(1);

super.setParamImage(2097158, 0, true, 100);

super.setSymetrie(0);

super.setZoneClicage(true);

super.setPCodeMultitouch(false);

super.setChargementEnTacheDeFond(false);

super.setOrientationExif(false);

super.setParamAnimation(1, 1, false, 300, true, false);

super.setAnimationInitiale(false);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x0, creerPolice_GEN("MS Shell Dlg", -11.000000, 0), -1, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xF1000000, 0xF3000000, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDIMG_Diffi mWD_IMG_Diffi = new GWDIMG_Diffi();
/**
 * Initialise tous les champs de FEN_Programm_Preregister3.ZR_ProgrammePreenregistre
 */
public void initialiserSousObjets()
{
////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FEN_Programm_Preregister3.ZR_ProgrammePreenregistre
////////////////////////////////////////////////////////////////////////////
super.initialiserSousObjets();
mWD_ATT_Nom.initialiserObjet();
super.ajouterAttributZR(mWD_ATT_Nom);
mWD_ATT_Duree.initialiserObjet();
super.ajouterAttributZR(mWD_ATT_Duree);
mWD_ATT_Difficulte.initialiserObjet();
super.ajouterAttributZR(mWD_ATT_Difficulte);
mWD_ATT_NbExo.initialiserObjet();
super.ajouterAttributZR(mWD_ATT_NbExo);
mWD_Id_Programme.initialiserObjet();
super.ajouterChamp("Id_Programme",mWD_Id_Programme);
mWD_Difficulte.initialiserObjet();
super.ajouterChamp("Difficulte",mWD_Difficulte);
mWD_Id_Compte_Client.initialiserObjet();
super.ajouterChamp("Id_Compte_Client",mWD_Id_Compte_Client);
mWD_LIB_Temps.initialiserObjet();
super.ajouterChamp("LIB_Temps",mWD_LIB_Temps);
mWD_LIB_NB_Exercice.initialiserObjet();
super.ajouterChamp("LIB_NB_Exercice",mWD_LIB_NB_Exercice);
mWD_Nom.initialiserObjet();
super.ajouterChamp("Nom",mWD_Nom);
mWD_LIB_Container.initialiserObjet();
super.ajouterChamp("LIB_Container",mWD_LIB_Container);
mWD_IMG_Diffi.initialiserObjet();
super.ajouterChamp("IMG_Diffi",mWD_IMG_Diffi);
creerAttributAuto();
}
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setRectCompPrincipal(0,0,293,143);
super.setQuid(3448017831620304252l);

super.setChecksum("1087038687");

super.setNom("ZR_ProgrammePreenregistre");

super.setType(30);

super.setBulle("");

super.setLibelle("Zone répétée");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setNavigable(true);

super.setEtatInitial(1);

super.setPositionInitiale(11, 114);

super.setTailleInitiale(296, 429);

super.setValeurInitiale("");

super.setPlan(0);

super.setSourceRemplissage("req_recherche_programmepreenregistre", "id_programme", "id_programme", true, "", false);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(1);

super.setAncrageInitial(10, 1000, 1000, 1000, 1000, 0);

super.setNumTab(4);

super.setModeAscenseur(1, 1);

super.setModeSelection(99);

super.setSaisieEnCascade(false);

super.setLettreAppel(65535);

super.setEnregistrementSortieLigne(true);

super.setPersistant(false);

super.setParamAffichage(0, 0, 1, 293, 143);

super.setBtnEnrouleDeroule(true);

super.setScrollRapide(false, null);

super.setDeplacementParDnd(0);

super.setSwipe(0, "", false, false, "", false, false);

super.setRecyclageChamp(true);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x212121, creerPolice_GEN("Roboto", -8.000000, 0), -1, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1), 0, 0, 0, 0);

super.setStyleSeparateurVerticaux(false, 0xFFFFFFFF);

super.setStyleSeparateurHorizontaux(0, 0xFFFFFFFF);

super.setDessinerLigneVide(false);

super.setCouleurCellule(0xFFFFFFFF, 0xFFFFFFFF, 0x212121, 0x80CCFF, 0xFFFFFF);

super.setImagePlusMoins("C:\\Users\\axelm\\Desktop\\COURS IUT INFO\\Semestre 3\\M3302 - Projet tutore - Mise en situation professionnelle\\ptut-home-trainer\\Home_Biking\\Gabarits\\WM\\210 Material Design Orange\\Material Design Orange_Break_Pict@dpi1x.png?E2_4O");

activerEcoute();
initialiserSousObjets();
super.terminerInitialisation();
}

/**
 * Traitement: Fin d'initialisation de ZR_ProgrammePreenregistre
 */
public void finInit()
{
super.finInit();

// nIdProg			est un entier
//MAP:2fd9d1f310f0dd7c:00000023:1:FEN_Programm_Preregister3.ZR_ProgrammePreenregistre:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDZR_ProgrammePreenregistre:Fin d'initialisation de ZR_ProgrammePreenregistre

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables locales au traitement
// (En WLangage les variables sont encore visibles après la fin du bloc dans lequel elles sont déclarées)
////////////////////////////////////////////////////////////////////////////
WDObjet vWD_nIdProg = new WDEntier4();

WDObjet vWD_sDiff = new WDChaineU();

WDObjet vWD_nTemps = new WDEntier4();

WDObjet vWD_nExercice = new WDEntier4();

WDObjet vWD_i = new WDEntier4();



// nIdProg			est un entier
//MAP:2fd9d1f310f0dd7c:00000023:1:FEN_Programm_Preregister3.ZR_ProgrammePreenregistre:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDZR_ProgrammePreenregistre:Fin d'initialisation de ZR_ProgrammePreenregistre


// sDiff			est une chaîne
//MAP:2fd9d1f310f0dd7c:00000023:2:FEN_Programm_Preregister3.ZR_ProgrammePreenregistre:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDZR_ProgrammePreenregistre:Fin d'initialisation de ZR_ProgrammePreenregistre


// nTemps			est un entier
//MAP:2fd9d1f310f0dd7c:00000023:3:FEN_Programm_Preregister3.ZR_ProgrammePreenregistre:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDZR_ProgrammePreenregistre:Fin d'initialisation de ZR_ProgrammePreenregistre


// nExercice		est un entier
//MAP:2fd9d1f310f0dd7c:00000023:4:FEN_Programm_Preregister3.ZR_ProgrammePreenregistre:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDZR_ProgrammePreenregistre:Fin d'initialisation de ZR_ProgrammePreenregistre


// i est un entier
//MAP:2fd9d1f310f0dd7c:00000023:5:FEN_Programm_Preregister3.ZR_ProgrammePreenregistre:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDZR_ProgrammePreenregistre:Fin d'initialisation de ZR_ProgrammePreenregistre


// i=1
//MAP:2fd9d1f310f0dd7c:00000023:7:FEN_Programm_Preregister3.ZR_ProgrammePreenregistre:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDZR_ProgrammePreenregistre:Fin d'initialisation de ZR_ProgrammePreenregistre
vWD_i.setValeur(1);

// Pour CHAQUE LIGNE DE ZR_ProgrammePreenregistre
//MAP:2fd9d1f310f0dd7c:00000023:8:FEN_Programm_Preregister3.ZR_ProgrammePreenregistre:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDZR_ProgrammePreenregistre:Fin d'initialisation de ZR_ProgrammePreenregistre
IWDParcours parcours1 = null;
try
{
parcours1 = WDParcoursChamp.pourTout(this, 0x2);
while(parcours1.testParcours())
{
// 	nIdProg		= ZR_ProgrammePreenregistre[i].Id_Programme..Valeur //récupération de l'idProgramme de la ligne d'indice i de la zone répétée
//MAP:2fd9d1f310f0dd7c:00000023:9:FEN_Programm_Preregister3.ZR_ProgrammePreenregistre:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDZR_ProgrammePreenregistre:Fin d'initialisation de ZR_ProgrammePreenregistre
vWD_nIdProg.setValeur(this.get(vWD_i).get("Id_Programme").getProp(EWDPropriete.PROP_VALEUR));

// 	sDiff		= ZR_ProgrammePreenregistre[i].Difficulte..Valeur //récupération de la difficulté du programme de la ligne d'indice i de la zone répétée
//MAP:2fd9d1f310f0dd7c:00000023:a:FEN_Programm_Preregister3.ZR_ProgrammePreenregistre:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDZR_ProgrammePreenregistre:Fin d'initialisation de ZR_ProgrammePreenregistre
vWD_sDiff.setValeur(this.get(vWD_i).get("Difficulte").getProp(EWDPropriete.PROP_VALEUR));

// 	nTemps		=0
//MAP:2fd9d1f310f0dd7c:00000023:b:FEN_Programm_Preregister3.ZR_ProgrammePreenregistre:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDZR_ProgrammePreenregistre:Fin d'initialisation de ZR_ProgrammePreenregistre
vWD_nTemps.setValeur(0);

// 	nExercice	=0
//MAP:2fd9d1f310f0dd7c:00000023:c:FEN_Programm_Preregister3.ZR_ProgrammePreenregistre:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDZR_ProgrammePreenregistre:Fin d'initialisation de ZR_ProgrammePreenregistre
vWD_nExercice.setValeur(0);

// 	HLitRecherchePremier(contenir,Id_Programme,nIdProg)
//MAP:2fd9d1f310f0dd7c:00000023:f:FEN_Programm_Preregister3.ZR_ProgrammePreenregistre:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDZR_ProgrammePreenregistre:Fin d'initialisation de ZR_ProgrammePreenregistre
WDAPIHF.hLitRecherchePremier(WDAPIHF.getFichierSansCasseNiAccent("contenir"),WDAPIHF.getRubriqueSansCasseNiAccent("id_programme"),vWD_nIdProg);

// 	BOUCLE
//MAP:2fd9d1f310f0dd7c:00000023:10:FEN_Programm_Preregister3.ZR_ProgrammePreenregistre:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDZR_ProgrammePreenregistre:Fin d'initialisation de ZR_ProgrammePreenregistre
{
do
{
// 		nExercice++ //Ajoute un au nombre d'exercices
//MAP:2fd9d1f310f0dd7c:00000023:11:FEN_Programm_Preregister3.ZR_ProgrammePreenregistre:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDZR_ProgrammePreenregistre:Fin d'initialisation de ZR_ProgrammePreenregistre
vWD_nExercice.opInc();

// 		nTemps+=((contenir.temps+contenir.repos)*contenir.nb_rep) //Ajoute le temps nécessaire à l'exécution d'un exercice complet pour le programme
//MAP:2fd9d1f310f0dd7c:00000023:12:FEN_Programm_Preregister3.ZR_ProgrammePreenregistre:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDZR_ProgrammePreenregistre:Fin d'initialisation de ZR_ProgrammePreenregistre
vWD_nTemps.setValeur(vWD_nTemps.opPlus(WDAPIHF.getFichierSansCasseNiAccent("contenir").getRubriqueSansCasseNiAccent("temps").opPlus(WDAPIHF.getFichierSansCasseNiAccent("contenir").getRubriqueSansCasseNiAccent("repos")).opMult(WDAPIHF.getFichierSansCasseNiAccent("contenir").getRubriqueSansCasseNiAccent("nb_rep"))));

// 		HLitSuivant(contenir) //Passe à l'exercice suivant
//MAP:2fd9d1f310f0dd7c:00000023:13:FEN_Programm_Preregister3.ZR_ProgrammePreenregistre:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDZR_ProgrammePreenregistre:Fin d'initialisation de ZR_ProgrammePreenregistre
WDAPIHF.hLitSuivant(WDAPIHF.getFichierSansCasseNiAccent("contenir"));

}
while(WDAPIHF.hTrouve(WDAPIHF.getFichierSansCasseNiAccent("contenir")).opEgal(true));
}

// 	ZR_ProgrammePreenregistre[i].LIB_NB_Exercice			= "NB Exercices : " + nExercice //Affectation au champs LIB_NB_Exercices le nombre d'exercices
//MAP:2fd9d1f310f0dd7c:00000023:17:FEN_Programm_Preregister3.ZR_ProgrammePreenregistre:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDZR_ProgrammePreenregistre:Fin d'initialisation de ZR_ProgrammePreenregistre
this.get(vWD_i).get("LIB_NB_Exercice").setValeur(new WDChaineU("NB Exercices : ").opPlus(vWD_nExercice));

// 	ZR_ProgrammePreenregistre[i].ATT_NbExo					= nExercice
//MAP:2fd9d1f310f0dd7c:00000023:18:FEN_Programm_Preregister3.ZR_ProgrammePreenregistre:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDZR_ProgrammePreenregistre:Fin d'initialisation de ZR_ProgrammePreenregistre
this.get(vWD_i).get("ATT_NbExo").setValeur(vWD_nExercice);

// 	ZR_ProgrammePreenregistre[i].LIB_Temps					= "Durée : " + PartieEntière(nTemps/60)+"min" //Affectation du temps en minutes à partir du temps total récupéré en secondes
//MAP:2fd9d1f310f0dd7c:00000023:19:FEN_Programm_Preregister3.ZR_ProgrammePreenregistre:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDZR_ProgrammePreenregistre:Fin d'initialisation de ZR_ProgrammePreenregistre
this.get(vWD_i).get("LIB_Temps").setValeur(new WDChaineU("Durée : ").opPlus(WDAPIMath.partieEntiere(vWD_nTemps.opDiv(60))).opPlus("min"));

// 	ZR_ProgrammePreenregistre[i].ATT_Duree					= nTemps
//MAP:2fd9d1f310f0dd7c:00000023:1a:FEN_Programm_Preregister3.ZR_ProgrammePreenregistre:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDZR_ProgrammePreenregistre:Fin d'initialisation de ZR_ProgrammePreenregistre
this.get(vWD_i).get("ATT_Duree").setValeur(vWD_nTemps);

// 	ZR_ProgrammePreenregistre[i].IMG_Diffi..CouleurFond		= CouleurDifficulte(sDiff) //Transforme la difficulté en une couleur grâce à la fonction CouleurDifficulte
//MAP:2fd9d1f310f0dd7c:00000023:1b:FEN_Programm_Preregister3.ZR_ProgrammePreenregistre:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDZR_ProgrammePreenregistre:Fin d'initialisation de ZR_ProgrammePreenregistre
this.get(vWD_i).get("IMG_Diffi").setProp(EWDPropriete.PROP_COULEURFOND,GWDCPCouleur_Difficulte.fWD_couleurDifficulte(vWD_sDiff));

// 	i++
//MAP:2fd9d1f310f0dd7c:00000023:1c:FEN_Programm_Preregister3.ZR_ProgrammePreenregistre:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDZR_ProgrammePreenregistre:Fin d'initialisation de ZR_ProgrammePreenregistre
vWD_i.opInc();

}

}
finally
{
if(parcours1 != null)
{
parcours1.finParcours();
}
}


}




/**
 * Traitement: Sélection d'une ligne de ZR_ProgrammePreenregistre
 */
public void selectionLigne()
{
super.selectionLigne();

// OuvreFille(FEN_Preseance,ZR_ProgrammePreenregistre.Id_Programme)
//MAP:2fd9d1f310f0dd7c:00000021:1:FEN_Programm_Preregister3.ZR_ProgrammePreenregistre:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDZR_ProgrammePreenregistre:Sélection d'une ligne de ZR_ProgrammePreenregistre
// OuvreFille(FEN_Preseance,ZR_ProgrammePreenregistre.Id_Programme)
//MAP:2fd9d1f310f0dd7c:00000021:1:FEN_Programm_Preregister3.ZR_ProgrammePreenregistre:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDZR_ProgrammePreenregistre:Sélection d'une ligne de ZR_ProgrammePreenregistre
WDAPIFenetre.ouvreFille(GWDPHome_Biking.getInstance().mWD_FEN_Preseance,new WDObjet[] {mWD_Id_Programme} );

// TableAffiche(FEN_Preseance.TABLE_FEN_Preseance)
//MAP:2fd9d1f310f0dd7c:00000021:2:FEN_Programm_Preregister3.ZR_ProgrammePreenregistre:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDZR_ProgrammePreenregistre:Sélection d'une ligne de ZR_ProgrammePreenregistre
WDAPITable.tableAffiche(GWDPHome_Biking.getInstance().getFEN_Preseance().mWD_TABLE_FEN_Preseance);

// SAI_Recherche_Programme=""
//MAP:2fd9d1f310f0dd7c:00000021:3:FEN_Programm_Preregister3.ZR_ProgrammePreenregistre:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDZR_ProgrammePreenregistre:Sélection d'une ligne de ZR_ProgrammePreenregistre
mWD_SAI_Recherche_Programme.setValeur("");

// ExécuteTraitement(FEN_Preseance,trtModification)
//MAP:2fd9d1f310f0dd7c:00000021:4:FEN_Programm_Preregister3.ZR_ProgrammePreenregistre:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDZR_ProgrammePreenregistre:Sélection d'une ligne de ZR_ProgrammePreenregistre
WDAPIVM.executeTraitement(GWDPHome_Biking.getInstance().mWD_FEN_Preseance,17);

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurSelection();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDZR_ProgrammePreenregistre mWD_ZR_ProgrammePreenregistre;

/**
 * IMG_Button_Back
 */
class GWDIMG_Button_Back extends WDChampImage
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°2 de FEN_Programm_Preregister3.IMG_Button_Back
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3448017831620894076l);

super.setChecksum("1087618479");

super.setNom("IMG_Button_Back");

super.setType(30001);

super.setBulle("");

super.setLibelle("");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setNavigable(false);

super.setEtatInitial(0);

super.setPositionInitiale(12, 0);

super.setTailleInitiale(52, 52);

super.setValeurInitiale("C:\\Users\\axelm\\Desktop\\COURS IUT INFO\\Semestre 3\\M3302 - Projet tutore - Mise en situation professionnelle\\ptut-home-trainer\\Home_Biking\\Bouton_previous_page.png");

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(2);

super.setAncrageInitial(0, 1000, 1000, 1000, 1000, 0);

super.setTransparence(1);

super.setParamImage(2097158, 0, true, 100);

super.setSymetrie(0);

super.setZoneClicage(true);

super.setPCodeMultitouch(false);

super.setChargementEnTacheDeFond(false);

super.setOrientationExif(false);

super.setParamAnimation(1, 1, false, 300, true, false);

super.setAnimationInitiale(false);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -1, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

/**
 * Traitement: Clic sur IMG_Button_Back
 */
public void clicSurBoutonGauche()
{
super.clicSurBoutonGauche();

// Ferme()
//MAP:2fd9d1f310f9dd7c:00000012:1:FEN_Programm_Preregister3.IMG_Button_Back:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDIMG_Button_Back:Clic sur IMG_Button_Back
// Ferme()
//MAP:2fd9d1f310f9dd7c:00000012:1:FEN_Programm_Preregister3.IMG_Button_Back:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDIMG_Button_Back:Clic sur IMG_Button_Back
WDAPIFenetre.ferme();

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurClic();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDIMG_Button_Back mWD_IMG_Button_Back;

/**
 * SAI_Recherche_Programme
 */
class GWDSAI_Recherche_Programme extends WDChampSaisieSimple
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°3 de FEN_Programm_Preregister3.SAI_Recherche_Programme
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setRectLibelle(0,2,56,23);
super.setRectCompPrincipal(56,2,162,23);
super.setQuid(3448017831620959612l);

super.setChecksum("1087681279");

super.setNom("SAI_Recherche_Programme");

super.setType(20001);

super.setBulle("");

super.setLibelle("");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setTaille(0);

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(90, 13);

super.setTailleInitiale(218, 27);

super.setValeurInitiale("");

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setMotDePasse(false);

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(3);

super.setAncrageInitial(8, 1000, 1000, 1000, 1000, 0);

super.setEllipse(0);

super.setIndication("");

super.setNumTab(1);

super.setModeAscenseur(2, 2);

super.setEffacementAutomatique(true);

super.setFinSaisieAutomatique(false);

super.setLettreAppel(65535);

super.setSelectionEnAffichage(true);

super.setPersistant(false);

super.setClavierEnSaisie(true);

super.setMasqueAffichage(new WDChaineU(""));

super.setParamBtnActionClavier(0, "");

super.setRetraitGauche(2);

super.setMiseABlancSiZero(true);

super.setVerifieOrthographe(true);

super.setTauxParallaxe(0, 0);

super.setBoutonSuppression(0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -2, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN(2, 0x757575, 0x0, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1));

super.setStyleSaisie(0x222222, creerPolice_GEN("Roboto", -8.000000, 0));

super.setStyleTexteIndication(0x8E8E8F, creerPolice_GEN("Roboto", -8.000000, 0), 0);

super.setStyleJeton(WDCadreFactory.creerCadre_GEN(2, 0x98FF, 0x187F, 0x98FF, 4.000000, 4.000000, 1, 1), 0xFFFFFF, "", 1);

activerEcoute();
super.terminerInitialisation();
}

/**
 * Traitement: Initialisation de SAI_Recherche_Programme
 */
public void init()
{
super.init();

// HExécuteRequête(REQ_Recherche_ProgrammePreenregistre,hRequêteDéfaut,"")
//MAP:2fd9d1f310fadd7c:0000000e:1:FEN_Programm_Preregister3.SAI_Recherche_Programme:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDSAI_Recherche_Programme:Initialisation de SAI_Recherche_Programme
// HExécuteRequête(REQ_Recherche_ProgrammePreenregistre,hRequêteDéfaut,"")
//MAP:2fd9d1f310fadd7c:0000000e:1:FEN_Programm_Preregister3.SAI_Recherche_Programme:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDSAI_Recherche_Programme:Initialisation de SAI_Recherche_Programme
WDAPIHF.hExecuteRequete(WDAPIHF.getRequeteSansCasseNiAccent("req_recherche_programmepreenregistre"),new WDEntier4(0),new WDObjet[] {new WDChaineU("")} );

// ZoneRépétéeAffiche(ZR_ProgrammePreenregistre,taInit)
//MAP:2fd9d1f310fadd7c:0000000e:2:FEN_Programm_Preregister3.SAI_Recherche_Programme:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDSAI_Recherche_Programme:Initialisation de SAI_Recherche_Programme
WDAPIZoneRepetee.zoneRepeteeAffiche(mWD_ZR_ProgrammePreenregistre,new WDChaineU("Reset"));

}




/**
 * Traitement: A chaque modification de SAI_Recherche_Programme
 */
public void modification()
{
super.modification();

// HExécuteRequête(REQ_Recherche_ProgrammePreenregistre,hRequêteDéfaut,SAI_Recherche_Programme)
//MAP:2fd9d1f310fadd7c:00000011:1:FEN_Programm_Preregister3.SAI_Recherche_Programme:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDSAI_Recherche_Programme:A chaque modification de SAI_Recherche_Programme
// HExécuteRequête(REQ_Recherche_ProgrammePreenregistre,hRequêteDéfaut,SAI_Recherche_Programme)
//MAP:2fd9d1f310fadd7c:00000011:1:FEN_Programm_Preregister3.SAI_Recherche_Programme:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDSAI_Recherche_Programme:A chaque modification de SAI_Recherche_Programme
WDAPIHF.hExecuteRequete(WDAPIHF.getRequeteSansCasseNiAccent("req_recherche_programmepreenregistre"),new WDEntier4(0),new WDObjet[] {this} );

// ZoneRépétéeAffiche(ZR_ProgrammePreenregistre,taInit)
//MAP:2fd9d1f310fadd7c:00000011:2:FEN_Programm_Preregister3.SAI_Recherche_Programme:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDSAI_Recherche_Programme:A chaque modification de SAI_Recherche_Programme
WDAPIZoneRepetee.zoneRepeteeAffiche(mWD_ZR_ProgrammePreenregistre,new WDChaineU("Reset"));

// SI HTrouve()=Vrai ALORS
//MAP:2fd9d1f310fadd7c:00000011:3:FEN_Programm_Preregister3.SAI_Recherche_Programme:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDSAI_Recherche_Programme:A chaque modification de SAI_Recherche_Programme
if(WDAPIHF.hTrouve().opEgal(true))
{
// 	nIdProg				est un entier
//MAP:2fd9d1f310fadd7c:00000011:4:FEN_Programm_Preregister3.SAI_Recherche_Programme:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDSAI_Recherche_Programme:A chaque modification de SAI_Recherche_Programme
WDObjet vWD_nIdProg = new WDEntier4();



// 	sDiff				est une chaîne
//MAP:2fd9d1f310fadd7c:00000011:5:FEN_Programm_Preregister3.SAI_Recherche_Programme:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDSAI_Recherche_Programme:A chaque modification de SAI_Recherche_Programme
WDObjet vWD_sDiff = new WDChaineU();



// 	nTemps				est un entier
//MAP:2fd9d1f310fadd7c:00000011:6:FEN_Programm_Preregister3.SAI_Recherche_Programme:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDSAI_Recherche_Programme:A chaque modification de SAI_Recherche_Programme
WDObjet vWD_nTemps = new WDEntier4();



// 	nExercice			est un entier
//MAP:2fd9d1f310fadd7c:00000011:7:FEN_Programm_Preregister3.SAI_Recherche_Programme:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDSAI_Recherche_Programme:A chaque modification de SAI_Recherche_Programme
WDObjet vWD_nExercice = new WDEntier4();



// 	i					est un entier
//MAP:2fd9d1f310fadd7c:00000011:8:FEN_Programm_Preregister3.SAI_Recherche_Programme:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDSAI_Recherche_Programme:A chaque modification de SAI_Recherche_Programme
WDObjet vWD_i = new WDEntier4();



// 	i=HNbEnr(REQ_Recherche_ProgrammePreenregistre)
//MAP:2fd9d1f310fadd7c:00000011:a:FEN_Programm_Preregister3.SAI_Recherche_Programme:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDSAI_Recherche_Programme:A chaque modification de SAI_Recherche_Programme
vWD_i.setValeur(WDAPIHF.hNbEnr(WDAPIHF.getRequeteSansCasseNiAccent("req_recherche_programmepreenregistre")));

// 	BOUCLE
//MAP:2fd9d1f310fadd7c:00000011:b:FEN_Programm_Preregister3.SAI_Recherche_Programme:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDSAI_Recherche_Programme:A chaque modification de SAI_Recherche_Programme
{
do
{
// 		nIdProg		= ZR_ProgrammePreenregistre[i].Id_Programme..Valeur
//MAP:2fd9d1f310fadd7c:00000011:c:FEN_Programm_Preregister3.SAI_Recherche_Programme:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDSAI_Recherche_Programme:A chaque modification de SAI_Recherche_Programme
vWD_nIdProg.setValeur(mWD_ZR_ProgrammePreenregistre.get(vWD_i).get("Id_Programme").getProp(EWDPropriete.PROP_VALEUR));

// 		sDiff		= ZR_ProgrammePreenregistre[i].Difficulte..Valeur
//MAP:2fd9d1f310fadd7c:00000011:d:FEN_Programm_Preregister3.SAI_Recherche_Programme:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDSAI_Recherche_Programme:A chaque modification de SAI_Recherche_Programme
vWD_sDiff.setValeur(mWD_ZR_ProgrammePreenregistre.get(vWD_i).get("Difficulte").getProp(EWDPropriete.PROP_VALEUR));

// 		nTemps		=0
//MAP:2fd9d1f310fadd7c:00000011:e:FEN_Programm_Preregister3.SAI_Recherche_Programme:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDSAI_Recherche_Programme:A chaque modification de SAI_Recherche_Programme
vWD_nTemps.setValeur(0);

// 		nExercice	=0
//MAP:2fd9d1f310fadd7c:00000011:f:FEN_Programm_Preregister3.SAI_Recherche_Programme:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDSAI_Recherche_Programme:A chaque modification de SAI_Recherche_Programme
vWD_nExercice.setValeur(0);

// 		HLitRecherchePremier(contenir,Id_Programme,nIdProg)
//MAP:2fd9d1f310fadd7c:00000011:10:FEN_Programm_Preregister3.SAI_Recherche_Programme:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDSAI_Recherche_Programme:A chaque modification de SAI_Recherche_Programme
WDAPIHF.hLitRecherchePremier(WDAPIHF.getFichierSansCasseNiAccent("contenir"),WDAPIHF.getRubriqueSansCasseNiAccent("id_programme"),vWD_nIdProg);

// 		BOUCLE
//MAP:2fd9d1f310fadd7c:00000011:11:FEN_Programm_Preregister3.SAI_Recherche_Programme:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDSAI_Recherche_Programme:A chaque modification de SAI_Recherche_Programme
{
do
{
// 			nExercice++
//MAP:2fd9d1f310fadd7c:00000011:12:FEN_Programm_Preregister3.SAI_Recherche_Programme:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDSAI_Recherche_Programme:A chaque modification de SAI_Recherche_Programme
vWD_nExercice.opInc();

// 			nTemps+=((contenir.temps+contenir.repos)*contenir.nb_rep)
//MAP:2fd9d1f310fadd7c:00000011:13:FEN_Programm_Preregister3.SAI_Recherche_Programme:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDSAI_Recherche_Programme:A chaque modification de SAI_Recherche_Programme
vWD_nTemps.setValeur(vWD_nTemps.opPlus(WDAPIHF.getFichierSansCasseNiAccent("contenir").getRubriqueSansCasseNiAccent("temps").opPlus(WDAPIHF.getFichierSansCasseNiAccent("contenir").getRubriqueSansCasseNiAccent("repos")).opMult(WDAPIHF.getFichierSansCasseNiAccent("contenir").getRubriqueSansCasseNiAccent("nb_rep"))));

// 			HLitSuivant(contenir)
//MAP:2fd9d1f310fadd7c:00000011:14:FEN_Programm_Preregister3.SAI_Recherche_Programme:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDSAI_Recherche_Programme:A chaque modification de SAI_Recherche_Programme
WDAPIHF.hLitSuivant(WDAPIHF.getFichierSansCasseNiAccent("contenir"));

}
while(WDAPIHF.hTrouve(WDAPIHF.getFichierSansCasseNiAccent("contenir")).opEgal(true));
}

// 		ZR_ProgrammePreenregistre[i].LIB_NB_Exercice			="NB Exercices : " + nExercice
//MAP:2fd9d1f310fadd7c:00000011:16:FEN_Programm_Preregister3.SAI_Recherche_Programme:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDSAI_Recherche_Programme:A chaque modification de SAI_Recherche_Programme
mWD_ZR_ProgrammePreenregistre.get(vWD_i).get("LIB_NB_Exercice").setValeur(new WDChaineU("NB Exercices : ").opPlus(vWD_nExercice));

// 		ZR_ProgrammePreenregistre[i].LIB_Temps					="Temps : " + PartieEntière(nTemps/60)+"min"
//MAP:2fd9d1f310fadd7c:00000011:17:FEN_Programm_Preregister3.SAI_Recherche_Programme:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDSAI_Recherche_Programme:A chaque modification de SAI_Recherche_Programme
mWD_ZR_ProgrammePreenregistre.get(vWD_i).get("LIB_Temps").setValeur(new WDChaineU("Temps : ").opPlus(WDAPIMath.partieEntiere(vWD_nTemps.opDiv(60))).opPlus("min"));

// 		ZR_ProgrammePreenregistre[i].IMG_Diffi..CouleurFond		=CouleurDifficulte(sDiff)
//MAP:2fd9d1f310fadd7c:00000011:18:FEN_Programm_Preregister3.SAI_Recherche_Programme:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDSAI_Recherche_Programme:A chaque modification de SAI_Recherche_Programme
mWD_ZR_ProgrammePreenregistre.get(vWD_i).get("IMG_Diffi").setProp(EWDPropriete.PROP_COULEURFOND,GWDCPCouleur_Difficulte.fWD_couleurDifficulte(vWD_sDiff));

// 		i--
//MAP:2fd9d1f310fadd7c:00000011:19:FEN_Programm_Preregister3.SAI_Recherche_Programme:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDSAI_Recherche_Programme:A chaque modification de SAI_Recherche_Programme
vWD_i.opDec();

}
while(vWD_i.opSupEgal(1));
}

}

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurModification();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDSAI_Recherche_Programme mWD_SAI_Recherche_Programme;

/**
 * COMBO_tri
 */
class GWDCOMBO_tri extends WDCombo
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°4 de FEN_Programm_Preregister3.COMBO_tri
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setRectLibelle(0,2,41,27);
super.setRectCompPrincipal(41,2,127,27);
super.setQuid(2321060503563841598l);

super.setChecksum("560805748");

super.setNom("COMBO_tri");

super.setType(10002);

super.setBulle("");

super.setLibelle("Tri");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(11, 54);

super.setTailleInitiale(168, 31);

super.setValeurInitiale("");

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setContenuInitial("Nom\r\nDurée\r\nDifficulté\r\nNbExo");

super.setTriee(false);

super.setTailleMin(0, 0);

super.setTailleMax(134217727, 134217727);

super.setVisibleInitial(true);

super.setAltitude(4);

super.setAncrageInitial(0, 1000, 1000, 1000, 1000, 0);

super.setNumTab(3);

super.setLettreAppel(65535);

super.setRetourneValeurProgrammation(false);

super.setPersistant(false);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0xF4000000, creerPolice_GEN("MS Shell Dlg", -9.000000, 0), 4, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xF1000000, 0xF3000000, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN(2, 0xF4000000, 0x0, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1));

super.setStyleElement(0xF6000000, 0xFFFFFFFF, creerPolice_GEN("MS Shell Dlg", -8.000000, 0), 30);

super.setStyleSelection(0xF5800000, 0xF5000000, creerPolice_GEN("MS Shell Dlg", -11.000000, 0));

super.setStyleBouton(WDCadreFactory.creerCadre_GEN(2, 0xF4000000, 0x0, 0xF2000000, 2.000000, 2.000000, 1, 1), 0xF4000000);

activerEcoute();
super.terminerInitialisation();
}

/**
 * Traitement: Initialisation de COMBO_tri
 */
public void init()
{
super.init();

// gsTri="Nom";
//MAP:2036103b011cb03e:0000000e:1:FEN_Programm_Preregister3.COMBO_tri:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDCOMBO_tri:Initialisation de COMBO_tri
// gsTri="Nom";
//MAP:2036103b011cb03e:0000000e:1:FEN_Programm_Preregister3.COMBO_tri:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDCOMBO_tri:Initialisation de COMBO_tri
vWD_gsTri.setValeur("Nom");

}




/**
 * Traitement: Sélection d'une ligne de COMBO_tri
 */
public void selectionLigne()
{
super.selectionLigne();

// gsTri=COMBO_tri..ValeurAffichée
//MAP:2036103b011cb03e:00000021:1:FEN_Programm_Preregister3.COMBO_tri:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDCOMBO_tri:Sélection d'une ligne de COMBO_tri

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables locales au traitement
// (En WLangage les variables sont encore visibles après la fin du bloc dans lequel elles sont déclarées)
////////////////////////////////////////////////////////////////////////////
WDObjet vWD_sOrdre = new WDChaineU();

WDObjet vWD_sAttribut = new WDChaineU();



// gsTri=COMBO_tri..ValeurAffichée
//MAP:2036103b011cb03e:00000021:1:FEN_Programm_Preregister3.COMBO_tri:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDCOMBO_tri:Sélection d'une ligne de COMBO_tri
vWD_gsTri.setValeur(this.getProp(EWDPropriete.PROP_VALEURAFFICHEE));

// sOrdre		est une chaîne
//MAP:2036103b011cb03e:00000021:3:FEN_Programm_Preregister3.COMBO_tri:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDCOMBO_tri:Sélection d'une ligne de COMBO_tri


// sAttribut	est une chaîne
//MAP:2036103b011cb03e:00000021:4:FEN_Programm_Preregister3.COMBO_tri:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDCOMBO_tri:Sélection d'une ligne de COMBO_tri


// SELON gsOrdre
//MAP:2036103b011cb03e:00000021:6:FEN_Programm_Preregister3.COMBO_tri:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDCOMBO_tri:Sélection d'une ligne de COMBO_tri
// Délimiteur de visibilité pour ne pas étendre la visibilité de la variable temporaire _WDExpSelon
{
// SELON gsOrdre
//MAP:2036103b011cb03e:00000021:6:FEN_Programm_Preregister3.COMBO_tri:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDCOMBO_tri:Sélection d'une ligne de COMBO_tri
WDObjet _WDExpSelon0 = vWD_gsOrdre;
if(_WDExpSelon0.opEgal(1))
{
// 	CAS 1: sOrdre=""
//MAP:2036103b011cb03e:00000021:7:FEN_Programm_Preregister3.COMBO_tri:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDCOMBO_tri:Sélection d'une ligne de COMBO_tri
vWD_sOrdre.setValeur("");

}
else if(_WDExpSelon0.opEgal(2))
{
// 	CAS 2: sOrdre="-"
//MAP:2036103b011cb03e:00000021:8:FEN_Programm_Preregister3.COMBO_tri:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDCOMBO_tri:Sélection d'une ligne de COMBO_tri
vWD_sOrdre.setValeur("-");

}

}

// SELON gsTri
//MAP:2036103b011cb03e:00000021:a:FEN_Programm_Preregister3.COMBO_tri:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDCOMBO_tri:Sélection d'une ligne de COMBO_tri
// Délimiteur de visibilité pour ne pas étendre la visibilité de la variable temporaire _WDExpSelon
{
// SELON gsTri
//MAP:2036103b011cb03e:00000021:a:FEN_Programm_Preregister3.COMBO_tri:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDCOMBO_tri:Sélection d'une ligne de COMBO_tri
WDObjet _WDExpSelon1 = vWD_gsTri;
if(_WDExpSelon1.opEgal("Nom"))
{
// 	CAS "Nom": sAttribut = "ATT_Nom"
//MAP:2036103b011cb03e:00000021:b:FEN_Programm_Preregister3.COMBO_tri:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDCOMBO_tri:Sélection d'une ligne de COMBO_tri
vWD_sAttribut.setValeur("ATT_Nom");

}
else if(_WDExpSelon1.opEgal("Durée"))
{
// 	CAS "Durée": sAttribut = "ATT_Duree"
//MAP:2036103b011cb03e:00000021:c:FEN_Programm_Preregister3.COMBO_tri:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDCOMBO_tri:Sélection d'une ligne de COMBO_tri
vWD_sAttribut.setValeur("ATT_Duree");

}
else if(_WDExpSelon1.opEgal("NbExo"))
{
// 	CAS "NbExo": sAttribut = "ATT_NbExo"
//MAP:2036103b011cb03e:00000021:d:FEN_Programm_Preregister3.COMBO_tri:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDCOMBO_tri:Sélection d'une ligne de COMBO_tri
vWD_sAttribut.setValeur("ATT_NbExo");

}
else if(_WDExpSelon1.opEgal("Difficulté"))
{
// 	CAS "Difficulté": sAttribut = "ATT_Difficulte"
//MAP:2036103b011cb03e:00000021:e:FEN_Programm_Preregister3.COMBO_tri:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDCOMBO_tri:Sélection d'une ligne de COMBO_tri
vWD_sAttribut.setValeur("ATT_Difficulte");

}

}

// ZoneRépétéeTrie(ZR_ProgrammePreenregistre, sOrdre+sAttribut)
//MAP:2036103b011cb03e:00000021:10:FEN_Programm_Preregister3.COMBO_tri:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDCOMBO_tri:Sélection d'une ligne de COMBO_tri
WDAPIZoneRepetee.zoneRepeteeTrie(mWD_ZR_ProgrammePreenregistre,vWD_sOrdre.opPlus(vWD_sAttribut));

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurSelection();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDCOMBO_tri mWD_COMBO_tri;

/**
 * SEL_Sélecteur_2_colonnes
 */
class GWDSEL_Selecteur_2_colonnes extends WDSelecteur
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°5 de FEN_Programm_Preregister3.SEL_Sélecteur_2_colonnes
////////////////////////////////////////////////////////////////////////////

/**
 * SEL_Sélecteur_2_colonnes_Option_0
 */
class GWDSEL_Selecteur_2_colonnes_Option_0 extends WDBoutonRadio
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FEN_Programm_Preregister3.SEL_Sélecteur_2_colonnes.SEL_Sélecteur_2_colonnes_Option_0
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setLibelle("Croissant");

super.setHauteurOption(0);

super.setValeurRenvoyee((new WDChaineU("")));

super.setStyleLibelleOption(0xF4000000, creerPolice_GEN("MS Shell Dlg", -7.000000, 0));

super.terminerInitialisation();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDSEL_Selecteur_2_colonnes_Option_0 mWD_SEL_Selecteur_2_colonnes_Option_0 = new GWDSEL_Selecteur_2_colonnes_Option_0();

/**
 * SEL_Sélecteur_2_colonnes_Option_1
 */
class GWDSEL_Selecteur_2_colonnes_Option_1 extends WDBoutonRadio
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°2 de FEN_Programm_Preregister3.SEL_Sélecteur_2_colonnes.SEL_Sélecteur_2_colonnes_Option_1
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setLibelle("Décroissant");

super.setHauteurOption(0);

super.setValeurRenvoyee((new WDChaineU("")));

super.setStyleLibelleOption(0xF4000000, creerPolice_GEN("MS Shell Dlg", -7.000000, 0));

super.terminerInitialisation();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDSEL_Selecteur_2_colonnes_Option_1 mWD_SEL_Selecteur_2_colonnes_Option_1 = new GWDSEL_Selecteur_2_colonnes_Option_1();
/**
 * Initialise tous les champs de FEN_Programm_Preregister3.SEL_Sélecteur_2_colonnes
 */
public void initialiserSousObjets()
{
////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FEN_Programm_Preregister3.SEL_Sélecteur_2_colonnes
////////////////////////////////////////////////////////////////////////////
super.initialiserSousObjets();
super.ajouterOption(mWD_SEL_Selecteur_2_colonnes_Option_0);
super.ajouterOption(mWD_SEL_Selecteur_2_colonnes_Option_1);
positionnerOptions();
}
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setRectCompPrincipal(0,0,131,74);
super.setQuid(2321061203666611461l);

super.setChecksum("582174867");

super.setNom("SEL_Sélecteur_2_colonnes");

super.setType(6);

super.setBulle("");

super.setLibelle("");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(183, 44);

super.setTailleInitiale(131, 74);

super.setValeurInitiale("1");

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(134217727, 134217727);

super.setVisibleInitial(true);

super.setAltitude(5);

super.setAncrageInitial(0, 1000, 1000, 1000, 1000, 0);

super.setNumTab(2);

super.setLettreAppel(65535);

super.setPersistant(false);

super.setParamOptions(false, 1, false, true, false);

super.setValeurRenvoyeeParProgrammation(false);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0xF4000000, creerPolice_GEN("MS Shell Dlg", -7.000000, 0), -1, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xF3000000, 0xF3800000, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN(1, 0xF1000000, 0xF3000000, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1));

super.setParamAnimationChamp(18, 19, 300);
super.setParamAnimationChamp(19, 20, 300);

super.setImageCoche("", 1);

activerEcoute();
initialiserSousObjets();
super.terminerInitialisation();
}

/**
 * Traitement: Initialisation de SEL_Sélecteur_2_colonnes
 */
public void init()
{
super.init();

// gsOrdre=1
//MAP:203610de027d2d05:0000000e:1:FEN_Programm_Preregister3.SEL_Sélecteur_2_colonnes:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDSEL_Selecteur_2_colonnes:Initialisation de SEL_Sélecteur_2_colonnes
// gsOrdre=1
//MAP:203610de027d2d05:0000000e:1:FEN_Programm_Preregister3.SEL_Sélecteur_2_colonnes:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDSEL_Selecteur_2_colonnes:Initialisation de SEL_Sélecteur_2_colonnes
vWD_gsOrdre.setValeur(1);

}




/**
 * Traitement: A chaque modification de SEL_Sélecteur_2_colonnes
 */
public void modification()
{
super.modification();

// gsOrdre=SEL_Sélecteur_2_colonnes
//MAP:203610de027d2d05:00000011:1:FEN_Programm_Preregister3.SEL_Sélecteur_2_colonnes:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDSEL_Selecteur_2_colonnes:A chaque modification de SEL_Sélecteur_2_colonnes

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables locales au traitement
// (En WLangage les variables sont encore visibles après la fin du bloc dans lequel elles sont déclarées)
////////////////////////////////////////////////////////////////////////////
WDObjet vWD_sOrdre = new WDChaineU();

WDObjet vWD_sAttribut = new WDChaineU();



// gsOrdre=SEL_Sélecteur_2_colonnes
//MAP:203610de027d2d05:00000011:1:FEN_Programm_Preregister3.SEL_Sélecteur_2_colonnes:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDSEL_Selecteur_2_colonnes:A chaque modification de SEL_Sélecteur_2_colonnes
vWD_gsOrdre.setValeur(this);

// sOrdre est une chaîne
//MAP:203610de027d2d05:00000011:3:FEN_Programm_Preregister3.SEL_Sélecteur_2_colonnes:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDSEL_Selecteur_2_colonnes:A chaque modification de SEL_Sélecteur_2_colonnes


// sAttribut est une chaîne
//MAP:203610de027d2d05:00000011:4:FEN_Programm_Preregister3.SEL_Sélecteur_2_colonnes:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDSEL_Selecteur_2_colonnes:A chaque modification de SEL_Sélecteur_2_colonnes


// SELON gsOrdre
//MAP:203610de027d2d05:00000011:6:FEN_Programm_Preregister3.SEL_Sélecteur_2_colonnes:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDSEL_Selecteur_2_colonnes:A chaque modification de SEL_Sélecteur_2_colonnes
// Délimiteur de visibilité pour ne pas étendre la visibilité de la variable temporaire _WDExpSelon
{
// SELON gsOrdre
//MAP:203610de027d2d05:00000011:6:FEN_Programm_Preregister3.SEL_Sélecteur_2_colonnes:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDSEL_Selecteur_2_colonnes:A chaque modification de SEL_Sélecteur_2_colonnes
WDObjet _WDExpSelon0 = vWD_gsOrdre;
if(_WDExpSelon0.opEgal(1))
{
// 	CAS 1: sOrdre=""
//MAP:203610de027d2d05:00000011:7:FEN_Programm_Preregister3.SEL_Sélecteur_2_colonnes:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDSEL_Selecteur_2_colonnes:A chaque modification de SEL_Sélecteur_2_colonnes
vWD_sOrdre.setValeur("");

}
else if(_WDExpSelon0.opEgal(2))
{
// 	CAS 2: sOrdre="-"
//MAP:203610de027d2d05:00000011:8:FEN_Programm_Preregister3.SEL_Sélecteur_2_colonnes:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDSEL_Selecteur_2_colonnes:A chaque modification de SEL_Sélecteur_2_colonnes
vWD_sOrdre.setValeur("-");

}

}

// SELON gsTri
//MAP:203610de027d2d05:00000011:a:FEN_Programm_Preregister3.SEL_Sélecteur_2_colonnes:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDSEL_Selecteur_2_colonnes:A chaque modification de SEL_Sélecteur_2_colonnes
// Délimiteur de visibilité pour ne pas étendre la visibilité de la variable temporaire _WDExpSelon
{
// SELON gsTri
//MAP:203610de027d2d05:00000011:a:FEN_Programm_Preregister3.SEL_Sélecteur_2_colonnes:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDSEL_Selecteur_2_colonnes:A chaque modification de SEL_Sélecteur_2_colonnes
WDObjet _WDExpSelon1 = vWD_gsTri;
if(_WDExpSelon1.opEgal("Nom"))
{
// 	CAS "Nom": sAttribut = "ATT_Nom"
//MAP:203610de027d2d05:00000011:b:FEN_Programm_Preregister3.SEL_Sélecteur_2_colonnes:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDSEL_Selecteur_2_colonnes:A chaque modification de SEL_Sélecteur_2_colonnes
vWD_sAttribut.setValeur("ATT_Nom");

}
else if(_WDExpSelon1.opEgal("Durée"))
{
// 	CAS "Durée": sAttribut = "ATT_Duree"
//MAP:203610de027d2d05:00000011:c:FEN_Programm_Preregister3.SEL_Sélecteur_2_colonnes:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDSEL_Selecteur_2_colonnes:A chaque modification de SEL_Sélecteur_2_colonnes
vWD_sAttribut.setValeur("ATT_Duree");

}
else if(_WDExpSelon1.opEgal("NbExo"))
{
// 	CAS "NbExo": sAttribut = "ATT_NbExo"
//MAP:203610de027d2d05:00000011:d:FEN_Programm_Preregister3.SEL_Sélecteur_2_colonnes:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDSEL_Selecteur_2_colonnes:A chaque modification de SEL_Sélecteur_2_colonnes
vWD_sAttribut.setValeur("ATT_NbExo");

}
else if(_WDExpSelon1.opEgal("Difficulté"))
{
// 	CAS "Difficulté": sAttribut = "ATT_Difficulte"
//MAP:203610de027d2d05:00000011:e:FEN_Programm_Preregister3.SEL_Sélecteur_2_colonnes:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDSEL_Selecteur_2_colonnes:A chaque modification de SEL_Sélecteur_2_colonnes
vWD_sAttribut.setValeur("ATT_Difficulte");

}

}

// ZoneRépétéeTrie(ZR_ProgrammePreenregistre, sOrdre+sAttribut)
//MAP:203610de027d2d05:00000011:10:FEN_Programm_Preregister3.SEL_Sélecteur_2_colonnes:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3$GWDSEL_Selecteur_2_colonnes:A chaque modification de SEL_Sélecteur_2_colonnes
WDAPIZoneRepetee.zoneRepeteeTrie(mWD_ZR_ProgrammePreenregistre,vWD_sOrdre.opPlus(vWD_sAttribut));

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurModification();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDSEL_Selecteur_2_colonnes mWD_SEL_Selecteur_2_colonnes;

/**
 * Traitement: Déclarations globales de FEN_Programm_Preregister3
 */
public void declarerGlobale(WDObjet[] WD_tabParam)
{
// PROCEDURE MaFenêtre()
//MAP:2fd9d1f310efdd7c:00000000:1:FEN_Programm_Preregister3:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3:Déclarations globales de FEN_Programm_Preregister3
super.declarerGlobale(WD_tabParam, 0, 0);
int WD_ntabParamLen = 0;
if(WD_tabParam!=null) WD_ntabParamLen = WD_tabParam.length;


// gsTri est une chaîne
//MAP:2fd9d1f310efdd7c:00000000:2:FEN_Programm_Preregister3:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3:Déclarations globales de FEN_Programm_Preregister3
vWD_gsTri = new WDChaineU();

super.ajouterVariableGlobale("gsTri",vWD_gsTri);



// gsOrdre est une chaîne
//MAP:2fd9d1f310efdd7c:00000000:3:FEN_Programm_Preregister3:com.logicorp.home_biking.wdgen.GWDFFEN_Programm_Preregister3:Déclarations globales de FEN_Programm_Preregister3
vWD_gsOrdre = new WDChaineU();

super.ajouterVariableGlobale("gsOrdre",vWD_gsOrdre);



}




// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
 public WDObjet vWD_gsTri = WDVarNonAllouee.ref;
 public WDObjet vWD_gsOrdre = WDVarNonAllouee.ref;
////////////////////////////////////////////////////////////////////////////
// Création des champs de la fenêtre FEN_Programm_Preregister3
////////////////////////////////////////////////////////////////////////////
protected void creerChamps()
{
mWD_ZR_ProgrammePreenregistre = new GWDZR_ProgrammePreenregistre();
mWD_IMG_Button_Back = new GWDIMG_Button_Back();
mWD_SAI_Recherche_Programme = new GWDSAI_Recherche_Programme();
mWD_COMBO_tri = new GWDCOMBO_tri();
mWD_SEL_Selecteur_2_colonnes = new GWDSEL_Selecteur_2_colonnes();

}
////////////////////////////////////////////////////////////////////////////
// Initialisation de la fenêtre FEN_Programm_Preregister3
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.setQuid(3448017831620238716l);

super.setChecksum("1092705480");

super.setNom("FEN_Programm_Preregister3");

super.setType(1);

super.setBulle("");

super.setMenuContextuelSysteme();

super.setCurseurSouris(0);

super.setNote("", "");

super.setCouleur(0x0);

super.setCouleurFond(0xFFFFFF);

super.setPositionInitiale(0, 0);

super.setTailleInitiale(316, 543);

super.setTitre("Programm_Preregister3");

super.setTailleMin(-1, -1);

super.setTailleMax(20000, 20000);

super.setVisibleInitial(true);

super.setPositionFenetre(2);

super.setPersistant(true);

super.setGFI(true);

super.setAnimationFenetre(0);

super.setImageFond("", 1, 0, 1);

super.setCouleurTexteAutomatique(0x303030);

super.setCouleurBarreSysteme(0x80FF);


activerEcoute();

////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FEN_Programm_Preregister3
////////////////////////////////////////////////////////////////////////////
mWD_ZR_ProgrammePreenregistre.initialiserObjet();
super.ajouter("ZR_ProgrammePreenregistre", mWD_ZR_ProgrammePreenregistre);
mWD_IMG_Button_Back.initialiserObjet();
super.ajouter("IMG_Button_Back", mWD_IMG_Button_Back);
mWD_SAI_Recherche_Programme.initialiserObjet();
super.ajouter("SAI_Recherche_Programme", mWD_SAI_Recherche_Programme);
mWD_COMBO_tri.initialiserObjet();
super.ajouter("COMBO_tri", mWD_COMBO_tri);
mWD_SEL_Selecteur_2_colonnes.initialiserObjet();
super.ajouter("SEL_Sélecteur_2_colonnes", mWD_SEL_Selecteur_2_colonnes);

super.terminerInitialisation();
}

////////////////////////////////////////////////////////////////////////////
public boolean isUniteAffichageLogique()
{
return false;
}

public WDProjet getProjet()
{
return GWDPHome_Biking.getInstance();
}

public IWDEnsembleElement getEnsemble()
{
return GWDPHome_Biking.getInstance();
}
public int getModeContexteHF()
{
return 1;
}
/**
* Retourne le mode d'affichage de l'ActionBar de la fenêtre.
*/
public int getModeActionBar()
{
return 0;
}
/**
* Retourne vrai si la fenêtre est maximisée, faux sinon.
*/
public boolean isMaximisee()
{
return true;
}
/**
* Retourne vrai si la fenêtre a une barre de titre, faux sinon.
*/
public boolean isAvecBarreDeTitre()
{
return false;
}
/**
* Retourne le mode d'affichage de la barre système de la fenêtre.
*/
public int getModeBarreSysteme()
{
return 1;
}
/**
* Retourne vrai si la fenêtre est munie d'ascenseurs automatique, faux sinon.
*/
public boolean isAvecAscenseurAuto()
{
return false;
}
/**
* Retourne Vrai si on doit appliquer un theme "dark" (sombre) ou Faux si on doit appliquer "light" (clair) à la fenêtre.
* Ce choix se base sur la couleur du libellé par défaut dans le gabarit de la fenêtre.
*/
public boolean isThemeDark()
{
return false;
}
/**
* Retourne vrai si l'option de masquage automatique de l'ActionBar lorsqu'on scrolle dans un champ de la fenêtre a été activée.
*/
public boolean isMasquageAutomatiqueActionBar()
{
return false;
}
public static class WDActiviteFenetre extends WDActivite
{
protected WDFenetre getFenetre()
{
return GWDPHome_Biking.getInstance().mWD_FEN_Programm_Preregister3;
}
}
/**
* Retourne le nom du gabarit associée à la fenêtre.
*/
public String getNomGabarit()
{
return "";
}
}
