/**
 * Code généré par WINDEV Mobile - NE PAS MODIFIER !
 * Objet WINDEV Mobile : Collection
 * Classe Android : COL_Suppr_Creation
 * Date : 06/03/2021 17:20:14
 * Version de wdjava.dll  : 25.0.221.6
 */


package com.logicorp.home_biking.wdgen;


import com.logicorp.home_biking.*;
import fr.pcsoft.wdjava.core.types.*;
import fr.pcsoft.wdjava.core.*;
import fr.pcsoft.wdjava.core.application.*;
import fr.pcsoft.wdjava.api.*;
/*Imports trouvés dans le code WL*/
/*Fin Imports trouvés dans le code WL*/



public class GWDCPCOL_Suppr_Creation extends WDCollProcAndroid
{

public WDProjet getProjet()
{
return GWDPHome_Biking.getInstance();
}

public IWDEnsembleElement getEnsemble()
{
return GWDPHome_Biking.getInstance();
}

protected String getNomCollection()
{
return "COL_Suppr_Creation";
}
private final static GWDCPCOL_Suppr_Creation ms_instance = new GWDCPCOL_Suppr_Creation();
public final static GWDCPCOL_Suppr_Creation getInstance()
{
return ms_instance;
}

// Code de déclaration de COL_Suppr_Creation
static public void init()
{
// 
//MAP:10438fa1003e3d39:000b0000:1:COL_Suppr_Creation:com.logicorp.home_biking.wdgen.GWDCPCOL_Suppr_Creation:Déclaration de COL_Suppr_Creation
ms_instance.initDeclarationCollection();

try
{
}
finally
{
finDeclarationCollection();
}

}




// Code de terminaison de COL_Suppr_Creation
static public void term()
{
// 
//MAP:10438fa1003e3d39:000b0002:1:COL_Suppr_Creation:com.logicorp.home_biking.wdgen.GWDCPCOL_Suppr_Creation:Terminaison de COL_Suppr_Creation
ms_instance.initTerminaisonCollection();

try
{
}
finally
{
finTerminaisonCollection();
}

}



// Nombre de Procédures : 1
static public void fWD_supprCrea()
{
// PROCÉDURE SupprCrea()
//MAP:10438fb1003f7cf2:00070000:1:COL_Suppr_Creation.SupprCrea:com.logicorp.home_biking.wdgen.GWDCPCOL_Suppr_Creation:SupprCrea
ms_instance.initExecProcGlobale("SupprCrea");

try
{
// SI HNbEnr(Creation_Programme)>0 ALORS
//MAP:10438fb1003f7cf2:00070000:2:COL_Suppr_Creation.SupprCrea:com.logicorp.home_biking.wdgen.GWDCPCOL_Suppr_Creation:SupprCrea
if(WDAPIHF.hNbEnr(WDAPIHF.getFichierSansCasseNiAccent("creation_programme")).opSup(0))
{
// 	HLitPremier(Creation_Programme)
//MAP:10438fb1003f7cf2:00070000:3:COL_Suppr_Creation.SupprCrea:com.logicorp.home_biking.wdgen.GWDCPCOL_Suppr_Creation:SupprCrea
WDAPIHF.hLitPremier(WDAPIHF.getFichierSansCasseNiAccent("creation_programme"));

// 	BOUCLE
//MAP:10438fb1003f7cf2:00070000:4:COL_Suppr_Creation.SupprCrea:com.logicorp.home_biking.wdgen.GWDCPCOL_Suppr_Creation:SupprCrea
{
do
{
// 		HSupprime(Creation_Programme,hNumEnrEnCours)
//MAP:10438fb1003f7cf2:00070000:5:COL_Suppr_Creation.SupprCrea:com.logicorp.home_biking.wdgen.GWDCPCOL_Suppr_Creation:SupprCrea
WDAPIHF.hSupprime(WDAPIHF.getFichierSansCasseNiAccent("creation_programme"),(long)0);

// 		HModifie(Creation_Programme,hNumEnrEnCours)
//MAP:10438fb1003f7cf2:00070000:6:COL_Suppr_Creation.SupprCrea:com.logicorp.home_biking.wdgen.GWDCPCOL_Suppr_Creation:SupprCrea
WDAPIHF.hModifie(WDAPIHF.getFichierSansCasseNiAccent("creation_programme"),(long)0);

// 		HLitSuivant(Creation_Programme)
//MAP:10438fb1003f7cf2:00070000:7:COL_Suppr_Creation.SupprCrea:com.logicorp.home_biking.wdgen.GWDCPCOL_Suppr_Creation:SupprCrea
WDAPIHF.hLitSuivant(WDAPIHF.getFichierSansCasseNiAccent("creation_programme"));

}
while(WDAPIHF.hTrouve().getBoolean());
}

}

// SI HNbEnr(Creation_contenir)>0 ALORS
//MAP:10438fb1003f7cf2:00070000:d:COL_Suppr_Creation.SupprCrea:com.logicorp.home_biking.wdgen.GWDCPCOL_Suppr_Creation:SupprCrea
if(WDAPIHF.hNbEnr(WDAPIHF.getFichierSansCasseNiAccent("creation_contenir")).opSup(0))
{
// 	HLitPremier(Creation_contenir)
//MAP:10438fb1003f7cf2:00070000:e:COL_Suppr_Creation.SupprCrea:com.logicorp.home_biking.wdgen.GWDCPCOL_Suppr_Creation:SupprCrea
WDAPIHF.hLitPremier(WDAPIHF.getFichierSansCasseNiAccent("creation_contenir"));

// 	BOUCLE
//MAP:10438fb1003f7cf2:00070000:f:COL_Suppr_Creation.SupprCrea:com.logicorp.home_biking.wdgen.GWDCPCOL_Suppr_Creation:SupprCrea
{
do
{
// 		HSupprime(Creation_contenir,hNumEnrEnCours)
//MAP:10438fb1003f7cf2:00070000:10:COL_Suppr_Creation.SupprCrea:com.logicorp.home_biking.wdgen.GWDCPCOL_Suppr_Creation:SupprCrea
WDAPIHF.hSupprime(WDAPIHF.getFichierSansCasseNiAccent("creation_contenir"),(long)0);

// 		HLitSuivant(Creation_contenir)
//MAP:10438fb1003f7cf2:00070000:11:COL_Suppr_Creation.SupprCrea:com.logicorp.home_biking.wdgen.GWDCPCOL_Suppr_Creation:SupprCrea
WDAPIHF.hLitSuivant(WDAPIHF.getFichierSansCasseNiAccent("creation_contenir"));

}
while(WDAPIHF.hTrouve().getBoolean());
}

}

}
finally
{
finExecProcGlobale();
}

}



////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
