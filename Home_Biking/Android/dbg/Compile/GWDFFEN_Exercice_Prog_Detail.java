/**
 * Code généré par WINDEV Mobile - NE PAS MODIFIER !
 * Objet WINDEV Mobile : Fenêtre
 * Classe Android : FEN_Exercice_Prog_Detail
 * Date : 02/04/2021 14:52:47
 * Version de wdjava.dll  : 25.0.221.6
 */


package com.logicorp.home_biking.wdgen;


import com.logicorp.home_biking.*;
import fr.pcsoft.wdjava.core.types.*;
import fr.pcsoft.wdjava.core.*;
import fr.pcsoft.wdjava.ui.champs.fenetre.*;
import fr.pcsoft.wdjava.ui.champs.libelle.*;
import fr.pcsoft.wdjava.ui.cadre.*;
import fr.pcsoft.wdjava.ui.champs.combo.*;
import fr.pcsoft.wdjava.ui.champs.saisie.*;
import fr.pcsoft.wdjava.ui.champs.image.*;
import fr.pcsoft.wdjava.api.*;
import fr.pcsoft.wdjava.core.application.*;
import fr.pcsoft.wdjava.ui.activite.*;
/*Imports trouvés dans le code WL*/
/*Fin Imports trouvés dans le code WL*/



public class GWDFFEN_Exercice_Prog_Detail extends WDFenetre
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs de FEN_Exercice_Prog_Detail
////////////////////////////////////////////////////////////////////////////

/**
 * LIB_Nom
 */
class GWDLIB_Nom extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FEN_Exercice_Prog_Detail.LIB_Nom
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3450238101824806919l);

super.setChecksum("833280677");

super.setNom("LIB_Nom");

super.setType(3);

super.setBulle("");

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("Nom Exercice");

super.setNote("", "");

super.setCurseurSouris(0);

super.setEtatInitial(0);

super.setPositionInitiale(68, 11);

super.setTailleInitiale(244, 45);

super.setPlan(0);

super.setCadrageHorizontal(2);

super.setCadrageVertical(1);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(1);

super.setAncrageInitial(8, 1000, 1000, 1000, 1000, 0);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, 0xFFFFFFFF, creerPolice_GEN("Roboto", -13.000000, 0), 3, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_Nom mWD_LIB_Nom;

/**
 * COMBO_Place
 */
class GWDCOMBO_Place extends WDCombo
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°2 de FEN_Exercice_Prog_Detail.COMBO_Place
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setRectLibelle(0,2,57,40);
super.setRectCompPrincipal(57,2,86,40);
super.setQuid(3450238668764291585l);

super.setChecksum("839839432");

super.setNom("COMBO_Place");

super.setType(10002);

super.setBulle("");

super.setLibelle("Place");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(13, 86);

super.setTailleInitiale(143, 44);

super.setValeurInitiale("");

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setContenuInitial("");

super.setTriee(false);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(2);

super.setAncrageInitial(0, 1000, 1000, 1000, 1000, 0);

super.setNumTab(1);

super.setLettreAppel(65535);

super.setRetourneValeurProgrammation(false);

super.setPersistant(false);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -2, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN(2, 0x757575, 0x0, 0xE2E2E2, 2.000000, 2.000000, 1, 1));

super.setStyleElement(0x222222, 0xE2E2E2, creerPolice_GEN("Roboto", -8.000000, 0), 48);

super.setStyleSelection(0x222222, 0xE2E2E2, creerPolice_GEN("Roboto", -8.000000, 0));

super.setStyleBouton(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1), 0x222222);

activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDCOMBO_Place mWD_COMBO_Place;

/**
 * SAI_FCM
 */
class GWDSAI_FCM extends WDChampSaisieSimple
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°3 de FEN_Exercice_Prog_Detail.SAI_FCM
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setRectLibelle(0,2,65,40);
super.setRectCompPrincipal(65,2,70,40);
super.setQuid(3450238728895223797l);

super.setChecksum("838518490");

super.setNom("SAI_FCM");

super.setType(20004);

super.setBulle("");

super.setLibelle("FCM(%)");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setTaille(0);

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(177, 86);

super.setTailleInitiale(135, 44);

super.setValeurInitiale("50");

super.setPlan(0);

super.setCadrageHorizontal(2);

super.setMotDePasse(false);

super.setTypeSaisie(1);

super.setMasqueSaisie(new WDChaineU("999"));

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(3);

super.setAncrageInitial(8, 1000, 1000, 1000, 1000, 0);

super.setEllipse(0);

super.setIndication("");

super.setNumTab(2);

super.setModeAscenseur(2, 2);

super.setEffacementAutomatique(true);

super.setFinSaisieAutomatique(false);

super.setLettreAppel(65535);

super.setSelectionEnAffichage(true);

super.setPersistant(false);

super.setClavierEnSaisie(true);

super.setMasqueAffichage(new WDChaineU(""));

super.setParamBtnActionClavier(0, "");

super.setRetraitGauche(2);

super.setMiseABlancSiZero(true);

super.setVerifieOrthographe(true);

super.setTauxParallaxe(0, 0);

super.setBoutonSuppression(0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -2, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN(2, 0x757575, 0x0, 0xF0F0F0, 2.000000, 2.000000, 1, 1));

super.setStyleSaisie(0x222222, creerPolice_GEN("Roboto", -8.000000, 0));

super.setStyleTexteIndication(0x8E8E8F, creerPolice_GEN("Roboto", -8.000000, 0), 0);

super.setStyleJeton(WDCadreFactory.creerCadre_GEN(2, 0x98FF, 0x187F, 0x98FF, 4.000000, 4.000000, 1, 1), 0xFFFFFF, "", 1);

activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDSAI_FCM mWD_SAI_FCM;

/**
 * SAI_Cadence
 */
class GWDSAI_Cadence extends WDChampSaisieSimple
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°4 de FEN_Exercice_Prog_Detail.SAI_Cadence
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setRectLibelle(0,2,80,40);
super.setRectCompPrincipal(80,2,62,40);
super.setQuid(3450239557826792308l);

super.setChecksum("841399066");

super.setNom("SAI_Cadence");

super.setType(20004);

super.setBulle("");

super.setLibelle("Cadence");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setTaille(0);

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(14, 142);

super.setTailleInitiale(142, 44);

super.setValeurInitiale("");

super.setPlan(0);

super.setCadrageHorizontal(2);

super.setMotDePasse(false);

super.setTypeSaisie(1);

super.setMasqueSaisie(new WDChaineU("999"));

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(4);

super.setAncrageInitial(0, 1000, 1000, 1000, 1000, 0);

super.setEllipse(0);

super.setIndication("");

super.setNumTab(3);

super.setModeAscenseur(2, 2);

super.setEffacementAutomatique(true);

super.setFinSaisieAutomatique(false);

super.setLettreAppel(65535);

super.setSelectionEnAffichage(true);

super.setPersistant(false);

super.setClavierEnSaisie(true);

super.setMasqueAffichage(new WDChaineU(""));

super.setParamBtnActionClavier(0, "");

super.setRetraitGauche(2);

super.setMiseABlancSiZero(true);

super.setVerifieOrthographe(true);

super.setTauxParallaxe(0, 0);

super.setBoutonSuppression(0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -2, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN(2, 0x757575, 0x0, 0xF0F0F0, 2.000000, 2.000000, 1, 1));

super.setStyleSaisie(0x222222, creerPolice_GEN("Roboto", -8.000000, 0));

super.setStyleTexteIndication(0x8E8E8F, creerPolice_GEN("Roboto", -8.000000, 0), 0);

super.setStyleJeton(WDCadreFactory.creerCadre_GEN(2, 0x98FF, 0x187F, 0x98FF, 4.000000, 4.000000, 1, 1), 0xFFFFFF, "", 1);

activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDSAI_Cadence mWD_SAI_Cadence;

/**
 * SAI_nbrep
 */
class GWDSAI_nbrep extends WDChampSaisieSimple
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°5 de FEN_Exercice_Prog_Detail.SAI_nbrep
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setRectLibelle(0,2,80,40);
super.setRectCompPrincipal(80,2,62,40);
super.setQuid(3450240184895112034l);

super.setChecksum("844493722");

super.setNom("SAI_nbrep");

super.setType(20004);

super.setBulle("");

super.setLibelle("Repetition");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setTaille(0);

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(14, 344);

super.setTailleInitiale(142, 44);

super.setValeurInitiale("1");

super.setPlan(0);

super.setCadrageHorizontal(2);

super.setMotDePasse(false);

super.setTypeSaisie(1);

super.setMasqueSaisie(new WDChaineU("99"));

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(5);

super.setAncrageInitial(8, 1000, 1000, 1000, 1000, 0);

super.setEllipse(0);

super.setIndication("");

super.setNumTab(7);

super.setModeAscenseur(2, 2);

super.setEffacementAutomatique(true);

super.setFinSaisieAutomatique(false);

super.setLettreAppel(65535);

super.setSelectionEnAffichage(true);

super.setPersistant(false);

super.setClavierEnSaisie(true);

super.setMasqueAffichage(new WDChaineU(""));

super.setParamBtnActionClavier(0, "");

super.setRetraitGauche(2);

super.setMiseABlancSiZero(true);

super.setVerifieOrthographe(true);

super.setTauxParallaxe(0, 0);

super.setBoutonSuppression(0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -2, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN(2, 0x757575, 0x0, 0xF0F0F0, 2.000000, 2.000000, 1, 1));

super.setStyleSaisie(0x222222, creerPolice_GEN("Roboto", -8.000000, 0));

super.setStyleTexteIndication(0x8E8E8F, creerPolice_GEN("Roboto", -8.000000, 0), 0);

super.setStyleJeton(WDCadreFactory.creerCadre_GEN(2, 0x98FF, 0x187F, 0x98FF, 4.000000, 4.000000, 1, 1), 0xFFFFFF, "", 1);

activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDSAI_nbrep mWD_SAI_nbrep;

/**
 * LIB_SansNom1
 */
class GWDLIB_SansNom1 extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°6 de FEN_Exercice_Prog_Detail.LIB_SansNom1
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3450240395349018040l);

super.setChecksum("844956268");

super.setNom("LIB_SansNom1");

super.setType(3);

super.setBulle("");

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("(en tr/min)");

super.setNote("", "");

super.setCurseurSouris(0);

super.setEtatInitial(0);

super.setPositionInitiale(177, 155);

super.setTailleInitiale(120, 19);

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(6);

super.setAncrageInitial(8, 1000, 1000, 1000, 1000, 0);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, 0xFFFFFFFF, creerPolice_GEN("Roboto", -8.000000, 0), 3, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_SansNom1 mWD_LIB_SansNom1;

/**
 * SAI_Temps
 */
class GWDSAI_Temps extends WDChampSaisieSimple
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°7 de FEN_Exercice_Prog_Detail.SAI_Temps
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setRectLibelle(0,2,74,40);
super.setRectCompPrincipal(74,2,66,40);
super.setQuid(3450241396084691983l);

super.setChecksum("853389403");

super.setNom("SAI_Temps");

super.setType(20003);

super.setBulle("");

super.setLibelle("Temps");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setTaille(0);

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(15, 240);

super.setTailleInitiale(142, 44);

super.setValeurInitiale("00:00:00\r\n");

super.setPlan(0);

super.setCadrageHorizontal(2);

super.setMotDePasse(false);

super.setTypeSaisie(3);

super.setFormatMemorise("");

super.setMasqueSaisie(new WDChaineU("HH:MM:SS"));

super.setTailleMin(0, 0);

super.setTailleMax(134217727, 134217727);

super.setVisibleInitial(true);

super.setAltitude(7);

super.setAncrageInitial(8, 1000, 1000, 1000, 1000, 0);

super.setEllipse(0);

super.setIndication("");

super.setNumTab(5);

super.setModeAscenseur(2, 2);

super.setEffacementAutomatique(true);

super.setFinSaisieAutomatique(false);

super.setLettreAppel(65535);

super.setSelectionEnAffichage(true);

super.setPersistant(false);

super.setClavierEnSaisie(true);

super.setMasqueAffichage(new WDChaineU(""));

super.setParamBtnActionClavier(0, "");

super.setRetraitGauche(2);

super.setMiseABlancSiZero(false);

super.setVerifieOrthographe(true);

super.setTauxParallaxe(0, 0);

super.setBoutonSuppression(0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -2, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN(2, 0x757575, 0x0, 0xE2E2E2, 2.000000, 2.000000, 1, 1));

super.setStyleSaisie(0x222222, creerPolice_GEN("Roboto", -8.000000, 0));

super.setStyleTexteIndication(0x8E8E8F, creerPolice_GEN("Roboto", -8.000000, 0), 0);

super.setStyleJeton(WDCadreFactory.creerCadre_GEN(2, 0x98FF, 0x187F, 0x98FF, 4.000000, 4.000000, 1, 1), 0xFFFFFF, "", 1);

activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDSAI_Temps mWD_SAI_Temps;

/**
 * IMG_Button_Back
 */
class GWDIMG_Button_Back extends WDChampImage
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°8 de FEN_Exercice_Prog_Detail.IMG_Button_Back
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3450245592272648882l);

super.setChecksum("858162440");

super.setNom("IMG_Button_Back");

super.setType(30001);

super.setBulle("");

super.setLibelle("");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setNavigable(false);

super.setEtatInitial(0);

super.setPositionInitiale(8, 4);

super.setTailleInitiale(52, 52);

super.setValeurInitiale("C:\\Users\\axelm\\Desktop\\COURS IUT INFO\\Semestre 3\\M3302 - Projet tutore - Mise en situation professionnelle\\ptut-home-trainer\\Home_Biking\\Bouton_previous_page.png");

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(8);

super.setAncrageInitial(0, 1000, 1000, 1000, 1000, 0);

super.setTransparence(1);

super.setParamImage(2097158, 0, true, 100);

super.setSymetrie(0);

super.setZoneClicage(true);

super.setPCodeMultitouch(false);

super.setChargementEnTacheDeFond(false);

super.setOrientationExif(false);

super.setParamAnimation(1, 1, false, 300, true, false);

super.setAnimationInitiale(false);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -1, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

/**
 * Traitement: Clic sur IMG_Button_Back
 */
public void clicSurBoutonGauche()
{
super.clicSurBoutonGauche();

// Ferme()
//MAP:2fe1bc160344bab2:00000012:1:FEN_Exercice_Prog_Detail.IMG_Button_Back:com.logicorp.home_biking.wdgen.GWDFFEN_Exercice_Prog_Detail$GWDIMG_Button_Back:Clic sur IMG_Button_Back
// Ferme()
//MAP:2fe1bc160344bab2:00000012:1:FEN_Exercice_Prog_Detail.IMG_Button_Back:com.logicorp.home_biking.wdgen.GWDFFEN_Exercice_Prog_Detail$GWDIMG_Button_Back:Clic sur IMG_Button_Back
WDAPIFenetre.ferme();

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurClic();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDIMG_Button_Back mWD_IMG_Button_Back;

/**
 * IMG_Button_Validate
 */
class GWDIMG_Button_Validate extends WDChampImage
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°9 de FEN_Exercice_Prog_Detail.IMG_Button_Validate
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3450245824201264202l);

super.setChecksum("858543830");

super.setNom("IMG_Button_Validate");

super.setType(30001);

super.setBulle("");

super.setLibelle("");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setNavigable(false);

super.setEtatInitial(0);

super.setPositionInitiale(247, 465);

super.setTailleInitiale(52, 52);

super.setValeurInitiale("C:\\Users\\axelm\\Desktop\\COURS IUT INFO\\Semestre 3\\M3302 - Projet tutore - Mise en situation professionnelle\\ptut-home-trainer\\Home_Biking\\button validate.png");

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(9);

super.setAncrageInitial(5, 0, 1000, 1000, 1000, 0);

super.setTransparence(1);

super.setParamImage(2097158, 0, true, 100);

super.setSymetrie(0);

super.setZoneClicage(true);

super.setPCodeMultitouch(false);

super.setChargementEnTacheDeFond(false);

super.setOrientationExif(false);

super.setParamAnimation(1, 1, false, 300, true, false);

super.setAnimationInitiale(false);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -1, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

/**
 * Traitement: Clic sur IMG_Button_Validate
 */
public void clicSurBoutonGauche()
{
super.clicSurBoutonGauche();

// SI SAI_FCM<>Null ET SAI_nbrep<>Null ET SAI_Temps<>Null ET COMBO_Place.ValeurAffichée<>Null ALORS
//MAP:2fe1bc4c034a8c4a:00000012:1:FEN_Exercice_Prog_Detail.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Exercice_Prog_Detail$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
// SI SAI_FCM<>Null ET SAI_nbrep<>Null ET SAI_Temps<>Null ET COMBO_Place.ValeurAffichée<>Null ALORS
//MAP:2fe1bc4c034a8c4a:00000012:1:FEN_Exercice_Prog_Detail.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Exercice_Prog_Detail$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
if((((mWD_SAI_FCM.isNotNull() & mWD_SAI_nbrep.isNotNull()) & mWD_SAI_Temps.isNotNull()) & mWD_COMBO_Place.getProp(EWDPropriete.PROP_VALEURAFFICHEE).isNotNull()))
{
// 	SI HNbEnr(Creation_contenir)+1 <> COMBO_Place.ValeurAffichée ET HNbEnr(Creation_contenir)>0 ALORS
//MAP:2fe1bc4c034a8c4a:00000012:4:FEN_Exercice_Prog_Detail.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Exercice_Prog_Detail$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
if((WDAPIHF.hNbEnr(WDAPIHF.getFichierSansCasseNiAccent("creation_contenir")).opPlus(1).opDiff(mWD_COMBO_Place.getProp(EWDPropriete.PROP_VALEURAFFICHEE)) & WDAPIHF.hNbEnr(WDAPIHF.getFichierSansCasseNiAccent("creation_contenir")).opSup(0)))
{
// 		nPlace est un entier
//MAP:2fe1bc4c034a8c4a:00000012:6:FEN_Exercice_Prog_Detail.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Exercice_Prog_Detail$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDObjet vWD_nPlace = new WDEntier4();



// 		nPlace = HNbEnr(Creation_contenir)+1
//MAP:2fe1bc4c034a8c4a:00000012:7:FEN_Exercice_Prog_Detail.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Exercice_Prog_Detail$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
vWD_nPlace.setValeur(WDAPIHF.hNbEnr(WDAPIHF.getFichierSansCasseNiAccent("creation_contenir")).opPlus(1));

// 		BOUCLE
//MAP:2fe1bc4c034a8c4a:00000012:8:FEN_Exercice_Prog_Detail.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Exercice_Prog_Detail$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
while(true)
{
// 			HLitRecherche(Creation_contenir,place,nPlace)
//MAP:2fe1bc4c034a8c4a:00000012:9:FEN_Exercice_Prog_Detail.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Exercice_Prog_Detail$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDAPIHF.hLitRecherche(WDAPIHF.getFichierSansCasseNiAccent("creation_contenir"),WDAPIHF.getRubriqueSansCasseNiAccent("place"),vWD_nPlace);

// 			Creation_contenir.place+=1
//MAP:2fe1bc4c034a8c4a:00000012:a:FEN_Exercice_Prog_Detail.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Exercice_Prog_Detail$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDAPIHF.getFichierSansCasseNiAccent("creation_contenir").getRubriqueSansCasseNiAccent("place").setValeur(WDAPIHF.getFichierSansCasseNiAccent("creation_contenir").getRubriqueSansCasseNiAccent("place").opPlus(1));

// 			HModifie(Creation_contenir,hNumEnrEnCours)
//MAP:2fe1bc4c034a8c4a:00000012:b:FEN_Exercice_Prog_Detail.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Exercice_Prog_Detail$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDAPIHF.hModifie(WDAPIHF.getFichierSansCasseNiAccent("creation_contenir"),(long)0);

// 			nPlace--
//MAP:2fe1bc4c034a8c4a:00000012:d:FEN_Exercice_Prog_Detail.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Exercice_Prog_Detail$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
vWD_nPlace.opDec();

// 			SI nPlace = Val(COMBO_Place.ValeurAffichée)-1 ALORS
//MAP:2fe1bc4c034a8c4a:00000012:e:FEN_Exercice_Prog_Detail.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Exercice_Prog_Detail$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
if(vWD_nPlace.opEgal(WDAPIChaine.val(mWD_COMBO_Place.getProp(EWDPropriete.PROP_VALEURAFFICHEE)).opMoins(1)))
{
// 				SORTIR
//MAP:2fe1bc4c034a8c4a:00000012:f:FEN_Exercice_Prog_Detail.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Exercice_Prog_Detail$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
break;

//////////////////////////////////////////////////////////
// Code Inaccessible
// 
}

}


}

// 	tpsH, tpsM, tpsS est une chaîne
//MAP:2fe1bc4c034a8c4a:00000012:14:FEN_Exercice_Prog_Detail.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Exercice_Prog_Detail$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDObjet vWD_tpsH = new WDChaineU();


WDObjet vWD_tpsM = new WDChaineU();


WDObjet vWD_tpsS = new WDChaineU();



// 	(tpsH, tpsM,tpsS) = ChaîneDécoupe(SAI_Temps,":")
//MAP:2fe1bc4c034a8c4a:00000012:15:FEN_Exercice_Prog_Detail.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Exercice_Prog_Detail$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
new WDTuple(vWD_tpsH, vWD_tpsM, vWD_tpsS).setValeur(WDAPIChaine.chaineDecoupe(mWD_SAI_Temps,new WDObjet[] {new WDChaineU(":")} ));

// 	repH, repM, repS est une chaîne
//MAP:2fe1bc4c034a8c4a:00000012:17:FEN_Exercice_Prog_Detail.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Exercice_Prog_Detail$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDObjet vWD_repH = new WDChaineU();


WDObjet vWD_repM = new WDChaineU();


WDObjet vWD_repS = new WDChaineU();



// 	(repH, repM,repS) = ChaîneDécoupe(SAI_Repos,":")
//MAP:2fe1bc4c034a8c4a:00000012:18:FEN_Exercice_Prog_Detail.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Exercice_Prog_Detail$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
new WDTuple(vWD_repH, vWD_repM, vWD_repS).setValeur(WDAPIChaine.chaineDecoupe(mWD_SAI_Repos,new WDObjet[] {new WDChaineU(":")} ));

// 	Creation_contenir.place				=COMBO_Place.ValeurAffichée
//MAP:2fe1bc4c034a8c4a:00000012:1b:FEN_Exercice_Prog_Detail.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Exercice_Prog_Detail$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDAPIHF.getFichierSansCasseNiAccent("creation_contenir").getRubriqueSansCasseNiAccent("place").setValeur(mWD_COMBO_Place.getProp(EWDPropriete.PROP_VALEURAFFICHEE));

// 	Creation_contenir.Id_Exercice		=FEN_Exercice_Prog_Detail.gIdExercice
//MAP:2fe1bc4c034a8c4a:00000012:1c:FEN_Exercice_Prog_Detail.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Exercice_Prog_Detail$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDAPIHF.getFichierSansCasseNiAccent("creation_contenir").getRubriqueSansCasseNiAccent("id_exercice").setValeur(vWD_gIdExercice);

// 	Creation_contenir.nb_rep			=SAI_nbrep
//MAP:2fe1bc4c034a8c4a:00000012:1d:FEN_Exercice_Prog_Detail.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Exercice_Prog_Detail$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDAPIHF.getFichierSansCasseNiAccent("creation_contenir").getRubriqueSansCasseNiAccent("nb_rep").setValeur(mWD_SAI_nbrep);

// 	Creation_contenir.temps				= Val(tpsH)*3600 + Val(tpsM)*60 + Val(tpsS)
//MAP:2fe1bc4c034a8c4a:00000012:1e:FEN_Exercice_Prog_Detail.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Exercice_Prog_Detail$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDAPIHF.getFichierSansCasseNiAccent("creation_contenir").getRubriqueSansCasseNiAccent("temps").setValeur(WDAPIChaine.val(vWD_tpsH).opMult(3600).opPlus(WDAPIChaine.val(vWD_tpsM).opMult(60)).opPlus(WDAPIChaine.val(vWD_tpsS)));

// 	Creation_contenir.repos				= Val(repH)*3600 + Val(repM)*60 + Val(repS)
//MAP:2fe1bc4c034a8c4a:00000012:1f:FEN_Exercice_Prog_Detail.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Exercice_Prog_Detail$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDAPIHF.getFichierSansCasseNiAccent("creation_contenir").getRubriqueSansCasseNiAccent("repos").setValeur(WDAPIChaine.val(vWD_repH).opMult(3600).opPlus(WDAPIChaine.val(vWD_repM).opMult(60)).opPlus(WDAPIChaine.val(vWD_repS)));

// 	Creation_contenir.pourcentage_fcm	=SAI_FCM
//MAP:2fe1bc4c034a8c4a:00000012:20:FEN_Exercice_Prog_Detail.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Exercice_Prog_Detail$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDAPIHF.getFichierSansCasseNiAccent("creation_contenir").getRubriqueSansCasseNiAccent("pourcentage_fcm").setValeur(mWD_SAI_FCM);

// 	Creation_contenir.cadence			=SAI_Cadence
//MAP:2fe1bc4c034a8c4a:00000012:21:FEN_Exercice_Prog_Detail.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Exercice_Prog_Detail$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDAPIHF.getFichierSansCasseNiAccent("creation_contenir").getRubriqueSansCasseNiAccent("cadence").setValeur(mWD_SAI_Cadence);

// 	Creation_contenir.puissance			=SAI_puiss
//MAP:2fe1bc4c034a8c4a:00000012:22:FEN_Exercice_Prog_Detail.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Exercice_Prog_Detail$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDAPIHF.getFichierSansCasseNiAccent("creation_contenir").getRubriqueSansCasseNiAccent("puissance").setValeur(mWD_SAI_puiss);

// 	HAjoute(Creation_contenir)
//MAP:2fe1bc4c034a8c4a:00000012:23:FEN_Exercice_Prog_Detail.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Exercice_Prog_Detail$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDAPIHF.hAjoute(WDAPIHF.getFichierSansCasseNiAccent("creation_contenir"));

// 	ZoneRépétéeAffiche(FEN_Creation_Prog.ZR_REQ_Creation_Prog,taRéExécuteRequete)
//MAP:2fe1bc4c034a8c4a:00000012:25:FEN_Exercice_Prog_Detail.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Exercice_Prog_Detail$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDAPIZoneRepetee.zoneRepeteeAffiche(GWDPHome_Biking.getInstance().getFEN_Creation_Prog().mWD_ZR_REQ_Creation_Prog,new WDChaineU("Reexecute"));

// 	Ferme(FEN_Exercices)
//MAP:2fe1bc4c034a8c4a:00000012:27:FEN_Exercice_Prog_Detail.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Exercice_Prog_Detail$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDAPIFenetre.ferme(GWDPHome_Biking.getInstance().mWD_FEN_Exercices);

}
else
{
// 	ToastAffiche("Au minimum la place, la FCM, le nombre de rep et le temps doivent être renseignés",toastLong)
//MAP:2fe1bc4c034a8c4a:00000012:2a:FEN_Exercice_Prog_Detail.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Exercice_Prog_Detail$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDAPIToast.toastAffiche("Au minimum la place, la FCM, le nombre de rep et le temps doivent être renseignés",1);

}

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurClic();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDIMG_Button_Validate mWD_IMG_Button_Validate;

/**
 * SAI_puiss
 */
class GWDSAI_puiss extends WDChampSaisieSimple
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°10 de FEN_Exercice_Prog_Detail.SAI_puiss
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setRectLibelle(0,2,80,40);
super.setRectCompPrincipal(80,2,62,40);
super.setQuid(2324844077707397743l);

super.setChecksum("573555931");

super.setNom("SAI_puiss");

super.setType(20004);

super.setBulle("");

super.setLibelle("Puissance");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setTaille(0);

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(14, 192);

super.setTailleInitiale(142, 44);

super.setValeurInitiale("");

super.setPlan(0);

super.setCadrageHorizontal(2);

super.setMotDePasse(false);

super.setTypeSaisie(1);

super.setMasqueSaisie(new WDChaineU("9999"));

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(10);

super.setAncrageInitial(0, 1000, 1000, 1000, 1000, 0);

super.setEllipse(0);

super.setIndication("");

super.setNumTab(4);

super.setModeAscenseur(2, 2);

super.setEffacementAutomatique(true);

super.setFinSaisieAutomatique(false);

super.setLettreAppel(65535);

super.setSelectionEnAffichage(true);

super.setPersistant(false);

super.setClavierEnSaisie(true);

super.setMasqueAffichage(new WDChaineU(""));

super.setParamBtnActionClavier(0, "");

super.setRetraitGauche(2);

super.setMiseABlancSiZero(true);

super.setVerifieOrthographe(true);

super.setTauxParallaxe(0, 0);

super.setBoutonSuppression(0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -2, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN(2, 0x757575, 0x0, 0xF0F0F0, 2.000000, 2.000000, 1, 1));

super.setStyleSaisie(0x222222, creerPolice_GEN("Roboto", -8.000000, 0));

super.setStyleTexteIndication(0x8E8E8F, creerPolice_GEN("Roboto", -8.000000, 0), 0);

super.setStyleJeton(WDCadreFactory.creerCadre_GEN(2, 0x98FF, 0x187F, 0x98FF, 4.000000, 4.000000, 1, 1), 0xFFFFFF, "", 1);

activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDSAI_puiss mWD_SAI_puiss;

/**
 * LIB_SansNom2
 */
class GWDLIB_SansNom2 extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°11 de FEN_Exercice_Prog_Detail.LIB_SansNom2
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(2324844206556970724l);

super.setChecksum("574064057");

super.setNom("LIB_SansNom2");

super.setType(3);

super.setBulle("");

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("(en Watts)");

super.setNote("", "");

super.setCurseurSouris(0);

super.setEtatInitial(0);

super.setPositionInitiale(179, 205);

super.setTailleInitiale(120, 19);

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(11);

super.setAncrageInitial(8, 1000, 1000, 1000, 1000, 0);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, 0xFFFFFFFF, creerPolice_GEN("Roboto", -8.000000, 0), 3, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_SansNom2 mWD_LIB_SansNom2;

/**
 * SAI_Repos
 */
class GWDSAI_Repos extends WDChampSaisieSimple
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°12 de FEN_Exercice_Prog_Detail.SAI_Repos
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setRectLibelle(0,2,74,40);
super.setRectCompPrincipal(74,2,70,40);
super.setQuid(2334419853312941035l);

super.setChecksum("566101615");

super.setNom("SAI_Repos");

super.setType(20003);

super.setBulle("");

super.setLibelle("Repos");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setTaille(0);

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(13, 292);

super.setTailleInitiale(144, 44);

super.setValeurInitiale("00:00:00");

super.setPlan(0);

super.setCadrageHorizontal(2);

super.setMotDePasse(false);

super.setTypeSaisie(3);

super.setFormatMemorise("");

super.setMasqueSaisie(new WDChaineU("HH:MM:SS"));

super.setTailleMin(0, 0);

super.setTailleMax(134217727, 134217727);

super.setVisibleInitial(true);

super.setAltitude(12);

super.setAncrageInitial(8, 1000, 1000, 1000, 1000, 0);

super.setEllipse(0);

super.setIndication("");

super.setNumTab(6);

super.setModeAscenseur(2, 2);

super.setEffacementAutomatique(true);

super.setFinSaisieAutomatique(false);

super.setLettreAppel(65535);

super.setSelectionEnAffichage(true);

super.setPersistant(false);

super.setClavierEnSaisie(true);

super.setMasqueAffichage(new WDChaineU(""));

super.setParamBtnActionClavier(0, "");

super.setRetraitGauche(2);

super.setMiseABlancSiZero(false);

super.setVerifieOrthographe(true);

super.setTauxParallaxe(0, 0);

super.setBoutonSuppression(0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -2, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN(2, 0x757575, 0x0, 0xE2E2E2, 2.000000, 2.000000, 1, 1));

super.setStyleSaisie(0x222222, creerPolice_GEN("Roboto", -8.000000, 0));

super.setStyleTexteIndication(0x8E8E8F, creerPolice_GEN("Roboto", -8.000000, 0), 0);

super.setStyleJeton(WDCadreFactory.creerCadre_GEN(2, 0x98FF, 0x187F, 0x98FF, 4.000000, 4.000000, 1, 1), 0xFFFFFF, "", 1);

activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDSAI_Repos mWD_SAI_Repos;

/**
 * Traitement: Déclarations globales de FEN_Exercice_Prog_Detail
 */
public void declarerGlobale(WDObjet[] WD_tabParam)
{
// PROCEDURE MaFenêtre(gIdExercice)
//MAP:2fe1ad9801431fba:00000000:1:FEN_Exercice_Prog_Detail:com.logicorp.home_biking.wdgen.GWDFFEN_Exercice_Prog_Detail:Déclarations globales de FEN_Exercice_Prog_Detail
super.declarerGlobale(WD_tabParam, 1, 1);
int WD_ntabParamLen = 0;
if(WD_tabParam!=null) WD_ntabParamLen = WD_tabParam.length;

// Traitement du paramètre n°0
if(0<WD_ntabParamLen) 
{
vWD_gIdExercice = WD_tabParam[0];
}
else { vWD_gIdExercice = null; }
super.ajouterVariableGlobale("gIdExercice",vWD_gIdExercice);


}




/**
 * Traitement: Fin d'initialisation de FEN_Exercice_Prog_Detail
 */
public void init()
{
super.init();

// HLitRecherche(Exercice,Id_Exercice,gIdExercice)
//MAP:2fe1ad9801431fba:00000022:1:FEN_Exercice_Prog_Detail:com.logicorp.home_biking.wdgen.GWDFFEN_Exercice_Prog_Detail:Fin d'initialisation de FEN_Exercice_Prog_Detail

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables locales au traitement
// (En WLangage les variables sont encore visibles après la fin du bloc dans lequel elles sont déclarées)
////////////////////////////////////////////////////////////////////////////
WDObjet vWD_nNbEnr = new WDEntier4();



// HLitRecherche(Exercice,Id_Exercice,gIdExercice)
//MAP:2fe1ad9801431fba:00000022:1:FEN_Exercice_Prog_Detail:com.logicorp.home_biking.wdgen.GWDFFEN_Exercice_Prog_Detail:Fin d'initialisation de FEN_Exercice_Prog_Detail
WDAPIHF.hLitRecherche(WDAPIHF.getFichierSansCasseNiAccent("exercice"),WDAPIHF.getRubriqueSansCasseNiAccent("id_exercice"),vWD_gIdExercice);

// LIB_Nom=Exercice.Nom + gIdExercice
//MAP:2fe1ad9801431fba:00000022:2:FEN_Exercice_Prog_Detail:com.logicorp.home_biking.wdgen.GWDFFEN_Exercice_Prog_Detail:Fin d'initialisation de FEN_Exercice_Prog_Detail
mWD_LIB_Nom.setValeur(WDAPIHF.getFichierSansCasseNiAccent("exercice").getRubriqueSansCasseNiAccent("nom").opPlus(vWD_gIdExercice));

// nNbEnr est un entier
//MAP:2fe1ad9801431fba:00000022:3:FEN_Exercice_Prog_Detail:com.logicorp.home_biking.wdgen.GWDFFEN_Exercice_Prog_Detail:Fin d'initialisation de FEN_Exercice_Prog_Detail


// nNbEnr=HNbEnr(Creation_contenir)
//MAP:2fe1ad9801431fba:00000022:4:FEN_Exercice_Prog_Detail:com.logicorp.home_biking.wdgen.GWDFFEN_Exercice_Prog_Detail:Fin d'initialisation de FEN_Exercice_Prog_Detail
vWD_nNbEnr.setValeur(WDAPIHF.hNbEnr(WDAPIHF.getFichierSansCasseNiAccent("creation_contenir")));

// SI nNbEnr<=0 ALORS
//MAP:2fe1ad9801431fba:00000022:6:FEN_Exercice_Prog_Detail:com.logicorp.home_biking.wdgen.GWDFFEN_Exercice_Prog_Detail:Fin d'initialisation de FEN_Exercice_Prog_Detail
if(vWD_nNbEnr.opInfEgal(0))
{
// 	nNbEnr=0
//MAP:2fe1ad9801431fba:00000022:7:FEN_Exercice_Prog_Detail:com.logicorp.home_biking.wdgen.GWDFFEN_Exercice_Prog_Detail:Fin d'initialisation de FEN_Exercice_Prog_Detail
vWD_nNbEnr.setValeur(0);

}

// POUR nCompteur = 1 À nNbEnr+1
//MAP:2fe1ad9801431fba:00000022:a:FEN_Exercice_Prog_Detail:com.logicorp.home_biking.wdgen.GWDFFEN_Exercice_Prog_Detail:Fin d'initialisation de FEN_Exercice_Prog_Detail
// POUR nCompteur = 1 À nNbEnr+1
//MAP:2fe1ad9801431fba:00000022:a:FEN_Exercice_Prog_Detail:com.logicorp.home_biking.wdgen.GWDFFEN_Exercice_Prog_Detail:Fin d'initialisation de FEN_Exercice_Prog_Detail
for(WDObjet vWD_nCompteur = new WDEntier8(1);vWD_nCompteur.opInfEgal(vWD_nNbEnr.opPlus(1));vWD_nCompteur.opInc())
{
// 	COMBO_Place.Ajoute(nCompteur)
//MAP:2fe1ad9801431fba:00000022:b:FEN_Exercice_Prog_Detail:com.logicorp.home_biking.wdgen.GWDFFEN_Exercice_Prog_Detail:Fin d'initialisation de FEN_Exercice_Prog_Detail
WDAPIListe.listeAjoute(mWD_COMBO_Place,vWD_nCompteur.getString());

}

}




// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
 public WDObjet vWD_gIdExercice = WDVarNonAllouee.ref;
////////////////////////////////////////////////////////////////////////////
// Création des champs de la fenêtre FEN_Exercice_Prog_Detail
////////////////////////////////////////////////////////////////////////////
protected void creerChamps()
{
mWD_LIB_Nom = new GWDLIB_Nom();
mWD_COMBO_Place = new GWDCOMBO_Place();
mWD_SAI_FCM = new GWDSAI_FCM();
mWD_SAI_Cadence = new GWDSAI_Cadence();
mWD_SAI_nbrep = new GWDSAI_nbrep();
mWD_LIB_SansNom1 = new GWDLIB_SansNom1();
mWD_SAI_Temps = new GWDSAI_Temps();
mWD_IMG_Button_Back = new GWDIMG_Button_Back();
mWD_IMG_Button_Validate = new GWDIMG_Button_Validate();
mWD_SAI_puiss = new GWDSAI_puiss();
mWD_LIB_SansNom2 = new GWDLIB_SansNom2();
mWD_SAI_Repos = new GWDSAI_Repos();

}
////////////////////////////////////////////////////////////////////////////
// Initialisation de la fenêtre FEN_Exercice_Prog_Detail
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.setQuid(3450229657910321082l);

super.setChecksum("830245451");

super.setNom("FEN_Exercice_Prog_Detail");

super.setType(1);

super.setBulle("");

super.setMenuContextuelSysteme();

super.setCurseurSouris(0);

super.setNote("", "");

super.setCouleur(0x0);

super.setCouleurFond(0xFFFFFF);

super.setPositionInitiale(0, 0);

super.setTailleInitiale(320, 543);

super.setTitre("Exercice_Prog_Detail");

super.setTailleMin(-1, -1);

super.setTailleMax(20000, 20000);

super.setVisibleInitial(true);

super.setPositionFenetre(2);

super.setPersistant(true);

super.setGFI(true);

super.setAnimationFenetre(0);

super.setImageFond("", 1, 0, 1);

super.setCouleurTexteAutomatique(0x303030);

super.setCouleurBarreSysteme(0x80FF);


activerEcoute();

////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FEN_Exercice_Prog_Detail
////////////////////////////////////////////////////////////////////////////
mWD_LIB_Nom.initialiserObjet();
super.ajouter("LIB_Nom", mWD_LIB_Nom);
mWD_COMBO_Place.initialiserObjet();
super.ajouter("COMBO_Place", mWD_COMBO_Place);
mWD_SAI_FCM.initialiserObjet();
super.ajouter("SAI_FCM", mWD_SAI_FCM);
mWD_SAI_Cadence.initialiserObjet();
super.ajouter("SAI_Cadence", mWD_SAI_Cadence);
mWD_SAI_nbrep.initialiserObjet();
super.ajouter("SAI_nbrep", mWD_SAI_nbrep);
mWD_LIB_SansNom1.initialiserObjet();
super.ajouter("LIB_SansNom1", mWD_LIB_SansNom1);
mWD_SAI_Temps.initialiserObjet();
super.ajouter("SAI_Temps", mWD_SAI_Temps);
mWD_IMG_Button_Back.initialiserObjet();
super.ajouter("IMG_Button_Back", mWD_IMG_Button_Back);
mWD_IMG_Button_Validate.initialiserObjet();
super.ajouter("IMG_Button_Validate", mWD_IMG_Button_Validate);
mWD_SAI_puiss.initialiserObjet();
super.ajouter("SAI_puiss", mWD_SAI_puiss);
mWD_LIB_SansNom2.initialiserObjet();
super.ajouter("LIB_SansNom2", mWD_LIB_SansNom2);
mWD_SAI_Repos.initialiserObjet();
super.ajouter("SAI_Repos", mWD_SAI_Repos);

super.terminerInitialisation();
}

////////////////////////////////////////////////////////////////////////////
public boolean isUniteAffichageLogique()
{
return false;
}

public WDProjet getProjet()
{
return GWDPHome_Biking.getInstance();
}

public IWDEnsembleElement getEnsemble()
{
return GWDPHome_Biking.getInstance();
}
public int getModeContexteHF()
{
return 1;
}
/**
* Retourne le mode d'affichage de l'ActionBar de la fenêtre.
*/
public int getModeActionBar()
{
return 0;
}
/**
* Retourne vrai si la fenêtre est maximisée, faux sinon.
*/
public boolean isMaximisee()
{
return true;
}
/**
* Retourne vrai si la fenêtre a une barre de titre, faux sinon.
*/
public boolean isAvecBarreDeTitre()
{
return false;
}
/**
* Retourne le mode d'affichage de la barre système de la fenêtre.
*/
public int getModeBarreSysteme()
{
return 1;
}
/**
* Retourne vrai si la fenêtre est munie d'ascenseurs automatique, faux sinon.
*/
public boolean isAvecAscenseurAuto()
{
return false;
}
/**
* Retourne Vrai si on doit appliquer un theme "dark" (sombre) ou Faux si on doit appliquer "light" (clair) à la fenêtre.
* Ce choix se base sur la couleur du libellé par défaut dans le gabarit de la fenêtre.
*/
public boolean isThemeDark()
{
return false;
}
/**
* Retourne vrai si l'option de masquage automatique de l'ActionBar lorsqu'on scrolle dans un champ de la fenêtre a été activée.
*/
public boolean isMasquageAutomatiqueActionBar()
{
return false;
}
public static class WDActiviteFenetre extends WDActivite
{
protected WDFenetre getFenetre()
{
return GWDPHome_Biking.getInstance().mWD_FEN_Exercice_Prog_Detail;
}
}
/**
* Retourne le nom du gabarit associée à la fenêtre.
*/
public String getNomGabarit()
{
return "210 MATERIAL DESIGN ORANGE#WM";
}
}
