/**
 * Code généré par WINDEV Mobile - NE PAS MODIFIER !
 * Objet WINDEV Mobile : Requête
 * Classe Android : REQ_Select_Utiliser
 * Date : 16/12/2020 08:39:47
 * Version de wdjava.dll  : 25.0.221.6
 */


package com.logicorp.home_biking.wdgen;


import com.logicorp.home_biking.*;
import fr.pcsoft.wdjava.core.types.*;
import fr.pcsoft.wdjava.core.*;
import fr.pcsoft.wdjava.database.hf.requete.parsing.*;
/*Imports trouvés dans le code WL*/
/*Fin Imports trouvés dans le code WL*/



public class GWDRREQ_Select_Utiliser extends WDDescRequeteWDR
{
public String getNomLogique()
{
return "REQ_Select_Utiliser";
}
public String getCodeSQLOriginal()
{
return " SELECT  utiliser.Id_Compte_Client AS Id_Compte_Client,\t utiliser.Id_Programme AS Id_Programme,\t utiliser.DateUtilisation AS DateUtilisation  FROM  utiliser";
}
public Requete initArbre() throws WDInvalidSQLException
{
Select varSelect = new Select();
varSelect.setType(1);
Rubrique rub_Id_Compte_Client = new Rubrique();
rub_Id_Compte_Client.setNom("Id_Compte_Client");
rub_Id_Compte_Client.setAlias("Id_Compte_Client");
rub_Id_Compte_Client.setNomFichier("utiliser");
rub_Id_Compte_Client.setAliasFichier("utiliser");
varSelect.ajouterElement(rub_Id_Compte_Client);
Rubrique rub_Id_Programme = new Rubrique();
rub_Id_Programme.setNom("Id_Programme");
rub_Id_Programme.setAlias("Id_Programme");
rub_Id_Programme.setNomFichier("utiliser");
rub_Id_Programme.setAliasFichier("utiliser");
varSelect.ajouterElement(rub_Id_Programme);
Rubrique rub_DateUtilisation = new Rubrique();
rub_DateUtilisation.setNom("DateUtilisation");
rub_DateUtilisation.setAlias("DateUtilisation");
rub_DateUtilisation.setNomFichier("utiliser");
rub_DateUtilisation.setAliasFichier("utiliser");
varSelect.ajouterElement(rub_DateUtilisation);
From varFrom = new From();
Fichier fic_utiliser = new Fichier();
fic_utiliser.setNom("utiliser");
fic_utiliser.setAlias("utiliser");
varFrom.ajouterElement(fic_utiliser);
Requete varReqSelect = new Requete(1);
varReqSelect.ajouterClause(varSelect);
varReqSelect.ajouterClause(varFrom);
Limit varLimit = new Limit();
varLimit.setType(0);
varLimit.setNbEnregs(0);
varLimit.setOffset(0);
varReqSelect.ajouterClause(varLimit);
return varReqSelect;
}
public String getNomFichier(int nIndex)
{
switch(nIndex)
{
case 0 : return "utiliser";
default: return null;
}
}
public String getAliasFichier(int nIndex)
{
switch(nIndex)
{
case 0 : return "utiliser";
default: return null;
}
}


public int getIdWDR()
{
return com.logicorp.home_biking.R.raw.req_select_utiliser;
}
public String getNomFichierWDR()
{
return "req_select_utiliser";
}
}
