/**
 * Code généré par WINDEV Mobile - NE PAS MODIFIER !
 * Objet WINDEV Mobile : Fenêtre
 * Classe Android : FEN_CreationTables
 * Date : 06/03/2021 17:18:02
 * Version de wdjava.dll  : 25.0.221.6
 */


package com.logicorp.home_biking.wdgen;


import com.logicorp.home_biking.*;
import fr.pcsoft.wdjava.core.types.*;
import fr.pcsoft.wdjava.core.*;
import fr.pcsoft.wdjava.ui.champs.fenetre.*;
import fr.pcsoft.wdjava.api.*;
import fr.pcsoft.wdjava.core.parcours.*;
import fr.pcsoft.wdjava.core.parcours.chaine.*;
import fr.pcsoft.wdjava.core.application.*;
import fr.pcsoft.wdjava.ui.activite.*;
/*Imports trouvés dans le code WL*/
/*Fin Imports trouvés dans le code WL*/



public class GWDFFEN_CreationTables extends WDFenetre
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs de FEN_CreationTables
////////////////////////////////////////////////////////////////////////////

/**
 * Traitement: Déclarations globales de FEN_CreationTables
 */
public void declarerGlobale(WDObjet[] WD_tabParam)
{
// PROCEDURE MaFenêtre()
//MAP:2fd4c2f300f6a6f0:00000000:1:FEN_CreationTables:com.logicorp.home_biking.wdgen.GWDFFEN_CreationTables:Déclarations globales de FEN_CreationTables
super.declarerGlobale(WD_tabParam, 0, 0);
int WD_ntabParamLen = 0;
if(WD_tabParam!=null) WD_ntabParamLen = WD_tabParam.length;


}




/**
 * Traitement: Fin d'initialisation de FEN_CreationTables
 */
public void init()
{
super.init();

// 
//MAP:2fd4c2f300f6a6f0:00000022:1:FEN_CreationTables:com.logicorp.home_biking.wdgen.GWDFFEN_CreationTables:Fin d'initialisation de FEN_CreationTables

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables locales au traitement
// (En WLangage les variables sont encore visibles après la fin du bloc dans lequel elles sont déclarées)
////////////////////////////////////////////////////////////////////////////
WDObjet vWD_sContenuFichier = new WDChaineU();

WDObjet vWD_sLigne = new WDChaineU();



// sContenuFichier	est une chaîne UNICODE	
//MAP:2fd4c2f300f6a6f0:00000022:2:FEN_CreationTables:com.logicorp.home_biking.wdgen.GWDFFEN_CreationTables:Fin d'initialisation de FEN_CreationTables


// sLigne			est une chaîne UNICODE
//MAP:2fd4c2f300f6a6f0:00000022:3:FEN_CreationTables:com.logicorp.home_biking.wdgen.GWDFFEN_CreationTables:Fin d'initialisation de FEN_CreationTables


// sContenuFichier = fChargeTexte("PTUT_BDD_Programme.csv")
//MAP:2fd4c2f300f6a6f0:00000022:7:FEN_CreationTables:com.logicorp.home_biking.wdgen.GWDFFEN_CreationTables:Fin d'initialisation de FEN_CreationTables
vWD_sContenuFichier.setValeur(WDAPIFichier.fChargeTexte("PTUT_BDD_Programme.csv"));

// POUR TOUT CHAÎNE sLigne DE sContenuFichier SÉPARÉE PAR RC
//MAP:2fd4c2f300f6a6f0:00000022:8:FEN_CreationTables:com.logicorp.home_biking.wdgen.GWDFFEN_CreationTables:Fin d'initialisation de FEN_CreationTables
IWDParcours parcours1 = null;
try
{
parcours1 = WDParcoursSousChaine.pourTout(vWD_sLigne, null, null, vWD_sContenuFichier, "\r\n", 0x2);
while(parcours1.testParcours())
{
// 	Programme.Id_Programme	=ExtraitChaîne(sLigne,1,",",DepuisDébut)
//MAP:2fd4c2f300f6a6f0:00000022:9:FEN_CreationTables:com.logicorp.home_biking.wdgen.GWDFFEN_CreationTables:Fin d'initialisation de FEN_CreationTables
WDAPIHF.getFichierSansCasseNiAccent("programme").getRubriqueSansCasseNiAccent("id_programme").setValeur(WDAPIChaine.extraitChaine(parcours1.getVariableParcours(),1,new WDChaineU(","),0));

// 	Programme.Nom			=ExtraitChaîne(sLigne,2,",",DepuisDébut)
//MAP:2fd4c2f300f6a6f0:00000022:a:FEN_CreationTables:com.logicorp.home_biking.wdgen.GWDFFEN_CreationTables:Fin d'initialisation de FEN_CreationTables
WDAPIHF.getFichierSansCasseNiAccent("programme").getRubriqueSansCasseNiAccent("nom").setValeur(WDAPIChaine.extraitChaine(parcours1.getVariableParcours(),2,new WDChaineU(","),0));

// 	Programme.Difficulte	=ExtraitChaîne(sLigne,3,",",DepuisDébut)
//MAP:2fd4c2f300f6a6f0:00000022:b:FEN_CreationTables:com.logicorp.home_biking.wdgen.GWDFFEN_CreationTables:Fin d'initialisation de FEN_CreationTables
WDAPIHF.getFichierSansCasseNiAccent("programme").getRubriqueSansCasseNiAccent("difficulte").setValeur(WDAPIChaine.extraitChaine(parcours1.getVariableParcours(),3,new WDChaineU(","),0));

// 	Programme.Id_Compte_Client	=ExtraitChaîne(sLigne,4,",",DepuisDébut)
//MAP:2fd4c2f300f6a6f0:00000022:c:FEN_CreationTables:com.logicorp.home_biking.wdgen.GWDFFEN_CreationTables:Fin d'initialisation de FEN_CreationTables
WDAPIHF.getFichierSansCasseNiAccent("programme").getRubriqueSansCasseNiAccent("id_compte_client").setValeur(WDAPIChaine.extraitChaine(parcours1.getVariableParcours(),4,new WDChaineU(","),0));

// 	HAjoute(Programme)
//MAP:2fd4c2f300f6a6f0:00000022:d:FEN_CreationTables:com.logicorp.home_biking.wdgen.GWDFFEN_CreationTables:Fin d'initialisation de FEN_CreationTables
WDAPIHF.hAjoute(WDAPIHF.getFichierSansCasseNiAccent("programme"));

// 	HModifie(Programme)
//MAP:2fd4c2f300f6a6f0:00000022:e:FEN_CreationTables:com.logicorp.home_biking.wdgen.GWDFFEN_CreationTables:Fin d'initialisation de FEN_CreationTables
WDAPIHF.hModifie(WDAPIHF.getFichierSansCasseNiAccent("programme"));

// 	ToastAffiche("Code prog")
//MAP:2fd4c2f300f6a6f0:00000022:f:FEN_CreationTables:com.logicorp.home_biking.wdgen.GWDFFEN_CreationTables:Fin d'initialisation de FEN_CreationTables
WDAPIToast.toastAffiche("Code prog");

}

}
finally
{
if(parcours1 != null)
{
parcours1.finParcours();
}
}


}




/**
 * Traitement: Changement d'orientation de FEN_CreationTables
 */
public void changementOrientation()
//  Chargement en mémoire du contenu du fichier
{
super.changementOrientation();

// // Chargement en mémoire du contenu du fichier
//MAP:2fd4c2f300f6a6f0:000000a5:1:FEN_CreationTables:com.logicorp.home_biking.wdgen.GWDFFEN_CreationTables:Changement d'orientation de FEN_CreationTables

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables locales au traitement
// (En WLangage les variables sont encore visibles après la fin du bloc dans lequel elles sont déclarées)
////////////////////////////////////////////////////////////////////////////
WDObjet vWD_sContenuFichier = new WDChaineU();

WDObjet vWD_sLigne = new WDChaineU();



// sContenuFichier	est une chaîne UNICODE	
//MAP:2fd4c2f300f6a6f0:000000a5:2:FEN_CreationTables:com.logicorp.home_biking.wdgen.GWDFFEN_CreationTables:Changement d'orientation de FEN_CreationTables


// sLigne			est une chaîne UNICODE
//MAP:2fd4c2f300f6a6f0:000000a5:3:FEN_CreationTables:com.logicorp.home_biking.wdgen.GWDFFEN_CreationTables:Changement d'orientation de FEN_CreationTables


// sContenuFichier = fChargeTexte("PTUT_BDD_Exercice.csv")
//MAP:2fd4c2f300f6a6f0:000000a5:4:FEN_CreationTables:com.logicorp.home_biking.wdgen.GWDFFEN_CreationTables:Changement d'orientation de FEN_CreationTables
vWD_sContenuFichier.setValeur(WDAPIFichier.fChargeTexte("PTUT_BDD_Exercice.csv"));

// POUR TOUT CHAÎNE sLigne DE sContenuFichier SÉPARÉE PAR RC
//MAP:2fd4c2f300f6a6f0:000000a5:5:FEN_CreationTables:com.logicorp.home_biking.wdgen.GWDFFEN_CreationTables:Changement d'orientation de FEN_CreationTables
IWDParcours parcours2 = null;
try
{
parcours2 = WDParcoursSousChaine.pourTout(vWD_sLigne, null, null, vWD_sContenuFichier, "\r\n", 0x2);
while(parcours2.testParcours())
{
// 	Exercice.Id_Exercice	=ExtraitChaîne(sLigne,1,",",DepuisDébut)
//MAP:2fd4c2f300f6a6f0:000000a5:6:FEN_CreationTables:com.logicorp.home_biking.wdgen.GWDFFEN_CreationTables:Changement d'orientation de FEN_CreationTables
WDAPIHF.getFichierSansCasseNiAccent("exercice").getRubriqueSansCasseNiAccent("id_exercice").setValeur(WDAPIChaine.extraitChaine(parcours2.getVariableParcours(),1,new WDChaineU(","),0));

// 	Exercice.Nom			=ExtraitChaîne(sLigne,2,",",DepuisDébut)
//MAP:2fd4c2f300f6a6f0:000000a5:7:FEN_CreationTables:com.logicorp.home_biking.wdgen.GWDFFEN_CreationTables:Changement d'orientation de FEN_CreationTables
WDAPIHF.getFichierSansCasseNiAccent("exercice").getRubriqueSansCasseNiAccent("nom").setValeur(WDAPIChaine.extraitChaine(parcours2.getVariableParcours(),2,new WDChaineU(","),0));

// 	Exercice.Description	=ExtraitChaîne(sLigne,3,",",DepuisDébut)
//MAP:2fd4c2f300f6a6f0:000000a5:8:FEN_CreationTables:com.logicorp.home_biking.wdgen.GWDFFEN_CreationTables:Changement d'orientation de FEN_CreationTables
WDAPIHF.getFichierSansCasseNiAccent("exercice").getRubriqueSansCasseNiAccent("description").setValeur(WDAPIChaine.extraitChaine(parcours2.getVariableParcours(),3,new WDChaineU(","),0));

// 	HAjoute(Exercice)
//MAP:2fd4c2f300f6a6f0:000000a5:9:FEN_CreationTables:com.logicorp.home_biking.wdgen.GWDFFEN_CreationTables:Changement d'orientation de FEN_CreationTables
WDAPIHF.hAjoute(WDAPIHF.getFichierSansCasseNiAccent("exercice"));

// 	HModifie(Exercice)
//MAP:2fd4c2f300f6a6f0:000000a5:a:FEN_CreationTables:com.logicorp.home_biking.wdgen.GWDFFEN_CreationTables:Changement d'orientation de FEN_CreationTables
WDAPIHF.hModifie(WDAPIHF.getFichierSansCasseNiAccent("exercice"));

// 	ToastAffiche("Code exo")
//MAP:2fd4c2f300f6a6f0:000000a5:b:FEN_CreationTables:com.logicorp.home_biking.wdgen.GWDFFEN_CreationTables:Changement d'orientation de FEN_CreationTables
WDAPIToast.toastAffiche("Code exo");

}

}
finally
{
if(parcours2 != null)
{
parcours2.finParcours();
}
}


// sContenuFichier = fChargeTexte("PTUT_BDD_Contenir.csv")
//MAP:2fd4c2f300f6a6f0:000000a5:e:FEN_CreationTables:com.logicorp.home_biking.wdgen.GWDFFEN_CreationTables:Changement d'orientation de FEN_CreationTables
vWD_sContenuFichier.setValeur(WDAPIFichier.fChargeTexte("PTUT_BDD_Contenir.csv"));

// POUR TOUT CHAÎNE sLigne DE sContenuFichier SÉPARÉE PAR RC
//MAP:2fd4c2f300f6a6f0:000000a5:f:FEN_CreationTables:com.logicorp.home_biking.wdgen.GWDFFEN_CreationTables:Changement d'orientation de FEN_CreationTables
IWDParcours parcours3 = null;
try
{
parcours3 = WDParcoursSousChaine.pourTout(vWD_sLigne, null, null, vWD_sContenuFichier, "\r\n", 0x2);
while(parcours3.testParcours())
{
// 	contenir.Id_Exercice				=ExtraitChaîne(sLigne,1,",",DepuisDébut)
//MAP:2fd4c2f300f6a6f0:000000a5:10:FEN_CreationTables:com.logicorp.home_biking.wdgen.GWDFFEN_CreationTables:Changement d'orientation de FEN_CreationTables
WDAPIHF.getFichierSansCasseNiAccent("contenir").getRubriqueSansCasseNiAccent("id_exercice").setValeur(WDAPIChaine.extraitChaine(parcours3.getVariableParcours(),1,new WDChaineU(","),0));

// 	contenir.Id_Programme				=ExtraitChaîne(sLigne,2,",",DepuisDébut)
//MAP:2fd4c2f300f6a6f0:000000a5:11:FEN_CreationTables:com.logicorp.home_biking.wdgen.GWDFFEN_CreationTables:Changement d'orientation de FEN_CreationTables
WDAPIHF.getFichierSansCasseNiAccent("contenir").getRubriqueSansCasseNiAccent("id_programme").setValeur(WDAPIChaine.extraitChaine(parcours3.getVariableParcours(),2,new WDChaineU(","),0));

// 	contenir.place						=ExtraitChaîne(sLigne,3,",",DepuisDébut)
//MAP:2fd4c2f300f6a6f0:000000a5:12:FEN_CreationTables:com.logicorp.home_biking.wdgen.GWDFFEN_CreationTables:Changement d'orientation de FEN_CreationTables
WDAPIHF.getFichierSansCasseNiAccent("contenir").getRubriqueSansCasseNiAccent("place").setValeur(WDAPIChaine.extraitChaine(parcours3.getVariableParcours(),3,new WDChaineU(","),0));

// 	contenir.nb_rep						=ExtraitChaîne(sLigne,4,",",DepuisDébut)
//MAP:2fd4c2f300f6a6f0:000000a5:13:FEN_CreationTables:com.logicorp.home_biking.wdgen.GWDFFEN_CreationTables:Changement d'orientation de FEN_CreationTables
WDAPIHF.getFichierSansCasseNiAccent("contenir").getRubriqueSansCasseNiAccent("nb_rep").setValeur(WDAPIChaine.extraitChaine(parcours3.getVariableParcours(),4,new WDChaineU(","),0));

// 	contenir.temps						=ExtraitChaîne(sLigne,5,",",DepuisDébut)
//MAP:2fd4c2f300f6a6f0:000000a5:14:FEN_CreationTables:com.logicorp.home_biking.wdgen.GWDFFEN_CreationTables:Changement d'orientation de FEN_CreationTables
WDAPIHF.getFichierSansCasseNiAccent("contenir").getRubriqueSansCasseNiAccent("temps").setValeur(WDAPIChaine.extraitChaine(parcours3.getVariableParcours(),5,new WDChaineU(","),0));

// 	contenir.repos						=ExtraitChaîne(sLigne,6,",",DepuisDébut)
//MAP:2fd4c2f300f6a6f0:000000a5:15:FEN_CreationTables:com.logicorp.home_biking.wdgen.GWDFFEN_CreationTables:Changement d'orientation de FEN_CreationTables
WDAPIHF.getFichierSansCasseNiAccent("contenir").getRubriqueSansCasseNiAccent("repos").setValeur(WDAPIChaine.extraitChaine(parcours3.getVariableParcours(),6,new WDChaineU(","),0));

// 	contenir.pourcentage_fcm			=ExtraitChaîne(sLigne,7,",",DepuisDébut)
//MAP:2fd4c2f300f6a6f0:000000a5:16:FEN_CreationTables:com.logicorp.home_biking.wdgen.GWDFFEN_CreationTables:Changement d'orientation de FEN_CreationTables
WDAPIHF.getFichierSansCasseNiAccent("contenir").getRubriqueSansCasseNiAccent("pourcentage_fcm").setValeur(WDAPIChaine.extraitChaine(parcours3.getVariableParcours(),7,new WDChaineU(","),0));

// 	contenir.puissance					=ExtraitChaîne(sLigne,8,",",DepuisDébut)
//MAP:2fd4c2f300f6a6f0:000000a5:17:FEN_CreationTables:com.logicorp.home_biking.wdgen.GWDFFEN_CreationTables:Changement d'orientation de FEN_CreationTables
WDAPIHF.getFichierSansCasseNiAccent("contenir").getRubriqueSansCasseNiAccent("puissance").setValeur(WDAPIChaine.extraitChaine(parcours3.getVariableParcours(),8,new WDChaineU(","),0));

// 	contenir.cadence					=ExtraitChaîne(sLigne,9,",",DepuisDébut)
//MAP:2fd4c2f300f6a6f0:000000a5:18:FEN_CreationTables:com.logicorp.home_biking.wdgen.GWDFFEN_CreationTables:Changement d'orientation de FEN_CreationTables
WDAPIHF.getFichierSansCasseNiAccent("contenir").getRubriqueSansCasseNiAccent("cadence").setValeur(WDAPIChaine.extraitChaine(parcours3.getVariableParcours(),9,new WDChaineU(","),0));

// 	HAjoute(contenir)
//MAP:2fd4c2f300f6a6f0:000000a5:19:FEN_CreationTables:com.logicorp.home_biking.wdgen.GWDFFEN_CreationTables:Changement d'orientation de FEN_CreationTables
WDAPIHF.hAjoute(WDAPIHF.getFichierSansCasseNiAccent("contenir"));

// 	HModifie(contenir)
//MAP:2fd4c2f300f6a6f0:000000a5:1a:FEN_CreationTables:com.logicorp.home_biking.wdgen.GWDFFEN_CreationTables:Changement d'orientation de FEN_CreationTables
WDAPIHF.hModifie(WDAPIHF.getFichierSansCasseNiAccent("contenir"));

// 	ToastAffiche("Code contenir")
//MAP:2fd4c2f300f6a6f0:000000a5:1b:FEN_CreationTables:com.logicorp.home_biking.wdgen.GWDFFEN_CreationTables:Changement d'orientation de FEN_CreationTables
WDAPIToast.toastAffiche("Code contenir");

}

}
finally
{
if(parcours3 != null)
{
parcours3.finParcours();
}
}


}




// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// Création des champs de la fenêtre FEN_CreationTables
////////////////////////////////////////////////////////////////////////////
protected void creerChamps()
{

}
////////////////////////////////////////////////////////////////////////////
// Initialisation de la fenêtre FEN_CreationTables
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.setQuid(3446593963794278128l);

super.setChecksum("824387292");

super.setNom("FEN_CreationTables");

super.setType(1);

super.setBulle("");

super.setMenuContextuelSysteme();

super.setCurseurSouris(0);

super.setNote("", "");

super.setCouleur(0x0);

super.setCouleurFond(0xFFFFFF);

super.setPositionInitiale(0, 0);

super.setTailleInitiale(320, 543);

super.setTitre("CreationTables");

super.setTailleMin(-1, -1);

super.setTailleMax(20000, 20000);

super.setVisibleInitial(true);

super.setPositionFenetre(2);

super.setPersistant(true);

super.setGFI(true);

super.setAnimationFenetre(0);

super.setImageFond("", 1, 0, 1);

super.setCouleurTexteAutomatique(0x303030);

super.setCouleurBarreSysteme(0x80FF);


activerEcoute();

////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FEN_CreationTables
////////////////////////////////////////////////////////////////////////////

super.terminerInitialisation();
}

////////////////////////////////////////////////////////////////////////////
public boolean isUniteAffichageLogique()
{
return false;
}

public WDProjet getProjet()
{
return GWDPHome_Biking.getInstance();
}

public IWDEnsembleElement getEnsemble()
{
return GWDPHome_Biking.getInstance();
}
public int getModeContexteHF()
{
return 1;
}
/**
* Retourne le mode d'affichage de l'ActionBar de la fenêtre.
*/
public int getModeActionBar()
{
return 0;
}
/**
* Retourne vrai si la fenêtre est maximisée, faux sinon.
*/
public boolean isMaximisee()
{
return true;
}
/**
* Retourne vrai si la fenêtre a une barre de titre, faux sinon.
*/
public boolean isAvecBarreDeTitre()
{
return false;
}
/**
* Retourne le mode d'affichage de la barre système de la fenêtre.
*/
public int getModeBarreSysteme()
{
return 1;
}
/**
* Retourne vrai si la fenêtre est munie d'ascenseurs automatique, faux sinon.
*/
public boolean isAvecAscenseurAuto()
{
return false;
}
/**
* Retourne Vrai si on doit appliquer un theme "dark" (sombre) ou Faux si on doit appliquer "light" (clair) à la fenêtre.
* Ce choix se base sur la couleur du libellé par défaut dans le gabarit de la fenêtre.
*/
public boolean isThemeDark()
{
return false;
}
/**
* Retourne vrai si l'option de masquage automatique de l'ActionBar lorsqu'on scrolle dans un champ de la fenêtre a été activée.
*/
public boolean isMasquageAutomatiqueActionBar()
{
return false;
}
public static class WDActiviteFenetre extends WDActivite
{
protected WDFenetre getFenetre()
{
return GWDPHome_Biking.getInstance().mWD_FEN_CreationTables;
}
}
/**
* Retourne le nom du gabarit associée à la fenêtre.
*/
public String getNomGabarit()
{
return "210 MATERIAL DESIGN ORANGE#WM";
}
}
