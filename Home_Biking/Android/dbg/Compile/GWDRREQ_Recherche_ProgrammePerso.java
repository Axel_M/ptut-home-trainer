/**
 * Code généré par WINDEV Mobile - NE PAS MODIFIER !
 * Objet WINDEV Mobile : Requête
 * Classe Android : REQ_Recherche_ProgrammePerso
 * Date : 02/04/2021 14:52:48
 * Version de wdjava.dll  : 25.0.221.6
 */


package com.logicorp.home_biking.wdgen;


import com.logicorp.home_biking.*;
import fr.pcsoft.wdjava.core.types.*;
import fr.pcsoft.wdjava.core.*;
import fr.pcsoft.wdjava.database.hf.requete.parsing.*;
/*Imports trouvés dans le code WL*/
/*Fin Imports trouvés dans le code WL*/



public class GWDRREQ_Recherche_ProgrammePerso extends WDDescRequeteWDR
{
public String getNomLogique()
{
return "REQ_Recherche_ProgrammePerso";
}
public String getCodeSQLOriginal()
{
return "SELECT\r\n\tProgramme.Id_Programme AS Id_Programme,\r\n\tProgramme.nom AS nom,\r\n\tProgramme.Difficulte AS Difficulte,\r\n\tProgramme.Id_Compte_Client AS Id_Compte_Client\r\nFROM\r\n\tProgramme\r\nWHERE\r\n\tProgramme.nom LIKE %{ParamNom#0}%\r\n\tAND\r\n\tProgramme.Id_Compte_Client = 1\r\n";
}
public Requete initArbre() throws WDInvalidSQLException
{
Select varSelect = new Select();
varSelect.setType(1);
Rubrique rub_Id_Programme = new Rubrique();
rub_Id_Programme.setNom("Id_Programme");
rub_Id_Programme.setAlias("Id_Programme");
rub_Id_Programme.setNomFichier("Programme");
rub_Id_Programme.setAliasFichier("Programme");
varSelect.ajouterElement(rub_Id_Programme);
Rubrique rub_nom = new Rubrique();
rub_nom.setNom("nom");
rub_nom.setAlias("nom");
rub_nom.setNomFichier("Programme");
rub_nom.setAliasFichier("Programme");
varSelect.ajouterElement(rub_nom);
Rubrique rub_Difficulte = new Rubrique();
rub_Difficulte.setNom("Difficulte");
rub_Difficulte.setAlias("Difficulte");
rub_Difficulte.setNomFichier("Programme");
rub_Difficulte.setAliasFichier("Programme");
varSelect.ajouterElement(rub_Difficulte);
Rubrique rub_Id_Compte_Client = new Rubrique();
rub_Id_Compte_Client.setNom("Id_Compte_Client");
rub_Id_Compte_Client.setAlias("Id_Compte_Client");
rub_Id_Compte_Client.setNomFichier("Programme");
rub_Id_Compte_Client.setAliasFichier("Programme");
varSelect.ajouterElement(rub_Id_Compte_Client);
From varFrom = new From();
Fichier fic_Programme = new Fichier();
fic_Programme.setNom("Programme");
fic_Programme.setAlias("Programme");
varFrom.ajouterElement(fic_Programme);
Requete varReqSelect = new Requete(1);
varReqSelect.ajouterClause(varSelect);
varReqSelect.ajouterClause(varFrom);
Expression expr_AND = new Expression(24, "AND", "Programme.nom LIKE %{ParamNom}%\r\n\tAND\r\n\tProgramme.Id_Compte_Client = 1");
Expression expr_LIKE = new Expression(32, "LIKE", "Programme.nom LIKE %{ParamNom}%");
expr_LIKE.ajouterOption(EWDOptionRequete.LIKE_CASE_SENSITIVE, "0");
expr_LIKE.ajouterOption(EWDOptionRequete.LIKE_COMMENCE_PAR, "0");
expr_LIKE.ajouterOption(EWDOptionRequete.LIKE_CONTIENT, "1");
expr_LIKE.ajouterOption(EWDOptionRequete.LIKE_FINI_PAR, "0");
expr_LIKE.ajouterOption(EWDOptionRequete.NOT_LIKE, "0");
expr_LIKE.ajouterOption(EWDOptionRequete.LIKE_CARACT_ECHAP, "92");
Rubrique rub_nom_1 = new Rubrique();
rub_nom_1.setNom("Programme.nom");
rub_nom_1.setAlias("nom");
rub_nom_1.setNomFichier("Programme");
rub_nom_1.setAliasFichier("Programme");
expr_LIKE.ajouterElement(rub_nom_1);
Parametre param_ParamNom = new Parametre();
param_ParamNom.setNom("ParamNom");
expr_LIKE.ajouterElement(param_ParamNom);
expr_AND.ajouterElement(expr_LIKE);
Expression expr__ = new Expression(9, "=", "Programme.Id_Compte_Client = 1");
Rubrique rub_Id_Compte_Client_1 = new Rubrique();
rub_Id_Compte_Client_1.setNom("Programme.Id_Compte_Client");
rub_Id_Compte_Client_1.setAlias("Id_Compte_Client");
rub_Id_Compte_Client_1.setNomFichier("Programme");
rub_Id_Compte_Client_1.setAliasFichier("Programme");
expr__.ajouterElement(rub_Id_Compte_Client_1);
Literal varLiteral = new Literal();
varLiteral.setValeur("1");
varLiteral.setTypeWL(8);
expr__.ajouterElement(varLiteral);
expr_AND.ajouterElement(expr__);
Where varWhere = new Where();
varWhere.ajouterElement(expr_AND);
varReqSelect.ajouterClause(varWhere);
Limit varLimit = new Limit();
varLimit.setType(0);
varLimit.setNbEnregs(0);
varLimit.setOffset(0);
varReqSelect.ajouterClause(varLimit);
return varReqSelect;
}
public String getNomFichier(int nIndex)
{
switch(nIndex)
{
case 0 : return "Programme";
default: return null;
}
}
public String getAliasFichier(int nIndex)
{
switch(nIndex)
{
case 0 : return "Programme";
default: return null;
}
}


public int getIdWDR()
{
return com.logicorp.home_biking.R.raw.req_recherche_programmeperso;
}
public String getNomFichierWDR()
{
return "req_recherche_programmeperso";
}
}
