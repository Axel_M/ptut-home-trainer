/**
 * Code généré par WINDEV Mobile - NE PAS MODIFIER !
 * Objet WINDEV Mobile : Fenêtre
 * Classe Android : FEN_User_Profile
 * Date : 06/03/2021 17:18:02
 * Version de wdjava.dll  : 25.0.221.6
 */


package com.logicorp.home_biking.wdgen;


import com.logicorp.home_biking.*;
import fr.pcsoft.wdjava.core.types.*;
import fr.pcsoft.wdjava.core.*;
import fr.pcsoft.wdjava.ui.champs.fenetre.*;
import fr.pcsoft.wdjava.ui.champs.image.*;
import fr.pcsoft.wdjava.ui.cadre.*;
import fr.pcsoft.wdjava.api.*;
import fr.pcsoft.wdjava.ui.champs.saisie.*;
import fr.pcsoft.wdjava.ui.champs.libelle.*;
import fr.pcsoft.wdjava.core.application.*;
import fr.pcsoft.wdjava.ui.activite.*;
/*Imports trouvés dans le code WL*/
/*Fin Imports trouvés dans le code WL*/



public class GWDFFEN_User_Profile extends WDFenetre
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs de FEN_User_Profile
////////////////////////////////////////////////////////////////////////////

/**
 * IMG_Button_Back
 */
class GWDIMG_Button_Back extends WDChampImage
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FEN_User_Profile.IMG_Button_Back
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3440255206302634324l);

super.setChecksum("874093942");

super.setNom("IMG_Button_Back");

super.setType(30001);

super.setBulle("");

super.setLibelle("");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setNavigable(false);

super.setEtatInitial(0);

super.setPositionInitiale(8, 8);

super.setTailleInitiale(52, 52);

super.setValeurInitiale("C:\\Users\\axelm\\Desktop\\COURS IUT INFO\\Semestre 3\\M3302 - Projet tutore - Mise en situation professionnelle\\ptut-home-trainer\\Home_Biking\\Bouton_previous_page.png");

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(1);

super.setAncrageInitial(0, 1000, 1000, 1000, 1000, 0);

super.setTransparence(1);

super.setParamImage(2097158, 0, true, 100);

super.setSymetrie(0);

super.setZoneClicage(true);

super.setPCodeMultitouch(false);

super.setChargementEnTacheDeFond(false);

super.setOrientationExif(false);

super.setParamAnimation(1, 1, false, 300, true, false);

super.setAnimationInitiale(false);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -1, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

/**
 * Traitement: Clic sur IMG_Button_Back
 */
public void clicSurBoutonGauche()
{
super.clicSurBoutonGauche();

// Ferme()
//MAP:2fbe3de2045b5154:00000012:1:FEN_User_Profile.IMG_Button_Back:com.logicorp.home_biking.wdgen.GWDFFEN_User_Profile$GWDIMG_Button_Back:Clic sur IMG_Button_Back
// Ferme()
//MAP:2fbe3de2045b5154:00000012:1:FEN_User_Profile.IMG_Button_Back:com.logicorp.home_biking.wdgen.GWDFFEN_User_Profile$GWDIMG_Button_Back:Clic sur IMG_Button_Back
WDAPIFenetre.ferme();

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurClic();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDIMG_Button_Back mWD_IMG_Button_Back;

/**
 * SAI_Prenom
 */
class GWDSAI_Prenom extends WDChampSaisieSimple
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°2 de FEN_User_Profile.SAI_Prenom
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setRectLibelle(0,0,314,21);
super.setRectCompPrincipal(10,21,284,40);
super.setQuid(3440932595616636319l);

super.setChecksum("831227686");

super.setNom("SAI_Prenom");

super.setType(20001);

super.setBulle("");

super.setLibelle("Prénom");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setTaille(0);

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(8, 195);

super.setTailleInitiale(314, 63);

super.setValeurInitiale("");

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setMotDePasse(false);

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(2);

super.setAncrageInitial(8, 1000, 1000, 1000, 1000, 0);

super.setEllipse(0);

super.setIndication("");

super.setNumTab(2);

super.setModeAscenseur(2, 2);

super.setEffacementAutomatique(true);

super.setFinSaisieAutomatique(false);

super.setLettreAppel(65535);

super.setSelectionEnAffichage(true);

super.setPersistant(false);

super.setClavierEnSaisie(true);

super.setMasqueAffichage(new WDChaineU(""));

super.setParamBtnActionClavier(0, "");

super.setRetraitGauche(2);

super.setMiseABlancSiZero(true);

super.setVerifieOrthographe(true);

super.setTauxParallaxe(0, 0);

super.setBoutonSuppression(0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), 0, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN(2, 0x222222, 0x0, 0xF0F0F0, 2.000000, 2.000000, 1, 1));

super.setStyleSaisie(0x757575, creerPolice_GEN("Roboto", -8.000000, 0));

super.setStyleTexteIndication(0x8E8E8F, creerPolice_GEN("Roboto", -8.000000, 0), 0);

super.setStyleJeton(WDCadreFactory.creerCadre_GEN(2, 0x98FF, 0x187F, 0x98FF, 4.000000, 4.000000, 1, 1), 0xFFFFFF, "", 1);

activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDSAI_Prenom mWD_SAI_Prenom;

/**
 * SAI_Nom
 */
class GWDSAI_Nom extends WDChampSaisieSimple
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°3 de FEN_User_Profile.SAI_Nom
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setRectLibelle(0,0,314,21);
super.setRectCompPrincipal(10,21,284,40);
super.setQuid(3440933437431798950l);

super.setChecksum("832800497");

super.setNom("SAI_Nom");

super.setType(20001);

super.setBulle("");

super.setLibelle("Nom");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setTaille(0);

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(8, 112);

super.setTailleInitiale(314, 63);

super.setValeurInitiale("");

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setMotDePasse(false);

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(3);

super.setAncrageInitial(8, 1000, 1000, 1000, 1000, 0);

super.setEllipse(0);

super.setIndication("");

super.setNumTab(1);

super.setModeAscenseur(2, 2);

super.setEffacementAutomatique(true);

super.setFinSaisieAutomatique(false);

super.setLettreAppel(65535);

super.setSelectionEnAffichage(true);

super.setPersistant(false);

super.setClavierEnSaisie(true);

super.setMasqueAffichage(new WDChaineU(""));

super.setParamBtnActionClavier(0, "");

super.setRetraitGauche(2);

super.setMiseABlancSiZero(true);

super.setVerifieOrthographe(true);

super.setTauxParallaxe(0, 0);

super.setBoutonSuppression(0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), 0, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN(2, 0x222222, 0x0, 0xF0F0F0, 2.000000, 2.000000, 1, 1));

super.setStyleSaisie(0x757575, creerPolice_GEN("Roboto", -8.000000, 0));

super.setStyleTexteIndication(0x8E8E8F, creerPolice_GEN("Roboto", -8.000000, 0), 0);

super.setStyleJeton(WDCadreFactory.creerCadre_GEN(2, 0x98FF, 0x187F, 0x98FF, 4.000000, 4.000000, 1, 1), 0xFFFFFF, "", 1);

activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDSAI_Nom mWD_SAI_Nom;

/**
 * SAI_Date_Naissance
 */
class GWDSAI_Date_Naissance extends WDChampSaisieSimple
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°4 de FEN_User_Profile.SAI_Date_Naissance
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setRectLibelle(0,0,235,21);
super.setRectCompPrincipal(10,21,110,40);
super.setQuid(3440933755260632502l);

super.setChecksum("834147141");

super.setNom("SAI_Date_Naissance");

super.setType(20002);

super.setBulle("");

super.setLibelle("Date Naissance");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setTaille(0);

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(8, 266);

super.setTailleInitiale(235, 63);

super.setValeurInitiale("");

super.setPlan(0);

super.setCadrageHorizontal(2);

super.setMotDePasse(false);

super.setTypeSaisie(2);

super.setFormatMemorise("AAAAMMJJ");

super.setMasqueSaisie(new WDChaineU("JJ/MM/AAAA"));

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(4);

super.setAncrageInitial(8, 1000, 1000, 1000, 1000, 0);

super.setEllipse(0);

super.setIndication("");

super.setNumTab(3);

super.setModeAscenseur(2, 2);

super.setEffacementAutomatique(true);

super.setFinSaisieAutomatique(false);

super.setLettreAppel(65535);

super.setSelectionEnAffichage(true);

super.setPersistant(false);

super.setClavierEnSaisie(true);

super.setMasqueAffichage(new WDChaineU(""));

super.setParamBtnActionClavier(0, "");

super.setRetraitGauche(2);

super.setMiseABlancSiZero(true);

super.setVerifieOrthographe(true);

super.setTauxParallaxe(0, 0);

super.setBoutonSuppression(0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), 0, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN(2, 0x757575, 0x0, 0xF0F0F0, 2.000000, 2.000000, 1, 1));

super.setStyleSaisie(0x222222, creerPolice_GEN("Roboto", -8.000000, 0));

super.setStyleTexteIndication(0x8E8E8F, creerPolice_GEN("Roboto", -8.000000, 0), 0);

super.setStyleJeton(WDCadreFactory.creerCadre_GEN(2, 0x98FF, 0x187F, 0x98FF, 4.000000, 4.000000, 1, 1), 0xFFFFFF, "", 1);

activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDSAI_Date_Naissance mWD_SAI_Date_Naissance;

/**
 * LIB_InfoTech
 */
class GWDLIB_InfoTech extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°5 de FEN_User_Profile.LIB_InfoTech
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3440935322926353724l);

super.setChecksum("836713222");

super.setNom("LIB_InfoTech");

super.setType(3);

super.setBulle("");

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("Informations Techniques");

super.setNote("", "");

super.setCurseurSouris(0);

super.setEtatInitial(0);

super.setPositionInitiale(108, 334);

super.setTailleInitiale(204, 25);

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(5);

super.setAncrageInitial(8, 1000, 1000, 1000, 1000, 0);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, 0xFFFFFFFF, creerPolice_GEN("Roboto", -9.000000, 0), 3, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_InfoTech mWD_LIB_InfoTech;

/**
 * SAI_PMA
 */
class GWDSAI_PMA extends WDChampSaisieSimple
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°6 de FEN_User_Profile.SAI_PMA
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setRectLibelle(0,0,143,21);
super.setRectCompPrincipal(10,21,93,40);
super.setQuid(3440935645049500790l);

super.setChecksum("837312707");

super.setNom("SAI_PMA");

super.setType(20001);

super.setBulle("");

super.setLibelle("PMA");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setTaille(4);

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(8, 359);

super.setTailleInitiale(143, 63);

super.setValeurInitiale("");

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setMotDePasse(false);

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("5"));

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(6);

super.setAncrageInitial(8, 1000, 1000, 1000, 1000, 0);

super.setEllipse(0);

super.setIndication("");

super.setNumTab(4);

super.setModeAscenseur(2, 2);

super.setEffacementAutomatique(true);

super.setFinSaisieAutomatique(false);

super.setLettreAppel(65535);

super.setSelectionEnAffichage(true);

super.setPersistant(false);

super.setClavierEnSaisie(true);

super.setMasqueAffichage(new WDChaineU(""));

super.setParamBtnActionClavier(0, "");

super.setRetraitGauche(2);

super.setMiseABlancSiZero(true);

super.setVerifieOrthographe(true);

super.setTauxParallaxe(0, 0);

super.setBoutonSuppression(0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), 0, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN(2, 0x222222, 0x0, 0xF0F0F0, 2.000000, 2.000000, 1, 1));

super.setStyleSaisie(0x222222, creerPolice_GEN("Roboto", -8.000000, 0));

super.setStyleTexteIndication(0x8E8E8F, creerPolice_GEN("Roboto", -8.000000, 0), 2);

super.setStyleJeton(WDCadreFactory.creerCadre_GEN(2, 0x98FF, 0x187F, 0x98FF, 4.000000, 4.000000, 1, 1), 0xFFFFFF, "", 1);

activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDSAI_PMA mWD_SAI_PMA;

/**
 * SAI_BPM
 */
class GWDSAI_BPM extends WDChampSaisieSimple
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°7 de FEN_User_Profile.SAI_BPM
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setRectLibelle(0,0,179,21);
super.setRectCompPrincipal(10,21,93,40);
super.setQuid(3440936302182402403l);

super.setChecksum("840218185");

super.setNom("SAI_BPM");

super.setType(20001);

super.setBulle("");

super.setLibelle("BPM_MAX");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setTaille(3);

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(8, 430);

super.setTailleInitiale(179, 63);

super.setValeurInitiale("");

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setMotDePasse(false);

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("5"));

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(7);

super.setAncrageInitial(8, 1000, 1000, 1000, 1000, 0);

super.setEllipse(0);

super.setIndication("");

super.setNumTab(5);

super.setModeAscenseur(2, 2);

super.setEffacementAutomatique(true);

super.setFinSaisieAutomatique(false);

super.setLettreAppel(65535);

super.setSelectionEnAffichage(true);

super.setPersistant(false);

super.setClavierEnSaisie(true);

super.setMasqueAffichage(new WDChaineU(""));

super.setParamBtnActionClavier(0, "");

super.setRetraitGauche(2);

super.setMiseABlancSiZero(true);

super.setVerifieOrthographe(true);

super.setTauxParallaxe(0, 0);

super.setBoutonSuppression(0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), 0, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN(2, 0x222222, 0x0, 0xF0F0F0, 2.000000, 2.000000, 1, 1));

super.setStyleSaisie(0x222222, creerPolice_GEN("Roboto", -8.000000, 0));

super.setStyleTexteIndication(0x8E8E8F, creerPolice_GEN("Roboto", -8.000000, 0), 2);

super.setStyleJeton(WDCadreFactory.creerCadre_GEN(2, 0x98FF, 0x187F, 0x98FF, 4.000000, 4.000000, 1, 1), 0xFFFFFF, "", 1);

activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDSAI_BPM mWD_SAI_BPM;

/**
 * IMG_Button_Validate
 */
class GWDIMG_Button_Validate extends WDChampImage
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°8 de FEN_User_Profile.IMG_Button_Validate
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3440936598536393850l);

super.setChecksum("841469013");

super.setNom("IMG_Button_Validate");

super.setType(30001);

super.setBulle("");

super.setLibelle("");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setNavigable(false);

super.setEtatInitial(0);

super.setPositionInitiale(231, 8);

super.setTailleInitiale(88, 52);

super.setValeurInitiale("C:\\Users\\axelm\\Desktop\\COURS IUT INFO\\Semestre 3\\M3302 - Projet tutore - Mise en situation professionnelle\\ptut-home-trainer\\Home_Biking\\button validate.png");

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(8);

super.setAncrageInitial(4, 1000, 1000, 1000, 1000, 0);

super.setTransparence(1);

super.setParamImage(2097158, 0, true, 100);

super.setSymetrie(0);

super.setZoneClicage(true);

super.setPCodeMultitouch(false);

super.setChargementEnTacheDeFond(false);

super.setOrientationExif(false);

super.setParamAnimation(1, 1, false, 300, true, false);

super.setAnimationInitiale(false);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -1, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

/**
 * Traitement: Clic sur IMG_Button_Validate
 */
public void clicSurBoutonGauche()
{
super.clicSurBoutonGauche();

// HLitRecherche(Compte_Client,Id_Compte_Client,1)
//MAP:2fc0a99b0267147a:00000012:1:FEN_User_Profile.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_User_Profile$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
// HLitRecherche(Compte_Client,Id_Compte_Client,1)
//MAP:2fc0a99b0267147a:00000012:1:FEN_User_Profile.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_User_Profile$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDAPIHF.hLitRecherche(WDAPIHF.getFichierSansCasseNiAccent("compte_client"),WDAPIHF.getRubriqueSansCasseNiAccent("id_compte_client"),new WDEntier4(1));

// Compte_Client.nom=SAI_Nom
//MAP:2fc0a99b0267147a:00000012:2:FEN_User_Profile.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_User_Profile$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDAPIHF.getFichierSansCasseNiAccent("compte_client").getRubriqueSansCasseNiAccent("nom").setValeur(mWD_SAI_Nom);

// Compte_Client.prenom=SAI_Prenom
//MAP:2fc0a99b0267147a:00000012:3:FEN_User_Profile.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_User_Profile$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDAPIHF.getFichierSansCasseNiAccent("compte_client").getRubriqueSansCasseNiAccent("prenom").setValeur(mWD_SAI_Prenom);

// Compte_Client.dateNaissance=SAI_Date_Naissance
//MAP:2fc0a99b0267147a:00000012:4:FEN_User_Profile.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_User_Profile$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDAPIHF.getFichierSansCasseNiAccent("compte_client").getRubriqueSansCasseNiAccent("datenaissance").setValeur(mWD_SAI_Date_Naissance);

// Compte_Client.pma=SAI_PMA
//MAP:2fc0a99b0267147a:00000012:5:FEN_User_Profile.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_User_Profile$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDAPIHF.getFichierSansCasseNiAccent("compte_client").getRubriqueSansCasseNiAccent("pma").setValeur(mWD_SAI_PMA);

// Compte_Client.fcm=SAI_BPM
//MAP:2fc0a99b0267147a:00000012:6:FEN_User_Profile.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_User_Profile$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDAPIHF.getFichierSansCasseNiAccent("compte_client").getRubriqueSansCasseNiAccent("fcm").setValeur(mWD_SAI_BPM);

// HModifie(Compte_Client)
//MAP:2fc0a99b0267147a:00000012:7:FEN_User_Profile.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_User_Profile$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDAPIHF.hModifie(WDAPIHF.getFichierSansCasseNiAccent("compte_client"));

// ToastAffiche("Modifications effectuées")
//MAP:2fc0a99b0267147a:00000012:8:FEN_User_Profile.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_User_Profile$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDAPIToast.toastAffiche("Modifications effectuées");

// Multitâche(2s)
//MAP:2fc0a99b0267147a:00000012:9:FEN_User_Profile.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_User_Profile$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDAPIVM.multitache((new WDDuree("0000002000")));

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurClic();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDIMG_Button_Validate mWD_IMG_Button_Validate;

/**
 * LIB_desc
 */
class GWDLIB_desc extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°9 de FEN_User_Profile.LIB_desc
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(2321054722560370411l);

super.setChecksum("581580604");

super.setNom("LIB_desc");

super.setType(3);

super.setBulle("");

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("Paramètres du Compte");

super.setNote("", "");

super.setCurseurSouris(0);

super.setEtatInitial(0);

super.setPositionInitiale(52, 68);

super.setTailleInitiale(227, 30);

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(9);

super.setAncrageInitial(0, 1000, 1000, 1000, 1000, 0);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, 0xFFFFFFFF, creerPolice_GEN("Roboto", -11.000000, 0), 3, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_desc mWD_LIB_desc;

/**
 * Traitement: Déclarations globales de FEN_User_Profile
 */
public void declarerGlobale(WDObjet[] WD_tabParam)
{
// PROCEDURE MaFenêtre()
//MAP:2fbe3dcc0424fae7:00000000:1:FEN_User_Profile:com.logicorp.home_biking.wdgen.GWDFFEN_User_Profile:Déclarations globales de FEN_User_Profile
super.declarerGlobale(WD_tabParam, 0, 0);
int WD_ntabParamLen = 0;
if(WD_tabParam!=null) WD_ntabParamLen = WD_tabParam.length;


}




/**
 * Traitement: Fin d'initialisation de FEN_User_Profile
 */
public void init()
{
super.init();

// HLitRecherche(Compte_Client,Id_Compte_Client,1)
//MAP:2fbe3dcc0424fae7:00000022:1:FEN_User_Profile:com.logicorp.home_biking.wdgen.GWDFFEN_User_Profile:Fin d'initialisation de FEN_User_Profile
// HLitRecherche(Compte_Client,Id_Compte_Client,1)
//MAP:2fbe3dcc0424fae7:00000022:1:FEN_User_Profile:com.logicorp.home_biking.wdgen.GWDFFEN_User_Profile:Fin d'initialisation de FEN_User_Profile
WDAPIHF.hLitRecherche(WDAPIHF.getFichierSansCasseNiAccent("compte_client"),WDAPIHF.getRubriqueSansCasseNiAccent("id_compte_client"),new WDEntier4(1));

// SAI_Nom=Compte_Client.nom
//MAP:2fbe3dcc0424fae7:00000022:2:FEN_User_Profile:com.logicorp.home_biking.wdgen.GWDFFEN_User_Profile:Fin d'initialisation de FEN_User_Profile
mWD_SAI_Nom.setValeur(WDAPIHF.getFichierSansCasseNiAccent("compte_client").getRubriqueSansCasseNiAccent("nom"));

// SAI_Prenom=Compte_Client.prenom
//MAP:2fbe3dcc0424fae7:00000022:3:FEN_User_Profile:com.logicorp.home_biking.wdgen.GWDFFEN_User_Profile:Fin d'initialisation de FEN_User_Profile
mWD_SAI_Prenom.setValeur(WDAPIHF.getFichierSansCasseNiAccent("compte_client").getRubriqueSansCasseNiAccent("prenom"));

// SAI_Date_Naissance=Compte_Client.dateNaissance
//MAP:2fbe3dcc0424fae7:00000022:4:FEN_User_Profile:com.logicorp.home_biking.wdgen.GWDFFEN_User_Profile:Fin d'initialisation de FEN_User_Profile
mWD_SAI_Date_Naissance.setValeur(WDAPIHF.getFichierSansCasseNiAccent("compte_client").getRubriqueSansCasseNiAccent("datenaissance"));

// SAI_PMA=Compte_Client.pma
//MAP:2fbe3dcc0424fae7:00000022:5:FEN_User_Profile:com.logicorp.home_biking.wdgen.GWDFFEN_User_Profile:Fin d'initialisation de FEN_User_Profile
mWD_SAI_PMA.setValeur(WDAPIHF.getFichierSansCasseNiAccent("compte_client").getRubriqueSansCasseNiAccent("pma"));

// SAI_BPM=Compte_Client.fcm
//MAP:2fbe3dcc0424fae7:00000022:6:FEN_User_Profile:com.logicorp.home_biking.wdgen.GWDFFEN_User_Profile:Fin d'initialisation de FEN_User_Profile
mWD_SAI_BPM.setValeur(WDAPIHF.getFichierSansCasseNiAccent("compte_client").getRubriqueSansCasseNiAccent("fcm"));

}




// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// Création des champs de la fenêtre FEN_User_Profile
////////////////////////////////////////////////////////////////////////////
protected void creerChamps()
{
mWD_IMG_Button_Back = new GWDIMG_Button_Back();
mWD_SAI_Prenom = new GWDSAI_Prenom();
mWD_SAI_Nom = new GWDSAI_Nom();
mWD_SAI_Date_Naissance = new GWDSAI_Date_Naissance();
mWD_LIB_InfoTech = new GWDLIB_InfoTech();
mWD_SAI_PMA = new GWDSAI_PMA();
mWD_SAI_BPM = new GWDSAI_BPM();
mWD_IMG_Button_Validate = new GWDIMG_Button_Validate();
mWD_LIB_desc = new GWDLIB_desc();

}
////////////////////////////////////////////////////////////////////////////
// Initialisation de la fenêtre FEN_User_Profile
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.setQuid(3440255111809792743l);

super.setChecksum("876279212");

super.setNom("FEN_User_Profile");

super.setType(1);

super.setBulle("");

super.setMenuContextuelSysteme();

super.setCurseurSouris(0);

super.setNote("", "");

super.setCouleur(0x0);

super.setCouleurFond(0xFFFFFF);

super.setPositionInitiale(0, 0);

super.setTailleInitiale(320, 543);

super.setTitre("User_Profile");

super.setTailleMin(-1, -1);

super.setTailleMax(20000, 20000);

super.setVisibleInitial(true);

super.setPositionFenetre(2);

super.setPersistant(true);

super.setGFI(true);

super.setAnimationFenetre(0);

super.setImageFond("", 1, 0, 1);

super.setCouleurTexteAutomatique(0x303030);

super.setCouleurBarreSysteme(0x80FF);


activerEcoute();

////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FEN_User_Profile
////////////////////////////////////////////////////////////////////////////
mWD_IMG_Button_Back.initialiserObjet();
super.ajouter("IMG_Button_Back", mWD_IMG_Button_Back);
mWD_SAI_Prenom.initialiserObjet();
super.ajouter("SAI_Prenom", mWD_SAI_Prenom);
mWD_SAI_Nom.initialiserObjet();
super.ajouter("SAI_Nom", mWD_SAI_Nom);
mWD_SAI_Date_Naissance.initialiserObjet();
super.ajouter("SAI_Date_Naissance", mWD_SAI_Date_Naissance);
mWD_LIB_InfoTech.initialiserObjet();
super.ajouter("LIB_InfoTech", mWD_LIB_InfoTech);
mWD_SAI_PMA.initialiserObjet();
super.ajouter("SAI_PMA", mWD_SAI_PMA);
mWD_SAI_BPM.initialiserObjet();
super.ajouter("SAI_BPM", mWD_SAI_BPM);
mWD_IMG_Button_Validate.initialiserObjet();
super.ajouter("IMG_Button_Validate", mWD_IMG_Button_Validate);
mWD_LIB_desc.initialiserObjet();
super.ajouter("LIB_desc", mWD_LIB_desc);

super.terminerInitialisation();
}

////////////////////////////////////////////////////////////////////////////
public boolean isUniteAffichageLogique()
{
return false;
}

public WDProjet getProjet()
{
return GWDPHome_Biking.getInstance();
}

public IWDEnsembleElement getEnsemble()
{
return GWDPHome_Biking.getInstance();
}
public int getModeContexteHF()
{
return 1;
}
/**
* Retourne le mode d'affichage de l'ActionBar de la fenêtre.
*/
public int getModeActionBar()
{
return 0;
}
/**
* Retourne vrai si la fenêtre est maximisée, faux sinon.
*/
public boolean isMaximisee()
{
return true;
}
/**
* Retourne vrai si la fenêtre a une barre de titre, faux sinon.
*/
public boolean isAvecBarreDeTitre()
{
return false;
}
/**
* Retourne le mode d'affichage de la barre système de la fenêtre.
*/
public int getModeBarreSysteme()
{
return 1;
}
/**
* Retourne vrai si la fenêtre est munie d'ascenseurs automatique, faux sinon.
*/
public boolean isAvecAscenseurAuto()
{
return true;
}
/**
* Retourne Vrai si on doit appliquer un theme "dark" (sombre) ou Faux si on doit appliquer "light" (clair) à la fenêtre.
* Ce choix se base sur la couleur du libellé par défaut dans le gabarit de la fenêtre.
*/
public boolean isThemeDark()
{
return false;
}
/**
* Retourne vrai si l'option de masquage automatique de l'ActionBar lorsqu'on scrolle dans un champ de la fenêtre a été activée.
*/
public boolean isMasquageAutomatiqueActionBar()
{
return false;
}
public static class WDActiviteFenetre extends WDActivite
{
protected WDFenetre getFenetre()
{
return GWDPHome_Biking.getInstance().mWD_FEN_User_Profile;
}
}
/**
* Retourne le nom du gabarit associée à la fenêtre.
*/
public String getNomGabarit()
{
return "210 MATERIAL DESIGN ORANGE#WM";
}
}
