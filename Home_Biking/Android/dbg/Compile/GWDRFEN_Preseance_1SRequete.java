/**
 * Code généré par WINDEV Mobile - NE PAS MODIFIER !
 * Objet WINDEV Mobile : Requête
 * Classe Android : FEN_Preseance_1$Requête
 * Date : 02/04/2021 14:52:47
 * Version de wdjava.dll  : 25.0.221.6
 */


package com.logicorp.home_biking.wdgen;


import com.logicorp.home_biking.*;
import fr.pcsoft.wdjava.core.types.*;
import fr.pcsoft.wdjava.core.*;
import fr.pcsoft.wdjava.database.hf.requete.parsing.*;
/*Imports trouvés dans le code WL*/
/*Fin Imports trouvés dans le code WL*/



public class GWDRFEN_Preseance_1SRequete extends WDDescRequeteWDR
{
public String getNomLogique()
{
return "FEN_Preseance_1$Requête";
}
public String getCodeSQLOriginal()
{
return " SELECT  Exercice.Nom AS Nom,\t contenir.nb_rep AS nb_rep,\t contenir.temps AS temps,\t contenir.repos AS repos  FROM  Exercice,\t contenir  WHERE   Exercice.Id_Exercice = contenir.Id_Exercice  AND  ( contenir.Id_Programme = {ParamId_Programme#0} )";
}
public Requete initArbre() throws WDInvalidSQLException
{
Select varSelect = new Select();
varSelect.setType(1);
Rubrique rub_Nom = new Rubrique();
rub_Nom.setNom("Nom");
rub_Nom.setAlias("Nom");
rub_Nom.setNomFichier("Exercice");
rub_Nom.setAliasFichier("Exercice");
varSelect.ajouterElement(rub_Nom);
Rubrique rub_nb_rep = new Rubrique();
rub_nb_rep.setNom("nb_rep");
rub_nb_rep.setAlias("nb_rep");
rub_nb_rep.setNomFichier("contenir");
rub_nb_rep.setAliasFichier("contenir");
varSelect.ajouterElement(rub_nb_rep);
Rubrique rub_temps = new Rubrique();
rub_temps.setNom("temps");
rub_temps.setAlias("temps");
rub_temps.setNomFichier("contenir");
rub_temps.setAliasFichier("contenir");
varSelect.ajouterElement(rub_temps);
Rubrique rub_repos = new Rubrique();
rub_repos.setNom("repos");
rub_repos.setAlias("repos");
rub_repos.setNomFichier("contenir");
rub_repos.setAliasFichier("contenir");
varSelect.ajouterElement(rub_repos);
From varFrom = new From();
Fichier fic_Exercice = new Fichier();
fic_Exercice.setNom("Exercice");
fic_Exercice.setAlias("Exercice");
varFrom.ajouterElement(fic_Exercice);
Fichier fic_contenir = new Fichier();
fic_contenir.setNom("contenir");
fic_contenir.setAlias("contenir");
varFrom.ajouterElement(fic_contenir);
Requete varReqSelect = new Requete(1);
varReqSelect.ajouterClause(varSelect);
varReqSelect.ajouterClause(varFrom);
Expression expr_AND = new Expression(24, "AND", "Exercice.Id_Exercice = contenir.Id_Exercice\r\n\tAND\r\n\t(\r\n\t\tcontenir.Id_Programme = {ParamId_Programme}\r\n\t)");
Expression expr__ = new Expression(9, "=", "Exercice.Id_Exercice = contenir.Id_Exercice");
Rubrique rub_Id_Exercice = new Rubrique();
rub_Id_Exercice.setNom("Exercice.Id_Exercice");
rub_Id_Exercice.setAlias("Id_Exercice");
rub_Id_Exercice.setNomFichier("Exercice");
rub_Id_Exercice.setAliasFichier("Exercice");
expr__.ajouterElement(rub_Id_Exercice);
Rubrique rub_Id_Exercice_1 = new Rubrique();
rub_Id_Exercice_1.setNom("contenir.Id_Exercice");
rub_Id_Exercice_1.setAlias("Id_Exercice");
rub_Id_Exercice_1.setNomFichier("contenir");
rub_Id_Exercice_1.setAliasFichier("contenir");
expr__.ajouterElement(rub_Id_Exercice_1);
expr_AND.ajouterElement(expr__);
Expression expr___1 = new Expression(9, "=", "contenir.Id_Programme = {ParamId_Programme}");
Rubrique rub_Id_Programme = new Rubrique();
rub_Id_Programme.setNom("contenir.Id_Programme");
rub_Id_Programme.setAlias("Id_Programme");
rub_Id_Programme.setNomFichier("contenir");
rub_Id_Programme.setAliasFichier("contenir");
expr___1.ajouterElement(rub_Id_Programme);
Parametre param_ParamId_Programme = new Parametre();
param_ParamId_Programme.setNom("ParamId_Programme");
expr___1.ajouterElement(param_ParamId_Programme);
expr_AND.ajouterElement(expr___1);
Where varWhere = new Where();
varWhere.ajouterElement(expr_AND);
varReqSelect.ajouterClause(varWhere);
Limit varLimit = new Limit();
varLimit.setType(0);
varLimit.setNbEnregs(0);
varLimit.setOffset(0);
varReqSelect.ajouterClause(varLimit);
return varReqSelect;
}
public String getNomFichier(int nIndex)
{
switch(nIndex)
{
case 0 : return "Exercice";
case 1 : return "contenir";
default: return null;
}
}
public String getAliasFichier(int nIndex)
{
switch(nIndex)
{
case 0 : return "Exercice";
case 1 : return "contenir";
default: return null;
}
}


}
