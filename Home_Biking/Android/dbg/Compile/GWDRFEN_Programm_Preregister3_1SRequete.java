/**
 * Code généré par WINDEV Mobile - NE PAS MODIFIER !
 * Objet WINDEV Mobile : Requête
 * Classe Android : FEN_Programm_Preregister3_1$Requête
 * Date : 02/04/2021 14:52:48
 * Version de wdjava.dll  : 25.0.221.6
 */


package com.logicorp.home_biking.wdgen;


import com.logicorp.home_biking.*;
import fr.pcsoft.wdjava.core.types.*;
import fr.pcsoft.wdjava.core.*;
import fr.pcsoft.wdjava.database.hf.requete.parsing.*;
/*Imports trouvés dans le code WL*/
/*Fin Imports trouvés dans le code WL*/



public class GWDRFEN_Programm_Preregister3_1SRequete extends WDDescRequeteWDR
{
public String getNomLogique()
{
return "FEN_Programm_Preregister3_1$Requête";
}
public String getCodeSQLOriginal()
{
return " SELECT  Programme.Id_Programme AS Id_Programme,\t Programme.Nom AS Nom,\t Programme.Difficulte AS Difficulte,\t COUNT(contenir.Id_Exercice) AS Comptage_1,\t contenir.temps AS temps,\t SUM(contenir.repos) AS la_somme_repos,\t contenir.nb_rep AS nb_rep  FROM  Programme,\t contenir  WHERE   Programme.Id_Programme = contenir.Id_Programme   GROUP BY  Programme.Id_Programme,\t Programme.Nom,\t Programme.Difficulte,\t contenir.temps,\t contenir.nb_rep";
}
public Requete initArbre() throws WDInvalidSQLException
{
Select varSelect = new Select();
varSelect.setType(1);
Rubrique rub_Id_Programme = new Rubrique();
rub_Id_Programme.setNom("Id_Programme");
rub_Id_Programme.setAlias("Id_Programme");
rub_Id_Programme.setNomFichier("Programme");
rub_Id_Programme.setAliasFichier("Programme");
varSelect.ajouterElement(rub_Id_Programme);
Rubrique rub_Nom = new Rubrique();
rub_Nom.setNom("Nom");
rub_Nom.setAlias("Nom");
rub_Nom.setNomFichier("Programme");
rub_Nom.setAliasFichier("Programme");
varSelect.ajouterElement(rub_Nom);
Rubrique rub_Difficulte = new Rubrique();
rub_Difficulte.setNom("Difficulte");
rub_Difficulte.setAlias("Difficulte");
rub_Difficulte.setNomFichier("Programme");
rub_Difficulte.setAliasFichier("Programme");
varSelect.ajouterElement(rub_Difficulte);
Expression varExprAgregat = new Expression(31, "COUNT", "COUNT(contenir.Id_Exercice)");
varExprAgregat.ajouterOption(EWDOptionRequete.SELECT, "1");
Rubrique rub_Id_Exercice = new Rubrique();
rub_Id_Exercice.setNom("contenir.Id_Exercice");
rub_Id_Exercice.setAlias("Id_Exercice");
rub_Id_Exercice.setNomFichier("contenir");
rub_Id_Exercice.setAliasFichier("contenir");
varExprAgregat.setAlias("Comptage_1");
varExprAgregat.ajouterElement(rub_Id_Exercice);
varSelect.ajouterElement(varExprAgregat);
Rubrique rub_temps = new Rubrique();
rub_temps.setNom("temps");
rub_temps.setAlias("temps");
rub_temps.setNomFichier("contenir");
rub_temps.setAliasFichier("contenir");
varSelect.ajouterElement(rub_temps);
Expression varExprAgregat_1 = new Expression(28, "SUM", "SUM(contenir.repos)");
varExprAgregat_1.ajouterOption(EWDOptionRequete.SELECT, "1");
Rubrique rub_repos = new Rubrique();
rub_repos.setNom("contenir.repos");
rub_repos.setAlias("repos");
rub_repos.setNomFichier("contenir");
rub_repos.setAliasFichier("contenir");
varExprAgregat_1.setAlias("la_somme_repos");
varExprAgregat_1.ajouterElement(rub_repos);
varSelect.ajouterElement(varExprAgregat_1);
Rubrique rub_nb_rep = new Rubrique();
rub_nb_rep.setNom("nb_rep");
rub_nb_rep.setAlias("nb_rep");
rub_nb_rep.setNomFichier("contenir");
rub_nb_rep.setAliasFichier("contenir");
varSelect.ajouterElement(rub_nb_rep);
From varFrom = new From();
Fichier fic_Programme = new Fichier();
fic_Programme.setNom("Programme");
fic_Programme.setAlias("Programme");
varFrom.ajouterElement(fic_Programme);
Fichier fic_contenir = new Fichier();
fic_contenir.setNom("contenir");
fic_contenir.setAlias("contenir");
varFrom.ajouterElement(fic_contenir);
Requete varReqSelect = new Requete(1);
varReqSelect.ajouterClause(varSelect);
varReqSelect.ajouterClause(varFrom);
Expression expr__ = new Expression(9, "=", "Programme.Id_Programme = contenir.Id_Programme");
Rubrique rub_Id_Programme_1 = new Rubrique();
rub_Id_Programme_1.setNom("Programme.Id_Programme");
rub_Id_Programme_1.setAlias("Id_Programme");
rub_Id_Programme_1.setNomFichier("Programme");
rub_Id_Programme_1.setAliasFichier("Programme");
expr__.ajouterElement(rub_Id_Programme_1);
Rubrique rub_Id_Programme_2 = new Rubrique();
rub_Id_Programme_2.setNom("contenir.Id_Programme");
rub_Id_Programme_2.setAlias("Id_Programme");
rub_Id_Programme_2.setNomFichier("contenir");
rub_Id_Programme_2.setAliasFichier("contenir");
expr__.ajouterElement(rub_Id_Programme_2);
Where varWhere = new Where();
varWhere.ajouterElement(expr__);
varReqSelect.ajouterClause(varWhere);
GroupBy varGroupeBy = new GroupBy();
Rubrique rub_Id_Programme_3 = new Rubrique();
rub_Id_Programme_3.setNom("Id_Programme");
rub_Id_Programme_3.setAlias("Id_Programme");
rub_Id_Programme_3.setNomFichier("Programme");
rub_Id_Programme_3.setAliasFichier("Programme");
varGroupeBy.ajouterElement(rub_Id_Programme_3);
Rubrique rub_Nom_1 = new Rubrique();
rub_Nom_1.setNom("Nom");
rub_Nom_1.setAlias("Nom");
rub_Nom_1.setNomFichier("Programme");
rub_Nom_1.setAliasFichier("Programme");
varGroupeBy.ajouterElement(rub_Nom_1);
Rubrique rub_Difficulte_1 = new Rubrique();
rub_Difficulte_1.setNom("Difficulte");
rub_Difficulte_1.setAlias("Difficulte");
rub_Difficulte_1.setNomFichier("Programme");
rub_Difficulte_1.setAliasFichier("Programme");
varGroupeBy.ajouterElement(rub_Difficulte_1);
Rubrique rub_temps_1 = new Rubrique();
rub_temps_1.setNom("temps");
rub_temps_1.setAlias("temps");
rub_temps_1.setNomFichier("contenir");
rub_temps_1.setAliasFichier("contenir");
varGroupeBy.ajouterElement(rub_temps_1);
Rubrique rub_nb_rep_1 = new Rubrique();
rub_nb_rep_1.setNom("nb_rep");
rub_nb_rep_1.setAlias("nb_rep");
rub_nb_rep_1.setNomFichier("contenir");
rub_nb_rep_1.setAliasFichier("contenir");
varGroupeBy.ajouterElement(rub_nb_rep_1);
varReqSelect.ajouterClause(varGroupeBy);
Limit varLimit = new Limit();
varLimit.setType(0);
varLimit.setNbEnregs(0);
varLimit.setOffset(0);
varReqSelect.ajouterClause(varLimit);
return varReqSelect;
}
public String getNomFichier(int nIndex)
{
switch(nIndex)
{
case 0 : return "Programme";
case 1 : return "contenir";
default: return null;
}
}
public String getAliasFichier(int nIndex)
{
switch(nIndex)
{
case 0 : return "Programme";
case 1 : return "contenir";
default: return null;
}
}


}
