/**
 * Code généré par WINDEV Mobile - NE PAS MODIFIER !
 * Objet WINDEV Mobile : Fenêtre
 * Classe Android : FEN_Accueil
 * Date : 02/04/2021 14:52:47
 * Version de wdjava.dll  : 25.0.221.6
 */


package com.logicorp.home_biking.wdgen;


import com.logicorp.home_biking.*;
import fr.pcsoft.wdjava.core.types.*;
import fr.pcsoft.wdjava.core.*;
import fr.pcsoft.wdjava.ui.champs.fenetre.*;
import fr.pcsoft.wdjava.ui.champs.image.*;
import fr.pcsoft.wdjava.ui.cadre.*;
import fr.pcsoft.wdjava.api.*;
import fr.pcsoft.wdjava.ui.champs.zr.*;
import fr.pcsoft.wdjava.ui.champs.saisie.*;
import fr.pcsoft.wdjava.ui.champs.libelle.*;
import fr.pcsoft.wdjava.core.parcours.*;
import fr.pcsoft.wdjava.core.parcours.champ.*;
import fr.pcsoft.wdjava.ui.champs.bouton.*;
import fr.pcsoft.wdjava.core.application.*;
import fr.pcsoft.wdjava.ui.activite.*;
/*Imports trouvés dans le code WL*/
/*Fin Imports trouvés dans le code WL*/



public class GWDFFEN_Accueil extends WDFenetre
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs de FEN_Accueil
////////////////////////////////////////////////////////////////////////////

/**
 * IMG_button_preenregistre
 */
class GWDIMG_button_preenregistre extends WDChampImage
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FEN_Accueil.IMG_button_preenregistre
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3440252143947968775l);

super.setChecksum("831109728");

super.setNom("IMG_button_preenregistre");

super.setType(30001);

super.setBulle("");

super.setLibelle("Programmes Préenregistrés");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setNavigable(false);

super.setEtatInitial(0);

super.setPositionInitiale(59, 337);

super.setTailleInitiale(201, 26);

super.setValeurInitiale("C:\\Users\\axelm\\Desktop\\COURS IUT INFO\\Semestre 3\\M3302 - Projet tutore - Mise en situation professionnelle\\ptut-home-trainer\\Home_Biking\\Cadre Bouton accueil.png");

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(1);

super.setAncrageInitial(5, 1000, 1000, 500, 500, 0);

super.setTransparence(1);

super.setParamImage(2097158, 0, true, 100);

super.setSymetrie(0);

super.setZoneClicage(true);

super.setPCodeMultitouch(false);

super.setChargementEnTacheDeFond(false);

super.setOrientationExif(false);

super.setParamAnimation(1, 1, false, 300, true, false);

super.setAnimationInitiale(false);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -7.000000, 0), -1, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(2, 0x222222, 0x0, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

/**
 * Traitement: Clic sur IMG_button_preenregistre
 */
public void clicSurBoutonGauche()
{
super.clicSurBoutonGauche();

// OuvreFille(FEN_Programm_Preregister3)
//MAP:2fbe3b1901cb7107:00000012:1:FEN_Accueil.IMG_button_preenregistre:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil$GWDIMG_button_preenregistre:Clic sur IMG_button_preenregistre
// OuvreFille(FEN_Programm_Preregister3)
//MAP:2fbe3b1901cb7107:00000012:1:FEN_Accueil.IMG_button_preenregistre:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil$GWDIMG_button_preenregistre:Clic sur IMG_button_preenregistre
WDAPIFenetre.ouvreFille(GWDPHome_Biking.getInstance().mWD_FEN_Programm_Preregister3);

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurClic();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDIMG_button_preenregistre mWD_IMG_button_preenregistre;

/**
 * IMG_Bouton_UserProfile
 */
class GWDIMG_Bouton_UserProfile extends WDChampImage
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°2 de FEN_Accueil.IMG_Bouton_UserProfile
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3440252143948230934l);

super.setChecksum("831371887");

super.setNom("IMG_Bouton_UserProfile");

super.setType(30001);

super.setBulle("");

super.setLibelle("");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setNavigable(false);

super.setEtatInitial(0);

super.setPositionInitiale(191, 8);

super.setTailleInitiale(52, 52);

super.setValeurInitiale("C:\\Users\\axelm\\Desktop\\COURS IUT INFO\\Semestre 3\\M3302 - Projet tutore - Mise en situation professionnelle\\ptut-home-trainer\\Home_Biking\\Bouton Profile.png");

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(2);

super.setAncrageInitial(4, 1000, 1000, 1000, 1000, 0);

super.setTransparence(1);

super.setParamImage(2097158, 0, true, 100);

super.setSymetrie(0);

super.setZoneClicage(true);

super.setPCodeMultitouch(false);

super.setChargementEnTacheDeFond(false);

super.setOrientationExif(false);

super.setParamAnimation(1, 1, false, 300, true, false);

super.setAnimationInitiale(false);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -1, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

/**
 * Traitement: Clic sur IMG_Bouton_UserProfile
 */
public void clicSurBoutonGauche()
{
super.clicSurBoutonGauche();

// OuvreFille(FEN_User_Profile, Paramètre 1, Paramètre N)
//MAP:2fbe3b1901cf7116:00000012:1:FEN_Accueil.IMG_Bouton_UserProfile:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil$GWDIMG_Bouton_UserProfile:Clic sur IMG_Bouton_UserProfile
// OuvreFille(FEN_User_Profile, Paramètre 1, Paramètre N)
//MAP:2fbe3b1901cf7116:00000012:1:FEN_Accueil.IMG_Bouton_UserProfile:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil$GWDIMG_Bouton_UserProfile:Clic sur IMG_Bouton_UserProfile
WDAPIFenetre.ouvreFille(GWDPHome_Biking.getInstance().mWD_FEN_User_Profile);

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurClic();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDIMG_Bouton_UserProfile mWD_IMG_Bouton_UserProfile;

/**
 * IMG_Button_Settings
 */
class GWDIMG_Button_Settings extends WDChampImage
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°3 de FEN_Accueil.IMG_Button_Settings
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3440252143948493078l);

super.setChecksum("831634031");

super.setNom("IMG_Button_Settings");

super.setType(30001);

super.setBulle("");

super.setLibelle("");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setNavigable(false);

super.setEtatInitial(0);

super.setPositionInitiale(260, 8);

super.setTailleInitiale(52, 52);

super.setValeurInitiale("C:\\Users\\axelm\\Desktop\\COURS IUT INFO\\Semestre 3\\M3302 - Projet tutore - Mise en situation professionnelle\\ptut-home-trainer\\Home_Biking\\Bouton Settings.png");

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(3);

super.setAncrageInitial(4, 1000, 1000, 1000, 1000, 0);

super.setTransparence(1);

super.setParamImage(2097158, 0, true, 100);

super.setSymetrie(0);

super.setZoneClicage(true);

super.setPCodeMultitouch(false);

super.setChargementEnTacheDeFond(false);

super.setOrientationExif(false);

super.setParamAnimation(1, 1, false, 300, true, false);

super.setAnimationInitiale(false);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -1, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

/**
 * Traitement: Clic sur IMG_Button_Settings
 */
public void clicSurBoutonGauche()
{
super.clicSurBoutonGauche();

// ToastAffiche("Dans une version future", Durée d'affichage, Cadrage Vertical, Cadrage Horizontal, Couleur de fond)
//MAP:2fbe3b1901d37116:00000012:1:FEN_Accueil.IMG_Button_Settings:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil$GWDIMG_Button_Settings:Clic sur IMG_Button_Settings
// ToastAffiche("Dans une version future", Durée d'affichage, Cadrage Vertical, Cadrage Horizontal, Couleur de fond)
//MAP:2fbe3b1901d37116:00000012:1:FEN_Accueil.IMG_Button_Settings:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil$GWDIMG_Button_Settings:Clic sur IMG_Button_Settings
WDAPIToast.toastAffiche("Dans une version future");

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurClic();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDIMG_Button_Settings mWD_IMG_Button_Settings;

/**
 * IMG_button_perso
 */
class GWDIMG_button_perso extends WDChampImage
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°4 de FEN_Accueil.IMG_button_perso
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3444438684778825414l);

super.setChecksum("816328642");

super.setNom("IMG_button_perso");

super.setType(30001);

super.setBulle("");

super.setLibelle("Programmes Préenregistrés");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setNavigable(false);

super.setEtatInitial(0);

super.setPositionInitiale(60, 383);

super.setTailleInitiale(200, 26);

super.setValeurInitiale("C:\\Users\\axelm\\Desktop\\COURS IUT INFO\\Semestre 3\\M3302 - Projet tutore - Mise en situation professionnelle\\ptut-home-trainer\\Home_Biking\\Cadre Bouton accueil.png");

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(4);

super.setAncrageInitial(5, 1000, 1000, 500, 500, 0);

super.setTransparence(1);

super.setParamImage(2097158, 0, true, 100);

super.setSymetrie(0);

super.setZoneClicage(true);

super.setPCodeMultitouch(false);

super.setChargementEnTacheDeFond(false);

super.setOrientationExif(false);

super.setParamAnimation(1, 1, false, 300, true, false);

super.setAnimationInitiale(false);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -1, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(2, 0x222222, 0x0, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

/**
 * Traitement: Clic sur IMG_button_perso
 */
public void clicSurBoutonGauche()
{
super.clicSurBoutonGauche();

// ToastAffiche("Dans une version future")
//MAP:2fcd1abc00db06c6:00000012:1:FEN_Accueil.IMG_button_perso:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil$GWDIMG_button_perso:Clic sur IMG_button_perso
// ToastAffiche("Dans une version future")
//MAP:2fcd1abc00db06c6:00000012:1:FEN_Accueil.IMG_button_perso:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil$GWDIMG_button_perso:Clic sur IMG_button_perso
WDAPIToast.toastAffiche("Dans une version future");

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurClic();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDIMG_button_perso mWD_IMG_button_perso;

/**
 * IMG_button_exo
 */
class GWDIMG_button_exo extends WDChampImage
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°5 de FEN_Accueil.IMG_button_exo
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3444438706253929163l);

super.setChecksum("816595916");

super.setNom("IMG_button_exo");

super.setType(30001);

super.setBulle("");

super.setLibelle("Programmes Préenregistrés");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setNavigable(false);

super.setEtatInitial(0);

super.setPositionInitiale(60, 433);

super.setTailleInitiale(200, 26);

super.setValeurInitiale("C:\\Users\\axelm\\Desktop\\COURS IUT INFO\\Semestre 3\\M3302 - Projet tutore - Mise en situation professionnelle\\ptut-home-trainer\\Home_Biking\\Cadre Bouton accueil.png");

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(5);

super.setAncrageInitial(5, 1000, 1000, 500, 500, 0);

super.setTransparence(1);

super.setParamImage(2097158, 0, true, 100);

super.setSymetrie(0);

super.setZoneClicage(true);

super.setPCodeMultitouch(false);

super.setChargementEnTacheDeFond(false);

super.setOrientationExif(false);

super.setParamAnimation(1, 1, false, 300, true, false);

super.setAnimationInitiale(false);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -1, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(2, 0x222222, 0x0, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDIMG_button_exo mWD_IMG_button_exo;

/**
 * ZR_Programme_Recent
 */
class GWDZR_Programme_Recent extends WDZoneRepeteeFichierEnMemoire
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°6 de FEN_Accueil.ZR_Programme_Recent
////////////////////////////////////////////////////////////////////////////

/**
 * Difficulte
 */
class GWDDifficulte extends WDChampSaisieSimple
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FEN_Accueil.ZR_Programme_Recent.Difficulte
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setRectLibelle(0,2,109,40);
super.setRectCompPrincipal(109,2,4,40);
super.setQuid(3444440471520567348l);

super.setChecksum("851673120");

super.setNom("Difficulte");

super.setType(20001);

super.setBulle("");

super.setLibelle("Difficulte");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setTaille(15);

super.setNavigable(true);

super.setEtatInitial(1);

super.setPositionInitiale(376, 53);

super.setTailleInitiale(113, 44);

super.setValeurInitiale("");

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setMotDePasse(false);

super.setLiaisonFichier("fen_accueil_1$requete", "difficulte");

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(false);

super.setAltitude(1);

super.setAncrageInitial(8, 1000, 1000, 1000, 1000, 0);

super.setEllipse(0);

super.setIndication("");

super.setNumTab(1);

super.setModeAscenseur(2, 2);

super.setEffacementAutomatique(true);

super.setFinSaisieAutomatique(false);

super.setLettreAppel(65535);

super.setSelectionEnAffichage(true);

super.setPersistant(false);

super.setClavierEnSaisie(true);

super.setMasqueAffichage(new WDChaineU(""));

super.setParamBtnActionClavier(0, "");

super.setRetraitGauche(2);

super.setMiseABlancSiZero(true);

super.setVerifieOrthographe(true);

super.setTauxParallaxe(0, 0);

super.setBoutonSuppression(0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -2, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN("C:\\Users\\axelm\\Desktop\\COURS IUT INFO\\Semestre 3\\M3302 - Projet tutore - Mise en situation professionnelle\\ptut-home-trainer\\Home_Biking\\Gabarits\\WM\\210 Material Design Orange\\Material Design Orange_Edt@dpi1x.png?E5_3NP_8_8_8_8", new int[] {1,4,1,2,2,2,1,4,1}, new int[] {8, 8, 8, 8}, 0xFFFFFFFF, 1, 5));

super.setStyleSaisie(0x222222, creerPolice_GEN("Roboto", -8.000000, 0));

super.setStyleTexteIndication(0x8E8E8F, creerPolice_GEN("Roboto", -8.000000, 0), 0);

super.setStyleJeton(WDCadreFactory.creerCadre_GEN(2, 0x98FF, 0x187F, 0x98FF, 4.000000, 4.000000, 1, 1), 0xFFFFFF, "", 1);

activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
public int getModeSaisieAssistee()
{
return 1;
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDDifficulte mWD_Difficulte = new GWDDifficulte();

/**
 * Id_Programme
 */
class GWDId_Programme extends WDChampSaisieSimple
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°2 de FEN_Accueil.ZR_Programme_Recent.Id_Programme
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setRectLibelle(0,2,109,40);
super.setRectCompPrincipal(109,2,4,40);
super.setQuid(3444440471520763987l);

super.setChecksum("851916220");

super.setNom("Id_Programme");

super.setType(20004);

super.setBulle("");

super.setLibelle("Id_Programme");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setTaille(0);

super.setNavigable(true);

super.setEtatInitial(1);

super.setPositionInitiale(377, 105);

super.setTailleInitiale(113, 44);

super.setValeurInitiale("");

super.setPlan(0);

super.setCadrageHorizontal(2);

super.setMotDePasse(false);

super.setLiaisonFichier("fen_accueil_1$requete", "id_programme");

super.setTypeSaisie(1);

super.setMasqueSaisie(new WDChaineU("+9 999 999 999"));

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(false);

super.setAltitude(2);

super.setAncrageInitial(8, 1000, 1000, 1000, 1000, 0);

super.setEllipse(0);

super.setIndication("");

super.setNumTab(2);

super.setModeAscenseur(2, 2);

super.setEffacementAutomatique(true);

super.setFinSaisieAutomatique(false);

super.setLettreAppel(65535);

super.setSelectionEnAffichage(true);

super.setPersistant(false);

super.setClavierEnSaisie(true);

super.setMasqueAffichage(new WDChaineU(""));

super.setParamBtnActionClavier(0, "");

super.setRetraitGauche(2);

super.setMiseABlancSiZero(true);

super.setVerifieOrthographe(true);

super.setTauxParallaxe(0, 0);

super.setBoutonSuppression(0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -2, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN("C:\\Users\\axelm\\Desktop\\COURS IUT INFO\\Semestre 3\\M3302 - Projet tutore - Mise en situation professionnelle\\ptut-home-trainer\\Home_Biking\\Gabarits\\WM\\210 Material Design Orange\\Material Design Orange_Edt@dpi1x.png?E5_3NP_8_8_8_8", new int[] {1,4,1,2,2,2,1,4,1}, new int[] {8, 8, 8, 8}, 0xFFFFFFFF, 1, 5));

super.setStyleSaisie(0x222222, creerPolice_GEN("Roboto", -8.000000, 0));

super.setStyleTexteIndication(0x8E8E8F, creerPolice_GEN("Roboto", -8.000000, 0), 0);

super.setStyleJeton(WDCadreFactory.creerCadre_GEN(2, 0x98FF, 0x187F, 0x98FF, 4.000000, 4.000000, 1, 1), 0xFFFFFF, "", 1);

activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
public int getModeSaisieAssistee()
{
return 1;
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDId_Programme mWD_Id_Programme = new GWDId_Programme();

/**
 * LIB_NB_Exercice
 */
class GWDLIB_NB_Exercice extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°3 de FEN_Accueil.ZR_Programme_Recent.LIB_NB_Exercice
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3444441588226283241l);

super.setChecksum("865892769");

super.setNom("LIB_NB_Exercice");

super.setType(3);

super.setBulle("");

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("Libellé");

super.setNote("", "");

super.setCurseurSouris(0);

super.setEtatInitial(0);

super.setPositionInitiale(8, 60);

super.setTailleInitiale(198, 22);

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(3);

super.setAncrageInitial(8, 1000, 1000, 1000, 0, 0);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, 0xFFFFFFFF, creerPolice_GEN("Roboto", -9.000000, 0), 3, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_NB_Exercice mWD_LIB_NB_Exercice = new GWDLIB_NB_Exercice();

/**
 * LIB_Temps
 */
class GWDLIB_Temps extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°4 de FEN_Accueil.ZR_Programme_Recent.LIB_Temps
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3444441687011144186l);

super.setChecksum("866505929");

super.setNom("LIB_Temps");

super.setType(3);

super.setBulle("");

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("Libellé");

super.setNote("", "");

super.setCurseurSouris(0);

super.setEtatInitial(0);

super.setPositionInitiale(8, 126);

super.setTailleInitiale(212, 19);

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(4);

super.setAncrageInitial(8, 1000, 1000, 1000, 0, 0);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, 0xFFFFFFFF, creerPolice_GEN("Roboto", -9.000000, 0), 3, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_Temps mWD_LIB_Temps = new GWDLIB_Temps();

/**
 * IMG_Diffi
 */
class GWDIMG_Diffi extends WDChampImage
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°5 de FEN_Accueil.ZR_Programme_Recent.IMG_Diffi
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3444441755731291657l);

super.setChecksum("867178960");

super.setNom("IMG_Diffi");

super.setType(30001);

super.setBulle("");

super.setLibelle("");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setNavigable(false);

super.setEtatInitial(0);

super.setPositionInitiale(214, 60);

super.setTailleInitiale(42, 42);

super.setValeurInitiale("");

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(5);

super.setAncrageInitial(8, 1000, 1000, 1000, 1000, 0);

super.setTransparence(1);

super.setParamImage(2097158, 0, true, 100);

super.setSymetrie(0);

super.setZoneClicage(true);

super.setPCodeMultitouch(false);

super.setChargementEnTacheDeFond(false);

super.setOrientationExif(false);

super.setParamAnimation(1, 1, false, 300, true, false);

super.setAnimationInitiale(false);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -1, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDIMG_Diffi mWD_IMG_Diffi = new GWDIMG_Diffi();

/**
 * LIB_Nom
 */
class GWDLIB_Nom extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°6 de FEN_Accueil.ZR_Programme_Recent.LIB_Nom
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3444452682184245238l);

super.setChecksum("923331781");

super.setNom("LIB_Nom");

super.setType(3);

super.setBulle("");

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("Libellé");

super.setNote("", "");

super.setCurseurSouris(0);

super.setEtatInitial(0);

super.setPositionInitiale(8, 8);

super.setTailleInitiale(247, 37);

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setCadrageVertical(0);

super.setLiaisonFichier("fen_accueil_1$requete", "nom");

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(6);

super.setAncrageInitial(8, 1000, 1000, 1000, 0, 0);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, 0xFFFFFFFF, creerPolice_GEN("Roboto", -11.000000, 0), 3, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_Nom mWD_LIB_Nom = new GWDLIB_Nom();

/**
 * LIB_SansNom1
 */
class GWDLIB_SansNom1 extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°7 de FEN_Accueil.ZR_Programme_Recent.LIB_SansNom1
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3444455211940625504l);

super.setChecksum("943975292");

super.setNom("LIB_SansNom1");

super.setType(3);

super.setBulle("");

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("Libellé");

super.setNote("", "");

super.setCurseurSouris(0);

super.setEtatInitial(0);

super.setPositionInitiale(8, 50);

super.setTailleInitiale(258, 99);

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(7);

super.setAncrageInitial(8, 1000, 1000, 1000, 0, 0);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x222222, 0xFFFFFFFF, creerPolice_GEN("Roboto", -8.000000, 0), -1, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(2, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_SansNom1 mWD_LIB_SansNom1 = new GWDLIB_SansNom1();
/**
 * Initialise tous les champs de FEN_Accueil.ZR_Programme_Recent
 */
public void initialiserSousObjets()
{
////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FEN_Accueil.ZR_Programme_Recent
////////////////////////////////////////////////////////////////////////////
super.initialiserSousObjets();
mWD_Difficulte.initialiserObjet();
super.ajouterChamp("Difficulte",mWD_Difficulte);
mWD_Id_Programme.initialiserObjet();
super.ajouterChamp("Id_Programme",mWD_Id_Programme);
mWD_LIB_NB_Exercice.initialiserObjet();
super.ajouterChamp("LIB_NB_Exercice",mWD_LIB_NB_Exercice);
mWD_LIB_Temps.initialiserObjet();
super.ajouterChamp("LIB_Temps",mWD_LIB_Temps);
mWD_IMG_Diffi.initialiserObjet();
super.ajouterChamp("IMG_Diffi",mWD_IMG_Diffi);
mWD_LIB_Nom.initialiserObjet();
super.ajouterChamp("LIB_Nom",mWD_LIB_Nom);
mWD_LIB_SansNom1.initialiserObjet();
super.ajouterChamp("LIB_SansNom1",mWD_LIB_SansNom1);
creerAttributAuto();
}
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setRectCompPrincipal(1,1,274,169);
super.setQuid(3444440471518863349l);

super.setChecksum("849982125");

super.setNom("ZR_Programme_Recent");

super.setType(30);

super.setBulle("");

super.setLibelle("Zone répétée");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setNavigable(true);

super.setEtatInitial(1);

super.setPositionInitiale(22, 118);

super.setTailleInitiale(275, 171);

super.setValeurInitiale("");

super.setPlan(0);

super.setSourceRemplissage("fen_accueil_1$requete", "", "", true, "", false);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(6);

super.setAncrageInitial(8, 1000, 1000, 1000, 0, 0);

super.setNumTab(1);

super.setModeAscenseur(1, 2);

super.setModeSelection(99);

super.setSaisieEnCascade(false);

super.setLettreAppel(65535);

super.setEnregistrementSortieLigne(true);

super.setPersistant(false);

super.setParamAffichage(0, 0, 1, 274, 169);

super.setBtnEnrouleDeroule(true);

super.setScrollRapide(false, null);

super.setDeplacementParDnd(0);

super.setSwipe(0, "", false, false, "", false, false);

super.setRecyclageChamp(true);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x212121, creerPolice_GEN("Roboto", -8.000000, 0), -1, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(2, 0x654E44, 0x0, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1), 0, 0, 0, 0);

super.setStyleSeparateurVerticaux(false, 0xFFFFFFFF);

super.setStyleSeparateurHorizontaux(0, 0xFFFFFFFF);

super.setDessinerLigneVide(false);

super.setCouleurCellule(0xFFFFFFFF, 0xFFFFFFFF, 0x212121, 0x80CCFF, 0xFFFFFF);

super.setImagePlusMoins("C:\\Users\\axelm\\Desktop\\COURS IUT INFO\\Semestre 3\\M3302 - Projet tutore - Mise en situation professionnelle\\ptut-home-trainer\\Home_Biking\\Gabarits\\WM\\210 Material Design Orange\\Material Design Orange_Break_Pict@dpi1x.png?E2_4O");

activerEcoute();
initialiserSousObjets();
super.terminerInitialisation();
}

/**
 * Traitement: Fin d'initialisation de ZR_Programme_Recent
 */
public void finInit()
{
super.finInit();

// nIdProg			est un entier
//MAP:2fcd1c5c02dc5ff5:00000023:1:FEN_Accueil.ZR_Programme_Recent:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil$GWDZR_Programme_Recent:Fin d'initialisation de ZR_Programme_Recent

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables locales au traitement
// (En WLangage les variables sont encore visibles après la fin du bloc dans lequel elles sont déclarées)
////////////////////////////////////////////////////////////////////////////
WDObjet vWD_nIdProg = new WDEntier4();

WDObjet vWD_sDiff = new WDChaineU();

WDObjet vWD_nTemps = new WDEntier4();

WDObjet vWD_nExercice = new WDEntier4();

WDObjet vWD_i = new WDEntier4();



// nIdProg			est un entier
//MAP:2fcd1c5c02dc5ff5:00000023:1:FEN_Accueil.ZR_Programme_Recent:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil$GWDZR_Programme_Recent:Fin d'initialisation de ZR_Programme_Recent


// sDiff			est une chaîne
//MAP:2fcd1c5c02dc5ff5:00000023:2:FEN_Accueil.ZR_Programme_Recent:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil$GWDZR_Programme_Recent:Fin d'initialisation de ZR_Programme_Recent


// nTemps			est un entier
//MAP:2fcd1c5c02dc5ff5:00000023:3:FEN_Accueil.ZR_Programme_Recent:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil$GWDZR_Programme_Recent:Fin d'initialisation de ZR_Programme_Recent


// nExercice		est un entier
//MAP:2fcd1c5c02dc5ff5:00000023:4:FEN_Accueil.ZR_Programme_Recent:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil$GWDZR_Programme_Recent:Fin d'initialisation de ZR_Programme_Recent


// i				est un entier
//MAP:2fcd1c5c02dc5ff5:00000023:5:FEN_Accueil.ZR_Programme_Recent:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil$GWDZR_Programme_Recent:Fin d'initialisation de ZR_Programme_Recent


// i=1
//MAP:2fcd1c5c02dc5ff5:00000023:7:FEN_Accueil.ZR_Programme_Recent:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil$GWDZR_Programme_Recent:Fin d'initialisation de ZR_Programme_Recent
vWD_i.setValeur(1);

// POUR CHAQUE LIGNE DE ZR_Programme_Recent
//MAP:2fcd1c5c02dc5ff5:00000023:8:FEN_Accueil.ZR_Programme_Recent:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil$GWDZR_Programme_Recent:Fin d'initialisation de ZR_Programme_Recent
IWDParcours parcours1 = null;
try
{
parcours1 = WDParcoursChamp.pourTout(this, 0x2);
while(parcours1.testParcours())
{
// 	nIdProg		= ZR_Programme_Recent[i].Id_Programme..Valeur //récupération de l'idProgramme de la ligne d'indice i de la zone répétée
//MAP:2fcd1c5c02dc5ff5:00000023:9:FEN_Accueil.ZR_Programme_Recent:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil$GWDZR_Programme_Recent:Fin d'initialisation de ZR_Programme_Recent
vWD_nIdProg.setValeur(this.get(vWD_i).get("Id_Programme").getProp(EWDPropriete.PROP_VALEUR));

// 	sDiff		= ZR_Programme_Recent[i].Difficulte..Valeur //récupération de la difficulté du programme de la ligne d'indice i de la zone répétée
//MAP:2fcd1c5c02dc5ff5:00000023:a:FEN_Accueil.ZR_Programme_Recent:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil$GWDZR_Programme_Recent:Fin d'initialisation de ZR_Programme_Recent
vWD_sDiff.setValeur(this.get(vWD_i).get("Difficulte").getProp(EWDPropriete.PROP_VALEUR));

// 	nTemps		=0
//MAP:2fcd1c5c02dc5ff5:00000023:b:FEN_Accueil.ZR_Programme_Recent:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil$GWDZR_Programme_Recent:Fin d'initialisation de ZR_Programme_Recent
vWD_nTemps.setValeur(0);

// 	nExercice	=0
//MAP:2fcd1c5c02dc5ff5:00000023:c:FEN_Accueil.ZR_Programme_Recent:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil$GWDZR_Programme_Recent:Fin d'initialisation de ZR_Programme_Recent
vWD_nExercice.setValeur(0);

// 	HLitRecherchePremier(contenir,Id_Programme,nIdProg)
//MAP:2fcd1c5c02dc5ff5:00000023:f:FEN_Accueil.ZR_Programme_Recent:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil$GWDZR_Programme_Recent:Fin d'initialisation de ZR_Programme_Recent
WDAPIHF.hLitRecherchePremier(WDAPIHF.getFichierSansCasseNiAccent("contenir"),WDAPIHF.getRubriqueSansCasseNiAccent("id_programme"),vWD_nIdProg);

// 	BOUCLE
//MAP:2fcd1c5c02dc5ff5:00000023:10:FEN_Accueil.ZR_Programme_Recent:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil$GWDZR_Programme_Recent:Fin d'initialisation de ZR_Programme_Recent
{
do
{
// 		nExercice++ //Ajoute un au nombre d'exercices
//MAP:2fcd1c5c02dc5ff5:00000023:11:FEN_Accueil.ZR_Programme_Recent:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil$GWDZR_Programme_Recent:Fin d'initialisation de ZR_Programme_Recent
vWD_nExercice.opInc();

// 		nTemps+=((contenir.temps+contenir.repos)*contenir.nb_rep) //Ajoute le temps nécessaire à l'exécution d'un exercice complet pour le programme
//MAP:2fcd1c5c02dc5ff5:00000023:12:FEN_Accueil.ZR_Programme_Recent:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil$GWDZR_Programme_Recent:Fin d'initialisation de ZR_Programme_Recent
vWD_nTemps.setValeur(vWD_nTemps.opPlus(WDAPIHF.getFichierSansCasseNiAccent("contenir").getRubriqueSansCasseNiAccent("temps").opPlus(WDAPIHF.getFichierSansCasseNiAccent("contenir").getRubriqueSansCasseNiAccent("repos")).opMult(WDAPIHF.getFichierSansCasseNiAccent("contenir").getRubriqueSansCasseNiAccent("nb_rep"))));

// 		HLitSuivant(contenir) //Passe à l'exercice suivant
//MAP:2fcd1c5c02dc5ff5:00000023:13:FEN_Accueil.ZR_Programme_Recent:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil$GWDZR_Programme_Recent:Fin d'initialisation de ZR_Programme_Recent
WDAPIHF.hLitSuivant(WDAPIHF.getFichierSansCasseNiAccent("contenir"));

}
while(WDAPIHF.hTrouve(WDAPIHF.getFichierSansCasseNiAccent("contenir")).opEgal(true));
}

// 	ZR_Programme_Recent[i].LIB_NB_Exercice			="NB Exercices : " + nExercice //Affectation au champs LIB_NB_Exercices le nombre d'exercices
//MAP:2fcd1c5c02dc5ff5:00000023:17:FEN_Accueil.ZR_Programme_Recent:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil$GWDZR_Programme_Recent:Fin d'initialisation de ZR_Programme_Recent
this.get(vWD_i).get("LIB_NB_Exercice").setValeur(new WDChaineU("NB Exercices : ").opPlus(vWD_nExercice));

// 	ZR_Programme_Recent[i].LIB_Temps					="Temps : " + PartieEntière(nTemps/60)+"min" //Affectation du temps en minutes à partir du temps total récupéré en secondes
//MAP:2fcd1c5c02dc5ff5:00000023:18:FEN_Accueil.ZR_Programme_Recent:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil$GWDZR_Programme_Recent:Fin d'initialisation de ZR_Programme_Recent
this.get(vWD_i).get("LIB_Temps").setValeur(new WDChaineU("Temps : ").opPlus(WDAPIMath.partieEntiere(vWD_nTemps.opDiv(60))).opPlus("min"));

// 	ZR_Programme_Recent[i].IMG_Diffi..CouleurFond		=CouleurDifficulte(sDiff) //Transforme la difficulté en une couleur grâce à la fonction CouleurDifficulte
//MAP:2fcd1c5c02dc5ff5:00000023:19:FEN_Accueil.ZR_Programme_Recent:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil$GWDZR_Programme_Recent:Fin d'initialisation de ZR_Programme_Recent
this.get(vWD_i).get("IMG_Diffi").setProp(EWDPropriete.PROP_COULEURFOND,GWDCPCouleur_Difficulte.fWD_couleurDifficulte(vWD_sDiff));

// 	i++
//MAP:2fcd1c5c02dc5ff5:00000023:1a:FEN_Accueil.ZR_Programme_Recent:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil$GWDZR_Programme_Recent:Fin d'initialisation de ZR_Programme_Recent
vWD_i.opInc();

}

}
finally
{
if(parcours1 != null)
{
parcours1.finParcours();
}
}


}




/**
 * Traitement: Sélection d'une ligne de ZR_Programme_Recent
 */
public void selectionLigne()
{
super.selectionLigne();

// OuvreFille(FEN_Preseance,Id_Programme)
//MAP:2fcd1c5c02dc5ff5:00000021:1:FEN_Accueil.ZR_Programme_Recent:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil$GWDZR_Programme_Recent:Sélection d'une ligne de ZR_Programme_Recent
// OuvreFille(FEN_Preseance,Id_Programme)
//MAP:2fcd1c5c02dc5ff5:00000021:1:FEN_Accueil.ZR_Programme_Recent:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil$GWDZR_Programme_Recent:Sélection d'une ligne de ZR_Programme_Recent
WDAPIFenetre.ouvreFille(GWDPHome_Biking.getInstance().mWD_FEN_Preseance,new WDObjet[] {mWD_Id_Programme} );

// TableAffiche(FEN_Preseance.TABLE_FEN_Preseance)
//MAP:2fcd1c5c02dc5ff5:00000021:2:FEN_Accueil.ZR_Programme_Recent:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil$GWDZR_Programme_Recent:Sélection d'une ligne de ZR_Programme_Recent
WDAPITable.tableAffiche(GWDPHome_Biking.getInstance().getFEN_Preseance().mWD_TABLE_FEN_Preseance);

// ExécuteTraitement(FEN_Preseance,trtModification)
//MAP:2fcd1c5c02dc5ff5:00000021:3:FEN_Accueil.ZR_Programme_Recent:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil$GWDZR_Programme_Recent:Sélection d'une ligne de ZR_Programme_Recent
WDAPIVM.executeTraitement(GWDPHome_Biking.getInstance().mWD_FEN_Preseance,17);

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurSelection();
}

////////////////////////////////////////////////////////////////////////////
protected boolean isHorizontale()
{
return true;
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDZR_Programme_Recent mWD_ZR_Programme_Recent;

/**
 * LIB_Programme_Recent
 */
class GWDLIB_Programme_Recent extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°7 de FEN_Accueil.LIB_Programme_Recent
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3444448653481898177l);

super.setChecksum("900307430");

super.setNom("LIB_Programme_Recent");

super.setType(3);

super.setBulle("");

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("Programme Recent");

super.setNote("", "");

super.setCurseurSouris(0);

super.setEtatInitial(0);

super.setPositionInitiale(14, 86);

super.setTailleInitiale(197, 26);

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(7);

super.setAncrageInitial(8, 1000, 1000, 1000, 0, 0);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, 0xFFFFFFFF, creerPolice_GEN("Roboto", -11.000000, 0), 3, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_Programme_Recent mWD_LIB_Programme_Recent;

/**
 * LIB_ProgPré
 */
class GWDLIB_ProgPre extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°8 de FEN_Accueil.LIB_ProgPré
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3446613012100830007l);

super.setChecksum("945237205");

super.setNom("LIB_ProgPré");

super.setType(3);

super.setBulle("");

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("Programmes Préenregistrés");

super.setNote("", "");

super.setCurseurSouris(0);

super.setEtatInitial(0);

super.setPositionInitiale(59, 337);

super.setTailleInitiale(201, 26);

super.setPlan(0);

super.setCadrageHorizontal(1);

super.setCadrageVertical(1);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(8);

super.setAncrageInitial(5, 1000, 1000, 500, 500, 0);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, 0xFFFFFFFF, creerPolice_GEN("Roboto", -8.000000, 0), 3, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0x222222, 0x0, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_ProgPre mWD_LIB_ProgPre;

/**
 * LIB_ProgPré1
 */
class GWDLIB_ProgPre1 extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°9 de FEN_Accueil.LIB_ProgPré1
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3446613673531779702l);

super.setChecksum("951223470");

super.setNom("LIB_ProgPré1");

super.setType(3);

super.setBulle("");

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("Programmes Personnalisés");

super.setNote("", "");

super.setCurseurSouris(0);

super.setEtatInitial(0);

super.setPositionInitiale(60, 383);

super.setTailleInitiale(200, 26);

super.setPlan(0);

super.setCadrageHorizontal(1);

super.setCadrageVertical(1);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(9);

super.setAncrageInitial(5, 1000, 1000, 500, 500, 0);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, 0xFFFFFFFF, creerPolice_GEN("Roboto", -8.000000, 0), 3, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0x222222, 0x0, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_ProgPre1 mWD_LIB_ProgPre1;

/**
 * LIB_ProgPré2
 */
class GWDLIB_ProgPre2 extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°10 de FEN_Accueil.LIB_ProgPré2
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3446613733662450254l);

super.setChecksum("952351892");

super.setNom("LIB_ProgPré2");

super.setType(3);

super.setBulle("");

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("Catalogue Exercices");

super.setNote("", "");

super.setCurseurSouris(0);

super.setEtatInitial(0);

super.setPositionInitiale(60, 433);

super.setTailleInitiale(200, 25);

super.setPlan(0);

super.setCadrageHorizontal(1);

super.setCadrageVertical(1);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(10);

super.setAncrageInitial(5, 1000, 1000, 500, 500, 0);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, 0xFFFFFFFF, creerPolice_GEN("Roboto", -8.000000, 0), 3, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0x222222, 0x0, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_ProgPre2 mWD_LIB_ProgPre2;

/**
 * BTN_SansNom1
 */
class GWDBTN_SansNom1 extends WDBouton
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°11 de FEN_Accueil.BTN_SansNom1
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3449228775919459114l);

super.setChecksum("832192918");

super.setNom("BTN_SansNom1");

super.setType(4);

super.setBulle("");

super.setLibelle("Bouton");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(-547, 138);

super.setTailleInitiale(160, 48);

super.setPlan(0);

super.setImageEtat(1);

super.setImageFondEtat(5);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(11);

super.setAncrageInitial(0, 1000, 1000, 1000, 1000, 0);

super.setNumTab(2);

super.setLettreAppel(65535);

super.setTypeBouton(3);

super.setTypeActionPredefinie(0);

super.setBoutonOnOff(false);

super.setTauxParallaxe(0, 0);

super.setLibelleVAlign(1);

super.setLibelleHAlign(5);

super.setPresenceLibelle(true);

super.setImage("", 0, 2, 1, null, null, null);

super.setStyleLibelleRepos(0xFFFFFF, creerPolice_GEN("Roboto", -8.000000, 0), 0, 0x222222);

super.setStyleLibelleSurvol(0xFFFFFF, creerPolice_GEN("Roboto", -8.000000, 0), 0, 0x222222);

super.setStyleLibelleEnfonce(0xFFFFFF, creerPolice_GEN("Roboto", -8.000000, 0), 0, 0x222222);

super.setCadreRepos(WDCadreFactory.creerCadre_GEN(2, 0x98FF, 0x187F, 0x98FF, 2.000000, 2.000000, 1, 1));

super.setCadreSurvol(WDCadreFactory.creerCadre_GEN(2, 0x98FF, 0x187F, 0x98FF, 2.000000, 2.000000, 1, 1));

super.setCadreEnfonce(WDCadreFactory.creerCadre_GEN(2, 0x98FF, 0x187F, 0x98FF, 2.000000, 2.000000, 1, 1));

super.setImageFond9Images(new int[] {1,2,1,2,2,2,1,2,1}, 10, 10, 10, 10);

super.setImageFond("C:\\Users\\axelm\\Desktop\\COURS IUT INFO\\Semestre 3\\M3302 - Projet tutore - Mise en situation professionnelle\\ptut-home-trainer\\Home_Biking\\Gabarits\\WM\\210 Material Design Orange\\Material Design Orange_Btn_Std@dpi1x.png?E5_3NP_10_10_10_10", 1, 0, 1, 1);

activerEcoute();
super.terminerInitialisation();
}

/**
 * Traitement: Clic sur BTN_SansNom1
 */
public void clicSurBoutonGauche()
{
super.clicSurBoutonGauche();

// SI gnNombreRetour=1 ALORS //Si la personne à cliqué dans les deux dernières secondes une première fois, alors ferme l'application
//MAP:2fde1f4c01bc1b2a:00000012:1:FEN_Accueil.BTN_SansNom1:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil$GWDBTN_SansNom1:Clic sur BTN_SansNom1
// SI gnNombreRetour=1 ALORS //Si la personne à cliqué dans les deux dernières secondes une première fois, alors ferme l'application
//MAP:2fde1f4c01bc1b2a:00000012:1:FEN_Accueil.BTN_SansNom1:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil$GWDBTN_SansNom1:Clic sur BTN_SansNom1
if(vWD_gnNombreRetour.opEgal(1))
{
// 	Ferme
//MAP:2fde1f4c01bc1b2a:00000012:2:FEN_Accueil.BTN_SansNom1:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil$GWDBTN_SansNom1:Clic sur BTN_SansNom1
WDAPIFenetre.ferme();

}
else
{
// 	gnNombreRetour=1 
//MAP:2fde1f4c01bc1b2a:00000012:4:FEN_Accueil.BTN_SansNom1:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil$GWDBTN_SansNom1:Clic sur BTN_SansNom1
vWD_gnNombreRetour.setValeur(1);

// 	ToastAffiche("Appuyer deux fois pour quitter")
//MAP:2fde1f4c01bc1b2a:00000012:5:FEN_Accueil.BTN_SansNom1:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil$GWDBTN_SansNom1:Clic sur BTN_SansNom1
WDAPIToast.toastAffiche("Appuyer deux fois pour quitter");

// 	Multitâche(2s)
//MAP:2fde1f4c01bc1b2a:00000012:6:FEN_Accueil.BTN_SansNom1:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil$GWDBTN_SansNom1:Clic sur BTN_SansNom1
WDAPIVM.multitache((new WDDuree("0000002000")));

// 	gnNombreRetour=0
//MAP:2fde1f4c01bc1b2a:00000012:8:FEN_Accueil.BTN_SansNom1:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil$GWDBTN_SansNom1:Clic sur BTN_SansNom1
vWD_gnNombreRetour.setValeur(0);

}

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurClic();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDBTN_SansNom1 mWD_BTN_SansNom1;

/**
 * BTN_prog_pré
 */
class GWDBTN_prog_pre extends WDBouton
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°12 de FEN_Accueil.BTN_prog_pré
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(2321050234295431981l);

super.setChecksum("557465905");

super.setNom("BTN_prog_pré");

super.setType(4);

super.setBulle("");

super.setLibelle("Programmes Préenregistrées\r\n");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(31, 334);

super.setTailleInitiale(266, 40);

super.setPlan(0);

super.setImageEtat(1);

super.setImageFondEtat(5);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(12);

super.setAncrageInitial(0, 1000, 1000, 1000, 1000, 0);

super.setNumTab(3);

super.setLettreAppel(65535);

super.setTypeBouton(0);

super.setTypeActionPredefinie(0);

super.setBoutonOnOff(false);

super.setTauxParallaxe(0, 0);

super.setLibelleVAlign(1);

super.setLibelleHAlign(1);

super.setPresenceLibelle(true);

super.setImage("", 0, 2, 1, null, null, null);

super.setStyleLibelleRepos(0xFFFFFF, creerPolice_GEN("Roboto", -9.000000, 0), 0, 0x222222);

super.setStyleLibelleSurvol(0xFFFFFF, creerPolice_GEN("Roboto", -9.000000, 0), 0, 0x222222);

super.setStyleLibelleEnfonce(0xFFFFFF, creerPolice_GEN("Roboto", -9.000000, 0), 0, 0x222222);

super.setCadreRepos(WDCadreFactory.creerCadre_GEN(25, 0x1878FB, 0x7B, 0x1878FB, 2.000000, 2.000000, 0, 1));

super.setCadreSurvol(WDCadreFactory.creerCadre_GEN(25, 0x1878FB, 0x7B, 0x1878FB, 2.000000, 2.000000, 0, 1));

super.setCadreEnfonce(WDCadreFactory.creerCadre_GEN(25, 0x1878FB, 0x7B, 0x1878FB, 2.000000, 2.000000, 0, 1));

super.setImageFond9Images(new int[] {1,2,1,2,2,2,1,2,1}, 10, 10, 10, 10);

super.setImageFond("C:\\Users\\axelm\\Desktop\\COURS IUT INFO\\Semestre 3\\M3302 - Projet tutore - Mise en situation professionnelle\\ptut-home-trainer\\Home_Biking\\Gabarits\\WM\\210 Material Design Orange\\Material Design Orange_Btn_Std@dpi1x.png?E5_3NP_10_10_10_10", 1, 0, 1, 1);

activerEcoute();
super.terminerInitialisation();
}

/**
 * Traitement: Clic sur BTN_prog_pré
 */
public void clicSurBoutonGauche()
{
super.clicSurBoutonGauche();

// OuvreFille(FEN_Programm_Preregister3)
//MAP:203606e40104332d:00000012:1:FEN_Accueil.BTN_prog_pré:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil$GWDBTN_prog_pre:Clic sur BTN_prog_pré
// OuvreFille(FEN_Programm_Preregister3)
//MAP:203606e40104332d:00000012:1:FEN_Accueil.BTN_prog_pré:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil$GWDBTN_prog_pre:Clic sur BTN_prog_pré
WDAPIFenetre.ouvreFille(GWDPHome_Biking.getInstance().mWD_FEN_Programm_Preregister3);

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurClic();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDBTN_prog_pre mWD_BTN_prog_pre;

/**
 * LIB_SansNom1
 */
class GWDLIB_SansNom1 extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°13 de FEN_Accueil.LIB_SansNom1
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(2321050745406293263l);

super.setChecksum("567218626");

super.setNom("LIB_SansNom1");

super.setType(3);

super.setBulle("");

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("");

super.setNote("", "");

super.setCurseurSouris(0);

super.setEtatInitial(0);

super.setPositionInitiale(0, 0);

super.setTailleInitiale(60, 13);

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(13);

super.setAncrageInitial(0, 1000, 1000, 1000, 1000, 0);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x212121, 0xFFFFFFFF, creerPolice_GEN("Roboto", -13.000000, 0), 3, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE0E0E0, 0x808080, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_SansNom1 mWD_LIB_SansNom1;

/**
 * BTN_prog_perso
 */
class GWDBTN_prog_perso extends WDBouton
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°14 de FEN_Accueil.BTN_prog_perso
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(2321050784061663365l);

super.setChecksum("567883529");

super.setNom("BTN_prog_perso");

super.setType(4);

super.setBulle("");

super.setLibelle("Programmes Personnalisés\r\n");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(31, 383);

super.setTailleInitiale(266, 42);

super.setPlan(0);

super.setImageEtat(1);

super.setImageFondEtat(5);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(14);

super.setAncrageInitial(0, 1000, 1000, 1000, 1000, 0);

super.setNumTab(4);

super.setLettreAppel(65535);

super.setTypeBouton(0);

super.setTypeActionPredefinie(0);

super.setBoutonOnOff(false);

super.setTauxParallaxe(0, 0);

super.setLibelleVAlign(1);

super.setLibelleHAlign(1);

super.setPresenceLibelle(true);

super.setImage("", 0, 2, 1, null, null, null);

super.setStyleLibelleRepos(0xFFFFFF, creerPolice_GEN("Roboto", -9.000000, 0), 0, 0x222222);

super.setStyleLibelleSurvol(0xFFFFFF, creerPolice_GEN("Roboto", -9.000000, 0), 0, 0x222222);

super.setStyleLibelleEnfonce(0xFFFFFF, creerPolice_GEN("Roboto", -9.000000, 0), 0, 0x222222);

super.setCadreRepos(WDCadreFactory.creerCadre_GEN(2, 0x98FF, 0x187F, 0x98FF, 2.000000, 2.000000, 1, 1));

super.setCadreSurvol(WDCadreFactory.creerCadre_GEN(2, 0x98FF, 0x187F, 0x98FF, 2.000000, 2.000000, 1, 1));

super.setCadreEnfonce(WDCadreFactory.creerCadre_GEN(2, 0x98FF, 0x187F, 0x98FF, 2.000000, 2.000000, 1, 1));

super.setImageFond9Images(new int[] {1,2,1,2,2,2,1,2,1}, 10, 10, 10, 10);

super.setImageFond("C:\\Users\\axelm\\Desktop\\COURS IUT INFO\\Semestre 3\\M3302 - Projet tutore - Mise en situation professionnelle\\ptut-home-trainer\\Home_Biking\\Gabarits\\WM\\210 Material Design Orange\\Material Design Orange_Btn_Std@dpi1x.png?E5_3NP_10_10_10_10", 1, 0, 1, 1);

activerEcoute();
super.terminerInitialisation();
}

/**
 * Traitement: Clic sur BTN_prog_perso
 */
public void clicSurBoutonGauche()
// ToastAffiche("Dans une version future")
{
super.clicSurBoutonGauche();

// //ToastAffiche("Dans une version future")
//MAP:2036076401a32885:00000012:1:FEN_Accueil.BTN_prog_perso:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil$GWDBTN_prog_perso:Clic sur BTN_prog_perso
// OuvreFille(FEN_Programm_Perso)
//MAP:2036076401a32885:00000012:2:FEN_Accueil.BTN_prog_perso:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil$GWDBTN_prog_perso:Clic sur BTN_prog_perso
WDAPIFenetre.ouvreFille(GWDPHome_Biking.getInstance().mWD_FEN_Programm_Perso);

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurClic();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDBTN_prog_perso mWD_BTN_prog_perso;

/**
 * BTN_exo
 */
class GWDBTN_exo extends WDBouton
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°15 de FEN_Accueil.BTN_exo
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(2321050822717033670l);

super.setChecksum("568548179");

super.setNom("BTN_exo");

super.setType(4);

super.setBulle("");

super.setLibelle("Catalogue Exercices\r\n");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(31, 433);

super.setTailleInitiale(266, 42);

super.setPlan(0);

super.setImageEtat(1);

super.setImageFondEtat(5);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(15);

super.setAncrageInitial(1, 0, 1000, 1000, 1000, 0);

super.setNumTab(5);

super.setLettreAppel(65535);

super.setTypeBouton(0);

super.setTypeActionPredefinie(0);

super.setBoutonOnOff(false);

super.setTauxParallaxe(0, 0);

super.setLibelleVAlign(1);

super.setLibelleHAlign(1);

super.setPresenceLibelle(true);

super.setImage("", 0, 2, 1, null, null, null);

super.setStyleLibelleRepos(0xFFFFFF, creerPolice_GEN("Roboto", -9.000000, 0), 0, 0x222222);

super.setStyleLibelleSurvol(0xFFFFFF, creerPolice_GEN("Roboto", -9.000000, 0), 0, 0x222222);

super.setStyleLibelleEnfonce(0xFFFFFF, creerPolice_GEN("Roboto", -9.000000, 0), 0, 0x222222);

super.setCadreRepos(WDCadreFactory.creerCadre_GEN(2, 0x98FF, 0x187F, 0x98FF, 2.000000, 2.000000, 1, 1));

super.setCadreSurvol(WDCadreFactory.creerCadre_GEN(2, 0x98FF, 0x187F, 0x98FF, 2.000000, 2.000000, 1, 1));

super.setCadreEnfonce(WDCadreFactory.creerCadre_GEN(2, 0x98FF, 0x187F, 0x98FF, 2.000000, 2.000000, 1, 1));

super.setImageFond9Images(new int[] {1,2,1,2,2,2,1,2,1}, 10, 10, 10, 10);

super.setImageFond("C:\\Users\\axelm\\Desktop\\COURS IUT INFO\\Semestre 3\\M3302 - Projet tutore - Mise en situation professionnelle\\ptut-home-trainer\\Home_Biking\\Gabarits\\WM\\210 Material Design Orange\\Material Design Orange_Btn_Std@dpi1x.png?E5_3NP_10_10_10_10", 1, 0, 1, 1);

activerEcoute();
super.terminerInitialisation();
}

/**
 * Traitement: Clic sur BTN_exo
 */
public void clicSurBoutonGauche()
{
super.clicSurBoutonGauche();

// OuvreFille(FEN_Exercices,Faux)
//MAP:2036076d01ad4cc6:00000012:1:FEN_Accueil.BTN_exo:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil$GWDBTN_exo:Clic sur BTN_exo
// OuvreFille(FEN_Exercices,Faux)
//MAP:2036076d01ad4cc6:00000012:1:FEN_Accueil.BTN_exo:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil$GWDBTN_exo:Clic sur BTN_exo
WDAPIFenetre.ouvreFille(GWDPHome_Biking.getInstance().mWD_FEN_Exercices,new WDObjet[] {new WDBooleen(false)} );

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurClic();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDBTN_exo mWD_BTN_exo;

/**
 * Traitement: Déclarations globales de FEN_Accueil
 */
public void declarerGlobale(WDObjet[] WD_tabParam)
{
// PROCEDURE MaFenêtre()
//MAP:2fbe3ae80187af7c:00000000:1:FEN_Accueil:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil:Déclarations globales de FEN_Accueil
super.declarerGlobale(WD_tabParam, 0, 0);
int WD_ntabParamLen = 0;
if(WD_tabParam!=null) WD_ntabParamLen = WD_tabParam.length;


// gnNombreRetour est un entier
//MAP:2fbe3ae80187af7c:00000000:3:FEN_Accueil:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil:Déclarations globales de FEN_Accueil
vWD_gnNombreRetour = new WDEntier4();

super.ajouterVariableGlobale("gnNombreRetour",vWD_gnNombreRetour);



// gnNombreRetour=0
//MAP:2fbe3ae80187af7c:00000000:4:FEN_Accueil:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil:Déclarations globales de FEN_Accueil
vWD_gnNombreRetour.setValeur(0);

}




/**
 * Traitement: Fermeture de FEN_Accueil
 */
public void fermetureFenetre()
{
super.fermetureFenetre();

// Ferme(FEN_Splash_Screen)
//MAP:2fbe3ae80187af7c:00000002:1:FEN_Accueil:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil:Fermeture de FEN_Accueil
// Ferme(FEN_Splash_Screen)
//MAP:2fbe3ae80187af7c:00000002:1:FEN_Accueil:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil:Fermeture de FEN_Accueil
WDAPIFenetre.ferme(GWDPHome_Biking.getInstance().mWD_FEN_Splash_Screen);

}




/**
 * Traitement: Fermeture d'une fenêtre fille de FEN_Accueil
 */
public void fermetureFenetreFille()
{
super.fermetureFenetreFille();

// ZoneRépétéeAffiche(ZR_Programme_Recent,taInit)
//MAP:2fbe3ae80187af7c:000000b1:1:FEN_Accueil:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil:Fermeture d'une fenêtre fille de FEN_Accueil
// ZoneRépétéeAffiche(ZR_Programme_Recent,taInit)
//MAP:2fbe3ae80187af7c:000000b1:1:FEN_Accueil:com.logicorp.home_biking.wdgen.GWDFFEN_Accueil:Fermeture d'une fenêtre fille de FEN_Accueil
WDAPIZoneRepetee.zoneRepeteeAffiche(mWD_ZR_Programme_Recent,new WDChaineU("Reset"));

}




// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
 public WDObjet vWD_gnNombreRetour = WDVarNonAllouee.ref;
////////////////////////////////////////////////////////////////////////////
// Création des champs de la fenêtre FEN_Accueil
////////////////////////////////////////////////////////////////////////////
protected void creerChamps()
{
mWD_IMG_button_preenregistre = new GWDIMG_button_preenregistre();
mWD_IMG_Bouton_UserProfile = new GWDIMG_Bouton_UserProfile();
mWD_IMG_Button_Settings = new GWDIMG_Button_Settings();
mWD_IMG_button_perso = new GWDIMG_button_perso();
mWD_IMG_button_exo = new GWDIMG_button_exo();
mWD_ZR_Programme_Recent = new GWDZR_Programme_Recent();
mWD_LIB_Programme_Recent = new GWDLIB_Programme_Recent();
mWD_LIB_ProgPre = new GWDLIB_ProgPre();
mWD_LIB_ProgPre1 = new GWDLIB_ProgPre1();
mWD_LIB_ProgPre2 = new GWDLIB_ProgPre2();
mWD_BTN_SansNom1 = new GWDBTN_SansNom1();
mWD_BTN_prog_pre = new GWDBTN_prog_pre();
mWD_LIB_SansNom1 = new GWDLIB_SansNom1();
mWD_BTN_prog_perso = new GWDBTN_prog_perso();
mWD_BTN_exo = new GWDBTN_exo();

}
////////////////////////////////////////////////////////////////////////////
// Initialisation de la fenêtre FEN_Accueil
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.setQuid(3440251933490130812l);

super.setChecksum("832415581");

super.setNom("FEN_Accueil");

super.setType(1);

super.setBulle("");

super.setMenuContextuelSysteme();

super.setCurseurSouris(0);

super.setNote("", "");

super.setCouleur(0x0);

super.setCouleurFond(0xFFFFFF);

super.setPositionInitiale(0, 0);

super.setTailleInitiale(320, 543);

super.setTitre("Accueil");

super.setTailleMin(-1, -1);

super.setTailleMax(20000, 20000);

super.setVisibleInitial(true);

super.setPositionFenetre(2);

super.setPersistant(true);

super.setGFI(true);

super.setAnimationFenetre(0);

super.setImageFond("", 1, 0, 1);

super.setCouleurTexteAutomatique(0x303030);

super.setCouleurBarreSysteme(0x80FF);


activerEcoute();

////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FEN_Accueil
////////////////////////////////////////////////////////////////////////////
mWD_IMG_button_preenregistre.initialiserObjet();
super.ajouter("IMG_button_preenregistre", mWD_IMG_button_preenregistre);
mWD_IMG_Bouton_UserProfile.initialiserObjet();
super.ajouter("IMG_Bouton_UserProfile", mWD_IMG_Bouton_UserProfile);
mWD_IMG_Button_Settings.initialiserObjet();
super.ajouter("IMG_Button_Settings", mWD_IMG_Button_Settings);
mWD_IMG_button_perso.initialiserObjet();
super.ajouter("IMG_button_perso", mWD_IMG_button_perso);
mWD_IMG_button_exo.initialiserObjet();
super.ajouter("IMG_button_exo", mWD_IMG_button_exo);
mWD_ZR_Programme_Recent.initialiserObjet();
super.ajouter("ZR_Programme_Recent", mWD_ZR_Programme_Recent);
mWD_LIB_Programme_Recent.initialiserObjet();
super.ajouter("LIB_Programme_Recent", mWD_LIB_Programme_Recent);
mWD_LIB_ProgPre.initialiserObjet();
super.ajouter("LIB_ProgPré", mWD_LIB_ProgPre);
mWD_LIB_ProgPre1.initialiserObjet();
super.ajouter("LIB_ProgPré1", mWD_LIB_ProgPre1);
mWD_LIB_ProgPre2.initialiserObjet();
super.ajouter("LIB_ProgPré2", mWD_LIB_ProgPre2);
mWD_BTN_SansNom1.initialiserObjet();
super.ajouter("BTN_SansNom1", mWD_BTN_SansNom1);
mWD_BTN_prog_pre.initialiserObjet();
super.ajouter("BTN_prog_pré", mWD_BTN_prog_pre);
mWD_LIB_SansNom1.initialiserObjet();
super.ajouter("LIB_SansNom1", mWD_LIB_SansNom1);
mWD_BTN_prog_perso.initialiserObjet();
super.ajouter("BTN_prog_perso", mWD_BTN_prog_perso);
mWD_BTN_exo.initialiserObjet();
super.ajouter("BTN_exo", mWD_BTN_exo);

super.terminerInitialisation();
}

////////////////////////////////////////////////////////////////////////////
public boolean isUniteAffichageLogique()
{
return false;
}

public WDProjet getProjet()
{
return GWDPHome_Biking.getInstance();
}

public IWDEnsembleElement getEnsemble()
{
return GWDPHome_Biking.getInstance();
}
public int getModeContexteHF()
{
return 1;
}
/**
* Retourne le mode d'affichage de l'ActionBar de la fenêtre.
*/
public int getModeActionBar()
{
return 0;
}
/**
* Retourne vrai si la fenêtre est maximisée, faux sinon.
*/
public boolean isMaximisee()
{
return true;
}
/**
* Retourne vrai si la fenêtre a une barre de titre, faux sinon.
*/
public boolean isAvecBarreDeTitre()
{
return false;
}
/**
* Retourne le mode d'affichage de la barre système de la fenêtre.
*/
public int getModeBarreSysteme()
{
return 1;
}
/**
* Retourne vrai si la fenêtre est munie d'ascenseurs automatique, faux sinon.
*/
public boolean isAvecAscenseurAuto()
{
return false;
}
/**
* Retourne Vrai si on doit appliquer un theme "dark" (sombre) ou Faux si on doit appliquer "light" (clair) à la fenêtre.
* Ce choix se base sur la couleur du libellé par défaut dans le gabarit de la fenêtre.
*/
public boolean isThemeDark()
{
return false;
}
/**
* Retourne vrai si l'option de masquage automatique de l'ActionBar lorsqu'on scrolle dans un champ de la fenêtre a été activée.
*/
public boolean isMasquageAutomatiqueActionBar()
{
return false;
}
public static class WDActiviteFenetre extends WDActivite
{
protected WDFenetre getFenetre()
{
return GWDPHome_Biking.getInstance().mWD_FEN_Accueil;
}
}
/**
* Retourne le nom du gabarit associée à la fenêtre.
*/
public String getNomGabarit()
{
return "210 MATERIAL DESIGN ORANGE#WM";
}
}
