/**
 * Code généré par WINDEV Mobile - NE PAS MODIFIER !
 * Objet WINDEV Mobile : Collection
 * Classe Android : COL_iSPerso
 * Date : 16/12/2020 08:39:47
 * Version de wdjava.dll  : 25.0.221.6
 */


package com.logicorp.home_biking.wdgen;


import com.logicorp.home_biking.*;
import fr.pcsoft.wdjava.core.types.*;
import fr.pcsoft.wdjava.core.*;
import fr.pcsoft.wdjava.core.application.*;
import fr.pcsoft.wdjava.api.*;
/*Imports trouvés dans le code WL*/
/*Fin Imports trouvés dans le code WL*/



public class GWDCPCOL_iSPerso extends WDCollProcAndroid
{

public WDProjet getProjet()
{
return GWDPHome_Biking.getInstance();
}

public IWDEnsembleElement getEnsemble()
{
return GWDPHome_Biking.getInstance();
}

protected String getNomCollection()
{
return "COL_iSPerso";
}
private final static GWDCPCOL_iSPerso ms_instance = new GWDCPCOL_iSPerso();
public final static GWDCPCOL_iSPerso getInstance()
{
return ms_instance;
}

// Code de déclaration de COL_iSPerso
static public void init()
{
// 
//MAP:1fbe5dae0058874d:000b0000:1:COL_iSPerso:com.logicorp.home_biking.wdgen.GWDCPCOL_iSPerso:Déclaration de COL_iSPerso
ms_instance.initDeclarationCollection();

try
{
}
finally
{
finDeclarationCollection();
}

}




// Code de terminaison de COL_iSPerso
static public void term()
{
// 
//MAP:1fbe5dae0058874d:000b0002:1:COL_iSPerso:com.logicorp.home_biking.wdgen.GWDCPCOL_iSPerso:Terminaison de COL_iSPerso
ms_instance.initTerminaisonCollection();

try
{
}
finally
{
finTerminaisonCollection();
}

}



// Nombre de Procédures : 1
//  Résumé : <indiquez ici ce que fait la procédure>
//  Syntaxe :
// [ <Résultat> = ] isPersoOrNot (<ParamIdProgramme>)
// 
//  Paramètres :
// 	ParamIdProgramme : <indiquez ici le rôle de ParamIdProgramme>
//  Valeur de retour :
//  	booléen : // 	Aucune
// 
//  Exemple :
//  Indiquez ici un exemple d'utilisation.
// 
static public WDObjet fWD_isPersoOrNot( WDObjet vWD_ParamIdProgramme )
{
// FONCTION isPersoOrNot(ParamIdProgramme)
//MAP:1fbe5e10005c06f8:00070000:d:COL_iSPerso.isPersoOrNot:com.logicorp.home_biking.wdgen.GWDCPCOL_iSPerso:isPersoOrNot
ms_instance.initExecProcGlobale("isPersoOrNot");

try
{
// 	HLitRecherche(Programme,Id_Programme,ParamIdProgramme)
//MAP:1fbe5e10005c06f8:00070000:e:COL_iSPerso.isPersoOrNot:com.logicorp.home_biking.wdgen.GWDCPCOL_iSPerso:isPersoOrNot
WDAPIHF.hLitRecherche(WDAPIHF.getFichierSansCasseNiAccent("programme"),WDAPIHF.getRubriqueSansCasseNiAccent("id_programme"),vWD_ParamIdProgramme);

// 	renvoyer (Programme.Id_Compte_Client <> 0)
//MAP:1fbe5e10005c06f8:00070000:f:COL_iSPerso.isPersoOrNot:com.logicorp.home_biking.wdgen.GWDCPCOL_iSPerso:isPersoOrNot
return new WDBooleen(WDAPIHF.getFichierSansCasseNiAccent("programme").getRubriqueSansCasseNiAccent("id_compte_client").opDiff(0));

}
finally
{
finExecProcGlobale();
}

}



////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
