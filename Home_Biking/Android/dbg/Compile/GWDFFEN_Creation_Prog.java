/**
 * Code généré par WINDEV Mobile - NE PAS MODIFIER !
 * Objet WINDEV Mobile : Fenêtre
 * Classe Android : FEN_Creation_Prog
 * Date : 02/04/2021 14:52:47
 * Version de wdjava.dll  : 25.0.221.6
 */


package com.logicorp.home_biking.wdgen;


import com.logicorp.home_biking.*;
import fr.pcsoft.wdjava.core.types.*;
import fr.pcsoft.wdjava.core.*;
import fr.pcsoft.wdjava.ui.champs.fenetre.*;
import fr.pcsoft.wdjava.ui.champs.image.*;
import fr.pcsoft.wdjava.ui.cadre.*;
import fr.pcsoft.wdjava.api.*;
import fr.pcsoft.wdjava.ui.champs.libelle.*;
import fr.pcsoft.wdjava.ui.champs.saisie.*;
import fr.pcsoft.wdjava.core.parcours.*;
import fr.pcsoft.wdjava.core.parcours.champ.*;
import fr.pcsoft.wdjava.ui.champs.zr.*;
import fr.pcsoft.wdjava.ui.champs.bouton.*;
import fr.pcsoft.wdjava.ui.champs.combo.*;
import fr.pcsoft.wdjava.core.application.*;
import fr.pcsoft.wdjava.ui.activite.*;
/*Imports trouvés dans le code WL*/
/*Fin Imports trouvés dans le code WL*/



public class GWDFFEN_Creation_Prog extends WDFenetre
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs de FEN_Creation_Prog
////////////////////////////////////////////////////////////////////////////

/**
 * IMG_Button_Back
 */
class GWDIMG_Button_Back extends WDChampImage
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FEN_Creation_Prog.IMG_Button_Back
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3445799205986180026l);

super.setChecksum("938678809");

super.setNom("IMG_Button_Back");

super.setType(30001);

super.setBulle("");

super.setLibelle("");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setNavigable(false);

super.setEtatInitial(0);

super.setPositionInitiale(13, 1);

super.setTailleInitiale(52, 52);

super.setValeurInitiale("C:\\Users\\axelm\\Desktop\\COURS IUT INFO\\Semestre 3\\M3302 - Projet tutore - Mise en situation professionnelle\\ptut-home-trainer\\Home_Biking\\Bouton_previous_page.png");

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(1);

super.setAncrageInitial(0, 1000, 1000, 1000, 1000, 0);

super.setTransparence(1);

super.setParamImage(2097158, 0, true, 100);

super.setSymetrie(0);

super.setZoneClicage(true);

super.setPCodeMultitouch(false);

super.setChargementEnTacheDeFond(false);

super.setOrientationExif(false);

super.setParamAnimation(1, 1, false, 300, true, false);

super.setAnimationInitiale(false);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -1, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

/**
 * Traitement: Clic sur IMG_Button_Back
 */
public void clicSurBoutonGauche()
{
super.clicSurBoutonGauche();

// SELON Dialogue("Voulez-vous arrêtez la création de votre programme ?")
//MAP:2fd1f01f08211bba:00000012:1:FEN_Creation_Prog.IMG_Button_Back:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_Button_Back:Clic sur IMG_Button_Back
// SELON Dialogue("Voulez-vous arrêtez la création de votre programme ?")
//MAP:2fd1f01f08211bba:00000012:1:FEN_Creation_Prog.IMG_Button_Back:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_Button_Back:Clic sur IMG_Button_Back
// Délimiteur de visibilité pour ne pas étendre la visibilité de la variable temporaire _WDExpSelon
{
// SELON Dialogue("Voulez-vous arrêtez la création de votre programme ?")
//MAP:2fd1f01f08211bba:00000012:1:FEN_Creation_Prog.IMG_Button_Back:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_Button_Back:Clic sur IMG_Button_Back
WDObjet _WDExpSelon0 = WDAPIDialogue.dialogue(0);
if(_WDExpSelon0.opEgal(1))
{
// 		HSupprimeTout(Creation_contenir)
//MAP:2fd1f01f08211bba:00000012:4:FEN_Creation_Prog.IMG_Button_Back:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_Button_Back:Clic sur IMG_Button_Back
WDAPIHF.hSupprimeTout(WDAPIHF.getFichierSansCasseNiAccent("creation_contenir"));

// 		HSupprimeTout(Creation_Programme)
//MAP:2fd1f01f08211bba:00000012:5:FEN_Creation_Prog.IMG_Button_Back:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_Button_Back:Clic sur IMG_Button_Back
WDAPIHF.hSupprimeTout(WDAPIHF.getFichierSansCasseNiAccent("creation_programme"));

// 		Ferme(FEN_Programm_Perso)
//MAP:2fd1f01f08211bba:00000012:6:FEN_Creation_Prog.IMG_Button_Back:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_Button_Back:Clic sur IMG_Button_Back
WDAPIFenetre.ferme(GWDPHome_Biking.getInstance().mWD_FEN_Programm_Perso);

}
else if(_WDExpSelon0.opEgal(2))
{
}

}

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurClic();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDIMG_Button_Back mWD_IMG_Button_Back;

/**
 * LIB_Nom_du_Programme
 */
class GWDLIB_Nom_du_Programme extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°2 de FEN_Creation_Prog.LIB_Nom_du_Programme
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3445877382863050941l);

super.setChecksum("820843854");

super.setNom("LIB_Nom_du_Programme");

super.setType(3);

super.setBulle("");

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("Nom du Programme");

super.setNote("", "");

super.setCurseurSouris(0);

super.setEtatInitial(0);

super.setPositionInitiale(8, 59);

super.setTailleInitiale(157, 19);

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(2);

super.setAncrageInitial(0, 1000, 1000, 1000, 1000, 0);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, 0xFFFFFFFF, creerPolice_GEN("Roboto", -8.000000, 0), 3, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_Nom_du_Programme mWD_LIB_Nom_du_Programme;

/**
 * SAI_nomprog
 */
class GWDSAI_nomprog extends WDChampSaisieSimple
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°3 de FEN_Creation_Prog.SAI_nomprog
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setRectCompPrincipal(1,3,304,23);
super.setQuid(3445877460173922171l);

super.setChecksum("822303318");

super.setNom("SAI_nomprog");

super.setType(20001);

super.setBulle("");

super.setLibelle("");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setTaille(0);

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(8, 86);

super.setTailleInitiale(306, 29);

super.setValeurInitiale("");

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setMotDePasse(false);

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("18"));

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(3);

super.setAncrageInitial(0, 1000, 1000, 1000, 1000, 0);

super.setEllipse(0);

super.setIndication("");

super.setNumTab(1);

super.setModeAscenseur(2, 2);

super.setEffacementAutomatique(true);

super.setFinSaisieAutomatique(false);

super.setLettreAppel(65535);

super.setSelectionEnAffichage(true);

super.setPersistant(false);

super.setClavierEnSaisie(true);

super.setMasqueAffichage(new WDChaineU(""));

super.setParamBtnActionClavier(0, "");

super.setRetraitGauche(2);

super.setMiseABlancSiZero(true);

super.setVerifieOrthographe(true);

super.setTauxParallaxe(0, 0);

super.setBoutonSuppression(0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -1, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(2, 0x222222, 0x0, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN(1, 0x757575, 0x0, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1));

super.setStyleSaisie(0x222222, creerPolice_GEN("Roboto", -8.000000, 0));

super.setStyleTexteIndication(0x8E8E8F, creerPolice_GEN("Roboto", -8.000000, 2), 2);

super.setStyleJeton(WDCadreFactory.creerCadre_GEN(2, 0x98FF, 0x187F, 0x98FF, 4.000000, 4.000000, 1, 1), 0xFFFFFF, "", 1);

activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDSAI_nomprog mWD_SAI_nomprog;

/**
 * LIB_exo
 */
class GWDLIB_exo extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°4 de FEN_Creation_Prog.LIB_exo
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3445878413659373516l);

super.setChecksum("825015629");

super.setNom("LIB_exo");

super.setType(3);

super.setBulle("");

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("Exercices :");

super.setNote("", "");

super.setCurseurSouris(0);

super.setEtatInitial(0);

super.setPositionInitiale(8, 131);

super.setTailleInitiale(120, 19);

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(4);

super.setAncrageInitial(0, 1000, 1000, 1000, 1000, 0);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, 0xFFFFFFFF, creerPolice_GEN("Roboto", -8.000000, 0), 3, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_exo mWD_LIB_exo;

/**
 * IMG_Button_Validate
 */
class GWDIMG_Button_Validate extends WDChampImage
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°5 de FEN_Creation_Prog.IMG_Button_Validate
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3445887059423869005l);

super.setChecksum("820348563");

super.setNom("IMG_Button_Validate");

super.setType(30001);

super.setBulle("");

super.setLibelle("");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setNavigable(false);

super.setEtatInitial(0);

super.setPositionInitiale(29, 473);

super.setTailleInitiale(52, 52);

super.setValeurInitiale("C:\\Users\\axelm\\Desktop\\COURS IUT INFO\\Semestre 3\\M3302 - Projet tutore - Mise en situation professionnelle\\ptut-home-trainer\\Home_Biking\\button validate.png");

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(5);

super.setAncrageInitial(1, 1000, 1000, 1000, 1000, 0);

super.setTransparence(1);

super.setParamImage(2097158, 0, true, 100);

super.setSymetrie(0);

super.setZoneClicage(true);

super.setPCodeMultitouch(false);

super.setChargementEnTacheDeFond(false);

super.setOrientationExif(false);

super.setParamAnimation(1, 1, false, 300, true, false);

super.setAnimationInitiale(false);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -1, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

/**
 * Traitement: Initialisation de IMG_Button_Validate
 */
public void init()
{
super.init();

// IMG_Button_Validate..Visible=Faux
//MAP:2fd240060113384d:0000000e:1:FEN_Creation_Prog.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_Button_Validate:Initialisation de IMG_Button_Validate
// IMG_Button_Validate..Visible=Faux
//MAP:2fd240060113384d:0000000e:1:FEN_Creation_Prog.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_Button_Validate:Initialisation de IMG_Button_Validate
this.setProp(EWDPropriete.PROP_VISIBLE,false);

}




/**
 * Traitement: Clic sur IMG_Button_Validate
 */
public void clicSurBoutonGauche()
{
super.clicSurBoutonGauche();

// SI Taille(SAI_nomprog)>2 ALORS
//MAP:2fd240060113384d:00000012:1:FEN_Creation_Prog.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
// SI Taille(SAI_nomprog)>2 ALORS
//MAP:2fd240060113384d:00000012:1:FEN_Creation_Prog.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
if(WDAPIChaine.taille(mWD_SAI_nomprog).opSup(2))
{
// 	sNom est une chaîne
//MAP:2fd240060113384d:00000012:4:FEN_Creation_Prog.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDObjet vWD_sNom = new WDChaineU();



// 	sDiff est une chaîne
//MAP:2fd240060113384d:00000012:5:FEN_Creation_Prog.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDObjet vWD_sDiff = new WDChaineU();



// 	nCompteClient est un entier
//MAP:2fd240060113384d:00000012:6:FEN_Creation_Prog.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDObjet vWD_nCompteClient = new WDEntier4();



// 	nCompteClient=1
//MAP:2fd240060113384d:00000012:7:FEN_Creation_Prog.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
vWD_nCompteClient.setValeur(1);

// 	HLitPremier(Creation_Programme)
//MAP:2fd240060113384d:00000012:9:FEN_Creation_Prog.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDAPIHF.hLitPremier(WDAPIHF.getFichierSansCasseNiAccent("creation_programme"));

// 	sNom = SAI_nomprog
//MAP:2fd240060113384d:00000012:a:FEN_Creation_Prog.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
vWD_sNom.setValeur(mWD_SAI_nomprog);

// 	sDiff = COMBO_Difficulte.ValeurAffichée
//MAP:2fd240060113384d:00000012:b:FEN_Creation_Prog.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
vWD_sDiff.setValeur(mWD_COMBO_Difficulte.getProp(EWDPropriete.PROP_VALEURAFFICHEE));

// 	nId est un entier
//MAP:2fd240060113384d:00000012:d:FEN_Creation_Prog.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDObjet vWD_nId = new WDEntier4();



// 	HLitDernier(Programme,Id_Programme)
//MAP:2fd240060113384d:00000012:e:FEN_Creation_Prog.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDAPIHF.hLitDernier(WDAPIHF.getFichierSansCasseNiAccent("programme"),WDAPIHF.getRubriqueSansCasseNiAccent("id_programme"));

// 	nId = Programme.Id_Programme + 1
//MAP:2fd240060113384d:00000012:f:FEN_Creation_Prog.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
vWD_nId.setValeur(WDAPIHF.getFichierSansCasseNiAccent("programme").getRubriqueSansCasseNiAccent("id_programme").opPlus(1));

// 	Programme.Nom=sNom
//MAP:2fd240060113384d:00000012:12:FEN_Creation_Prog.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDAPIHF.getFichierSansCasseNiAccent("programme").getRubriqueSansCasseNiAccent("nom").setValeur(vWD_sNom);

// 	Programme.Id_Programme=nId
//MAP:2fd240060113384d:00000012:13:FEN_Creation_Prog.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDAPIHF.getFichierSansCasseNiAccent("programme").getRubriqueSansCasseNiAccent("id_programme").setValeur(vWD_nId);

// 	Programme.Id_Compte_Client=nCompteClient
//MAP:2fd240060113384d:00000012:14:FEN_Creation_Prog.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDAPIHF.getFichierSansCasseNiAccent("programme").getRubriqueSansCasseNiAccent("id_compte_client").setValeur(vWD_nCompteClient);

// 	Programme.Difficulte=sDiff
//MAP:2fd240060113384d:00000012:15:FEN_Creation_Prog.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDAPIHF.getFichierSansCasseNiAccent("programme").getRubriqueSansCasseNiAccent("difficulte").setValeur(vWD_sDiff);

// 	HAjoute(Programme)
//MAP:2fd240060113384d:00000012:16:FEN_Creation_Prog.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDAPIHF.hAjoute(WDAPIHF.getFichierSansCasseNiAccent("programme"));

// 	HLitPremier(Creation_contenir,place)
//MAP:2fd240060113384d:00000012:19:FEN_Creation_Prog.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDAPIHF.hLitPremier(WDAPIHF.getFichierSansCasseNiAccent("creation_contenir"),WDAPIHF.getRubriqueSansCasseNiAccent("place"));

// 	BOUCLE
//MAP:2fd240060113384d:00000012:1a:FEN_Creation_Prog.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
while(true)
{
// 		contenir.Id_Exercice=Creation_contenir.Id_Exercice
//MAP:2fd240060113384d:00000012:1d:FEN_Creation_Prog.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDAPIHF.getFichierSansCasseNiAccent("contenir").getRubriqueSansCasseNiAccent("id_exercice").setValeur(WDAPIHF.getFichierSansCasseNiAccent("creation_contenir").getRubriqueSansCasseNiAccent("id_exercice"));

// 		contenir.Id_Programme=nId
//MAP:2fd240060113384d:00000012:1e:FEN_Creation_Prog.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDAPIHF.getFichierSansCasseNiAccent("contenir").getRubriqueSansCasseNiAccent("id_programme").setValeur(vWD_nId);

// 		contenir.place=Creation_contenir.place
//MAP:2fd240060113384d:00000012:1f:FEN_Creation_Prog.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDAPIHF.getFichierSansCasseNiAccent("contenir").getRubriqueSansCasseNiAccent("place").setValeur(WDAPIHF.getFichierSansCasseNiAccent("creation_contenir").getRubriqueSansCasseNiAccent("place"));

// 		contenir.cadence=Creation_contenir.cadence
//MAP:2fd240060113384d:00000012:20:FEN_Creation_Prog.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDAPIHF.getFichierSansCasseNiAccent("contenir").getRubriqueSansCasseNiAccent("cadence").setValeur(WDAPIHF.getFichierSansCasseNiAccent("creation_contenir").getRubriqueSansCasseNiAccent("cadence"));

// 		contenir.nb_rep=Creation_contenir.nb_rep
//MAP:2fd240060113384d:00000012:21:FEN_Creation_Prog.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDAPIHF.getFichierSansCasseNiAccent("contenir").getRubriqueSansCasseNiAccent("nb_rep").setValeur(WDAPIHF.getFichierSansCasseNiAccent("creation_contenir").getRubriqueSansCasseNiAccent("nb_rep"));

// 		contenir.pourcentage_fcm=Creation_contenir.pourcentage_fcm
//MAP:2fd240060113384d:00000012:22:FEN_Creation_Prog.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDAPIHF.getFichierSansCasseNiAccent("contenir").getRubriqueSansCasseNiAccent("pourcentage_fcm").setValeur(WDAPIHF.getFichierSansCasseNiAccent("creation_contenir").getRubriqueSansCasseNiAccent("pourcentage_fcm"));

// 		contenir.puissance=Creation_contenir.puissance
//MAP:2fd240060113384d:00000012:23:FEN_Creation_Prog.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDAPIHF.getFichierSansCasseNiAccent("contenir").getRubriqueSansCasseNiAccent("puissance").setValeur(WDAPIHF.getFichierSansCasseNiAccent("creation_contenir").getRubriqueSansCasseNiAccent("puissance"));

// 		contenir.repos=Creation_contenir.repos
//MAP:2fd240060113384d:00000012:24:FEN_Creation_Prog.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDAPIHF.getFichierSansCasseNiAccent("contenir").getRubriqueSansCasseNiAccent("repos").setValeur(WDAPIHF.getFichierSansCasseNiAccent("creation_contenir").getRubriqueSansCasseNiAccent("repos"));

// 		contenir.temps=Creation_contenir.temps
//MAP:2fd240060113384d:00000012:25:FEN_Creation_Prog.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDAPIHF.getFichierSansCasseNiAccent("contenir").getRubriqueSansCasseNiAccent("temps").setValeur(WDAPIHF.getFichierSansCasseNiAccent("creation_contenir").getRubriqueSansCasseNiAccent("temps"));

// 		HAjoute(contenir)
//MAP:2fd240060113384d:00000012:26:FEN_Creation_Prog.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDAPIHF.hAjoute(WDAPIHF.getFichierSansCasseNiAccent("contenir"));

// 		HLitSuivant(Creation_contenir,place)
//MAP:2fd240060113384d:00000012:28:FEN_Creation_Prog.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDAPIHF.hLitSuivant(WDAPIHF.getFichierSansCasseNiAccent("creation_contenir"),WDAPIHF.getRubriqueSansCasseNiAccent("place"));

// 		SI HTrouve()=Faux ALORS
//MAP:2fd240060113384d:00000012:29:FEN_Creation_Prog.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
if(WDAPIHF.hTrouve().opEgal(false))
{
// 			SORTIR
//MAP:2fd240060113384d:00000012:2a:FEN_Creation_Prog.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
break;

//////////////////////////////////////////////////////////
// Code Inaccessible
// 
}

}


// 	HSupprimeTout(Creation_contenir)
//MAP:2fd240060113384d:00000012:2e:FEN_Creation_Prog.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDAPIHF.hSupprimeTout(WDAPIHF.getFichierSansCasseNiAccent("creation_contenir"));

// 	HSupprimeTout(Creation_Programme)
//MAP:2fd240060113384d:00000012:2f:FEN_Creation_Prog.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDAPIHF.hSupprimeTout(WDAPIHF.getFichierSansCasseNiAccent("creation_programme"));

// 	ZoneRépétéeAffiche(FEN_Programm_Perso.ZR_ProgrammePerso,taRéExécuteRequete)
//MAP:2fd240060113384d:00000012:31:FEN_Creation_Prog.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDAPIZoneRepetee.zoneRepeteeAffiche(GWDPHome_Biking.getInstance().getFEN_Programm_Perso().mWD_ZR_ProgrammePerso,new WDChaineU("Reexecute"));

// 	Ferme(FEN_Creation_Prog)
//MAP:2fd240060113384d:00000012:32:FEN_Creation_Prog.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDAPIFenetre.ferme(GWDPHome_Biking.getInstance().mWD_FEN_Creation_Prog);

}
else
{
// 	ToastAffiche("Saisir un nom de programme d'au moins 3 caractères",toastLong)
//MAP:2fd240060113384d:00000012:36:FEN_Creation_Prog.IMG_Button_Validate:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_Button_Validate:Clic sur IMG_Button_Validate
WDAPIToast.toastAffiche("Saisir un nom de programme d'au moins 3 caractères",1);

}

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurClic();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDIMG_Button_Validate mWD_IMG_Button_Validate;

/**
 * IMG_add_exo
 */
class GWDIMG_add_exo extends WDChampImage
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°6 de FEN_Creation_Prog.IMG_add_exo
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3445887175388930938l);

super.setChecksum("821293531");

super.setNom("IMG_add_exo");

super.setType(30001);

super.setBulle("");

super.setLibelle("");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setNavigable(false);

super.setEtatInitial(0);

super.setPositionInitiale(221, 486);

super.setTailleInitiale(27, 27);

super.setValeurInitiale("C:\\Users\\axelm\\Desktop\\COURS IUT INFO\\Semestre 3\\M3302 - Projet tutore - Mise en situation professionnelle\\ptut-home-trainer\\Home_Biking\\button add exercise.png");

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(6);

super.setAncrageInitial(5, 1000, 1000, 1000, 1000, 0);

super.setTransparence(1);

super.setParamImage(2097158, 0, true, 100);

super.setSymetrie(0);

super.setZoneClicage(true);

super.setPCodeMultitouch(false);

super.setChargementEnTacheDeFond(false);

super.setOrientationExif(false);

super.setParamAnimation(1, 1, false, 300, true, false);

super.setAnimationInitiale(false);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -1, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

/**
 * Traitement: Clic sur IMG_add_exo
 */
public void clicSurBoutonGauche()
{
super.clicSurBoutonGauche();

// SI HNbEnr(Creation_Programme)<=0 ALORS
//MAP:2fd240210121a37a:00000012:1:FEN_Creation_Prog.IMG_add_exo:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_add_exo:Clic sur IMG_add_exo
// SI HNbEnr(Creation_Programme)<=0 ALORS
//MAP:2fd240210121a37a:00000012:1:FEN_Creation_Prog.IMG_add_exo:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_add_exo:Clic sur IMG_add_exo
if(WDAPIHF.hNbEnr(WDAPIHF.getFichierSansCasseNiAccent("creation_programme")).opInfEgal(0))
{
// 	HAjoute(Creation_Programme)
//MAP:2fd240210121a37a:00000012:2:FEN_Creation_Prog.IMG_add_exo:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_add_exo:Clic sur IMG_add_exo
WDAPIHF.hAjoute(WDAPIHF.getFichierSansCasseNiAccent("creation_programme"));

// 	Creation_Programme.Nom			=SAI_nomprog
//MAP:2fd240210121a37a:00000012:3:FEN_Creation_Prog.IMG_add_exo:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_add_exo:Clic sur IMG_add_exo
WDAPIHF.getFichierSansCasseNiAccent("creation_programme").getRubriqueSansCasseNiAccent("nom").setValeur(mWD_SAI_nomprog);

// 	Creation_Programme.Difficulte	= COMBO_Difficulte.ValeurAffichée
//MAP:2fd240210121a37a:00000012:4:FEN_Creation_Prog.IMG_add_exo:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_add_exo:Clic sur IMG_add_exo
WDAPIHF.getFichierSansCasseNiAccent("creation_programme").getRubriqueSansCasseNiAccent("difficulte").setValeur(mWD_COMBO_Difficulte.getProp(EWDPropriete.PROP_VALEURAFFICHEE));

}
else
{
// 	HLitPremier(Creation_Programme)
//MAP:2fd240210121a37a:00000012:6:FEN_Creation_Prog.IMG_add_exo:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_add_exo:Clic sur IMG_add_exo
WDAPIHF.hLitPremier(WDAPIHF.getFichierSansCasseNiAccent("creation_programme"));

// 	Creation_Programme.Nom=SAI_nomprog
//MAP:2fd240210121a37a:00000012:7:FEN_Creation_Prog.IMG_add_exo:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_add_exo:Clic sur IMG_add_exo
WDAPIHF.getFichierSansCasseNiAccent("creation_programme").getRubriqueSansCasseNiAccent("nom").setValeur(mWD_SAI_nomprog);

// 	Creation_Programme.Difficulte = COMBO_Difficulte.ValeurAffichée
//MAP:2fd240210121a37a:00000012:8:FEN_Creation_Prog.IMG_add_exo:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_add_exo:Clic sur IMG_add_exo
WDAPIHF.getFichierSansCasseNiAccent("creation_programme").getRubriqueSansCasseNiAccent("difficulte").setValeur(mWD_COMBO_Difficulte.getProp(EWDPropriete.PROP_VALEURAFFICHEE));

// 	HModifie(Creation_Programme,hNumEnrEnCours)
//MAP:2fd240210121a37a:00000012:9:FEN_Creation_Prog.IMG_add_exo:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_add_exo:Clic sur IMG_add_exo
WDAPIHF.hModifie(WDAPIHF.getFichierSansCasseNiAccent("creation_programme"),(long)0);

}

// OuvreFille(FEN_Exercices,Vrai)
//MAP:2fd240210121a37a:00000012:c:FEN_Creation_Prog.IMG_add_exo:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_add_exo:Clic sur IMG_add_exo
WDAPIFenetre.ouvreFille(GWDPHome_Biking.getInstance().mWD_FEN_Exercices,new WDObjet[] {new WDBooleen(true)} );

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurClic();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDIMG_add_exo mWD_IMG_add_exo;

/**
 * IMG_less_exo
 */
class GWDIMG_less_exo extends WDChampImage
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°7 de FEN_Creation_Prog.IMG_less_exo
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3445887420203565024l);

super.setChecksum("822791802");

super.setNom("IMG_less_exo");

super.setType(30001);

super.setBulle("");

super.setLibelle("");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setNavigable(false);

super.setEtatInitial(0);

super.setPositionInitiale(266, 486);

super.setTailleInitiale(27, 27);

super.setValeurInitiale("C:\\Users\\axelm\\Desktop\\COURS IUT INFO\\Semestre 3\\M3302 - Projet tutore - Mise en situation professionnelle\\ptut-home-trainer\\Home_Biking\\delete exercise initial state.png");

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(7);

super.setAncrageInitial(5, 1000, 1000, 1000, 1000, 0);

super.setTransparence(1);

super.setParamImage(2097158, 0, true, 100);

super.setSymetrie(0);

super.setZoneClicage(true);

super.setPCodeMultitouch(false);

super.setChargementEnTacheDeFond(false);

super.setOrientationExif(false);

super.setParamAnimation(1, 1, false, 300, true, false);

super.setAnimationInitiale(false);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -1, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

/**
 * Traitement: Initialisation de IMG_less_exo
 */
public void init()
{
super.init();

// IMG_less_exo.Visible=Faux
//MAP:2fd2405a01387fe0:0000000e:1:FEN_Creation_Prog.IMG_less_exo:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_less_exo:Initialisation de IMG_less_exo
// IMG_less_exo.Visible=Faux
//MAP:2fd2405a01387fe0:0000000e:1:FEN_Creation_Prog.IMG_less_exo:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_less_exo:Initialisation de IMG_less_exo
this.setProp(EWDPropriete.PROP_VISIBLE,false);

}




/**
 * Traitement: Clic sur IMG_less_exo
 */
public void clicSurBoutonGauche()
{
super.clicSurBoutonGauche();

// gbEdition = PAS gbEdition
//MAP:2fd2405a01387fe0:00000012:1:FEN_Creation_Prog.IMG_less_exo:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_less_exo:Clic sur IMG_less_exo
// gbEdition = PAS gbEdition
//MAP:2fd2405a01387fe0:00000012:1:FEN_Creation_Prog.IMG_less_exo:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_less_exo:Clic sur IMG_less_exo
vWD_gbEdition.setValeur((!vWD_gbEdition.getBoolean()));

// SI gbEdition = Vrai ALORS
//MAP:2fd2405a01387fe0:00000012:3:FEN_Creation_Prog.IMG_less_exo:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_less_exo:Clic sur IMG_less_exo
if(vWD_gbEdition.opEgal(true))
{
// 	IMG_less_exo=IMG_less_select
//MAP:2fd2405a01387fe0:00000012:4:FEN_Creation_Prog.IMG_less_exo:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_less_exo:Clic sur IMG_less_exo
this.setValeur(mWD_IMG_less_select);

// 	IMG_Button_Validate..Visible	= Faux
//MAP:2fd2405a01387fe0:00000012:5:FEN_Creation_Prog.IMG_less_exo:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_less_exo:Clic sur IMG_less_exo
mWD_IMG_Button_Validate.setProp(EWDPropriete.PROP_VISIBLE,false);

// 	IMG_add_exo..Visible			= Faux
//MAP:2fd2405a01387fe0:00000012:6:FEN_Creation_Prog.IMG_less_exo:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_less_exo:Clic sur IMG_less_exo
mWD_IMG_add_exo.setProp(EWDPropriete.PROP_VISIBLE,false);

// 	i est un entier
//MAP:2fd2405a01387fe0:00000012:8:FEN_Creation_Prog.IMG_less_exo:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_less_exo:Clic sur IMG_less_exo
WDObjet vWD_i = new WDEntier4();



// 	i=1
//MAP:2fd2405a01387fe0:00000012:9:FEN_Creation_Prog.IMG_less_exo:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_less_exo:Clic sur IMG_less_exo
vWD_i.setValeur(1);

// 	POUR CHAQUE LIGNE DE ZR_REQ_Creation_Prog
//MAP:2fd2405a01387fe0:00000012:a:FEN_Creation_Prog.IMG_less_exo:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_less_exo:Clic sur IMG_less_exo
IWDParcours parcours1 = null;
try
{
parcours1 = WDParcoursChamp.pourTout(mWD_ZR_REQ_Creation_Prog, 0x2);
while(parcours1.testParcours())
{
// 		ZR_REQ_Creation_Prog[i].BTN_Supprimer.Visible= Vrai
//MAP:2fd2405a01387fe0:00000012:b:FEN_Creation_Prog.IMG_less_exo:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_less_exo:Clic sur IMG_less_exo
mWD_ZR_REQ_Creation_Prog.get(vWD_i).get("BTN_Supprimer").setProp(EWDPropriete.PROP_VISIBLE,true);

// 		i++
//MAP:2fd2405a01387fe0:00000012:c:FEN_Creation_Prog.IMG_less_exo:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_less_exo:Clic sur IMG_less_exo
vWD_i.opInc();

}

}
finally
{
if(parcours1 != null)
{
parcours1.finParcours();
}
}


}
else
{
// 	IMG_less_exo = IMG_less_exo1
//MAP:2fd2405a01387fe0:00000012:f:FEN_Creation_Prog.IMG_less_exo:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_less_exo:Clic sur IMG_less_exo
this.setValeur(mWD_IMG_less_exo1);

// 	SI HNbEnr(Creation_contenir)>2 ALORS
//MAP:2fd2405a01387fe0:00000012:10:FEN_Creation_Prog.IMG_less_exo:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_less_exo:Clic sur IMG_less_exo
if(WDAPIHF.hNbEnr(WDAPIHF.getFichierSansCasseNiAccent("creation_contenir")).opSup(2))
{
// 		IMG_Button_Validate..Visible	= Vrai
//MAP:2fd2405a01387fe0:00000012:11:FEN_Creation_Prog.IMG_less_exo:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_less_exo:Clic sur IMG_less_exo
mWD_IMG_Button_Validate.setProp(EWDPropriete.PROP_VISIBLE,true);

}

// 	IMG_add_exo..Visible			= Vrai
//MAP:2fd2405a01387fe0:00000012:13:FEN_Creation_Prog.IMG_less_exo:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_less_exo:Clic sur IMG_less_exo
mWD_IMG_add_exo.setProp(EWDPropriete.PROP_VISIBLE,true);

// 	i est un entier
//MAP:2fd2405a01387fe0:00000012:15:FEN_Creation_Prog.IMG_less_exo:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_less_exo:Clic sur IMG_less_exo
WDObjet vWD_i = new WDEntier4();



// 	i=1
//MAP:2fd2405a01387fe0:00000012:16:FEN_Creation_Prog.IMG_less_exo:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_less_exo:Clic sur IMG_less_exo
vWD_i.setValeur(1);

// 	POUR CHAQUE LIGNE DE ZR_REQ_Creation_Prog
//MAP:2fd2405a01387fe0:00000012:17:FEN_Creation_Prog.IMG_less_exo:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_less_exo:Clic sur IMG_less_exo
IWDParcours parcours2 = null;
try
{
parcours2 = WDParcoursChamp.pourTout(mWD_ZR_REQ_Creation_Prog, 0x2);
while(parcours2.testParcours())
{
// 		ZR_REQ_Creation_Prog[i].BTN_Supprimer.Visible= Faux
//MAP:2fd2405a01387fe0:00000012:18:FEN_Creation_Prog.IMG_less_exo:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_less_exo:Clic sur IMG_less_exo
mWD_ZR_REQ_Creation_Prog.get(vWD_i).get("BTN_Supprimer").setProp(EWDPropriete.PROP_VISIBLE,false);

// 		i++
//MAP:2fd2405a01387fe0:00000012:19:FEN_Creation_Prog.IMG_less_exo:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_less_exo:Clic sur IMG_less_exo
vWD_i.opInc();

}

}
finally
{
if(parcours2 != null)
{
parcours2.finParcours();
}
}


}

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurClic();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDIMG_less_exo mWD_IMG_less_exo;

/**
 * ZR_REQ_Creation_Prog
 */
class GWDZR_REQ_Creation_Prog extends WDZoneRepeteeFichierAccesDirect
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°8 de FEN_Creation_Prog.ZR_REQ_Creation_Prog
////////////////////////////////////////////////////////////////////////////

/**
 * nomExo
 */
class GWDnomExo extends WDChampSaisieSimple
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FEN_Creation_Prog.ZR_REQ_Creation_Prog.nomExo
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setRectCompPrincipal(0,2,249,23);
super.setQuid(3445901323126957832l);

super.setChecksum("937047959");

super.setNom("nomExo");

super.setType(20001);

super.setBulle("");

super.setLibelle("nomExo");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setTaille(50);

super.setNavigable(false);

super.setEtatInitial(1);

super.setPositionInitiale(8, 8);

super.setTailleInitiale(249, 27);

super.setValeurInitiale("");

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setMotDePasse(false);

super.setLiaisonFichier("fen_creation_prog_1$requete", "nom");

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(1);

super.setAncrageInitial(8, 1000, 1000, 1000, 1000, 0);

super.setEllipse(0);

super.setIndication("");

super.setNumTab(-1);

super.setModeAscenseur(2, 2);

super.setEffacementAutomatique(true);

super.setFinSaisieAutomatique(false);

super.setLettreAppel(65535);

super.setSelectionEnAffichage(true);

super.setPersistant(false);

super.setClavierEnSaisie(true);

super.setMasqueAffichage(new WDChaineU(""));

super.setParamBtnActionClavier(0, "");

super.setRetraitGauche(2);

super.setMiseABlancSiZero(true);

super.setVerifieOrthographe(true);

super.setTauxParallaxe(0, 0);

super.setBoutonSuppression(0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -1, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN("C:\\Users\\axelm\\Desktop\\COURS IUT INFO\\Semestre 3\\M3302 - Projet tutore - Mise en situation professionnelle\\ptut-home-trainer\\Home_Biking\\Gabarits\\WM\\210 Material Design Orange\\Material Design Orange_Edt@dpi1x.png?E5_3NP_8_8_8_8", new int[] {1,4,1,2,2,2,1,4,1}, new int[] {8, 8, 8, 8}, 0xFFFFFFFF, 1, 5));

super.setStyleSaisie(0x222222, creerPolice_GEN("Roboto", -8.000000, 0));

super.setStyleTexteIndication(0x8E8E8F, creerPolice_GEN("Roboto", -8.000000, 0), 0);

super.setStyleJeton(WDCadreFactory.creerCadre_GEN(2, 0x98FF, 0x187F, 0x98FF, 4.000000, 4.000000, 1, 1), 0xFFFFFF, "", 1);

activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
public int getModeSaisieAssistee()
{
return 1;
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDnomExo mWD_nomExo = new GWDnomExo();

/**
 * Place
 */
class GWDPlace extends WDChampSaisieSimple
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°2 de FEN_Creation_Prog.ZR_REQ_Creation_Prog.Place
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setRectLibelle(0,2,53,19);
super.setRectCompPrincipal(53,2,115,19);
super.setQuid(2334818211663520076l);

super.setChecksum("699930023");

super.setNom("Place");

super.setType(20001);

super.setBulle("");

super.setLibelle("Place");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setTaille(0);

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(334, 10);

super.setTailleInitiale(168, 23);

super.setValeurInitiale("");

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setMotDePasse(false);

super.setLiaisonFichier("fen_creation_prog_1$requete", "place");

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(2);

super.setAncrageInitial(4, 1000, 1000, 1000, 1000, 0);

super.setEllipse(0);

super.setIndication("");

super.setNumTab(2);

super.setModeAscenseur(2, 2);

super.setEffacementAutomatique(true);

super.setFinSaisieAutomatique(false);

super.setLettreAppel(65535);

super.setSelectionEnAffichage(true);

super.setPersistant(false);

super.setClavierEnSaisie(true);

super.setMasqueAffichage(new WDChaineU(""));

super.setParamBtnActionClavier(0, "");

super.setRetraitGauche(2);

super.setMiseABlancSiZero(true);

super.setVerifieOrthographe(true);

super.setTauxParallaxe(0, 0);

super.setBoutonSuppression(0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -2, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN("C:\\Users\\axelm\\Desktop\\COURS IUT INFO\\Semestre 3\\M3302 - Projet tutore - Mise en situation professionnelle\\ptut-home-trainer\\Home_Biking\\Gabarits\\WM\\210 Material Design Orange\\Material Design Orange_Edt@dpi1x.png?E5_3NP_8_8_8_8", new int[] {1,4,1,2,2,2,1,4,1}, new int[] {8, 8, 8, 8}, 0xFFFFFFFF, 1, 5));

super.setStyleSaisie(0x222222, creerPolice_GEN("Roboto", -8.000000, 0));

super.setStyleTexteIndication(0x8E8E8F, creerPolice_GEN("Roboto", -8.000000, 0), 0);

super.setStyleJeton(WDCadreFactory.creerCadre_GEN(2, 0x98FF, 0x187F, 0x98FF, 4.000000, 4.000000, 1, 1), 0xFFFFFF, "", 1);

activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDPlace mWD_Place = new GWDPlace();

/**
 * BTN_Supprimer
 */
class GWDBTN_Supprimer extends WDBouton
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°3 de FEN_Creation_Prog.ZR_REQ_Creation_Prog.BTN_Supprimer
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(2334848568591645106l);

super.setChecksum("799214905");

super.setNom("BTN_Supprimer");

super.setType(4);

super.setBulle("");

super.setLibelle("");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(278, 3);

super.setTailleInitiale(31, 32);

super.setPlan(0);

super.setImageEtat(1);

super.setImageFondEtat(5);

super.setTailleMin(0, 0);

super.setTailleMax(134217727, 134217727);

super.setVisibleInitial(true);

super.setAltitude(3);

super.setAncrageInitial(4, 1000, 1000, 1000, 1000, 0);

super.setNumTab(1);

super.setLettreAppel(65535);

super.setTypeBouton(0);

super.setTypeActionPredefinie(0);

super.setBoutonOnOff(false);

super.setTauxParallaxe(0, 0);

super.setLibelleVAlign(1);

super.setLibelleHAlign(2);

super.setPresenceLibelle(false);

super.setImage("C:\\Users\\axelm\\Desktop\\COURS IUT INFO\\Semestre 3\\M3302 - Projet tutore - Mise en situation professionnelle\\ptut-home-trainer\\Home_Biking\\Gabarits\\WM\\210 Material Design Orange\\Material Design Orange_Pict_Delete_16_5@dpi1x.png", 0, 1, 1, null, null, null);

super.setStyleLibelleRepos(0xFFFFFF, creerPolice_GEN("Roboto", -8.000000, 0), 0, 0x222222);

super.setStyleLibelleSurvol(0xFFFFFF, creerPolice_GEN("Roboto", -8.000000, 0), 0, 0x222222);

super.setStyleLibelleEnfonce(0xFFFFFF, creerPolice_GEN("Roboto", -8.000000, 0), 0, 0x222222);

super.setCadreRepos(WDCadreFactory.creerCadre_GEN(2, 0x98FF, 0x187F, 0x98FF, 2.000000, 2.000000, 1, 1));

super.setCadreSurvol(WDCadreFactory.creerCadre_GEN(2, 0x98FF, 0x187F, 0xE8C6B0, 2.000000, 2.000000, 1, 1));

super.setCadreEnfonce(WDCadreFactory.creerCadre_GEN(2, 0x98FF, 0x187F, 0xE2E2E2, 2.000000, 2.000000, 1, 1));

super.setImageFond9Images(new int[] {1,2,1,2,2,2,1,2,1}, 8, 8, 8, 8);

super.setImageFond("C:\\Users\\axelm\\Desktop\\COURS IUT INFO\\Semestre 3\\M3302 - Projet tutore - Mise en situation professionnelle\\ptut-home-trainer\\Home_Biking\\Gabarits\\WM\\210 Material Design Orange\\Material Design Orange_Btn_Cancel@dpi1x.png?E5_3NP_8_8_8_8", 1, 0, 1, 1);

activerEcoute();
super.terminerInitialisation();
}

/**
 * Traitement: Initialisation de BTN_Supprimer ( ZR_REQ_Creation_Prog )
 */
public void init()
{
super.init();

// 
//MAP:20670c670f3bf9b2:0000000e:1:FEN_Creation_Prog.ZR_REQ_Creation_Prog.BTN_Supprimer:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDZR_REQ_Creation_Prog$GWDBTN_Supprimer:Initialisation de BTN_Supprimer ( ZR_REQ_Creation_Prog )
// SI gbEdition = Faux ALORS
//MAP:20670c670f3bf9b2:0000000e:2:FEN_Creation_Prog.ZR_REQ_Creation_Prog.BTN_Supprimer:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDZR_REQ_Creation_Prog$GWDBTN_Supprimer:Initialisation de BTN_Supprimer ( ZR_REQ_Creation_Prog )
if(vWD_gbEdition.opEgal(false))
{
// 	BTN_Supprimer.Visible=Faux
//MAP:20670c670f3bf9b2:0000000e:3:FEN_Creation_Prog.ZR_REQ_Creation_Prog.BTN_Supprimer:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDZR_REQ_Creation_Prog$GWDBTN_Supprimer:Initialisation de BTN_Supprimer ( ZR_REQ_Creation_Prog )
this.setProp(EWDPropriete.PROP_VISIBLE,false);

}
else
{
// 	BTN_Supprimer.Visible=Vrai
//MAP:20670c670f3bf9b2:0000000e:5:FEN_Creation_Prog.ZR_REQ_Creation_Prog.BTN_Supprimer:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDZR_REQ_Creation_Prog$GWDBTN_Supprimer:Initialisation de BTN_Supprimer ( ZR_REQ_Creation_Prog )
this.setProp(EWDPropriete.PROP_VISIBLE,true);

}

}




/**
 * Traitement: Clic sur BTN_Supprimer ( ZR_REQ_Creation_Prog )
 */
public void clicSurBoutonGauche()
{
super.clicSurBoutonGauche();

// 
//MAP:20670c670f3bf9b2:00000012:1:FEN_Creation_Prog.ZR_REQ_Creation_Prog.BTN_Supprimer:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDZR_REQ_Creation_Prog$GWDBTN_Supprimer:Clic sur BTN_Supprimer ( ZR_REQ_Creation_Prog )
// ToastAffiche(ZR_REQ_Creation_Prog)
//MAP:20670c670f3bf9b2:00000012:2:FEN_Creation_Prog.ZR_REQ_Creation_Prog.BTN_Supprimer:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDZR_REQ_Creation_Prog$GWDBTN_Supprimer:Clic sur BTN_Supprimer ( ZR_REQ_Creation_Prog )
WDAPIToast.toastAffiche(mWD_ZR_REQ_Creation_Prog.getString());

// HLitRecherchePremier(Creation_contenir, place, ZR_REQ_Creation_Prog.Place)
//MAP:20670c670f3bf9b2:00000012:4:FEN_Creation_Prog.ZR_REQ_Creation_Prog.BTN_Supprimer:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDZR_REQ_Creation_Prog$GWDBTN_Supprimer:Clic sur BTN_Supprimer ( ZR_REQ_Creation_Prog )
WDAPIHF.hLitRecherchePremier(WDAPIHF.getFichierSansCasseNiAccent("creation_contenir"),WDAPIHF.getRubriqueSansCasseNiAccent("place"),mWD_Place);

// HSupprime()
//MAP:20670c670f3bf9b2:00000012:5:FEN_Creation_Prog.ZR_REQ_Creation_Prog.BTN_Supprimer:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDZR_REQ_Creation_Prog$GWDBTN_Supprimer:Clic sur BTN_Supprimer ( ZR_REQ_Creation_Prog )
WDAPIHF.hSupprime();

// HLitSuivant()
//MAP:20670c670f3bf9b2:00000012:7:FEN_Creation_Prog.ZR_REQ_Creation_Prog.BTN_Supprimer:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDZR_REQ_Creation_Prog$GWDBTN_Supprimer:Clic sur BTN_Supprimer ( ZR_REQ_Creation_Prog )
WDAPIHF.hLitSuivant();

// TANTQUE HTrouve()=Vrai
//MAP:20670c670f3bf9b2:00000012:8:FEN_Creation_Prog.ZR_REQ_Creation_Prog.BTN_Supprimer:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDZR_REQ_Creation_Prog$GWDBTN_Supprimer:Clic sur BTN_Supprimer ( ZR_REQ_Creation_Prog )
while(WDAPIHF.hTrouve().opEgal(true))
{
// 	Creation_contenir.place -= 1
//MAP:20670c670f3bf9b2:00000012:9:FEN_Creation_Prog.ZR_REQ_Creation_Prog.BTN_Supprimer:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDZR_REQ_Creation_Prog$GWDBTN_Supprimer:Clic sur BTN_Supprimer ( ZR_REQ_Creation_Prog )
WDAPIHF.getFichierSansCasseNiAccent("creation_contenir").getRubriqueSansCasseNiAccent("place").setValeur(WDAPIHF.getFichierSansCasseNiAccent("creation_contenir").getRubriqueSansCasseNiAccent("place").opMoins(1));

// 	HModifie(Creation_contenir,hNumEnrEnCours)
//MAP:20670c670f3bf9b2:00000012:a:FEN_Creation_Prog.ZR_REQ_Creation_Prog.BTN_Supprimer:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDZR_REQ_Creation_Prog$GWDBTN_Supprimer:Clic sur BTN_Supprimer ( ZR_REQ_Creation_Prog )
WDAPIHF.hModifie(WDAPIHF.getFichierSansCasseNiAccent("creation_contenir"),(long)0);

// 	HLitSuivant()
//MAP:20670c670f3bf9b2:00000012:b:FEN_Creation_Prog.ZR_REQ_Creation_Prog.BTN_Supprimer:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDZR_REQ_Creation_Prog$GWDBTN_Supprimer:Clic sur BTN_Supprimer ( ZR_REQ_Creation_Prog )
WDAPIHF.hLitSuivant();

}

// ZoneRépétéeAffiche(ZR_REQ_Creation_Prog, taInit)
//MAP:20670c670f3bf9b2:00000012:e:FEN_Creation_Prog.ZR_REQ_Creation_Prog.BTN_Supprimer:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDZR_REQ_Creation_Prog$GWDBTN_Supprimer:Clic sur BTN_Supprimer ( ZR_REQ_Creation_Prog )
WDAPIZoneRepetee.zoneRepeteeAffiche(mWD_ZR_REQ_Creation_Prog,new WDChaineU("Reset"));

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurClic();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDBTN_Supprimer mWD_BTN_Supprimer = new GWDBTN_Supprimer();
/**
 * Initialise tous les champs de FEN_Creation_Prog.ZR_REQ_Creation_Prog
 */
public void initialiserSousObjets()
{
////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FEN_Creation_Prog.ZR_REQ_Creation_Prog
////////////////////////////////////////////////////////////////////////////
super.initialiserSousObjets();
mWD_nomExo.initialiserObjet();
super.ajouterChamp("nomExo",mWD_nomExo);
mWD_Place.initialiserObjet();
super.ajouterChamp("Place",mWD_Place);
mWD_BTN_Supprimer.initialiserObjet();
super.ajouterChamp("BTN_Supprimer",mWD_BTN_Supprimer);
creerAttributAuto();
}
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setRectCompPrincipal(0,0,317,40);
super.setQuid(3445901323126630137l);

super.setChecksum("936733032");

super.setNom("ZR_REQ_Creation_Prog");

super.setType(30);

super.setBulle("");

super.setLibelle("Zone répétée");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(0, 158);

super.setTailleInitiale(320, 244);

super.setValeurInitiale("");

super.setPlan(0);

super.setSourceRemplissage("fen_creation_prog_1$requete", "", "", true, "", false);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(8);

super.setAncrageInitial(8, 1000, 1000, 1000, 0, 0);

super.setNumTab(2);

super.setModeAscenseur(1, 1);

super.setModeSelection(5);

super.setSaisieEnCascade(false);

super.setLettreAppel(65535);

super.setEnregistrementSortieLigne(true);

super.setPersistant(false);

super.setParamAffichage(0, 0, 1, 317, 40);

super.setBtnEnrouleDeroule(true);

super.setScrollRapide(false, null);

super.setDeplacementParDnd(0);

super.setSwipe(0, "", false, false, "", false, false);

super.setRecyclageChamp(true);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x212121, creerPolice_GEN("Roboto", -8.000000, 0), -1, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1), 0, 0, 0, 0);

super.setStyleSeparateurVerticaux(false, 0xFFFFFFFF);

super.setStyleSeparateurHorizontaux(0, 0xFFFFFFFF);

super.setDessinerLigneVide(false);

super.setCouleurCellule(0xFFFFFFFF, 0xFFFFFFFF, 0x212121, 0x80CCFF, 0xFFFFFF);

super.setImagePlusMoins("C:\\Users\\axelm\\Desktop\\COURS IUT INFO\\Semestre 3\\M3302 - Projet tutore - Mise en situation professionnelle\\ptut-home-trainer\\Home_Biking\\Gabarits\\WM\\210 Material Design Orange\\Material Design Orange_Break_Pict@dpi1x.png?E2_4O");

activerEcoute();
initialiserSousObjets();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDZR_REQ_Creation_Prog mWD_ZR_REQ_Creation_Prog;

/**
 * COMBO_Difficulte
 */
class GWDCOMBO_Difficulte extends WDCombo
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°9 de FEN_Creation_Prog.COMBO_Difficulte
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setRectLibelle(0,2,77,24);
super.setRectCompPrincipal(77,2,171,24);
super.setQuid(3449149748521563194l);

super.setChecksum("846193567");

super.setNom("COMBO_Difficulte");

super.setType(10002);

super.setBulle("");

super.setLibelle("Difficulté :");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(9, 427);

super.setTailleInitiale(248, 28);

super.setValeurInitiale("");

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setContenuInitial("Très facile\r\nFacile\r\nMoyen\r\nDifficile\r\nTrès difficile");

super.setTriee(false);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(9);

super.setAncrageInitial(1, 1000, 1000, 1000, 1000, 0);

super.setNumTab(3);

super.setLettreAppel(65535);

super.setRetourneValeurProgrammation(false);

super.setPersistant(false);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -2, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN(2, 0x757575, 0x0, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1));

super.setStyleElement(0x222222, 0xFFFFFFFF, creerPolice_GEN("Roboto", -8.000000, 0), 48);

super.setStyleSelection(0x222222, 0xE2E2E2, creerPolice_GEN("Roboto", -8.000000, 0));

super.setStyleBouton(WDCadreFactory.creerCadre_GEN(2, 0xE1E1E1, 0x616161, 0xF0F0F0, 2.000000, 2.000000, 1, 1), 0x222222);

activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDCOMBO_Difficulte mWD_COMBO_Difficulte;

/**
 * IMG_less_select
 */
class GWDIMG_less_select extends WDChampImage
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°10 de FEN_Creation_Prog.IMG_less_select
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(2334406225427820758l);

super.setChecksum("612071726");

super.setNom("IMG_less_select");

super.setType(30001);

super.setBulle("");

super.setLibelle("");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setNavigable(false);

super.setEtatInitial(0);

super.setPositionInitiale(864, 315);

super.setTailleInitiale(45, 68);

super.setValeurInitiale("C:\\Users\\axelm\\Desktop\\COURS IUT INFO\\Semestre 3\\M3302 - Projet tutore - Mise en situation professionnelle\\ptut-home-trainer\\Home_Biking\\delete exercise detelting state.png");

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(10);

super.setAncrageInitial(0, 1000, 1000, 1000, 1000, 0);

super.setTransparence(1);

super.setParamImage(2097158, 0, true, 100);

super.setSymetrie(0);

super.setZoneClicage(true);

super.setPCodeMultitouch(false);

super.setChargementEnTacheDeFond(false);

super.setOrientationExif(false);

super.setParamAnimation(1, 1, false, 300, true, false);

super.setAnimationInitiale(false);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -1, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDIMG_less_select mWD_IMG_less_select;

/**
 * IMG_less_exo1
 */
class GWDIMG_less_exo1 extends WDChampImage
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°11 de FEN_Creation_Prog.IMG_less_exo1
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(2334413952084567258l);

super.setChecksum("622654521");

super.setNom("IMG_less_exo1");

super.setType(30001);

super.setBulle("");

super.setLibelle("");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setNavigable(false);

super.setEtatInitial(0);

super.setPositionInitiale(876, 400);

super.setTailleInitiale(27, 27);

super.setValeurInitiale("C:\\Users\\axelm\\Desktop\\COURS IUT INFO\\Semestre 3\\M3302 - Projet tutore - Mise en situation professionnelle\\ptut-home-trainer\\Home_Biking\\delete exercise initial state.png");

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(11);

super.setAncrageInitial(0, 1000, 1000, 1000, 1000, 0);

super.setTransparence(1);

super.setParamImage(2097158, 0, true, 100);

super.setSymetrie(0);

super.setZoneClicage(true);

super.setPCodeMultitouch(false);

super.setChargementEnTacheDeFond(false);

super.setOrientationExif(false);

super.setParamAnimation(1, 1, false, 300, true, false);

super.setAnimationInitiale(false);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -1, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

/**
 * Traitement: Initialisation de IMG_less_exo1
 */
public void init()
{
super.init();

// IMG_less_exo.Visible=Faux
//MAP:2065811f04b764da:0000000e:1:FEN_Creation_Prog.IMG_less_exo1:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_less_exo1:Initialisation de IMG_less_exo1
// IMG_less_exo.Visible=Faux
//MAP:2065811f04b764da:0000000e:1:FEN_Creation_Prog.IMG_less_exo1:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_less_exo1:Initialisation de IMG_less_exo1
mWD_IMG_less_exo.setProp(EWDPropriete.PROP_VISIBLE,false);

}




/**
 * Traitement: Clic sur IMG_less_exo1
 */
public void clicSurBoutonGauche()
{
super.clicSurBoutonGauche();

// gbEdition = PAS gbEdition
//MAP:2065811f04b764da:00000012:1:FEN_Creation_Prog.IMG_less_exo1:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_less_exo1:Clic sur IMG_less_exo1

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables locales au traitement
// (En WLangage les variables sont encore visibles après la fin du bloc dans lequel elles sont déclarées)
////////////////////////////////////////////////////////////////////////////
WDObjet vWD_i = new WDEntier4();



// gbEdition = PAS gbEdition
//MAP:2065811f04b764da:00000012:1:FEN_Creation_Prog.IMG_less_exo1:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_less_exo1:Clic sur IMG_less_exo1
vWD_gbEdition.setValeur((!vWD_gbEdition.getBoolean()));

// SI gbEdition = Vrai ALORS
//MAP:2065811f04b764da:00000012:3:FEN_Creation_Prog.IMG_less_exo1:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_less_exo1:Clic sur IMG_less_exo1
if(vWD_gbEdition.opEgal(true))
{
// 	IMG_less_exo=IMG_less_select
//MAP:2065811f04b764da:00000012:4:FEN_Creation_Prog.IMG_less_exo1:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_less_exo1:Clic sur IMG_less_exo1
mWD_IMG_less_exo.setValeur(mWD_IMG_less_select);

}
else
{
// 	IMG_less_exo = IMG_less_exo
//MAP:2065811f04b764da:00000012:6:FEN_Creation_Prog.IMG_less_exo1:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_less_exo1:Clic sur IMG_less_exo1
mWD_IMG_less_exo.setValeur(mWD_IMG_less_exo);

}

// IMG_Button_Validate..Visible=PAS IMG_Button_Validate..Visible
//MAP:2065811f04b764da:00000012:9:FEN_Creation_Prog.IMG_less_exo1:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_less_exo1:Clic sur IMG_less_exo1
mWD_IMG_Button_Validate.setProp(EWDPropriete.PROP_VISIBLE,(!mWD_IMG_Button_Validate.getProp(EWDPropriete.PROP_VISIBLE).getBoolean()));

// IMG_add_exo..Visible		=PAS IMG_add_exo..Visible
//MAP:2065811f04b764da:00000012:a:FEN_Creation_Prog.IMG_less_exo1:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_less_exo1:Clic sur IMG_less_exo1
mWD_IMG_add_exo.setProp(EWDPropriete.PROP_VISIBLE,(!mWD_IMG_add_exo.getProp(EWDPropriete.PROP_VISIBLE).getBoolean()));

// i est un entier
//MAP:2065811f04b764da:00000012:c:FEN_Creation_Prog.IMG_less_exo1:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_less_exo1:Clic sur IMG_less_exo1


// i=1
//MAP:2065811f04b764da:00000012:d:FEN_Creation_Prog.IMG_less_exo1:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_less_exo1:Clic sur IMG_less_exo1
vWD_i.setValeur(1);

// POUR CHAQUE LIGNE DE ZR_REQ_Creation_Prog
//MAP:2065811f04b764da:00000012:e:FEN_Creation_Prog.IMG_less_exo1:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_less_exo1:Clic sur IMG_less_exo1
IWDParcours parcours3 = null;
try
{
parcours3 = WDParcoursChamp.pourTout(mWD_ZR_REQ_Creation_Prog, 0x2);
while(parcours3.testParcours())
{
// 	ZR_REQ_Creation_Prog[i].BTN_Supprimer.Visible= PAS ZR_REQ_Creation_Prog[i].BTN_Supprimer.Visible
//MAP:2065811f04b764da:00000012:f:FEN_Creation_Prog.IMG_less_exo1:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_less_exo1:Clic sur IMG_less_exo1
mWD_ZR_REQ_Creation_Prog.get(vWD_i).get("BTN_Supprimer").setProp(EWDPropriete.PROP_VISIBLE,(!mWD_ZR_REQ_Creation_Prog.get(vWD_i).get("BTN_Supprimer").getProp(EWDPropriete.PROP_VISIBLE).getBoolean()));

// 	i++
//MAP:2065811f04b764da:00000012:10:FEN_Creation_Prog.IMG_less_exo1:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog$GWDIMG_less_exo1:Clic sur IMG_less_exo1
vWD_i.opInc();

}

}
finally
{
if(parcours3 != null)
{
parcours3.finParcours();
}
}


}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurClic();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDIMG_less_exo1 mWD_IMG_less_exo1;

/**
 * Traitement: Déclarations globales de FEN_Creation_Prog
 */
public void declarerGlobale(WDObjet[] WD_tabParam)
{
// PROCEDURE MaFenêtre()
//MAP:2fd1ef1f07c43282:00000000:1:FEN_Creation_Prog:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog:Déclarations globales de FEN_Creation_Prog
super.declarerGlobale(WD_tabParam, 0, 0);
int WD_ntabParamLen = 0;
if(WD_tabParam!=null) WD_ntabParamLen = WD_tabParam.length;


// gbEdition est un booléen
//MAP:2fd1ef1f07c43282:00000000:2:FEN_Creation_Prog:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog:Déclarations globales de FEN_Creation_Prog
vWD_gbEdition = new WDBooleen();

super.ajouterVariableGlobale("gbEdition",vWD_gbEdition);



}




/**
 * Traitement: Fin d'initialisation de FEN_Creation_Prog
 */
public void init()
{
super.init();

// gbEdition=Faux
//MAP:2fd1ef1f07c43282:00000022:1:FEN_Creation_Prog:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog:Fin d'initialisation de FEN_Creation_Prog
// gbEdition=Faux
//MAP:2fd1ef1f07c43282:00000022:1:FEN_Creation_Prog:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog:Fin d'initialisation de FEN_Creation_Prog
vWD_gbEdition.setValeur(false);

// ZoneRépétéeAffiche(ZR_REQ_Creation_Prog,taInit)
//MAP:2fd1ef1f07c43282:00000022:2:FEN_Creation_Prog:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog:Fin d'initialisation de FEN_Creation_Prog
WDAPIZoneRepetee.zoneRepeteeAffiche(mWD_ZR_REQ_Creation_Prog,new WDChaineU("Reset"));

}




/**
 * Traitement: Fermeture d'une fenêtre fille de FEN_Creation_Prog
 */
public void fermetureFenetreFille()
{
super.fermetureFenetreFille();

// 
//MAP:2fd1ef1f07c43282:000000b1:1:FEN_Creation_Prog:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog:Fermeture d'une fenêtre fille de FEN_Creation_Prog
// SI HNbEnr(Creation_contenir)>=2 ALORS
//MAP:2fd1ef1f07c43282:000000b1:2:FEN_Creation_Prog:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog:Fermeture d'une fenêtre fille de FEN_Creation_Prog
if(WDAPIHF.hNbEnr(WDAPIHF.getFichierSansCasseNiAccent("creation_contenir")).opSupEgal(2))
{
// 	IMG_Button_Validate.Visible=Vrai
//MAP:2fd1ef1f07c43282:000000b1:3:FEN_Creation_Prog:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog:Fermeture d'une fenêtre fille de FEN_Creation_Prog
mWD_IMG_Button_Validate.setProp(EWDPropriete.PROP_VISIBLE,true);

}
else
{
// 	SI HNbEnr(Creation_contenir)>=1 ALORS
//MAP:2fd1ef1f07c43282:000000b1:5:FEN_Creation_Prog:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog:Fermeture d'une fenêtre fille de FEN_Creation_Prog
if(WDAPIHF.hNbEnr(WDAPIHF.getFichierSansCasseNiAccent("creation_contenir")).opSupEgal(1))
{
// 		IMG_less_exo.Visible=Vrai
//MAP:2fd1ef1f07c43282:000000b1:6:FEN_Creation_Prog:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog:Fermeture d'une fenêtre fille de FEN_Creation_Prog
mWD_IMG_less_exo.setProp(EWDPropriete.PROP_VISIBLE,true);

}
else
{
// 		IMG_less_exo.Visible=Faux
//MAP:2fd1ef1f07c43282:000000b1:8:FEN_Creation_Prog:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog:Fermeture d'une fenêtre fille de FEN_Creation_Prog
mWD_IMG_less_exo.setProp(EWDPropriete.PROP_VISIBLE,false);

}

// 	IMG_Button_Validate.Visible=Faux
//MAP:2fd1ef1f07c43282:000000b1:a:FEN_Creation_Prog:com.logicorp.home_biking.wdgen.GWDFFEN_Creation_Prog:Fermeture d'une fenêtre fille de FEN_Creation_Prog
mWD_IMG_Button_Validate.setProp(EWDPropriete.PROP_VISIBLE,false);

}

}




// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
 public WDObjet vWD_gbEdition = WDVarNonAllouee.ref;
////////////////////////////////////////////////////////////////////////////
// Création des champs de la fenêtre FEN_Creation_Prog
////////////////////////////////////////////////////////////////////////////
protected void creerChamps()
{
mWD_IMG_Button_Back = new GWDIMG_Button_Back();
mWD_LIB_Nom_du_Programme = new GWDLIB_Nom_du_Programme();
mWD_SAI_nomprog = new GWDSAI_nomprog();
mWD_LIB_exo = new GWDLIB_exo();
mWD_IMG_Button_Validate = new GWDIMG_Button_Validate();
mWD_IMG_add_exo = new GWDIMG_add_exo();
mWD_IMG_less_exo = new GWDIMG_less_exo();
mWD_ZR_REQ_Creation_Prog = new GWDZR_REQ_Creation_Prog();
mWD_COMBO_Difficulte = new GWDCOMBO_Difficulte();
mWD_IMG_less_select = new GWDIMG_less_select();
mWD_IMG_less_exo1 = new GWDIMG_less_exo1();

}
////////////////////////////////////////////////////////////////////////////
// Initialisation de la fenêtre FEN_Creation_Prog
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.setQuid(3445798106468463234l);

super.setChecksum("938335898");

super.setNom("FEN_Creation_Prog");

super.setType(1);

super.setBulle("");

super.setMenuContextuelSysteme();

super.setCurseurSouris(0);

super.setNote("", "");

super.setCouleur(0x0);

super.setCouleurFond(0xFFFFFF);

super.setPositionInitiale(0, 0);

super.setTailleInitiale(320, 543);

super.setTitre("Creation_Prog");

super.setTailleMin(-1, -1);

super.setTailleMax(20000, 20000);

super.setVisibleInitial(true);

super.setPositionFenetre(2);

super.setPersistant(true);

super.setGFI(true);

super.setAnimationFenetre(0);

super.setImageFond("", 1, 0, 1);

super.setCouleurTexteAutomatique(0x303030);

super.setCouleurBarreSysteme(0x80FF);


activerEcoute();

////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FEN_Creation_Prog
////////////////////////////////////////////////////////////////////////////
mWD_IMG_Button_Back.initialiserObjet();
super.ajouter("IMG_Button_Back", mWD_IMG_Button_Back);
mWD_LIB_Nom_du_Programme.initialiserObjet();
super.ajouter("LIB_Nom_du_Programme", mWD_LIB_Nom_du_Programme);
mWD_SAI_nomprog.initialiserObjet();
super.ajouter("SAI_nomprog", mWD_SAI_nomprog);
mWD_LIB_exo.initialiserObjet();
super.ajouter("LIB_exo", mWD_LIB_exo);
mWD_IMG_Button_Validate.initialiserObjet();
super.ajouter("IMG_Button_Validate", mWD_IMG_Button_Validate);
mWD_IMG_add_exo.initialiserObjet();
super.ajouter("IMG_add_exo", mWD_IMG_add_exo);
mWD_IMG_less_exo.initialiserObjet();
super.ajouter("IMG_less_exo", mWD_IMG_less_exo);
mWD_ZR_REQ_Creation_Prog.initialiserObjet();
super.ajouter("ZR_REQ_Creation_Prog", mWD_ZR_REQ_Creation_Prog);
mWD_COMBO_Difficulte.initialiserObjet();
super.ajouter("COMBO_Difficulte", mWD_COMBO_Difficulte);
mWD_IMG_less_select.initialiserObjet();
super.ajouter("IMG_less_select", mWD_IMG_less_select);
mWD_IMG_less_exo1.initialiserObjet();
super.ajouter("IMG_less_exo1", mWD_IMG_less_exo1);

super.terminerInitialisation();
}

////////////////////////////////////////////////////////////////////////////
public boolean isUniteAffichageLogique()
{
return false;
}

public WDProjet getProjet()
{
return GWDPHome_Biking.getInstance();
}

public IWDEnsembleElement getEnsemble()
{
return GWDPHome_Biking.getInstance();
}
public int getModeContexteHF()
{
return 1;
}
/**
* Retourne le mode d'affichage de l'ActionBar de la fenêtre.
*/
public int getModeActionBar()
{
return 0;
}
/**
* Retourne vrai si la fenêtre est maximisée, faux sinon.
*/
public boolean isMaximisee()
{
return true;
}
/**
* Retourne vrai si la fenêtre a une barre de titre, faux sinon.
*/
public boolean isAvecBarreDeTitre()
{
return false;
}
/**
* Retourne le mode d'affichage de la barre système de la fenêtre.
*/
public int getModeBarreSysteme()
{
return 1;
}
/**
* Retourne vrai si la fenêtre est munie d'ascenseurs automatique, faux sinon.
*/
public boolean isAvecAscenseurAuto()
{
return false;
}
/**
* Retourne Vrai si on doit appliquer un theme "dark" (sombre) ou Faux si on doit appliquer "light" (clair) à la fenêtre.
* Ce choix se base sur la couleur du libellé par défaut dans le gabarit de la fenêtre.
*/
public boolean isThemeDark()
{
return false;
}
/**
* Retourne vrai si l'option de masquage automatique de l'ActionBar lorsqu'on scrolle dans un champ de la fenêtre a été activée.
*/
public boolean isMasquageAutomatiqueActionBar()
{
return false;
}
public static class WDActiviteFenetre extends WDActivite
{
protected WDFenetre getFenetre()
{
return GWDPHome_Biking.getInstance().mWD_FEN_Creation_Prog;
}
}
/**
* Retourne le nom du gabarit associée à la fenêtre.
*/
public String getNomGabarit()
{
return "210 MATERIAL DESIGN ORANGE#WM";
}
}
