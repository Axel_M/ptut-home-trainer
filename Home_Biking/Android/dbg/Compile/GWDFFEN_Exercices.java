/**
 * Code généré par WINDEV Mobile - NE PAS MODIFIER !
 * Objet WINDEV Mobile : Fenêtre
 * Classe Android : FEN_Exercices
 * Date : 02/04/2021 14:52:47
 * Version de wdjava.dll  : 25.0.221.6
 */


package com.logicorp.home_biking.wdgen;


import com.logicorp.home_biking.*;
import fr.pcsoft.wdjava.core.types.*;
import fr.pcsoft.wdjava.core.*;
import fr.pcsoft.wdjava.ui.champs.fenetre.*;
import fr.pcsoft.wdjava.ui.champs.image.*;
import fr.pcsoft.wdjava.ui.cadre.*;
import fr.pcsoft.wdjava.api.*;
import fr.pcsoft.wdjava.ui.champs.libelle.*;
import fr.pcsoft.wdjava.ui.champs.saisie.*;
import fr.pcsoft.wdjava.ui.champs.zr.*;
import fr.pcsoft.wdjava.core.application.*;
import fr.pcsoft.wdjava.ui.activite.*;
/*Imports trouvés dans le code WL*/
/*Fin Imports trouvés dans le code WL*/



public class GWDFFEN_Exercices extends WDFenetre
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs de FEN_Exercices
////////////////////////////////////////////////////////////////////////////

/**
 * IMG_Button_Back
 */
class GWDIMG_Button_Back extends WDChampImage
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FEN_Exercices.IMG_Button_Back
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3445791487892041341l);

super.setChecksum("900769239");

super.setNom("IMG_Button_Back");

super.setType(30001);

super.setBulle("");

super.setLibelle("");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setNavigable(false);

super.setEtatInitial(0);

super.setPositionInitiale(8, 0);

super.setTailleInitiale(52, 52);

super.setValeurInitiale("C:\\Users\\axelm\\Desktop\\COURS IUT INFO\\Semestre 3\\M3302 - Projet tutore - Mise en situation professionnelle\\ptut-home-trainer\\Home_Biking\\Bouton_previous_page.png");

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(1);

super.setAncrageInitial(0, 1000, 1000, 1000, 1000, 0);

super.setTransparence(1);

super.setParamImage(2097158, 0, true, 100);

super.setSymetrie(0);

super.setZoneClicage(true);

super.setPCodeMultitouch(false);

super.setChargementEnTacheDeFond(false);

super.setOrientationExif(false);

super.setParamAnimation(1, 1, false, 300, true, false);

super.setAnimationInitiale(false);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -1, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

/**
 * Traitement: Clic sur IMG_Button_Back
 */
public void clicSurBoutonGauche()
{
super.clicSurBoutonGauche();

// Ferme()
//MAP:2fd1e91a05deae7d:00000012:1:FEN_Exercices.IMG_Button_Back:com.logicorp.home_biking.wdgen.GWDFFEN_Exercices$GWDIMG_Button_Back:Clic sur IMG_Button_Back
// Ferme()
//MAP:2fd1e91a05deae7d:00000012:1:FEN_Exercices.IMG_Button_Back:com.logicorp.home_biking.wdgen.GWDFFEN_Exercices$GWDIMG_Button_Back:Clic sur IMG_Button_Back
WDAPIFenetre.ferme();

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurClic();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDIMG_Button_Back mWD_IMG_Button_Back;

/**
 * LIB_titre
 */
class GWDLIB_titre extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°2 de FEN_Exercices.LIB_titre
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3445791487892303500l);

super.setChecksum("901029118");

super.setNom("LIB_titre");

super.setType(3);

super.setBulle("");

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("Exercices");

super.setNote("", "");

super.setCurseurSouris(0);

super.setEtatInitial(0);

super.setPositionInitiale(154, 0);

super.setTailleInitiale(158, 39);

super.setPlan(0);

super.setCadrageHorizontal(2);

super.setCadrageVertical(1);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(2);

super.setAncrageInitial(4, 1000, 1000, 1000, 1000, 0);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, 0xFFFFFFFF, creerPolice_GEN("Roboto", -16.000000, 0), 3, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_titre mWD_LIB_titre;

/**
 * SAI_Recherche_Programme
 */
class GWDSAI_Recherche_Programme extends WDChampSaisieSimple
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°3 de FEN_Exercices.SAI_Recherche_Programme
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setRectLibelle(0,2,56,23);
super.setRectCompPrincipal(56,2,162,23);
super.setQuid(3445791487892434572l);

super.setChecksum("901159734");

super.setNom("SAI_Recherche_Programme");

super.setType(20001);

super.setBulle("");

super.setLibelle("");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setTaille(0);

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(94, 39);

super.setTailleInitiale(218, 27);

super.setValeurInitiale("");

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setMotDePasse(false);

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(3);

super.setAncrageInitial(8, 1000, 1000, 1000, 1000, 0);

super.setEllipse(0);

super.setIndication("");

super.setNumTab(1);

super.setModeAscenseur(2, 2);

super.setEffacementAutomatique(true);

super.setFinSaisieAutomatique(false);

super.setLettreAppel(65535);

super.setSelectionEnAffichage(true);

super.setPersistant(false);

super.setClavierEnSaisie(true);

super.setMasqueAffichage(new WDChaineU(""));

super.setParamBtnActionClavier(0, "");

super.setRetraitGauche(2);

super.setMiseABlancSiZero(true);

super.setVerifieOrthographe(true);

super.setTauxParallaxe(0, 0);

super.setBoutonSuppression(0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -2, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN(2, 0x757575, 0x0, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1));

super.setStyleSaisie(0x222222, creerPolice_GEN("Roboto", -8.000000, 0));

super.setStyleTexteIndication(0x8E8E8F, creerPolice_GEN("Roboto", -8.000000, 0), 0);

super.setStyleJeton(WDCadreFactory.creerCadre_GEN(2, 0x98FF, 0x187F, 0x98FF, 4.000000, 4.000000, 1, 1), 0xFFFFFF, "", 1);

activerEcoute();
super.terminerInitialisation();
}

/**
 * Traitement: Initialisation de SAI_Recherche_Programme
 */
public void init()
{
super.init();

// HExécuteRequête(FEN_Exercices_1$Requête,hRequêteDéfaut,"")
//MAP:2fd1e91a05e4ae8c:0000000e:1:FEN_Exercices.SAI_Recherche_Programme:com.logicorp.home_biking.wdgen.GWDFFEN_Exercices$GWDSAI_Recherche_Programme:Initialisation de SAI_Recherche_Programme
// HExécuteRequête(FEN_Exercices_1$Requête,hRequêteDéfaut,"")
//MAP:2fd1e91a05e4ae8c:0000000e:1:FEN_Exercices.SAI_Recherche_Programme:com.logicorp.home_biking.wdgen.GWDFFEN_Exercices$GWDSAI_Recherche_Programme:Initialisation de SAI_Recherche_Programme
WDAPIHF.hExecuteRequete(WDAPIHF.getRequeteSansCasseNiAccent("fen_exercices_1$requete"),new WDEntier4(0),new WDObjet[] {new WDChaineU("")} );

// ZoneRépétéeAffiche(ZR_Exercices)
//MAP:2fd1e91a05e4ae8c:0000000e:2:FEN_Exercices.SAI_Recherche_Programme:com.logicorp.home_biking.wdgen.GWDFFEN_Exercices$GWDSAI_Recherche_Programme:Initialisation de SAI_Recherche_Programme
WDAPIZoneRepetee.zoneRepeteeAffiche(mWD_ZR_Exercices);

}




/**
 * Traitement: A chaque modification de SAI_Recherche_Programme
 */
public void modification()
{
super.modification();

// HExécuteRequête(FEN_Exercices_1$Requête,hRequêteDéfaut,SAI_Recherche_Programme)
//MAP:2fd1e91a05e4ae8c:00000011:1:FEN_Exercices.SAI_Recherche_Programme:com.logicorp.home_biking.wdgen.GWDFFEN_Exercices$GWDSAI_Recherche_Programme:A chaque modification de SAI_Recherche_Programme
// HExécuteRequête(FEN_Exercices_1$Requête,hRequêteDéfaut,SAI_Recherche_Programme)
//MAP:2fd1e91a05e4ae8c:00000011:1:FEN_Exercices.SAI_Recherche_Programme:com.logicorp.home_biking.wdgen.GWDFFEN_Exercices$GWDSAI_Recherche_Programme:A chaque modification de SAI_Recherche_Programme
WDAPIHF.hExecuteRequete(WDAPIHF.getRequeteSansCasseNiAccent("fen_exercices_1$requete"),new WDEntier4(0),new WDObjet[] {this} );

// ZoneRépétéeAffiche(ZR_Exercices)
//MAP:2fd1e91a05e4ae8c:00000011:2:FEN_Exercices.SAI_Recherche_Programme:com.logicorp.home_biking.wdgen.GWDFFEN_Exercices$GWDSAI_Recherche_Programme:A chaque modification de SAI_Recherche_Programme
WDAPIZoneRepetee.zoneRepeteeAffiche(mWD_ZR_Exercices);

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurModification();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDSAI_Recherche_Programme mWD_SAI_Recherche_Programme;

/**
 * ZR_Exercices
 */
class GWDZR_Exercices extends WDZoneRepeteeFichierEnMemoire
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°4 de FEN_Exercices.ZR_Exercices
////////////////////////////////////////////////////////////////////////////

/**
 * Nom
 */
class GWDNom extends WDChampSaisieSimple
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FEN_Exercices.ZR_Exercices.Nom
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setRectCompPrincipal(0,2,259,25);
super.setQuid(3445791487893155468l);

super.setChecksum("901880630");

super.setNom("Nom");

super.setType(20001);

super.setBulle("");

super.setLibelle("Nom");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setTaille(25);

super.setNavigable(false);

super.setEtatInitial(1);

super.setPositionInitiale(8, 8);

super.setTailleInitiale(259, 29);

super.setValeurInitiale("");

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setMotDePasse(false);

super.setLiaisonFichier("fen_exercices_1$requete", "nom");

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(1);

super.setAncrageInitial(8, 1000, 1000, 1000, 1000, 0);

super.setEllipse(0);

super.setIndication("");

super.setNumTab(-1);

super.setModeAscenseur(2, 2);

super.setEffacementAutomatique(false);

super.setFinSaisieAutomatique(false);

super.setLettreAppel(65535);

super.setSelectionEnAffichage(false);

super.setPersistant(false);

super.setClavierEnSaisie(false);

super.setMasqueAffichage(new WDChaineU(""));

super.setParamBtnActionClavier(0, "");

super.setRetraitGauche(2);

super.setMiseABlancSiZero(true);

super.setVerifieOrthographe(false);

super.setTauxParallaxe(0, 0);

super.setBoutonSuppression(0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -1, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN(1, 0x757575, 0x0, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1));

super.setStyleSaisie(0x222222, creerPolice_GEN("Roboto", -8.000000, 0));

super.setStyleTexteIndication(0x8E8E8F, creerPolice_GEN("Roboto", -8.000000, 0), 0);

super.setStyleJeton(WDCadreFactory.creerCadre_GEN(2, 0x98FF, 0x187F, 0x98FF, 4.000000, 4.000000, 1, 1), 0xFFFFFF, "", 1);

activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDNom mWD_Nom = new GWDNom();

/**
 * Description
 */
class GWDDescription extends WDChampSaisieMultiLigne
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°2 de FEN_Exercices.ZR_Exercices.Description
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setRectCompPrincipal(0,2,300,97);
super.setQuid(3445791487893221004l);

super.setChecksum("901954024");

super.setNom("Description");

super.setType(20001);

super.setBulle("");

super.setLibelle("Description");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setTaille(255);

super.setNavigable(true);

super.setEtatInitial(1);

super.setPositionInitiale(8, 38);

super.setTailleInitiale(300, 101);

super.setValeurInitiale("");

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setMotDePasse(false);

super.setLiaisonFichier("fen_exercices_1$requete", "description");

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(2);

super.setAncrageInitial(8, 1000, 1000, 1000, 1000, 0);

super.setEllipse(0);

super.setIndication("");

super.setNumTab(1);

super.setModeAscenseur(2, 0);

super.setEffacementAutomatique(false);

super.setFinSaisieAutomatique(false);

super.setLettreAppel(65535);

super.setSelectionEnAffichage(false);

super.setPersistant(false);

super.setClavierEnSaisie(false);

super.setMasqueAffichage(new WDChaineU(""));

super.setParamBtnActionClavier(0, "");

super.setRetraitGauche(2);

super.setMiseABlancSiZero(true);

super.setVerifieOrthographe(false);

super.setTauxParallaxe(0, 0);

super.setBoutonSuppression(0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -1, 0, 0xFFFFFF);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xFFFFFF, 0x7F7F7F, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN(1, 0xFFFFFF, 0x7F7F7F, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1));

super.setStyleSaisie(0x222222, creerPolice_GEN("Roboto", -7.000000, 0));

super.setStyleJeton(WDCadreFactory.creerCadre_GEN(2, 0x98FF, 0x187F, 0x98FF, 4.000000, 4.000000, 1, 1), 0xFFFFFF, "", 1);

activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDDescription mWD_Description = new GWDDescription();

/**
 * LIB_SansNom1
 */
class GWDLIB_SansNom1 extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°3 de FEN_Exercices.ZR_Exercices.LIB_SansNom1
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3445792900945654786l);

super.setChecksum("910140349");

super.setNom("LIB_SansNom1");

super.setType(3);

super.setBulle("");

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("Libellé");

super.setNote("", "");

super.setCurseurSouris(0);

super.setEtatInitial(1);

super.setPositionInitiale(8, 38);

super.setTailleInitiale(301, 101);

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(3);

super.setAncrageInitial(8, 1000, 1000, 1000, 1000, 0);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x222222, 0xFFFFFFFF, creerPolice_GEN("Roboto", -8.000000, 0), -1, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(2, 0xE2E2E2, 0x626262, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_SansNom1 mWD_LIB_SansNom1 = new GWDLIB_SansNom1();

/**
 * LIB_Exo
 */
class GWDLIB_Exo extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°4 de FEN_Exercices.ZR_Exercices.LIB_Exo
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3450230388060108168l);

super.setChecksum("829843746");

super.setNom("LIB_Exo");

super.setType(3);

super.setBulle("");

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("Libellé");

super.setNote("", "");

super.setCurseurSouris(0);

super.setEtatInitial(0);

super.setPositionInitiale(610, 73);

super.setTailleInitiale(120, 19);

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setCadrageVertical(0);

super.setLiaisonFichier("fen_exercices_1$requete", "id_exercice");

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(false);

super.setAltitude(4);

super.setAncrageInitial(0, 1000, 1000, 1000, 1000, 0);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, 0xFFFFFFFF, creerPolice_GEN("Roboto", -8.000000, 0), 3, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_Exo mWD_LIB_Exo = new GWDLIB_Exo();
/**
 * Initialise tous les champs de FEN_Exercices.ZR_Exercices
 */
public void initialiserSousObjets()
{
////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FEN_Exercices.ZR_Exercices
////////////////////////////////////////////////////////////////////////////
super.initialiserSousObjets();
mWD_Nom.initialiserObjet();
super.ajouterChamp("Nom",mWD_Nom);
mWD_Description.initialiserObjet();
super.ajouterChamp("Description",mWD_Description);
mWD_LIB_SansNom1.initialiserObjet();
super.ajouterChamp("LIB_SansNom1",mWD_LIB_SansNom1);
mWD_LIB_Exo.initialiserObjet();
super.ajouterChamp("LIB_Exo",mWD_LIB_Exo);
creerAttributAuto();
}
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setRectCompPrincipal(0,0,317,140);
super.setQuid(3445791487893089932l);

super.setChecksum("901827862");

super.setNom("ZR_Exercices");

super.setType(30);

super.setBulle("");

super.setLibelle("Zone répétée");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setNavigable(true);

super.setEtatInitial(1);

super.setPositionInitiale(0, 74);

super.setTailleInitiale(320, 469);

super.setValeurInitiale("");

super.setPlan(0);

super.setSourceRemplissage("fen_exercices_1$requete", "", "id_exercice", true, "", false);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(4);

super.setAncrageInitial(10, 1000, 1000, 1000, 1000, 0);

super.setNumTab(2);

super.setModeAscenseur(1, 1);

super.setModeSelection(99);

super.setSaisieEnCascade(false);

super.setLettreAppel(65535);

super.setEnregistrementSortieLigne(true);

super.setPersistant(false);

super.setParamAffichage(0, 0, 1, 317, 140);

super.setBtnEnrouleDeroule(true);

super.setScrollRapide(false, null);

super.setDeplacementParDnd(0);

super.setSwipe(0, "", false, false, "", false, false);

super.setRecyclageChamp(true);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x212121, creerPolice_GEN("Roboto", -8.000000, 0), -1, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1), 0, 0, 0, 0);

super.setStyleSeparateurVerticaux(false, 0xFFFFFFFF);

super.setStyleSeparateurHorizontaux(0, 0xFFFFFFFF);

super.setDessinerLigneVide(false);

super.setCouleurCellule(0xFFFFFFFF, 0xFFFFFFFF, 0x212121, 0x80CCFF, 0xFFFFFF);

super.setImagePlusMoins("C:\\Users\\axelm\\Desktop\\COURS IUT INFO\\Semestre 3\\M3302 - Projet tutore - Mise en situation professionnelle\\ptut-home-trainer\\Home_Biking\\Gabarits\\WM\\210 Material Design Orange\\Material Design Orange_Break_Pict@dpi1x.png?E2_4O");

activerEcoute();
initialiserSousObjets();
super.terminerInitialisation();
}

/**
 * Traitement: Sélection d'une ligne de ZR_Exercices
 */
public void selectionLigne()
{
super.selectionLigne();

// SI gbCreation = VRAI ALORS
//MAP:2fd1e91a05eeae8c:00000021:1:FEN_Exercices.ZR_Exercices:com.logicorp.home_biking.wdgen.GWDFFEN_Exercices$GWDZR_Exercices:Sélection d'une ligne de ZR_Exercices
// SI gbCreation = VRAI ALORS
//MAP:2fd1e91a05eeae8c:00000021:1:FEN_Exercices.ZR_Exercices:com.logicorp.home_biking.wdgen.GWDFFEN_Exercices$GWDZR_Exercices:Sélection d'une ligne de ZR_Exercices
if(vWD_gbCreation.opEgal(true))
{
// 	OuvreFille(FEN_Exercice_Prog_Detail,ZR_Exercices.LIB_Exo)
//MAP:2fd1e91a05eeae8c:00000021:2:FEN_Exercices.ZR_Exercices:com.logicorp.home_biking.wdgen.GWDFFEN_Exercices$GWDZR_Exercices:Sélection d'une ligne de ZR_Exercices
WDAPIFenetre.ouvreFille(GWDPHome_Biking.getInstance().mWD_FEN_Exercice_Prog_Detail,new WDObjet[] {mWD_LIB_Exo} );

}

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurSelection();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDZR_Exercices mWD_ZR_Exercices;

/**
 * Traitement: Déclarations globales de FEN_Exercices
 */
public void declarerGlobale(WDObjet[] WD_tabParam)
{
// PROCEDURE MaFenêtre(gbCreation)
//MAP:2fd1e2be02c7d5b5:00000000:1:FEN_Exercices:com.logicorp.home_biking.wdgen.GWDFFEN_Exercices:Déclarations globales de FEN_Exercices
super.declarerGlobale(WD_tabParam, 1, 1);
int WD_ntabParamLen = 0;
if(WD_tabParam!=null) WD_ntabParamLen = WD_tabParam.length;

// Traitement du paramètre n°0
if(0<WD_ntabParamLen) 
{
vWD_gbCreation = WD_tabParam[0];
}
else { vWD_gbCreation = null; }
super.ajouterVariableGlobale("gbCreation",vWD_gbCreation);


}




/**
 * Traitement: Fermeture d'une fenêtre fille de FEN_Exercices
 */
public void fermetureFenetreFille()
{
super.fermetureFenetreFille();

// Ferme()
//MAP:2fd1e2be02c7d5b5:000000b1:1:FEN_Exercices:com.logicorp.home_biking.wdgen.GWDFFEN_Exercices:Fermeture d'une fenêtre fille de FEN_Exercices
// Ferme()
//MAP:2fd1e2be02c7d5b5:000000b1:1:FEN_Exercices:com.logicorp.home_biking.wdgen.GWDFFEN_Exercices:Fermeture d'une fenêtre fille de FEN_Exercices
WDAPIFenetre.ferme();

}




// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
 public WDObjet vWD_gbCreation = WDVarNonAllouee.ref;
////////////////////////////////////////////////////////////////////////////
// Création des champs de la fenêtre FEN_Exercices
////////////////////////////////////////////////////////////////////////////
protected void creerChamps()
{
mWD_IMG_Button_Back = new GWDIMG_Button_Back();
mWD_LIB_titre = new GWDLIB_titre();
mWD_SAI_Recherche_Programme = new GWDSAI_Recherche_Programme();
mWD_ZR_Exercices = new GWDZR_Exercices();

}
////////////////////////////////////////////////////////////////////////////
// Initialisation de la fenêtre FEN_Exercices
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.setQuid(3445784495633454517l);

super.setChecksum("854685036");

super.setNom("FEN_Exercices");

super.setType(1);

super.setBulle("");

super.setMenuContextuelSysteme();

super.setCurseurSouris(0);

super.setNote("", "");

super.setCouleur(0x0);

super.setCouleurFond(0xFFFFFF);

super.setPositionInitiale(0, 0);

super.setTailleInitiale(320, 543);

super.setTitre("Exercices");

super.setTailleMin(-1, -1);

super.setTailleMax(20000, 20000);

super.setVisibleInitial(true);

super.setPositionFenetre(2);

super.setPersistant(true);

super.setGFI(true);

super.setAnimationFenetre(0);

super.setImageFond("", 1, 0, 1);

super.setCouleurTexteAutomatique(0x303030);

super.setCouleurBarreSysteme(0x80FF);


activerEcoute();

////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FEN_Exercices
////////////////////////////////////////////////////////////////////////////
mWD_IMG_Button_Back.initialiserObjet();
super.ajouter("IMG_Button_Back", mWD_IMG_Button_Back);
mWD_LIB_titre.initialiserObjet();
super.ajouter("LIB_titre", mWD_LIB_titre);
mWD_SAI_Recherche_Programme.initialiserObjet();
super.ajouter("SAI_Recherche_Programme", mWD_SAI_Recherche_Programme);
mWD_ZR_Exercices.initialiserObjet();
super.ajouter("ZR_Exercices", mWD_ZR_Exercices);

super.terminerInitialisation();
}

////////////////////////////////////////////////////////////////////////////
public boolean isUniteAffichageLogique()
{
return false;
}

public WDProjet getProjet()
{
return GWDPHome_Biking.getInstance();
}

public IWDEnsembleElement getEnsemble()
{
return GWDPHome_Biking.getInstance();
}
public int getModeContexteHF()
{
return 1;
}
/**
* Retourne le mode d'affichage de l'ActionBar de la fenêtre.
*/
public int getModeActionBar()
{
return 0;
}
/**
* Retourne vrai si la fenêtre est maximisée, faux sinon.
*/
public boolean isMaximisee()
{
return true;
}
/**
* Retourne vrai si la fenêtre a une barre de titre, faux sinon.
*/
public boolean isAvecBarreDeTitre()
{
return false;
}
/**
* Retourne le mode d'affichage de la barre système de la fenêtre.
*/
public int getModeBarreSysteme()
{
return 1;
}
/**
* Retourne vrai si la fenêtre est munie d'ascenseurs automatique, faux sinon.
*/
public boolean isAvecAscenseurAuto()
{
return false;
}
/**
* Retourne Vrai si on doit appliquer un theme "dark" (sombre) ou Faux si on doit appliquer "light" (clair) à la fenêtre.
* Ce choix se base sur la couleur du libellé par défaut dans le gabarit de la fenêtre.
*/
public boolean isThemeDark()
{
return false;
}
/**
* Retourne vrai si l'option de masquage automatique de l'ActionBar lorsqu'on scrolle dans un champ de la fenêtre a été activée.
*/
public boolean isMasquageAutomatiqueActionBar()
{
return false;
}
public static class WDActiviteFenetre extends WDActivite
{
protected WDFenetre getFenetre()
{
return GWDPHome_Biking.getInstance().mWD_FEN_Exercices;
}
}
/**
* Retourne le nom du gabarit associée à la fenêtre.
*/
public String getNomGabarit()
{
return "210 MATERIAL DESIGN ORANGE#WM";
}
}
