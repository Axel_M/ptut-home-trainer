/**
 * Code généré par WINDEV Mobile - NE PAS MODIFIER !
 * Objet WINDEV Mobile : Requête
 * Classe Android : REQ_Exo_Prog
 * Date : 06/03/2021 17:18:02
 * Version de wdjava.dll  : 25.0.221.6
 */


package com.logicorp.home_biking.wdgen;


import com.logicorp.home_biking.*;
import fr.pcsoft.wdjava.core.types.*;
import fr.pcsoft.wdjava.core.*;
import fr.pcsoft.wdjava.database.hf.requete.parsing.*;
/*Imports trouvés dans le code WL*/
/*Fin Imports trouvés dans le code WL*/



public class GWDRREQ_Exo_Prog extends WDDescRequeteWDR
{
public String getNomLogique()
{
return "REQ_Exo_Prog";
}
public String getCodeSQLOriginal()
{
return "SELECT \r\n  Exercice.Nom AS Nom,  \r\n  contenir.nb_rep AS nb_rep,  \r\n  contenir.temps AS temps,  \r\n  contenir.repos AS repos,\r\n  contenir.cadence AS cadence,\r\n  contenir.pourcentage_fcm AS pourcentage_fcm,\r\n  Exercice.Id_Exercice as idE,\r\n  contenir.place AS place\r\nFROM \r\n  Exercice,  \r\n  contenir\r\nWHERE \r\n  Exercice.Id_Exercice = contenir.Id_Exercice\r\n  AND\r\n  (\r\n    contenir.Id_Programme = {ParamId_Programme#0}\r\n  )\r\n  ORDER BY contenir.place ASC";
}
public Requete initArbre() throws WDInvalidSQLException
{
Select varSelect = new Select();
varSelect.setType(1);
Rubrique rub_Nom = new Rubrique();
rub_Nom.setNom("Nom");
rub_Nom.setAlias("Nom");
rub_Nom.setNomFichier("Exercice");
rub_Nom.setAliasFichier("Exercice");
varSelect.ajouterElement(rub_Nom);
Rubrique rub_nb_rep = new Rubrique();
rub_nb_rep.setNom("nb_rep");
rub_nb_rep.setAlias("nb_rep");
rub_nb_rep.setNomFichier("contenir");
rub_nb_rep.setAliasFichier("contenir");
varSelect.ajouterElement(rub_nb_rep);
Rubrique rub_temps = new Rubrique();
rub_temps.setNom("temps");
rub_temps.setAlias("temps");
rub_temps.setNomFichier("contenir");
rub_temps.setAliasFichier("contenir");
varSelect.ajouterElement(rub_temps);
Rubrique rub_repos = new Rubrique();
rub_repos.setNom("repos");
rub_repos.setAlias("repos");
rub_repos.setNomFichier("contenir");
rub_repos.setAliasFichier("contenir");
varSelect.ajouterElement(rub_repos);
Rubrique rub_cadence = new Rubrique();
rub_cadence.setNom("cadence");
rub_cadence.setAlias("cadence");
rub_cadence.setNomFichier("contenir");
rub_cadence.setAliasFichier("contenir");
varSelect.ajouterElement(rub_cadence);
Rubrique rub_pourcentage_fcm = new Rubrique();
rub_pourcentage_fcm.setNom("pourcentage_fcm");
rub_pourcentage_fcm.setAlias("pourcentage_fcm");
rub_pourcentage_fcm.setNomFichier("contenir");
rub_pourcentage_fcm.setAliasFichier("contenir");
varSelect.ajouterElement(rub_pourcentage_fcm);
Rubrique rub_idE = new Rubrique();
rub_idE.setNom("Id_Exercice");
rub_idE.setAlias("idE");
rub_idE.setNomFichier("Exercice");
rub_idE.setAliasFichier("Exercice");
varSelect.ajouterElement(rub_idE);
Rubrique rub_place = new Rubrique();
rub_place.setNom("place");
rub_place.setAlias("place");
rub_place.setNomFichier("contenir");
rub_place.setAliasFichier("contenir");
varSelect.ajouterElement(rub_place);
From varFrom = new From();
Fichier fic_Exercice = new Fichier();
fic_Exercice.setNom("Exercice");
fic_Exercice.setAlias("Exercice");
varFrom.ajouterElement(fic_Exercice);
Fichier fic_contenir = new Fichier();
fic_contenir.setNom("contenir");
fic_contenir.setAlias("contenir");
varFrom.ajouterElement(fic_contenir);
Requete varReqSelect = new Requete(1);
varReqSelect.ajouterClause(varSelect);
varReqSelect.ajouterClause(varFrom);
Expression expr_AND = new Expression(24, "AND", "Exercice.Id_Exercice = contenir.Id_Exercice\r\n  AND\r\n  (\r\n    contenir.Id_Programme = {ParamId_Programme}\r\n  )");
Expression expr__ = new Expression(9, "=", "Exercice.Id_Exercice = contenir.Id_Exercice");
Rubrique rub_Id_Exercice = new Rubrique();
rub_Id_Exercice.setNom("Exercice.Id_Exercice");
rub_Id_Exercice.setAlias("Id_Exercice");
rub_Id_Exercice.setNomFichier("Exercice");
rub_Id_Exercice.setAliasFichier("Exercice");
expr__.ajouterElement(rub_Id_Exercice);
Rubrique rub_Id_Exercice_1 = new Rubrique();
rub_Id_Exercice_1.setNom("contenir.Id_Exercice");
rub_Id_Exercice_1.setAlias("Id_Exercice");
rub_Id_Exercice_1.setNomFichier("contenir");
rub_Id_Exercice_1.setAliasFichier("contenir");
expr__.ajouterElement(rub_Id_Exercice_1);
expr_AND.ajouterElement(expr__);
Expression expr___1 = new Expression(9, "=", "contenir.Id_Programme = {ParamId_Programme}");
Rubrique rub_Id_Programme = new Rubrique();
rub_Id_Programme.setNom("contenir.Id_Programme");
rub_Id_Programme.setAlias("Id_Programme");
rub_Id_Programme.setNomFichier("contenir");
rub_Id_Programme.setAliasFichier("contenir");
expr___1.ajouterElement(rub_Id_Programme);
Parametre param_ParamId_Programme = new Parametre();
param_ParamId_Programme.setNom("ParamId_Programme");
expr___1.ajouterElement(param_ParamId_Programme);
expr_AND.ajouterElement(expr___1);
Where varWhere = new Where();
varWhere.ajouterElement(expr_AND);
varReqSelect.ajouterClause(varWhere);
OrderBy varOrderBy = new OrderBy();
Rubrique rub_place_1 = new Rubrique();
rub_place_1.setNom("place");
rub_place_1.setAlias("place");
rub_place_1.setNomFichier("contenir");
rub_place_1.setAliasFichier("contenir");
rub_place_1.ajouterOption(EWDOptionRequete.TRI, "0");
rub_place_1.ajouterOption(EWDOptionRequete.INDEX_RUB, "7");
varOrderBy.ajouterElement(rub_place_1);
varReqSelect.ajouterClause(varOrderBy);
Limit varLimit = new Limit();
varLimit.setType(0);
varLimit.setNbEnregs(0);
varLimit.setOffset(0);
varReqSelect.ajouterClause(varLimit);
return varReqSelect;
}
public String getNomFichier(int nIndex)
{
switch(nIndex)
{
case 0 : return "Exercice";
case 1 : return "contenir";
default: return null;
}
}
public String getAliasFichier(int nIndex)
{
switch(nIndex)
{
case 0 : return "Exercice";
case 1 : return "contenir";
default: return null;
}
}


public int getIdWDR()
{
return com.logicorp.home_biking.R.raw.req_exo_prog;
}
public String getNomFichierWDR()
{
return "req_exo_prog";
}
}
