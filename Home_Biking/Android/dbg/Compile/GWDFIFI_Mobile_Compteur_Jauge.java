/**
 * Code généré par WINDEV Mobile - NE PAS MODIFIER !
 * Objet WINDEV Mobile : Fenêtre
 * Classe Android : FI_Mobile_Compteur_Jauge
 * Date : 16/12/2020 08:39:47
 * Version de wdjava.dll  : 25.0.221.6
 */


package com.logicorp.home_biking.wdgen;


import com.logicorp.home_biking.*;
import fr.pcsoft.wdjava.core.types.*;
import fr.pcsoft.wdjava.core.*;
import fr.pcsoft.wdjava.ui.champs.fenetreinterne.*;
import fr.pcsoft.wdjava.ui.champs.image.*;
import fr.pcsoft.wdjava.ui.cadre.*;
import fr.pcsoft.wdjava.ui.champs.libelle.*;
import fr.pcsoft.wdjava.core.application.*;
import fr.pcsoft.wdjava.ui.dessin.*;
import fr.pcsoft.wdjava.core.poo.*;
import fr.pcsoft.wdjava.api.*;
/*Imports trouvés dans le code WL*/
/*Fin Imports trouvés dans le code WL*/



public class GWDFIFI_Mobile_Compteur_Jauge extends WDFenetreInterne
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs de FI_Mobile_Compteur_Jauge
////////////////////////////////////////////////////////////////////////////

/**
 * IMG_Dessin
 */
class GWDIMG_Dessin extends WDChampImage
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FI_Mobile_Compteur_Jauge.FI_Mobile_Compteur_Jauge.IMG_Dessin
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setQuid(3300695115197941282l);

super.setChecksum("1527513370");

super.setNom("IMG_Dessin");

super.setType(30001);

super.setBulle("");

super.setLibelle("");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setNavigable(false);

super.setEtatInitial(0);

super.setPositionInitiale(0, 0);

super.setTailleInitiale(250, 250);

super.setValeurInitiale("");

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(134217727, 134217727);

super.setVisibleInitial(true);

super.setAltitude(1);

super.setAncrageInitial(10, 1000, 1000, 1000, 1000, 0);

super.setTransparence(1);

super.setParamImage(1, 0, true, 2500);

super.setSymetrie(0);

super.setZoneClicage(true);

super.setPCodeMultitouch(false);

super.setChargementEnTacheDeFond(false);

super.setOrientationExif(false);

super.setParamAnimation(1, 1, false, 10, true, false);

super.setAnimationInitiale(false);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -1, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDIMG_Dessin mWD_IMG_Dessin;

/**
 * LIB_Total
 */
class GWDLIB_Total extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°2 de FI_Mobile_Compteur_Jauge.FI_Mobile_Compteur_Jauge.LIB_Total
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setQuid(3300695115198006818l);

super.setChecksum("1527576626");

super.setNom("LIB_Total");

super.setType(3);

super.setBulle("");

super.setTypeSaisie(1);

super.setMasqueSaisie(new WDChaineU("999 999"));

super.setLibelle("9999");

super.setNote("", "");

super.setCurseurSouris(0);

super.setEtatInitial(0);

super.setPositionInitiale(34, 135);

super.setTailleInitiale(182, 33);

super.setPlan(0);

super.setCadrageHorizontal(1);

super.setCadrageVertical(1);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(2);

super.setAncrageInitial(4, 1000, 1000, 500, 1000, 0);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x8E8E8F, 0xFFFFFFFF, creerPolice_GEN("Roboto", -22.000000, 0), 3, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_Total mWD_LIB_Total;

/**
 * LIB_Valeur
 */
class GWDLIB_Valeur extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°3 de FI_Mobile_Compteur_Jauge.FI_Mobile_Compteur_Jauge.LIB_Valeur
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setQuid(3300695115198072354l);

super.setChecksum("1527642162");

super.setNom("LIB_Valeur");

super.setType(3);

super.setBulle("");

super.setTypeSaisie(1);

super.setMasqueSaisie(new WDChaineU("999 999"));

super.setLibelle("9999");

super.setNote("", "");

super.setCurseurSouris(0);

super.setEtatInitial(0);

super.setPositionInitiale(34, 72);

super.setTailleInitiale(182, 56);

super.setPlan(0);

super.setCadrageHorizontal(1);

super.setCadrageVertical(1);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(3);

super.setAncrageInitial(4, 1000, 1000, 500, 1000, 0);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, 0xFFFFFFFF, creerPolice_GEN("Roboto", -40.000000, 0), 3, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_Valeur mWD_LIB_Valeur;


////////////////////////////////////////////////////////////////////////////
// Procédures utilisateur de FI_Mobile_Compteur_Jauge
////////////////////////////////////////////////////////////////////////////
public void fWD__DessineJaugeAnneau( WDObjet vWD_nPourcent )
{
// PROCÉDURE PRIVÉE _DessineJaugeAnneau(LOCAL nPourcent est un entier)
//MAP:1dce6d2b0e434844:00070000:1:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeAnneau:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeAnneau
initExecProcLocale("_DessineJaugeAnneau");

try
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables locales au traitement
// (En WLangage les variables sont encore visibles après la fin du bloc dans lequel elles sont déclarées)
////////////////////////////////////////////////////////////////////////////
WDObjet vWD_nDiametreCercle = new WDEntier4();

WDObjet vWD_nCouleurJauge = new WDEntier4();

WDObjet vWD_MonImage = WDVarNonAllouee.ref;
WDObjet vWD_nX1 = new WDEntier4();

WDObjet vWD_nX2 = new WDEntier4();

WDObjet vWD_nY1 = new WDEntier4();

WDObjet vWD_nY2 = new WDEntier4();

WDObjet vWD_MonImageRef = WDVarNonAllouee.ref;
WDObjet vWD_nFacteur = new WDEntier4();

WDObjet vWD_nLargeur = new WDEntier4();

WDObjet vWD_nHauteur = new WDEntier4();

WDObjet vWD_nCentreX = new WDEntier4();

WDObjet vWD_nCentreY = new WDEntier4();

WDObjet vWD_nEpaisseur = new WDEntier4();

WDObjet vWD_nAngleFin = new WDEntier4();



vWD_nPourcent = WDParametre.traiterParametre(vWD_nPourcent, 1, true, 8);


// nDiametreCercle est un entier 
//MAP:1dce6d2b0e434844:00070000:4:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeAnneau:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeAnneau


// nCouleurJauge est un entier = gnCouleurJauge
//MAP:1dce6d2b0e434844:00070000:5:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeAnneau:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeAnneau

vWD_nCouleurJauge.setValeur(vWD_gnCouleurJauge);


// MonImage est une Image
//MAP:1dce6d2b0e434844:00070000:6:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeAnneau:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeAnneau
vWD_MonImage = new WDInstance( new WDImage() );


// nX1, nX2, nY1, nY2 sont des entiers
//MAP:1dce6d2b0e434844:00070000:7:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeAnneau:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeAnneau





// MonImageRef est Image = IMG_Dessin
//MAP:1dce6d2b0e434844:00070000:a:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeAnneau:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeAnneau
vWD_MonImageRef = new WDInstance( new WDImage() );

vWD_MonImageRef.setValeur(mWD_IMG_Dessin);


// nFacteur est entier = MonImageRef..EchelleDessin
//MAP:1dce6d2b0e434844:00070000:c:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeAnneau:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeAnneau

vWD_nFacteur.setValeur(vWD_MonImageRef.getProp(EWDPropriete.PROP_ECHELLEDESSIN));


// nLargeur est entier = IMG_Dessin..Largeur 
//MAP:1dce6d2b0e434844:00070000:d:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeAnneau:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeAnneau

vWD_nLargeur.setValeur(mWD_IMG_Dessin.getProp(EWDPropriete.PROP_LARGEUR));


// nHauteur est entier = IMG_Dessin..Hauteur 
//MAP:1dce6d2b0e434844:00070000:e:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeAnneau:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeAnneau

vWD_nHauteur.setValeur(mWD_IMG_Dessin.getProp(EWDPropriete.PROP_HAUTEUR));


// MonImage..Largeur = nLargeur*nFacteur
//MAP:1dce6d2b0e434844:00070000:11:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeAnneau:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeAnneau
vWD_MonImage.setProp(EWDPropriete.PROP_LARGEUR,vWD_nLargeur.opMult(vWD_nFacteur));

// MonImage..Hauteur = nHauteur*nFacteur
//MAP:1dce6d2b0e434844:00070000:12:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeAnneau:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeAnneau
vWD_MonImage.setProp(EWDPropriete.PROP_HAUTEUR,vWD_nHauteur.opMult(vWD_nFacteur));

// MonImage..EchelleDessin = nFacteur
//MAP:1dce6d2b0e434844:00070000:13:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeAnneau:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeAnneau
vWD_MonImage.setProp(EWDPropriete.PROP_ECHELLEDESSIN,vWD_nFacteur);

// nCentreX est un entier = nLargeur/2
//MAP:1dce6d2b0e434844:00070000:15:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeAnneau:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeAnneau

vWD_nCentreX.setValeur(vWD_nLargeur.opDiv(2));


// nCentreY est un entier = nHauteur/2
//MAP:1dce6d2b0e434844:00070000:16:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeAnneau:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeAnneau

vWD_nCentreY.setValeur(vWD_nHauteur.opDiv(2));


// dDébutDessin(MonImage)
//MAP:1dce6d2b0e434844:00070000:19:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeAnneau:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeAnneau
WDAPIDessin.dDebutDessin(vWD_MonImage);

// SI nFacteur <= 1 ALORS
//MAP:1dce6d2b0e434844:00070000:1b:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeAnneau:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeAnneau
if(vWD_nFacteur.opInfEgal(1))
{
// 	dChangeMode(dessinAntiAliasing)
//MAP:1dce6d2b0e434844:00070000:1c:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeAnneau:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeAnneau
WDAPIDessin.dChangeMode(256);

}

// nDiametreCercle = MonImage..Largeur*0.9 
//MAP:1dce6d2b0e434844:00070000:20:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeAnneau:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeAnneau
vWD_nDiametreCercle.setValeur(vWD_MonImage.getProp(EWDPropriete.PROP_LARGEUR).opMult(0.9));

// nEpaisseur est un entier = MonImage..Largeur*10/200
//MAP:1dce6d2b0e434844:00070000:22:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeAnneau:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeAnneau

vWD_nEpaisseur.setValeur(vWD_MonImage.getProp(EWDPropriete.PROP_LARGEUR).opMult(10).opDiv(200));


// nAngleFin est un entier = modulo((nPourcent * 360 / 100)+gnAngleDépart,360)
//MAP:1dce6d2b0e434844:00070000:25:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeAnneau:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeAnneau

vWD_nAngleFin.setValeur(vWD_nPourcent.opMult(360).opDiv(100).opPlus(vWD_gnAngleDepart).opMod(360));


// _PositionsSelonAngle(gnAngleDépart, nCentreX, nCentreY, nDiametreCercle, nX1, nY1)
//MAP:1dce6d2b0e434844:00070000:29:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeAnneau:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeAnneau
fWD__PositionsSelonAngle(vWD_gnAngleDepart,vWD_nCentreX,vWD_nCentreY,vWD_nDiametreCercle,vWD_nX1,vWD_nY1);

// _PositionsSelonAngle(nAngleFin, nCentreX, nCentreY, nDiametreCercle, nX2, nY2)
//MAP:1dce6d2b0e434844:00070000:2b:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeAnneau:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeAnneau
fWD__PositionsSelonAngle(vWD_nAngleFin,vWD_nCentreX,vWD_nCentreY,vWD_nDiametreCercle,vWD_nX2,vWD_nY2);

// dArc((nCentreX - nDiametreCercle/2)/nFacteur, (nCentreY - nDiametreCercle/2)/nFacteur, (nCentreX + nDiametreCercle/2)/nFacteur, (nCentreY + nDiametreCercle/2)/nFacteur, nX2/nFacteur, nY2/nFacteur,nX1/nFacteur, nY1/nFacteur, gnCouleurDécor,nEpaisseur/nFacteur)
//MAP:1dce6d2b0e434844:00070000:2e:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeAnneau:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeAnneau
WDAPIDessin.dArc(vWD_nCentreX.opMoins(vWD_nDiametreCercle.opDiv(2)).opDiv(vWD_nFacteur),vWD_nCentreY.opMoins(vWD_nDiametreCercle.opDiv(2)).opDiv(vWD_nFacteur).getInt(),vWD_nCentreX.opPlus(vWD_nDiametreCercle.opDiv(2)).opDiv(vWD_nFacteur).getInt(),vWD_nCentreY.opPlus(vWD_nDiametreCercle.opDiv(2)).opDiv(vWD_nFacteur).getInt(),vWD_nX2.opDiv(vWD_nFacteur).getInt(),vWD_nY2.opDiv(vWD_nFacteur).getInt(),vWD_nX1.opDiv(vWD_nFacteur).getInt(),vWD_nY1.opDiv(vWD_nFacteur).getInt(),vWD_gnCouleurDecor,vWD_nEpaisseur.opDiv(vWD_nFacteur));

// SI nPourcent > 0 ALORS
//MAP:1dce6d2b0e434844:00070000:32:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeAnneau:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeAnneau
if(vWD_nPourcent.opSup(0))
{
// 	dArc(((MonImage..Largeur-nDiametreCercle)/2)/nFacteur, ((MonImage..Largeur-nDiametreCercle)/2)/nFacteur, (MonImage..Hauteur-(MonImage..Largeur-nDiametreCercle)/2)/nFacteur, (MonImage..Largeur-(MonImage..Largeur-nDiametreCercle)/2)/nFacteur, nX1/nFacteur, nY1/nFacteur, nX2/nFacteur, nY2/nFacteur, nCouleurJauge,nEpaisseur/nFacteur)	
//MAP:1dce6d2b0e434844:00070000:34:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeAnneau:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeAnneau
WDAPIDessin.dArc(vWD_MonImage.getProp(EWDPropriete.PROP_LARGEUR).opMoins(vWD_nDiametreCercle).opDiv(2).opDiv(vWD_nFacteur),vWD_MonImage.getProp(EWDPropriete.PROP_LARGEUR).opMoins(vWD_nDiametreCercle).opDiv(2).opDiv(vWD_nFacteur).getInt(),vWD_MonImage.getProp(EWDPropriete.PROP_HAUTEUR).opMoins(vWD_MonImage.getProp(EWDPropriete.PROP_LARGEUR).opMoins(vWD_nDiametreCercle).opDiv(2)).opDiv(vWD_nFacteur).getInt(),vWD_MonImage.getProp(EWDPropriete.PROP_LARGEUR).opMoins(vWD_MonImage.getProp(EWDPropriete.PROP_LARGEUR).opMoins(vWD_nDiametreCercle).opDiv(2)).opDiv(vWD_nFacteur).getInt(),vWD_nX1.opDiv(vWD_nFacteur).getInt(),vWD_nY1.opDiv(vWD_nFacteur).getInt(),vWD_nX2.opDiv(vWD_nFacteur).getInt(),vWD_nY2.opDiv(vWD_nFacteur).getInt(),vWD_nCouleurJauge,vWD_nEpaisseur.opDiv(vWD_nFacteur));

}

// SI gbSensHoraire ALORS
//MAP:1dce6d2b0e434844:00070000:38:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeAnneau:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeAnneau
if(vWD_gbSensHoraire.getBoolean())
{
// 	dSymétrieVerticale(MonImage)
//MAP:1dce6d2b0e434844:00070000:39:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeAnneau:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeAnneau
WDAPIDessin.dSymetrieVerticale(vWD_MonImage);

}

// IMG_Dessin = MonImage
//MAP:1dce6d2b0e434844:00070000:3d:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeAnneau:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeAnneau
mWD_IMG_Dessin.setValeur(vWD_MonImage);

}
finally
{
finExecProcLocale();
}

}



public void fWD__DessineJaugeRond( WDObjet vWD_nPourcent )
{
// PROCÉDURE PRIVÉ _DessineJaugeRond(LOCAL nPourcent est un entier)
//MAP:1dce6d2e0e49540c:00070000:1:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeRond:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeRond
initExecProcLocale("_DessineJaugeRond");

try
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables locales au traitement
// (En WLangage les variables sont encore visibles après la fin du bloc dans lequel elles sont déclarées)
////////////////////////////////////////////////////////////////////////////
WDObjet vWD_nDiametreCercle = new WDEntier4();

WDObjet vWD_nCouleurJauge = new WDEntier4();

WDObjet vWD_nX1 = new WDEntier4();

WDObjet vWD_nX2 = new WDEntier4();

WDObjet vWD_nY1 = new WDEntier4();

WDObjet vWD_nY2 = new WDEntier4();

WDObjet vWD_nDifference = new WDEntier4();

WDObjet vWD_MonImage = WDVarNonAllouee.ref;
WDObjet vWD_MonImageRef = WDVarNonAllouee.ref;
WDObjet vWD_nFacteur = new WDEntier4();

WDObjet vWD_nLargeur = new WDEntier4();

WDObjet vWD_nHauteur = new WDEntier4();

WDObjet vWD_nCentreX = new WDEntier4();

WDObjet vWD_nCentreY = new WDEntier4();



vWD_nPourcent = WDParametre.traiterParametre(vWD_nPourcent, 1, true, 8);


// nDiametreCercle est un entier 
//MAP:1dce6d2e0e49540c:00070000:4:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeRond:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeRond


// nCouleurJauge est un entier = gnCouleurJauge
//MAP:1dce6d2e0e49540c:00070000:5:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeRond:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeRond

vWD_nCouleurJauge.setValeur(vWD_gnCouleurJauge);


// nX1, nX2, nY1, nY2 sont des entiers
//MAP:1dce6d2e0e49540c:00070000:6:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeRond:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeRond





// nDifférence est un entier 
//MAP:1dce6d2e0e49540c:00070000:7:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeRond:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeRond


// MonImage est une Image
//MAP:1dce6d2e0e49540c:00070000:8:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeRond:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeRond
vWD_MonImage = new WDInstance( new WDImage() );


// MonImageRef est Image = IMG_Dessin
//MAP:1dce6d2e0e49540c:00070000:b:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeRond:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeRond
vWD_MonImageRef = new WDInstance( new WDImage() );

vWD_MonImageRef.setValeur(mWD_IMG_Dessin);


// nFacteur est entier = MonImageRef..EchelleDessin
//MAP:1dce6d2e0e49540c:00070000:c:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeRond:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeRond

vWD_nFacteur.setValeur(vWD_MonImageRef.getProp(EWDPropriete.PROP_ECHELLEDESSIN));


// nLargeur est entier = IMG_Dessin..Largeur 
//MAP:1dce6d2e0e49540c:00070000:d:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeRond:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeRond

vWD_nLargeur.setValeur(mWD_IMG_Dessin.getProp(EWDPropriete.PROP_LARGEUR));


// nHauteur est entier = IMG_Dessin..Hauteur 
//MAP:1dce6d2e0e49540c:00070000:e:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeRond:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeRond

vWD_nHauteur.setValeur(mWD_IMG_Dessin.getProp(EWDPropriete.PROP_HAUTEUR));


// MonImage..Largeur = nLargeur*nFacteur
//MAP:1dce6d2e0e49540c:00070000:11:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeRond:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeRond
vWD_MonImage.setProp(EWDPropriete.PROP_LARGEUR,vWD_nLargeur.opMult(vWD_nFacteur));

// MonImage..Hauteur = nHauteur*nFacteur
//MAP:1dce6d2e0e49540c:00070000:12:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeRond:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeRond
vWD_MonImage.setProp(EWDPropriete.PROP_HAUTEUR,vWD_nHauteur.opMult(vWD_nFacteur));

// MonImage..EchelleDessin = nFacteur
//MAP:1dce6d2e0e49540c:00070000:13:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeRond:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeRond
vWD_MonImage.setProp(EWDPropriete.PROP_ECHELLEDESSIN,vWD_nFacteur);

// nCentreX est un entier = nLargeur/2
//MAP:1dce6d2e0e49540c:00070000:15:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeRond:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeRond

vWD_nCentreX.setValeur(vWD_nLargeur.opDiv(2));


// nCentreY est un entier = nHauteur/2
//MAP:1dce6d2e0e49540c:00070000:16:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeRond:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeRond

vWD_nCentreY.setValeur(vWD_nHauteur.opDiv(2));


// dDébutDessin(MonImage)
//MAP:1dce6d2e0e49540c:00070000:19:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeRond:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeRond
WDAPIDessin.dDebutDessin(vWD_MonImage);

// SI nFacteur <= 1 ALORS
//MAP:1dce6d2e0e49540c:00070000:1b:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeRond:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeRond
if(vWD_nFacteur.opInfEgal(1))
{
// 	dChangeMode(dessinAntiAliasing)
//MAP:1dce6d2e0e49540c:00070000:1c:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeRond:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeRond
WDAPIDessin.dChangeMode(256);

}

// nDiametreCercle = MonImage..Largeur * 95/100
//MAP:1dce6d2e0e49540c:00070000:20:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeRond:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeRond
vWD_nDiametreCercle.setValeur(vWD_MonImage.getProp(EWDPropriete.PROP_LARGEUR).opMult(95).opDiv(100));

// nDifférence = MonImage..Largeur - nDiametreCercle
//MAP:1dce6d2e0e49540c:00070000:22:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeRond:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeRond
vWD_nDifference.setValeur(vWD_MonImage.getProp(EWDPropriete.PROP_LARGEUR).opMoins(vWD_nDiametreCercle));

// SI nPourcent > 0 ALORS
//MAP:1dce6d2e0e49540c:00070000:26:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeRond:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeRond
if(vWD_nPourcent.opSup(0))
{
// 	nAngleDébut est un entier = gnAngleDépart
//MAP:1dce6d2e0e49540c:00070000:29:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeRond:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeRond
WDObjet vWD_nAngleDebut = new WDEntier4();


vWD_nAngleDebut.setValeur(vWD_gnAngleDepart);


// 	nAngleFin est un entier = modulo((nPourcent * 360 / 100)+gnAngleDépart,360)
//MAP:1dce6d2e0e49540c:00070000:2a:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeRond:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeRond
WDObjet vWD_nAngleFin = new WDEntier4();


vWD_nAngleFin.setValeur(vWD_nPourcent.opMult(360).opDiv(100).opPlus(vWD_gnAngleDepart).opMod(360));


// 	_PositionsSelonAngle(gnAngleDépart, nCentreX, nCentreY, nDiametreCercle, nX1, nY1)
//MAP:1dce6d2e0e49540c:00070000:2c:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeRond:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeRond
fWD__PositionsSelonAngle(vWD_gnAngleDepart,vWD_nCentreX,vWD_nCentreY,vWD_nDiametreCercle,vWD_nX1,vWD_nY1);

// 	_PositionsSelonAngle(nAngleFin, nCentreX, nCentreY, nDiametreCercle, nX2, nY2)
//MAP:1dce6d2e0e49540c:00070000:2d:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeRond:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeRond
fWD__PositionsSelonAngle(vWD_nAngleFin,vWD_nCentreX,vWD_nCentreY,vWD_nDiametreCercle,vWD_nX2,vWD_nY2);

// 	dPortion(nDifférence/nFacteur, nDifférence/nFacteur, (MonImage..Hauteur-nDifférence)/nFacteur, (MonImage..Largeur-nDifférence)/nFacteur, nX1/nFacteur, nY1/nFacteur, nX2/nFacteur, nY2/nFacteur, nCouleurJauge, nCouleurJauge)
//MAP:1dce6d2e0e49540c:00070000:30:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeRond:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeRond
WDAPIDessin.dPortion(vWD_nDifference.opDiv(vWD_nFacteur),vWD_nDifference.opDiv(vWD_nFacteur).getInt(),vWD_MonImage.getProp(EWDPropriete.PROP_HAUTEUR).opMoins(vWD_nDifference).opDiv(vWD_nFacteur).getInt(),vWD_MonImage.getProp(EWDPropriete.PROP_LARGEUR).opMoins(vWD_nDifference).opDiv(vWD_nFacteur).getInt(),vWD_nX1.opDiv(vWD_nFacteur).getInt(),vWD_nY1.opDiv(vWD_nFacteur).getInt(),vWD_nX2.opDiv(vWD_nFacteur).getInt(),vWD_nY2.opDiv(vWD_nFacteur).getInt(),vWD_nCouleurJauge,vWD_nCouleurJauge);

}

// nDiametreCercle = MonImage..Largeur * 50/100
//MAP:1dce6d2e0e49540c:00070000:36:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeRond:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeRond
vWD_nDiametreCercle.setValeur(vWD_MonImage.getProp(EWDPropriete.PROP_LARGEUR).opMult(50).opDiv(100));

// dCercle((nCentreX - nDiametreCercle/2)/nFacteur, (nCentreY - nDiametreCercle/2)/nFacteur, (nCentreX + nDiametreCercle/2)/nFacteur, (nCentreY + nDiametreCercle/2)/nFacteur, gnCouleurDécor)
//MAP:1dce6d2e0e49540c:00070000:37:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeRond:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeRond
WDAPIDessin.dCercle(vWD_nCentreX.opMoins(vWD_nDiametreCercle.opDiv(2)).opDiv(vWD_nFacteur),vWD_nCentreY.opMoins(vWD_nDiametreCercle.opDiv(2)).opDiv(vWD_nFacteur).getInt(),vWD_nCentreX.opPlus(vWD_nDiametreCercle.opDiv(2)).opDiv(vWD_nFacteur).getInt(),vWD_nCentreY.opPlus(vWD_nDiametreCercle.opDiv(2)).opDiv(vWD_nFacteur).getInt(),vWD_gnCouleurDecor);

// SI gbSensHoraire ALORS
//MAP:1dce6d2e0e49540c:00070000:3a:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeRond:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeRond
if(vWD_gbSensHoraire.getBoolean())
{
// 	dSymétrieVerticale(MonImage)
//MAP:1dce6d2e0e49540c:00070000:3b:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeRond:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeRond
WDAPIDessin.dSymetrieVerticale(vWD_MonImage);

}

// IMG_Dessin = MonImage
//MAP:1dce6d2e0e49540c:00070000:3f:FI_Mobile_Compteur_Jauge.PROCEDURE._DessineJaugeRond:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_DessineJaugeRond
mWD_IMG_Dessin.setValeur(vWD_MonImage);

}
finally
{
finExecProcLocale();
}

}



//  Résumé : Récupère la position X et Y d'un point du cercle selon l'angle
//  Syntaxe :
// _PositionsSelonAngle (<nAngle> est entier, <nCentreX> est entier, <nCentreY> est entier, <nDiametreCercle> est entier, <nX> est entier, <nY> est entier)
// 
//  Paramètres :
// 	nAngle (entier) : Angle
// 	nCentreX (entier) : X du centre
// 	nCentreY (entier) : Y du centre
// 	nDiametreCercle (entier) : Diamètre
// 	nX (entier) : Position X déterminée
// 	nY (entier) : Position Y déterminée
//  Valeur de retour :
//  	Aucune
// 
public void fWD__PositionsSelonAngle( WDObjet vWD_nAngle , WDObjet vWD_nCentreX , WDObjet vWD_nCentreY , WDObjet vWD_nDiametreCercle , WDObjet vWD_nX , WDObjet vWD_nY )
{
// PROCEDURE PRIVÉE _PositionsSelonAngle(LOCAL nAngle est un entier, nCentreX est un entier, nCentreY est un entier, nDiametreCercle est un entier, nX est un entier, nY est un entier)
//MAP:1dce6d2f0e4a57c5:00070000:10:FI_Mobile_Compteur_Jauge.PROCEDURE._PositionsSelonAngle:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_PositionsSelonAngle
initExecProcLocale("_PositionsSelonAngle");

try
{
vWD_nAngle = WDParametre.traiterParametre(vWD_nAngle, 1, true, 8);

vWD_nCentreX = WDParametre.traiterParametre(vWD_nCentreX, 2, false, 8);

vWD_nCentreY = WDParametre.traiterParametre(vWD_nCentreY, 3, false, 8);

vWD_nDiametreCercle = WDParametre.traiterParametre(vWD_nDiametreCercle, 4, false, 8);

vWD_nX = WDParametre.traiterParametre(vWD_nX, 5, false, 8);

vWD_nY = WDParametre.traiterParametre(vWD_nY, 6, false, 8);


// nX = nCentreX + (nDiametreCercle/2) * Cos(nAngle)
//MAP:1dce6d2f0e4a57c5:00070000:13:FI_Mobile_Compteur_Jauge.PROCEDURE._PositionsSelonAngle:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_PositionsSelonAngle
vWD_nX.setValeur(vWD_nCentreX.opPlus(vWD_nDiametreCercle.opDiv(2).opMult(WDAPIMath.cosinus(vWD_nAngle.getDouble()))));

// nY = nCentreY + (nDiametreCercle/2) * -Sin(nAngle)
//MAP:1dce6d2f0e4a57c5:00070000:14:FI_Mobile_Compteur_Jauge.PROCEDURE._PositionsSelonAngle:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:_PositionsSelonAngle
vWD_nY.setValeur(vWD_nCentreY.opPlus(vWD_nDiametreCercle.opDiv(2).opMult(WDAPIMath.sinus(vWD_nAngle.getDouble()).opMoinsUnaire())));

}
finally
{
finExecProcLocale();
}

}



public void fWD_dessineJauge( WDObjet vWD_nValeur , WDObjet vWD_nValeurMax )
{
// PROCÉDURE DessineJauge(LOCAL nValeur est un entier, LOCAL nValeurMax est un entier)
//MAP:1dce6d300e4b5b8e:00070000:1:FI_Mobile_Compteur_Jauge.PROCEDURE.DessineJauge:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:DessineJauge
initExecProcLocale("DessineJauge");

try
{
vWD_nValeur = WDParametre.traiterParametre(vWD_nValeur, 1, true, 8);

vWD_nValeurMax = WDParametre.traiterParametre(vWD_nValeurMax, 2, true, 8);


// nValeur = Min(nValeur, nValeurMax)
//MAP:1dce6d300e4b5b8e:00070000:4:FI_Mobile_Compteur_Jauge.PROCEDURE.DessineJauge:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:DessineJauge
vWD_nValeur.setValeur(WDAPINum.min(new WDObjet[] {vWD_nValeur,vWD_nValeurMax} ));

// SELON gnType
//MAP:1dce6d300e4b5b8e:00070000:6:FI_Mobile_Compteur_Jauge.PROCEDURE.DessineJauge:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:DessineJauge
// Délimiteur de visibilité pour ne pas étendre la visibilité de la variable temporaire _WDExpSelon
{
// SELON gnType
//MAP:1dce6d300e4b5b8e:00070000:6:FI_Mobile_Compteur_Jauge.PROCEDURE.DessineJauge:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:DessineJauge
WDObjet _WDExpSelon0 = vWD_gnType;
if(_WDExpSelon0.opEgal(1))
{
// 	CAS TYPE_ROND : _DessineJaugeRond(100*nValeur/nValeurMax)
//MAP:1dce6d300e4b5b8e:00070000:7:FI_Mobile_Compteur_Jauge.PROCEDURE.DessineJauge:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:DessineJauge
fWD__DessineJaugeRond(new WDEntier4(100).opMult(vWD_nValeur).opDiv(vWD_nValeurMax));

}
else if(_WDExpSelon0.opEgal(2))
{
// 	CAS TYPE_ANNEAU : _DessineJaugeAnneau(100*nValeur/nValeurMax)		
//MAP:1dce6d300e4b5b8e:00070000:8:FI_Mobile_Compteur_Jauge.PROCEDURE.DessineJauge:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:DessineJauge
fWD__DessineJaugeAnneau(new WDEntier4(100).opMult(vWD_nValeur).opDiv(vWD_nValeurMax));

}

}

// IMG_Dessin..Visible=Vrai
//MAP:1dce6d300e4b5b8e:00070000:c:FI_Mobile_Compteur_Jauge.PROCEDURE.DessineJauge:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:DessineJauge
mWD_IMG_Dessin.setProp(EWDPropriete.PROP_VISIBLE,true);

// SI gbAvecTexte ALORS
//MAP:1dce6d300e4b5b8e:00070000:10:FI_Mobile_Compteur_Jauge.PROCEDURE.DessineJauge:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:DessineJauge
if(vWD_gbAvecTexte.getBoolean())
{
// 	LIB_Valeur = nValeur
//MAP:1dce6d300e4b5b8e:00070000:13:FI_Mobile_Compteur_Jauge.PROCEDURE.DessineJauge:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:DessineJauge
mWD_LIB_Valeur.setValeur(vWD_nValeur);

// 	LIB_Total = nValeurMax
//MAP:1dce6d300e4b5b8e:00070000:14:FI_Mobile_Compteur_Jauge.PROCEDURE.DessineJauge:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:DessineJauge
mWD_LIB_Total.setValeur(vWD_nValeurMax);

// 	LIB_Valeur..Visible = Vrai
//MAP:1dce6d300e4b5b8e:00070000:15:FI_Mobile_Compteur_Jauge.PROCEDURE.DessineJauge:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:DessineJauge
mWD_LIB_Valeur.setProp(EWDPropriete.PROP_VISIBLE,true);

// 	LIB_Total..Visible = Vrai
//MAP:1dce6d300e4b5b8e:00070000:16:FI_Mobile_Compteur_Jauge.PROCEDURE.DessineJauge:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:DessineJauge
mWD_LIB_Total.setProp(EWDPropriete.PROP_VISIBLE,true);

// 	LIB_Valeur..Couleur = gnCouleurTexte
//MAP:1dce6d300e4b5b8e:00070000:17:FI_Mobile_Compteur_Jauge.PROCEDURE.DessineJauge:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:DessineJauge
mWD_LIB_Valeur.setProp(EWDPropriete.PROP_COULEUR,vWD_gnCouleurTexte);

// 	LIB_Total..Couleur = gnCouleurTexte
//MAP:1dce6d300e4b5b8e:00070000:18:FI_Mobile_Compteur_Jauge.PROCEDURE.DessineJauge:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:DessineJauge
mWD_LIB_Total.setProp(EWDPropriete.PROP_COULEUR,vWD_gnCouleurTexte);

}
else
{
// 	LIB_Valeur..Visible = Faux
//MAP:1dce6d300e4b5b8e:00070000:1b:FI_Mobile_Compteur_Jauge.PROCEDURE.DessineJauge:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:DessineJauge
mWD_LIB_Valeur.setProp(EWDPropriete.PROP_VISIBLE,false);

// 	LIB_Total..Visible = Faux
//MAP:1dce6d300e4b5b8e:00070000:1c:FI_Mobile_Compteur_Jauge.PROCEDURE.DessineJauge:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:DessineJauge
mWD_LIB_Total.setProp(EWDPropriete.PROP_VISIBLE,false);

}

}
finally
{
finExecProcLocale();
}

}




/**
 * Traitement: Déclarations globales de FI_Mobile_Compteur_Jauge
 */
public void declarerGlobale(WDObjet[] WD_tabParam)
{
// PROCEDURE MaFenêtre(gnValeur est un entier = 0, gnValeurMaximale est un entier = 10 000)
//MAP:2dce6c3a226e9adc:00000000:1:FI_Mobile_Compteur_Jauge.FI_Mobile_Compteur_Jauge:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:Déclarations globales de FI_Mobile_Compteur_Jauge
super.declarerGlobale(WD_tabParam, 0, 2);
int WD_ntabParamLen = 0;
if(WD_tabParam!=null) WD_ntabParamLen = WD_tabParam.length;

// Traitement du paramètre n°0
if(0<WD_ntabParamLen) 
{
vWD_gnValeur = WD_tabParam[0];
}
else { vWD_gnValeur = new WDEntier4(0); }
super.ajouterVariableGlobale("gnValeur",vWD_gnValeur);

// Traitement du paramètre n°1
if(1<WD_ntabParamLen) 
{
vWD_gnValeurMaximale = WD_tabParam[1];
}
else { vWD_gnValeurMaximale = new WDEntier4(10000); }
super.ajouterVariableGlobale("gnValeurMaximale",vWD_gnValeurMaximale);


vWD_gnValeur = WDParametre.traiterParametre(vWD_gnValeur, 1, false, 8);

vWD_gnValeurMaximale = WDParametre.traiterParametre(vWD_gnValeurMaximale, 2, false, 8);


// CONSTANTE
//MAP:2dce6c3a226e9adc:00000000:4:FI_Mobile_Compteur_Jauge.FI_Mobile_Compteur_Jauge:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:Déclarations globales de FI_Mobile_Compteur_Jauge

// gnAngleDépart est un entier = 90 
//MAP:2dce6c3a226e9adc:00000000:a:FI_Mobile_Compteur_Jauge.FI_Mobile_Compteur_Jauge:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:Déclarations globales de FI_Mobile_Compteur_Jauge
vWD_gnAngleDepart = new WDEntier4();

vWD_gnAngleDepart.setValeur(90);

super.ajouterVariableGlobale("gnAngleDépart",vWD_gnAngleDepart);



// gnType est un entier = TYPE_ANNEAU
//MAP:2dce6c3a226e9adc:00000000:c:FI_Mobile_Compteur_Jauge.FI_Mobile_Compteur_Jauge:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:Déclarations globales de FI_Mobile_Compteur_Jauge
vWD_gnType = new WDEntier4();

vWD_gnType.setValeur(2);

super.ajouterVariableGlobale("gnType",vWD_gnType);



// gnCouleurJauge est un entier = RVB(41, 128, 185) 
//MAP:2dce6c3a226e9adc:00000000:e:FI_Mobile_Compteur_Jauge.FI_Mobile_Compteur_Jauge:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:Déclarations globales de FI_Mobile_Compteur_Jauge
vWD_gnCouleurJauge = new WDEntier4();

vWD_gnCouleurJauge.setValeur(WDAPIDessin.rvb(41,128,185));

super.ajouterVariableGlobale("gnCouleurJauge",vWD_gnCouleurJauge);



// gnCouleurDécor est un entier =  gnType=TYPE_ROND ? RVB(52, 73, 94) 	SINON  RVB(239, 246, 255) 
//MAP:2dce6c3a226e9adc:00000000:10:FI_Mobile_Compteur_Jauge.FI_Mobile_Compteur_Jauge:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:Déclarations globales de FI_Mobile_Compteur_Jauge
vWD_gnCouleurDecor = new WDEntier4();

vWD_gnCouleurDecor.setValeur((vWD_gnType.opEgal(1) ? (WDObjet)WDAPIDessin.rvb(52,73,94) : (WDObjet)WDAPIDessin.rvb(239,246,255)));

super.ajouterVariableGlobale("gnCouleurDécor",vWD_gnCouleurDecor);



// gnCouleurTexte est un entier =  gnType=TYPE_ROND ? RVB(226, 232, 233) SINON  RVB(52, 73, 94) 
//MAP:2dce6c3a226e9adc:00000000:12:FI_Mobile_Compteur_Jauge.FI_Mobile_Compteur_Jauge:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:Déclarations globales de FI_Mobile_Compteur_Jauge
vWD_gnCouleurTexte = new WDEntier4();

vWD_gnCouleurTexte.setValeur((vWD_gnType.opEgal(1) ? (WDObjet)WDAPIDessin.rvb(226,232,233) : (WDObjet)WDAPIDessin.rvb(52,73,94)));

super.ajouterVariableGlobale("gnCouleurTexte",vWD_gnCouleurTexte);



// gbAvecTexte est un booléen = Vrai 
//MAP:2dce6c3a226e9adc:00000000:14:FI_Mobile_Compteur_Jauge.FI_Mobile_Compteur_Jauge:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:Déclarations globales de FI_Mobile_Compteur_Jauge
vWD_gbAvecTexte = new WDBooleen();

vWD_gbAvecTexte.setValeur(true);

super.ajouterVariableGlobale("gbAvecTexte",vWD_gbAvecTexte);



// gbSensHoraire est un booléen = Vrai
//MAP:2dce6c3a226e9adc:00000000:16:FI_Mobile_Compteur_Jauge.FI_Mobile_Compteur_Jauge:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:Déclarations globales de FI_Mobile_Compteur_Jauge
vWD_gbSensHoraire = new WDBooleen();

vWD_gbSensHoraire.setValeur(true);

super.ajouterVariableGlobale("gbSensHoraire",vWD_gbSensHoraire);



// DessineJauge(gnValeur,gnValeurMaximale)
//MAP:2dce6c3a226e9adc:00000000:1a:FI_Mobile_Compteur_Jauge.FI_Mobile_Compteur_Jauge:com.logicorp.home_biking.wdgen.GWDFIFI_Mobile_Compteur_Jauge:Déclarations globales de FI_Mobile_Compteur_Jauge
fWD_dessineJauge(vWD_gnValeur,vWD_gnValeurMaximale);

}




// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
 public WDObjet vWD_gnValeur = WDVarNonAllouee.ref;
 public WDObjet vWD_gnValeurMaximale = WDVarNonAllouee.ref;
 public WDObjet vWD_gnAngleDepart = WDVarNonAllouee.ref;
 public WDObjet vWD_gnType = WDVarNonAllouee.ref;
 public WDObjet vWD_gnCouleurJauge = WDVarNonAllouee.ref;
 public WDObjet vWD_gnCouleurDecor = WDVarNonAllouee.ref;
 public WDObjet vWD_gnCouleurTexte = WDVarNonAllouee.ref;
 public WDObjet vWD_gbAvecTexte = WDVarNonAllouee.ref;
 public WDObjet vWD_gbSensHoraire = WDVarNonAllouee.ref;
////////////////////////////////////////////////////////////////////////////
// Création des champs de la fenêtre FI_Mobile_Compteur_Jauge
////////////////////////////////////////////////////////////////////////////
protected void creerChamps()
{
mWD_IMG_Dessin = new GWDIMG_Dessin();
mWD_LIB_Total = new GWDLIB_Total();
mWD_LIB_Valeur = new GWDLIB_Valeur();

}
////////////////////////////////////////////////////////////////////////////
// Initialisation de la fenêtre FI_Mobile_Compteur_Jauge
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setQuid(3300694573850729180l);

super.setChecksum("1346191382");

super.setNom("FI_Mobile_Compteur_Jauge");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setTailleInitiale(250, 250);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setCouleurFond(0xFFFFFFFF);


activerEcoute();

////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FI_Mobile_Compteur_Jauge
////////////////////////////////////////////////////////////////////////////
mWD_IMG_Dessin.initialiserObjet();
super.ajouter("IMG_Dessin", mWD_IMG_Dessin);
mWD_LIB_Total.initialiserObjet();
super.ajouter("LIB_Total", mWD_LIB_Total);
mWD_LIB_Valeur.initialiserObjet();
super.ajouter("LIB_Valeur", mWD_LIB_Valeur);

super.terminerInitialisation();
}

////////////////////////////////////////////////////////////////////////////
public boolean isUniteAffichageLogique()
{
return true;
}

public WDProjet getProjet()
{
return GWDPHome_Biking.getInstance();
}

public IWDEnsembleElement getEnsemble()
{
return GWDPHome_Biking.getInstance();
}
public int getModeContexteHF()
{
return 1;
}
}
