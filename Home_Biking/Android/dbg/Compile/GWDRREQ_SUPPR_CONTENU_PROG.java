/**
 * Code généré par WINDEV Mobile - NE PAS MODIFIER !
 * Objet WINDEV Mobile : Requête
 * Classe Android : REQ_SUPPR_CONTENU_PROG
 * Date : 06/03/2021 17:18:02
 * Version de wdjava.dll  : 25.0.221.6
 */


package com.logicorp.home_biking.wdgen;


import com.logicorp.home_biking.*;
import fr.pcsoft.wdjava.core.types.*;
import fr.pcsoft.wdjava.core.*;
import fr.pcsoft.wdjava.database.hf.requete.parsing.*;
/*Imports trouvés dans le code WL*/
/*Fin Imports trouvés dans le code WL*/



public class GWDRREQ_SUPPR_CONTENU_PROG extends WDDescRequeteWDR
{
public String getNomLogique()
{
return "REQ_SUPPR_CONTENU_PROG";
}
public String getCodeSQLOriginal()
{
return "DELETE FROM \r\n\tcontenir\r\nWHERE contenir.Id_Programme={ParamId_Programme#0}";
}
public Requete initArbre() throws WDInvalidSQLException
{
Fichier fic_contenir = new Fichier();
fic_contenir.setNom("contenir");
fic_contenir.setAlias("contenir");
Requete varReqDelete = new Requete(5);
varReqDelete.ajouterClause(fic_contenir);
Expression expr__ = new Expression(9, "=", "contenir.Id_Programme={ParamId_Programme}");
Rubrique rub_Id_Programme = new Rubrique();
rub_Id_Programme.setNom("contenir.Id_Programme");
rub_Id_Programme.setAlias("Id_Programme");
rub_Id_Programme.setNomFichier("contenir");
rub_Id_Programme.setAliasFichier("contenir");
expr__.ajouterElement(rub_Id_Programme);
Parametre param_ParamId_Programme = new Parametre();
param_ParamId_Programme.setNom("ParamId_Programme");
expr__.ajouterElement(param_ParamId_Programme);
Where varWhere = new Where();
varWhere.ajouterElement(expr__);
varReqDelete.ajouterClause(varWhere);
return varReqDelete;
}
public String getNomFichier(int nIndex)
{
switch(nIndex)
{
case 0 : return "contenir";
default: return null;
}
}
public String getAliasFichier(int nIndex)
{
switch(nIndex)
{
case 0 : return "contenir";
default: return null;
}
}


public int getIdWDR()
{
return com.logicorp.home_biking.R.raw.req_suppr_contenu_prog;
}
public String getNomFichierWDR()
{
return "req_suppr_contenu_prog";
}
}
