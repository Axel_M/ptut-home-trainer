/**
 * Code généré par WINDEV Mobile - NE PAS MODIFIER !
 * Objet WINDEV Mobile : Fenêtre
 * Classe Android : FEN_Preseance
 * Date : 02/04/2021 14:52:47
 * Version de wdjava.dll  : 25.0.221.6
 */


package com.logicorp.home_biking.wdgen;


import com.logicorp.home_biking.*;
import fr.pcsoft.wdjava.core.types.*;
import fr.pcsoft.wdjava.core.*;
import fr.pcsoft.wdjava.ui.champs.fenetre.*;
import fr.pcsoft.wdjava.ui.champs.image.*;
import fr.pcsoft.wdjava.ui.cadre.*;
import fr.pcsoft.wdjava.api.*;
import fr.pcsoft.wdjava.ui.champs.libelle.*;
import fr.pcsoft.wdjava.ui.champs.table.*;
import fr.pcsoft.wdjava.ui.champs.table.colonne.*;
import fr.pcsoft.wdjava.core.context.*;
import fr.pcsoft.wdjava.core.application.*;
import fr.pcsoft.wdjava.ui.activite.*;
/*Imports trouvés dans le code WL*/
/*Fin Imports trouvés dans le code WL*/



public class GWDFFEN_Preseance extends WDFenetre
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs de FEN_Preseance
////////////////////////////////////////////////////////////////////////////

/**
 * IMG_Button_Back
 */
class GWDIMG_Button_Back extends WDChampImage
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FEN_Preseance.IMG_Button_Back
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3440288797358204768l);

super.setChecksum("990450191");

super.setNom("IMG_Button_Back");

super.setType(30001);

super.setBulle("");

super.setLibelle("");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setNavigable(false);

super.setEtatInitial(0);

super.setPositionInitiale(8, 8);

super.setTailleInitiale(52, 52);

super.setValeurInitiale("C:\\Users\\axelm\\Desktop\\COURS IUT INFO\\Semestre 3\\M3302 - Projet tutore - Mise en situation professionnelle\\ptut-home-trainer\\Home_Biking\\Bouton_previous_page.png");

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(1);

super.setAncrageInitial(0, 1000, 1000, 1000, 1000, 0);

super.setTransparence(1);

super.setParamImage(2097158, 0, true, 100);

super.setSymetrie(0);

super.setZoneClicage(true);

super.setPCodeMultitouch(false);

super.setChargementEnTacheDeFond(false);

super.setOrientationExif(false);

super.setParamAnimation(1, 1, false, 300, true, false);

super.setAnimationInitiale(false);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -1, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

/**
 * Traitement: Clic sur IMG_Button_Back
 */
public void clicSurBoutonGauche()
{
super.clicSurBoutonGauche();

// Ferme()
//MAP:2fbe5c6f0b4aa760:00000012:1:FEN_Preseance.IMG_Button_Back:com.logicorp.home_biking.wdgen.GWDFFEN_Preseance$GWDIMG_Button_Back:Clic sur IMG_Button_Back
// Ferme()
//MAP:2fbe5c6f0b4aa760:00000012:1:FEN_Preseance.IMG_Button_Back:com.logicorp.home_biking.wdgen.GWDFFEN_Preseance$GWDIMG_Button_Back:Clic sur IMG_Button_Back
WDAPIFenetre.ferme();

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurClic();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDIMG_Button_Back mWD_IMG_Button_Back;

/**
 * IMG_Button_Modif
 */
class GWDIMG_Button_Modif extends WDChampImage
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°2 de FEN_Preseance.IMG_Button_Modif
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3440291434480273333l);

super.setChecksum("1002599626");

super.setNom("IMG_Button_Modif");

super.setType(30001);

super.setBulle("");

super.setLibelle("");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setNavigable(false);

super.setEtatInitial(0);

super.setPositionInitiale(475, 99);

super.setTailleInitiale(77, 37);

super.setValeurInitiale("C:\\Users\\axelm\\Desktop\\COURS IUT INFO\\Semestre 3\\M3302 - Projet tutore - Mise en situation professionnelle\\ptut-home-trainer\\Home_Biking\\button modif.png");

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(2);

super.setAncrageInitial(5, 1000, 1000, 1000, 500, 0);

super.setTransparence(1);

super.setParamImage(2097158, 0, true, 100);

super.setSymetrie(0);

super.setZoneClicage(true);

super.setPCodeMultitouch(false);

super.setChargementEnTacheDeFond(false);

super.setOrientationExif(false);

super.setParamAnimation(1, 1, false, 300, true, false);

super.setAnimationInitiale(false);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -1, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDIMG_Button_Modif mWD_IMG_Button_Modif;

/**
 * IMG_Button_Start
 */
class GWDIMG_Button_Start extends WDChampImage
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°3 de FEN_Preseance.IMG_Button_Start
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3440291601985085578l);

super.setChecksum("1003687366");

super.setNom("IMG_Button_Start");

super.setType(30001);

super.setBulle("");

super.setLibelle("");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setNavigable(false);

super.setEtatInitial(0);

super.setPositionInitiale(475, 265);

super.setTailleInitiale(77, 37);

super.setValeurInitiale("C:\\Users\\axelm\\Desktop\\COURS IUT INFO\\Semestre 3\\M3302 - Projet tutore - Mise en situation professionnelle\\ptut-home-trainer\\Home_Biking\\button start.png");

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(3);

super.setAncrageInitial(5, 1000, 1000, 1000, 1000, 0);

super.setTransparence(1);

super.setParamImage(2097158, 0, true, 100);

super.setSymetrie(0);

super.setZoneClicage(true);

super.setPCodeMultitouch(false);

super.setChargementEnTacheDeFond(false);

super.setOrientationExif(false);

super.setParamAnimation(1, 1, false, 300, true, false);

super.setAnimationInitiale(false);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -1, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

/**
 * Traitement: Clic sur IMG_Button_Start
 */
public void clicSurBoutonGauche()
{
super.clicSurBoutonGauche();

// OuvreFille(FEN_SeanceEnCours,FEN_Preseance.gParamIdProgramme)//amène sur la fenêtre de la seance du programme sélectionnée
//MAP:2fbe5efc0c14a08a:00000012:1:FEN_Preseance.IMG_Button_Start:com.logicorp.home_biking.wdgen.GWDFFEN_Preseance$GWDIMG_Button_Start:Clic sur IMG_Button_Start
// OuvreFille(FEN_SeanceEnCours,FEN_Preseance.gParamIdProgramme)//amène sur la fenêtre de la seance du programme sélectionnée
//MAP:2fbe5efc0c14a08a:00000012:1:FEN_Preseance.IMG_Button_Start:com.logicorp.home_biking.wdgen.GWDFFEN_Preseance$GWDIMG_Button_Start:Clic sur IMG_Button_Start
WDAPIFenetre.ouvreFille(GWDPHome_Biking.getInstance().mWD_FEN_SeanceEnCours,new WDObjet[] {vWD_gParamIdProgramme} );

// HLitRecherche(utiliser,Id_Programme,gParamIdProgramme)//cherche si le programme à déjà été utilisé
//MAP:2fbe5efc0c14a08a:00000012:3:FEN_Preseance.IMG_Button_Start:com.logicorp.home_biking.wdgen.GWDFFEN_Preseance$GWDIMG_Button_Start:Clic sur IMG_Button_Start
WDAPIHF.hLitRecherche(WDAPIHF.getFichierSansCasseNiAccent("utiliser"),WDAPIHF.getRubriqueSansCasseNiAccent("id_programme"),vWD_gParamIdProgramme);

// SI HTrouve()=VRAI ALORS//si oui
//MAP:2fbe5efc0c14a08a:00000012:5:FEN_Preseance.IMG_Button_Start:com.logicorp.home_biking.wdgen.GWDFFEN_Preseance$GWDIMG_Button_Start:Clic sur IMG_Button_Start
if(WDAPIHF.hTrouve().opEgal(true))
{
// 	utiliser.DateUtilisation=DateHeureSys() //met à jour sa date d'utilisation dans la table utiliser
//MAP:2fbe5efc0c14a08a:00000012:6:FEN_Preseance.IMG_Button_Start:com.logicorp.home_biking.wdgen.GWDFFEN_Preseance$GWDIMG_Button_Start:Clic sur IMG_Button_Start
WDAPIHF.getFichierSansCasseNiAccent("utiliser").getRubriqueSansCasseNiAccent("dateutilisation").setValeur(WDAPIDate.dateHeureSys());

// 	HModifie(utiliser,hNumEnrEnCours,hEcritureDéfaut)
//MAP:2fbe5efc0c14a08a:00000012:7:FEN_Preseance.IMG_Button_Start:com.logicorp.home_biking.wdgen.GWDFFEN_Preseance$GWDIMG_Button_Start:Clic sur IMG_Button_Start
WDAPIHF.hModifie(WDAPIHF.getFichierSansCasseNiAccent("utiliser"),(long)0,(long)128);

}
else
{
// 	HAjoute(utiliser,hEcritureDéfaut) //sinon
//MAP:2fbe5efc0c14a08a:00000012:9:FEN_Preseance.IMG_Button_Start:com.logicorp.home_biking.wdgen.GWDFFEN_Preseance$GWDIMG_Button_Start:Clic sur IMG_Button_Start
WDAPIHF.hAjoute(WDAPIHF.getFichierSansCasseNiAccent("utiliser"),new WDEntier4(128));

// 	utiliser.DateUtilisation=DateHeureSys()//le rajoute dans la table utiliser avec la date du jour
//MAP:2fbe5efc0c14a08a:00000012:a:FEN_Preseance.IMG_Button_Start:com.logicorp.home_biking.wdgen.GWDFFEN_Preseance$GWDIMG_Button_Start:Clic sur IMG_Button_Start
WDAPIHF.getFichierSansCasseNiAccent("utiliser").getRubriqueSansCasseNiAccent("dateutilisation").setValeur(WDAPIDate.dateHeureSys());

// 	HRecherche(Programme,Id_Programme,gParamIdProgramme)
//MAP:2fbe5efc0c14a08a:00000012:b:FEN_Preseance.IMG_Button_Start:com.logicorp.home_biking.wdgen.GWDFFEN_Preseance$GWDIMG_Button_Start:Clic sur IMG_Button_Start
WDAPIHF.hRecherche(WDAPIHF.getFichierSansCasseNiAccent("programme"),WDAPIHF.getRubriqueSansCasseNiAccent("id_programme"),vWD_gParamIdProgramme);

// 	utiliser.Id_Compte_Client	=Programme.Id_Compte_Client
//MAP:2fbe5efc0c14a08a:00000012:c:FEN_Preseance.IMG_Button_Start:com.logicorp.home_biking.wdgen.GWDFFEN_Preseance$GWDIMG_Button_Start:Clic sur IMG_Button_Start
WDAPIHF.getFichierSansCasseNiAccent("utiliser").getRubriqueSansCasseNiAccent("id_compte_client").setValeur(WDAPIHF.getFichierSansCasseNiAccent("programme").getRubriqueSansCasseNiAccent("id_compte_client"));

// 	utiliser.Id_Programme		=FEN_Preseance.gParamIdProgramme
//MAP:2fbe5efc0c14a08a:00000012:d:FEN_Preseance.IMG_Button_Start:com.logicorp.home_biking.wdgen.GWDFFEN_Preseance$GWDIMG_Button_Start:Clic sur IMG_Button_Start
WDAPIHF.getFichierSansCasseNiAccent("utiliser").getRubriqueSansCasseNiAccent("id_programme").setValeur(vWD_gParamIdProgramme);

// 	HModifie(utiliser,hNumEnrEnCours,hEcritureDéfaut)
//MAP:2fbe5efc0c14a08a:00000012:e:FEN_Preseance.IMG_Button_Start:com.logicorp.home_biking.wdgen.GWDFFEN_Preseance$GWDIMG_Button_Start:Clic sur IMG_Button_Start
WDAPIHF.hModifie(WDAPIHF.getFichierSansCasseNiAccent("utiliser"),(long)0,(long)128);

}

// ZoneRépétéeAffiche(FEN_Accueil.ZR_Programme_Recent,taRéExécuteRequete) //met à jour la zone répétée de l'accueil
//MAP:2fbe5efc0c14a08a:00000012:11:FEN_Preseance.IMG_Button_Start:com.logicorp.home_biking.wdgen.GWDFFEN_Preseance$GWDIMG_Button_Start:Clic sur IMG_Button_Start
WDAPIZoneRepetee.zoneRepeteeAffiche(GWDPHome_Biking.getInstance().getFEN_Accueil().mWD_ZR_Programme_Recent,new WDChaineU("Reexecute"));

// Multitâche(1s)
//MAP:2fbe5efc0c14a08a:00000012:12:FEN_Preseance.IMG_Button_Start:com.logicorp.home_biking.wdgen.GWDFFEN_Preseance$GWDIMG_Button_Start:Clic sur IMG_Button_Start
WDAPIVM.multitache((new WDDuree("0000001000")));

// ExécuteTraitement(FEN_SeanceEnCours,trtModification) //lancement du traitement du programme en cours 1 sec après l'arrivée sur l'écran de la séance
//MAP:2fbe5efc0c14a08a:00000012:13:FEN_Preseance.IMG_Button_Start:com.logicorp.home_biking.wdgen.GWDFFEN_Preseance$GWDIMG_Button_Start:Clic sur IMG_Button_Start
WDAPIVM.executeTraitement(GWDPHome_Biking.getInstance().mWD_FEN_SeanceEnCours,17);

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurClic();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDIMG_Button_Start mWD_IMG_Button_Start;

/**
 * IMG_Button_Suppr
 */
class GWDIMG_Button_Suppr extends WDChampImage
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°4 de FEN_Preseance.IMG_Button_Suppr
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3440291803849577858l);

super.setChecksum("1004716781");

super.setNom("IMG_Button_Suppr");

super.setType(30001);

super.setBulle("");

super.setLibelle("");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setNavigable(false);

super.setEtatInitial(0);

super.setPositionInitiale(475, 146);

super.setTailleInitiale(77, 37);

super.setValeurInitiale("C:\\Users\\axelm\\Desktop\\COURS IUT INFO\\Semestre 3\\M3302 - Projet tutore - Mise en situation professionnelle\\ptut-home-trainer\\Home_Biking\\button suppr.png");

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(4);

super.setAncrageInitial(5, 1000, 1000, 1000, 500, 0);

super.setTransparence(1);

super.setParamImage(2097158, 0, true, 100);

super.setSymetrie(0);

super.setZoneClicage(true);

super.setPCodeMultitouch(false);

super.setChargementEnTacheDeFond(false);

super.setOrientationExif(false);

super.setParamAnimation(1, 1, false, 300, true, false);

super.setAnimationInitiale(false);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -1, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

/**
 * Traitement: Clic sur IMG_Button_Suppr
 */
public void clicSurBoutonGauche()
// En cours de développement avec la partie personnalisation 
{
super.clicSurBoutonGauche();

// //En cours de développement avec la partie personnalisation 
//MAP:2fbe5f2b0c245582:00000012:1:FEN_Preseance.IMG_Button_Suppr:com.logicorp.home_biking.wdgen.GWDFFEN_Preseance$GWDIMG_Button_Suppr:Clic sur IMG_Button_Suppr
// SELON Dialogue("Voulez vous supprimer ce programme ?")
//MAP:2fbe5f2b0c245582:00000012:2:FEN_Preseance.IMG_Button_Suppr:com.logicorp.home_biking.wdgen.GWDFFEN_Preseance$GWDIMG_Button_Suppr:Clic sur IMG_Button_Suppr
// Délimiteur de visibilité pour ne pas étendre la visibilité de la variable temporaire _WDExpSelon
{
// SELON Dialogue("Voulez vous supprimer ce programme ?")
//MAP:2fbe5f2b0c245582:00000012:2:FEN_Preseance.IMG_Button_Suppr:com.logicorp.home_biking.wdgen.GWDFFEN_Preseance$GWDIMG_Button_Suppr:Clic sur IMG_Button_Suppr
WDObjet _WDExpSelon0 = WDAPIDialogue.dialogue(0);
if(_WDExpSelon0.opEgal(1))
{
// 		HLitRecherche(contenir,Id_Programme,gParamIdProgramme)
//MAP:2fbe5f2b0c245582:00000012:5:FEN_Preseance.IMG_Button_Suppr:com.logicorp.home_biking.wdgen.GWDFFEN_Preseance$GWDIMG_Button_Suppr:Clic sur IMG_Button_Suppr
WDAPIHF.hLitRecherche(WDAPIHF.getFichierSansCasseNiAccent("contenir"),WDAPIHF.getRubriqueSansCasseNiAccent("id_programme"),vWD_gParamIdProgramme);

// 		BOUCLE
//MAP:2fbe5f2b0c245582:00000012:6:FEN_Preseance.IMG_Button_Suppr:com.logicorp.home_biking.wdgen.GWDFFEN_Preseance$GWDIMG_Button_Suppr:Clic sur IMG_Button_Suppr
{
do
{
// 			HSupprime(contenir,hNumEnrEnCours)
//MAP:2fbe5f2b0c245582:00000012:7:FEN_Preseance.IMG_Button_Suppr:com.logicorp.home_biking.wdgen.GWDFFEN_Preseance$GWDIMG_Button_Suppr:Clic sur IMG_Button_Suppr
WDAPIHF.hSupprime(WDAPIHF.getFichierSansCasseNiAccent("contenir"),(long)0);

}
while(WDAPIHF.hLitSuivant().getBoolean());
}

// 		HLitRecherche(Programme,Id_Programme,gParamIdProgramme)
//MAP:2fbe5f2b0c245582:00000012:a:FEN_Preseance.IMG_Button_Suppr:com.logicorp.home_biking.wdgen.GWDFFEN_Preseance$GWDIMG_Button_Suppr:Clic sur IMG_Button_Suppr
WDAPIHF.hLitRecherche(WDAPIHF.getFichierSansCasseNiAccent("programme"),WDAPIHF.getRubriqueSansCasseNiAccent("id_programme"),vWD_gParamIdProgramme);

// 		HSupprime(Programme,hNumEnrEnCours)
//MAP:2fbe5f2b0c245582:00000012:b:FEN_Preseance.IMG_Button_Suppr:com.logicorp.home_biking.wdgen.GWDFFEN_Preseance$GWDIMG_Button_Suppr:Clic sur IMG_Button_Suppr
WDAPIHF.hSupprime(WDAPIHF.getFichierSansCasseNiAccent("programme"),(long)0);

// 		Ferme(Nom de la fenêtre, Valeur renvoyée)
//MAP:2fbe5f2b0c245582:00000012:d:FEN_Preseance.IMG_Button_Suppr:com.logicorp.home_biking.wdgen.GWDFFEN_Preseance$GWDIMG_Button_Suppr:Clic sur IMG_Button_Suppr
WDAPIFenetre.ferme();

}
else if(_WDExpSelon0.opEgal(2))
{
}

}

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurClic();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDIMG_Button_Suppr mWD_IMG_Button_Suppr;

/**
 * LIB_Temps
 */
class GWDLIB_Temps extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°5 de FEN_Preseance.LIB_Temps
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3440292387966249685l);

super.setChecksum("1005834208");

super.setNom("LIB_Temps");

super.setType(3);

super.setBulle("");

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("Temps : ");

super.setNote("", "");

super.setCurseurSouris(0);

super.setEtatInitial(0);

super.setPositionInitiale(8, 280);

super.setTailleInitiale(120, 19);

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(5);

super.setAncrageInitial(1, 1000, 1000, 1000, 1000, 0);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, 0xFFFFFFFF, creerPolice_GEN("Roboto", -8.000000, 0), 3, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_Temps mWD_LIB_Temps;

/**
 * LIB_Nom_Prog
 */
class GWDLIB_Nom_Prog extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°6 de FEN_Preseance.LIB_Nom_Prog
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3440294629948219596l);

super.setChecksum("1014876129");

super.setNom("LIB_Nom_Prog");

super.setType(3);

super.setBulle("");

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("Nom Programme");

super.setNote("", "");

super.setCurseurSouris(0);

super.setEtatInitial(0);

super.setPositionInitiale(233, 21);

super.setTailleInitiale(319, 30);

super.setPlan(0);

super.setCadrageHorizontal(2);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(6);

super.setAncrageInitial(4, 1000, 1000, 1000, 1000, 0);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, 0xFFFFFFFF, creerPolice_GEN("Roboto", -12.000000, 0), 3, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_Nom_Prog mWD_LIB_Nom_Prog;

/**
 * TABLE_FEN_Preseance
 */
class GWDTABLE_FEN_Preseance extends WDTableFichierEnMemoire
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°7 de FEN_Preseance.TABLE_FEN_Preseance
////////////////////////////////////////////////////////////////////////////

/**
 * COL_Nom
 */
class GWDCOL_Nom extends WDColonneTexteSimple
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FEN_Preseance.TABLE_FEN_Preseance.COL_Nom
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3440297988649799985l);

super.setNom("COL_Nom");

super.setType(20001);

super.setBulle("");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setEtatInitial(1);

super.setLargeurInitiale(195);

super.setTitre("Nom");

super.setLiaisonFichier("req_exo_prog", "nom");

super.setLargeurMin(14);

super.setVisibleInitial(true);

super.setTriable(false);

super.setDeplacable(false);

super.setTauxAncrageLargeur(0);

super.setAjustable(false);

super.setAvecRecherche(false);

super.setDessinBandeauSelection(true);

initChampAssocie();

super.terminerInitialisation();
}
protected void initChampAssocie()
{
super.setCadrageHorizontal(1);

super.setCadrageVertical(1);

super.setMotDePasse(false);

super.setTaille(25);

super.setEffacementAutomatique(true);

super.setFinSaisieAutomatique(false);

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setEllipse(0);

super.setMasqueAffichage(new WDChaineU(""));

super.setMiseABlancSiZero(false);

super.setVerifieOrthographe(true);

}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDCOL_Nom mWD_COL_Nom = new GWDCOL_Nom();

/**
 * COL_Nb_rep
 */
class GWDCOL_Nb_rep extends WDColonneTexteSimple
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°2 de FEN_Preseance.TABLE_FEN_Preseance.COL_Nb_rep
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3440297988649931057l);

super.setNom("COL_Nb_rep");

super.setType(20004);

super.setBulle("");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setEtatInitial(0);

super.setLargeurInitiale(76);

super.setTitre("NB Rep");

super.setLiaisonFichier("req_exo_prog", "nb_rep");

super.setLargeurMin(14);

super.setVisibleInitial(true);

super.setTriable(true);

super.setDeplacable(true);

super.setTauxAncrageLargeur(0);

super.setAjustable(true);

super.setAvecRecherche(true);

super.setDessinBandeauSelection(true);

initChampAssocie();

super.terminerInitialisation();
}
protected void initChampAssocie()
{
super.setCadrageHorizontal(2);

super.setCadrageVertical(1);

super.setMotDePasse(false);

super.setTaille(0);

super.setEffacementAutomatique(true);

super.setFinSaisieAutomatique(false);

super.setTypeSaisie(1);

super.setMasqueSaisie(new WDChaineU("999"));

super.setEllipse(0);

super.setMasqueAffichage(new WDChaineU(""));

super.setMiseABlancSiZero(false);

super.setVerifieOrthographe(false);

}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDCOL_Nb_rep mWD_COL_Nb_rep = new GWDCOL_Nb_rep();

/**
 * COL_Repos
 */
class GWDCOL_Repos extends WDColonneTexteSimple
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°3 de FEN_Preseance.TABLE_FEN_Preseance.COL_Repos
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3440297988650193233l);

super.setNom("COL_Repos");

super.setType(20004);

super.setBulle("");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setEtatInitial(0);

super.setLargeurInitiale(77);

super.setTitre("Repos");

super.setLiaisonFichier("req_exo_prog", "repos");

super.setLargeurMin(14);

super.setVisibleInitial(true);

super.setTriable(true);

super.setDeplacable(true);

super.setTauxAncrageLargeur(0);

super.setAjustable(true);

super.setAvecRecherche(true);

super.setDessinBandeauSelection(true);

initChampAssocie();

super.terminerInitialisation();
}
protected void initChampAssocie()
{
super.setCadrageHorizontal(2);

super.setCadrageVertical(1);

super.setMotDePasse(false);

super.setTaille(0);

super.setEffacementAutomatique(true);

super.setFinSaisieAutomatique(false);

super.setTypeSaisie(1);

super.setMasqueSaisie(new WDChaineU("+9 999 999 999"));

super.setEllipse(0);

super.setMasqueAffichage(new WDChaineU(""));

super.setMiseABlancSiZero(false);

super.setVerifieOrthographe(false);

}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDCOL_Repos mWD_COL_Repos = new GWDCOL_Repos();

/**
 * COL_Temps
 */
class GWDCOL_Temps extends WDColonneTexteSimple
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°4 de FEN_Preseance.TABLE_FEN_Preseance.COL_Temps
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3440297988650062145l);

super.setNom("COL_Temps");

super.setType(20004);

super.setBulle("");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setEtatInitial(0);

super.setLargeurInitiale(76);

super.setTitre("Temps");

super.setLiaisonFichier("req_exo_prog", "temps");

super.setLargeurMin(14);

super.setVisibleInitial(true);

super.setTriable(true);

super.setDeplacable(true);

super.setTauxAncrageLargeur(0);

super.setAjustable(true);

super.setAvecRecherche(true);

super.setDessinBandeauSelection(true);

initChampAssocie();

super.terminerInitialisation();
}
protected void initChampAssocie()
{
super.setCadrageHorizontal(2);

super.setCadrageVertical(1);

super.setMotDePasse(false);

super.setTaille(0);

super.setEffacementAutomatique(true);

super.setFinSaisieAutomatique(false);

super.setTypeSaisie(1);

super.setMasqueSaisie(new WDChaineU("+99 999"));

super.setEllipse(0);

super.setMasqueAffichage(new WDChaineU(""));

super.setMiseABlancSiZero(false);

super.setVerifieOrthographe(false);

}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDCOL_Temps mWD_COL_Temps = new GWDCOL_Temps();

/**
 * COL_idExo
 */
class GWDCOL_idExo extends WDColonneTexteSimple
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°5 de FEN_Preseance.TABLE_FEN_Preseance.COL_idExo
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3447999114097745279l);

super.setNom("COL_idExo");

super.setType(20004);

super.setBulle("");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setEtatInitial(1);

super.setLargeurInitiale(80);

super.setTitre("COL_idExo");

super.setLiaisonFichier("req_exo_prog", "ide");

super.setLargeurMin(14);

super.setVisibleInitial(false);

super.setTriable(true);

super.setDeplacable(true);

super.setTauxAncrageLargeur(0);

super.setAjustable(true);

super.setAvecRecherche(true);

super.setDessinBandeauSelection(true);

initChampAssocie();

super.terminerInitialisation();
}
protected void initChampAssocie()
{
super.setCadrageHorizontal(2);

super.setCadrageVertical(1);

super.setMotDePasse(false);

super.setTaille(0);

super.setEffacementAutomatique(true);

super.setFinSaisieAutomatique(false);

super.setTypeSaisie(1);

super.setMasqueSaisie(new WDChaineU("MoneySystemMask"));

super.setEllipse(0);

super.setMasqueAffichage(new WDChaineU(""));

super.setMiseABlancSiZero(false);

super.setVerifieOrthographe(false);

}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDCOL_idExo mWD_COL_idExo = new GWDCOL_idExo();
/**
 * Initialise tous les champs de FEN_Preseance.TABLE_FEN_Preseance
 */
public void initialiserSousObjets()
{
////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FEN_Preseance.TABLE_FEN_Preseance
////////////////////////////////////////////////////////////////////////////
super.initialiserSousObjets();
super.ajouterColonne("COL_Nom",mWD_COL_Nom);
super.ajouterColonne("COL_Nb_rep",mWD_COL_Nb_rep);
super.ajouterColonne("COL_Repos",mWD_COL_Repos);
super.ajouterColonne("COL_Temps",mWD_COL_Temps);
super.ajouterColonne("COL_idExo",mWD_COL_idExo);
mWD_COL_Nom.initialiserObjet();
mWD_COL_Nb_rep.initialiserObjet();
mWD_COL_Repos.initialiserObjet();
mWD_COL_Temps.initialiserObjet();
mWD_COL_idExo.initialiserObjet();

}
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3440297988649603362l);

super.setChecksum("1051837941");

super.setNom("TABLE_FEN_Preseance");

super.setType(9);

super.setBulle("");

super.setLibelle("Exercices");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setNavigable(true);

super.setEtatInitial(1);

super.setPositionInitiale(35, 82);

super.setTailleInitiale(425, 190);

super.setValeurInitiale("");

super.setPlan(0);

super.setSourceRemplissage("req_exo_prog", "", "", true, "", false);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(7);

super.setTailleCellule(24, 3, 3, 3, 3, 0);

super.setAncrageInitial(10, 1000, 1000, 1000, 1000, 0);

super.setNumTab(1);

super.setModeAscenseur(2, 1);

super.setModeSelection(99);

super.setSaisieEnCascade(false);

super.setLettreAppel(65535);

super.setNumColonneAncree(1);

super.setEnregistrementSortieLigne(true);

super.setPersistant(true);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -1, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0x222222, 0x0, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1), 0, 0, 0, 0);

super.setStyleSeparateurVerticaux(true, 0x222222);

super.setStyleSeparateurHorizontaux(1, 0x222222);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN(2, 0x222222, 0x0, 0xF0F0F0, 2.000000, 2.000000, 1, 1));

super.setDessinerLigneVide(true);

super.setCouleursLigne(0x222222, 0xF0F0F0, 0x222222, 0xFFFFFF);

super.setStyleSelection(0x222222, 0xE2E2E2, creerPolice_GEN("Roboto", -8.000000, 0));

super.setStyleEnteteColonne(15, 1, WDCadreFactory.creerCadre_GEN(1, 0x303030, 0x0, 0xFFFFFF, 2.000000, 2.000000, 1, 1), creerPolice_GEN("Roboto", -8.000000, 0), 0x222222, true, "C:\\Users\\axelm\\Desktop\\COURS IUT INFO\\Semestre 3\\M3302 - Projet tutore - Mise en situation professionnelle\\ptut-home-trainer\\Home_Biking\\Gabarits\\WM\\210 Material Design Orange\\Material Design Orange_Table_ColPict@dpi1x.png");

super.setCadreFondLigne(WDCadreFactory.creerCadre_GEN("", new int[] {1,2,1,2,2,2,1,2,1}, new int[] {4, 4, 4, 4}, 0xFFFFFFFF, 0, 5));

super.setPoliceColonne(creerPolice_GEN("Roboto", -8.000000, 0));

super.setScrollRapideTable(false, null);

super.setBtnEnrouleDeroule(true);

super.setSwipe(0);

super.setNbMaxLignes(0);

super.setImagePlusMoins("C:\\Users\\axelm\\Desktop\\COURS IUT INFO\\Semestre 3\\M3302 - Projet tutore - Mise en situation professionnelle\\ptut-home-trainer\\Home_Biking\\Gabarits\\WM\\210 Material Design Orange\\Material Design Orange_Break_Pict@dpi1x.png?E2_4O");

super.setMargeInterneEnteteColonne(0, 0, 0, 0);

activerEcoute();
initialiserSousObjets();
super.terminerInitialisation();
}

/**
 * Traitement: Initialisation de TABLE_FEN_Preseance
 */
public void init()
//  Le champ utilise une requête paramétrée pour afficher ses données.
//  Les paramètres de cette requête doivent être définis avant ou lors de l'initialisation du champ.
//  La requête sera exécutée automatiquement si au moins un paramètre a été défini.
// 
//  Pour plus d'informations, veuillez consulter l'aide :
//  Requête paramétrée, Utilisation dans un champ Table, une Liste ou une Combo
// 
//  Paramètres de la requête 'FEN_Preseance_1$Requête'
{
super.init();

// // Le champ utilise une requête paramétrée pour afficher ses données.
//MAP:2fbe64cb0ef35122:0000000e:1:FEN_Preseance.TABLE_FEN_Preseance:com.logicorp.home_biking.wdgen.GWDFFEN_Preseance$GWDTABLE_FEN_Preseance:Initialisation de TABLE_FEN_Preseance
// MaSource.ParamId_Programme = gParamIdProgramme
//MAP:2fbe64cb0ef35122:0000000e:9:FEN_Preseance.TABLE_FEN_Preseance:com.logicorp.home_biking.wdgen.GWDFFEN_Preseance$GWDTABLE_FEN_Preseance:Initialisation de TABLE_FEN_Preseance
WDContexte.getMaSource().get("ParamId_Programme").setValeur(vWD_gParamIdProgramme);

}




/**
 * Traitement: Sélection d'une ligne de TABLE_FEN_Preseance
 */
public void selectionLigne()
{
super.selectionLigne();

// OuvreFille(FEN_Description_Exo,TABLE_FEN_Preseance.COL_idExo) //A la sélection d'une ligne affiche la description de l'exercice de la ligne
//MAP:2fbe64cb0ef35122:00000021:1:FEN_Preseance.TABLE_FEN_Preseance:com.logicorp.home_biking.wdgen.GWDFFEN_Preseance$GWDTABLE_FEN_Preseance:Sélection d'une ligne de TABLE_FEN_Preseance
// OuvreFille(FEN_Description_Exo,TABLE_FEN_Preseance.COL_idExo) //A la sélection d'une ligne affiche la description de l'exercice de la ligne
//MAP:2fbe64cb0ef35122:00000021:1:FEN_Preseance.TABLE_FEN_Preseance:com.logicorp.home_biking.wdgen.GWDFFEN_Preseance$GWDTABLE_FEN_Preseance:Sélection d'une ligne de TABLE_FEN_Preseance
WDAPIFenetre.ouvreFille(GWDPHome_Biking.getInstance().mWD_FEN_Description_Exo,new WDObjet[] {mWD_COL_idExo} );

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurSelection();
}
protected final int getIdEnteteAt(int nColonne, int nLigne) {return 0;}
protected final String getLibelleEnteteFromId(int nId) {return null;}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDTABLE_FEN_Preseance mWD_TABLE_FEN_Preseance;


////////////////////////////////////////////////////////////////////////////
// Procédures utilisateur de FEN_Preseance
////////////////////////////////////////////////////////////////////////////
public void fWD_seance()
{
// PROCEDURE Seance()
//MAP:1fc79b02000c15a8:00070000:1:FEN_Preseance.PROCEDURE.Seance:com.logicorp.home_biking.wdgen.GWDFFEN_Preseance:Seance
initExecProcLocale("Seance");

try
{
}
finally
{
finExecProcLocale();
}

}




/**
 * Traitement: Déclarations globales de FEN_Preseance
 */
public void declarerGlobale(WDObjet[] WD_tabParam)
{
// PROCEDURE MaFenêtre(gParamIdProgramme)
//MAP:2fbe5a500a785f04:00000000:1:FEN_Preseance:com.logicorp.home_biking.wdgen.GWDFFEN_Preseance:Déclarations globales de FEN_Preseance
super.declarerGlobale(WD_tabParam, 1, 1);
int WD_ntabParamLen = 0;
if(WD_tabParam!=null) WD_ntabParamLen = WD_tabParam.length;

// Traitement du paramètre n°0
if(0<WD_ntabParamLen) 
{
vWD_gParamIdProgramme = WD_tabParam[0];
}
else { vWD_gParamIdProgramme = null; }
super.ajouterVariableGlobale("gParamIdProgramme",vWD_gParamIdProgramme);


}




/**
 * Traitement: A chaque modification de FEN_Preseance
 */
public void modification()
{
super.modification();

// HLitRecherche(Programme,Id_Programme,gParamIdProgramme) //Récupération du nom du programme
//MAP:2fbe5a500a785f04:00000011:1:FEN_Preseance:com.logicorp.home_biking.wdgen.GWDFFEN_Preseance:A chaque modification de FEN_Preseance
// HLitRecherche(Programme,Id_Programme,gParamIdProgramme) //Récupération du nom du programme
//MAP:2fbe5a500a785f04:00000011:1:FEN_Preseance:com.logicorp.home_biking.wdgen.GWDFFEN_Preseance:A chaque modification de FEN_Preseance
WDAPIHF.hLitRecherche(WDAPIHF.getFichierSansCasseNiAccent("programme"),WDAPIHF.getRubriqueSansCasseNiAccent("id_programme"),vWD_gParamIdProgramme);

// LIB_Nom_Prog=Programme.Nom
//MAP:2fbe5a500a785f04:00000011:2:FEN_Preseance:com.logicorp.home_biking.wdgen.GWDFFEN_Preseance:A chaque modification de FEN_Preseance
mWD_LIB_Nom_Prog.setValeur(WDAPIHF.getFichierSansCasseNiAccent("programme").getRubriqueSansCasseNiAccent("nom"));

// HExécuteRequête(REQ_InfoProgramme,hRequêteDéfaut,gParamIdProgramme) //Récupération du temps total du programme à afficher
//MAP:2fbe5a500a785f04:00000011:3:FEN_Preseance:com.logicorp.home_biking.wdgen.GWDFFEN_Preseance:A chaque modification de FEN_Preseance
WDAPIHF.hExecuteRequete(WDAPIHF.getRequeteSansCasseNiAccent("req_infoprogramme"),new WDEntier4(0),new WDObjet[] {vWD_gParamIdProgramme} );

// LIB_Temps="Temps : " + PartieEntière(REQ_InfoProgramme.Temps_Total/60)+"min"
//MAP:2fbe5a500a785f04:00000011:4:FEN_Preseance:com.logicorp.home_biking.wdgen.GWDFFEN_Preseance:A chaque modification de FEN_Preseance
mWD_LIB_Temps.setValeur(new WDChaineU("Temps : ").opPlus(WDAPIMath.partieEntiere(WDAPIHF.getFichierSansCasseNiAccent("req_infoprogramme").getRubriqueSansCasseNiAccent("temps_total").opDiv(60))).opPlus("min"));

// SI isPersoOrNot(gParamIdProgramme) ALORS //Si ce n'est pas un programme personnalisé, les boutons MODIF et SUPPR ne sont pas visible
//MAP:2fbe5a500a785f04:00000011:7:FEN_Preseance:com.logicorp.home_biking.wdgen.GWDFFEN_Preseance:A chaque modification de FEN_Preseance
if(GWDCPCOL_iSPerso.fWD_isPersoOrNot(vWD_gParamIdProgramme).getBoolean())
{
}
else
{
// 	IMG_Button_Modif..Visible	=Faux
//MAP:2fbe5a500a785f04:00000011:9:FEN_Preseance:com.logicorp.home_biking.wdgen.GWDFFEN_Preseance:A chaque modification de FEN_Preseance
mWD_IMG_Button_Modif.setProp(EWDPropriete.PROP_VISIBLE,false);

// 	IMG_Button_Suppr..Visible	=Faux
//MAP:2fbe5a500a785f04:00000011:a:FEN_Preseance:com.logicorp.home_biking.wdgen.GWDFFEN_Preseance:A chaque modification de FEN_Preseance
mWD_IMG_Button_Suppr.setProp(EWDPropriete.PROP_VISIBLE,false);

}

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurModification();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
 public WDObjet vWD_gParamIdProgramme = WDVarNonAllouee.ref;
////////////////////////////////////////////////////////////////////////////
// Création des champs de la fenêtre FEN_Preseance
////////////////////////////////////////////////////////////////////////////
protected void creerChamps()
{
mWD_IMG_Button_Back = new GWDIMG_Button_Back();
mWD_IMG_Button_Modif = new GWDIMG_Button_Modif();
mWD_IMG_Button_Start = new GWDIMG_Button_Start();
mWD_IMG_Button_Suppr = new GWDIMG_Button_Suppr();
mWD_LIB_Temps = new GWDLIB_Temps();
mWD_LIB_Nom_Prog = new GWDLIB_Nom_Prog();
mWD_TABLE_FEN_Preseance = new GWDTABLE_FEN_Preseance();

}
////////////////////////////////////////////////////////////////////////////
// Initialisation de la fenêtre FEN_Preseance
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.setQuid(3440286465177181956l);

super.setChecksum("980431925");

super.setNom("FEN_Preseance");

super.setType(1);

super.setBulle("");

super.setMenuContextuelSysteme();

super.setCurseurSouris(0);

super.setNote("", "");

super.setCouleur(0x0);

super.setCouleurFond(0xFFFFFF);

super.setPositionInitiale(0, 0);

super.setTailleInitiale(567, 320);

super.setTitre("Preseance");

super.setTailleMin(-1, -1);

super.setTailleMax(20000, 20000);

super.setVisibleInitial(true);

super.setPositionFenetre(3);

super.setPersistant(true);

super.setGFI(true);

super.setAnimationFenetre(0);

super.setImageFond("", 1, 0, 1);

super.setCouleurTexteAutomatique(0x303030);

super.setCouleurBarreSysteme(0x80FF);


activerEcoute();

////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FEN_Preseance
////////////////////////////////////////////////////////////////////////////
mWD_IMG_Button_Back.initialiserObjet();
super.ajouter("IMG_Button_Back", mWD_IMG_Button_Back);
mWD_IMG_Button_Modif.initialiserObjet();
super.ajouter("IMG_Button_Modif", mWD_IMG_Button_Modif);
mWD_IMG_Button_Start.initialiserObjet();
super.ajouter("IMG_Button_Start", mWD_IMG_Button_Start);
mWD_IMG_Button_Suppr.initialiserObjet();
super.ajouter("IMG_Button_Suppr", mWD_IMG_Button_Suppr);
mWD_LIB_Temps.initialiserObjet();
super.ajouter("LIB_Temps", mWD_LIB_Temps);
mWD_LIB_Nom_Prog.initialiserObjet();
super.ajouter("LIB_Nom_Prog", mWD_LIB_Nom_Prog);
mWD_TABLE_FEN_Preseance.initialiserObjet();
super.ajouter("TABLE_FEN_Preseance", mWD_TABLE_FEN_Preseance);

super.terminerInitialisation();
}

////////////////////////////////////////////////////////////////////////////
public boolean isUniteAffichageLogique()
{
return false;
}

public WDProjet getProjet()
{
return GWDPHome_Biking.getInstance();
}

public IWDEnsembleElement getEnsemble()
{
return GWDPHome_Biking.getInstance();
}
public int getModeContexteHF()
{
return 1;
}
/**
* Retourne le mode d'affichage de l'ActionBar de la fenêtre.
*/
public int getModeActionBar()
{
return 0;
}
/**
* Retourne vrai si la fenêtre est maximisée, faux sinon.
*/
public boolean isMaximisee()
{
return true;
}
/**
* Retourne vrai si la fenêtre a une barre de titre, faux sinon.
*/
public boolean isAvecBarreDeTitre()
{
return false;
}
/**
* Retourne le mode d'affichage de la barre système de la fenêtre.
*/
public int getModeBarreSysteme()
{
return 0;
}
/**
* Retourne vrai si la fenêtre est munie d'ascenseurs automatique, faux sinon.
*/
public boolean isAvecAscenseurAuto()
{
return false;
}
/**
* Retourne Vrai si on doit appliquer un theme "dark" (sombre) ou Faux si on doit appliquer "light" (clair) à la fenêtre.
* Ce choix se base sur la couleur du libellé par défaut dans le gabarit de la fenêtre.
*/
public boolean isThemeDark()
{
return false;
}
/**
* Retourne vrai si l'option de masquage automatique de l'ActionBar lorsqu'on scrolle dans un champ de la fenêtre a été activée.
*/
public boolean isMasquageAutomatiqueActionBar()
{
return false;
}
public static class WDActiviteFenetre extends WDActivite
{
protected WDFenetre getFenetre()
{
return GWDPHome_Biking.getInstance().mWD_FEN_Preseance;
}
}
/**
* Retourne le nom du gabarit associée à la fenêtre.
*/
public String getNomGabarit()
{
return "210 MATERIAL DESIGN ORANGE#WM";
}
}
