/**
 * Code généré par WINDEV Mobile - NE PAS MODIFIER !
 * Objet WINDEV Mobile : Fenêtre
 * Classe Android : FEN_SeanceEnCours
 * Date : 06/03/2021 17:18:02
 * Version de wdjava.dll  : 25.0.221.6
 */


package com.logicorp.home_biking.wdgen;


import com.logicorp.home_biking.*;
import fr.pcsoft.wdjava.core.types.*;
import fr.pcsoft.wdjava.core.*;
import fr.pcsoft.wdjava.ui.champs.fenetre.*;
import fr.pcsoft.wdjava.ui.champs.libelle.*;
import fr.pcsoft.wdjava.ui.cadre.*;
import fr.pcsoft.wdjava.ui.champs.image.*;
import fr.pcsoft.wdjava.api.*;
import fr.pcsoft.wdjava.ui.champs.jauge.*;
import fr.pcsoft.wdjava.ui.style.degrade.*;
import fr.pcsoft.wdjava.ui.champs.bouton.*;
import fr.pcsoft.wdjava.core.application.*;
import fr.pcsoft.wdjava.core.context.*;
import fr.pcsoft.wdjava.ui.activite.*;
/*Imports trouvés dans le code WL*/
/*Fin Imports trouvés dans le code WL*/



public class GWDFFEN_SeanceEnCours extends WDFenetre
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs de FEN_SeanceEnCours
////////////////////////////////////////////////////////////////////////////

/**
 * LIB_Exercice
 */
class GWDLIB_Exercice extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FEN_SeanceEnCours.LIB_Exercice
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3441392109884042742l);

super.setChecksum("842709810");

super.setNom("LIB_Exercice");

super.setType(3);

super.setBulle("");

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("Nom Exercice");

super.setNote("", "");

super.setCurseurSouris(0);

super.setEtatInitial(0);

super.setPositionInitiale(21, 83);

super.setTailleInitiale(291, 30);

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(1);

super.setAncrageInitial(2, 1000, 1000, 1000, 500, 0);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, 0xFFFFFFFF, creerPolice_GEN("Roboto", -12.000000, 0), 3, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_Exercice mWD_LIB_Exercice;

/**
 * LIB_Rep
 */
class GWDLIB_Rep extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°2 de FEN_SeanceEnCours.LIB_Rep
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3441392221554463435l);

super.setChecksum("843980833");

super.setNom("LIB_Rep");

super.setType(3);

super.setBulle("");

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("REP / NB REP");

super.setNote("", "");

super.setCurseurSouris(0);

super.setEtatInitial(0);

super.setPositionInitiale(21, 121);

super.setTailleInitiale(152, 30);

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(2);

super.setAncrageInitial(2, 1000, 1000, 1000, 500, 0);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, 0xFFFFFFFF, creerPolice_GEN("Roboto", -12.000000, 0), 3, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_Rep mWD_LIB_Rep;

/**
 * LIB_FCM
 */
class GWDLIB_FCM extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°3 de FEN_SeanceEnCours.LIB_FCM
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3441392346109068135l);

super.setChecksum("844533978");

super.setNom("LIB_FCM");

super.setType(3);

super.setBulle("");

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("%FCM");

super.setNote("", "");

super.setCurseurSouris(0);

super.setEtatInitial(0);

super.setPositionInitiale(21, 159);

super.setTailleInitiale(120, 30);

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(3);

super.setAncrageInitial(2, 1000, 1000, 1000, 500, 0);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, 0xFFFFFFFF, creerPolice_GEN("Roboto", -12.000000, 0), 3, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_FCM mWD_LIB_FCM;

/**
 * LIB_trparmin
 */
class GWDLIB_trparmin extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°4 de FEN_SeanceEnCours.LIB_trparmin
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3441392423419087677l);

super.setChecksum("845142210");

super.setNom("LIB_trparmin");

super.setType(3);

super.setBulle("");

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("FREQ PEDALAGE");

super.setNote("", "");

super.setCurseurSouris(0);

super.setEtatInitial(0);

super.setPositionInitiale(21, 197);

super.setTailleInitiale(213, 37);

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(4);

super.setAncrageInitial(2, 1000, 1000, 1000, 500, 0);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, 0xFFFFFFFF, creerPolice_GEN("Roboto", -12.000000, 0), 3, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_trparmin mWD_LIB_trparmin;

/**
 * IMG_ARRET
 */
class GWDIMG_ARRET extends WDChampImage
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°5 de FEN_SeanceEnCours.IMG_ARRET
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3441392921638687079l);

super.setChecksum("848537672");

super.setNom("IMG_ARRET");

super.setType(30001);

super.setBulle("");

super.setLibelle("");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setNavigable(false);

super.setEtatInitial(0);

super.setPositionInitiale(112, 9);

super.setTailleInitiale(83, 63);

super.setValeurInitiale("C:\\Users\\axelm\\Desktop\\COURS IUT INFO\\Semestre 3\\M3302 - Projet tutore - Mise en situation professionnelle\\ptut-home-trainer\\Home_Biking\\Bouton STOP.png");

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(5);

super.setAncrageInitial(0, 1000, 1000, 1000, 1000, 0);

super.setTransparence(1);

super.setParamImage(2097158, 0, true, 100);

super.setSymetrie(0);

super.setZoneClicage(true);

super.setPCodeMultitouch(false);

super.setChargementEnTacheDeFond(false);

super.setOrientationExif(false);

super.setParamAnimation(1, 1, false, 300, true, false);

super.setAnimationInitiale(false);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -1, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

/**
 * Traitement: Clic sur IMG_ARRET
 */
public void clicSurBoutonGauche()
{
super.clicSurBoutonGauche();

// gbPause=Vrai //mise en pause du programme
//MAP:2fc248a102d15167:00000012:1:FEN_SeanceEnCours.IMG_ARRET:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours$GWDIMG_ARRET:Clic sur IMG_ARRET
// gbPause=Vrai //mise en pause du programme
//MAP:2fc248a102d15167:00000012:1:FEN_SeanceEnCours.IMG_ARRET:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours$GWDIMG_ARRET:Clic sur IMG_ARRET
vWD_gbPause.setValeur(true);

// SELON Dialogue("Voulez-vous vraiment arrêter le programme en cours?")
//MAP:2fc248a102d15167:00000012:5:FEN_SeanceEnCours.IMG_ARRET:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours$GWDIMG_ARRET:Clic sur IMG_ARRET
// Délimiteur de visibilité pour ne pas étendre la visibilité de la variable temporaire _WDExpSelon
{
// SELON Dialogue("Voulez-vous vraiment arrêter le programme en cours?")
//MAP:2fc248a102d15167:00000012:5:FEN_SeanceEnCours.IMG_ARRET:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours$GWDIMG_ARRET:Clic sur IMG_ARRET
WDObjet _WDExpSelon0 = WDAPIDialogue.dialogue(0);
if(_WDExpSelon0.opEgal(1))
{
// 		SI PAS EnModeSimulateur() ALORS //Gestion de la mise en veille automatique de l'écran de l'utilisateur, ici réactivation de la mise en veille de l'écran
//MAP:2fc248a102d15167:00000012:8:FEN_SeanceEnCours.IMG_ARRET:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours$GWDIMG_ARRET:Clic sur IMG_ARRET
if((!WDAPIVM.enModeSimulateur().getBoolean()))
{
// 			SysMiseEnVeille(sysSecteur,sysVeilleActive)
//MAP:2fc248a102d15167:00000012:9:FEN_SeanceEnCours.IMG_ARRET:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours$GWDIMG_ARRET:Clic sur IMG_ARRET
WDAPIBatterie.sysMiseEnVeille(2,1);

// 			SysMiseEnVeille(sysBatterie,sysVeilleActive)
//MAP:2fc248a102d15167:00000012:a:FEN_SeanceEnCours.IMG_ARRET:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours$GWDIMG_ARRET:Clic sur IMG_ARRET
WDAPIBatterie.sysMiseEnVeille(1,1);

}

// 		Ferme(FEN_Preseance)//Ramène à la fenêtre ouverte avant la préséance (Ex: si programme récent, l'arrêt ramène à la page d'accueil)
//MAP:2fc248a102d15167:00000012:c:FEN_SeanceEnCours.IMG_ARRET:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours$GWDIMG_ARRET:Clic sur IMG_ARRET
WDAPIFenetre.ferme(GWDPHome_Biking.getInstance().mWD_FEN_Preseance);

}
else if(_WDExpSelon0.opEgal(2))
{
// 		FEN_SeanceEnCours.gbPause=Faux //reprise du programme en cours
//MAP:2fc248a102d15167:00000012:f:FEN_SeanceEnCours.IMG_ARRET:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours$GWDIMG_ARRET:Clic sur IMG_ARRET
vWD_gbPause.setValeur(false);

// 		Multitâche((gnTemps-1)*100) // rajout d'un multitache pour que le TimerSys continu correctement jusqu'à la dernière seconde de l'exercice
//MAP:2fc248a102d15167:00000012:10:FEN_SeanceEnCours.IMG_ARRET:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours$GWDIMG_ARRET:Clic sur IMG_ARRET
WDAPIVM.multitache(vWD_gnTemps.opMoins(1).opMult(100));

}

}

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurClic();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDIMG_ARRET mWD_IMG_ARRET;

/**
 * IMG_PAUSE
 */
class GWDIMG_PAUSE extends WDChampImage
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°6 de FEN_SeanceEnCours.IMG_PAUSE
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3441393286712565119l);

super.setChecksum("850195637");

super.setNom("IMG_PAUSE");

super.setType(30001);

super.setBulle("");

super.setLibelle("");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setNavigable(false);

super.setEtatInitial(0);

super.setPositionInitiale(20, 9);

super.setTailleInitiale(83, 63);

super.setValeurInitiale("C:\\Users\\axelm\\Desktop\\COURS IUT INFO\\Semestre 3\\M3302 - Projet tutore - Mise en situation professionnelle\\ptut-home-trainer\\Home_Biking\\Bouton pause.png");

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(6);

super.setAncrageInitial(0, 1000, 1000, 1000, 1000, 0);

super.setTransparence(1);

super.setParamImage(2097158, 0, true, 100);

super.setSymetrie(0);

super.setZoneClicage(true);

super.setPCodeMultitouch(false);

super.setChargementEnTacheDeFond(false);

super.setOrientationExif(false);

super.setParamAnimation(1, 1, false, 300, true, false);

super.setAnimationInitiale(false);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -1, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

/**
 * Traitement: Clic sur IMG_PAUSE
 */
public void clicSurBoutonGauche()
{
super.clicSurBoutonGauche();

// 
//MAP:2fc248f602ea9d7f:00000012:1:FEN_SeanceEnCours.IMG_PAUSE:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours$GWDIMG_PAUSE:Clic sur IMG_PAUSE
// gbPause = PAS gbPause //changement du booléen en sont inverse
//MAP:2fc248f602ea9d7f:00000012:2:FEN_SeanceEnCours.IMG_PAUSE:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours$GWDIMG_PAUSE:Clic sur IMG_PAUSE
vWD_gbPause.setValeur((!vWD_gbPause.getBoolean()));

// Si gbPause = Vrai ALORS //si il est devient vrai, alors on affiche play pour le bouton
//MAP:2fc248f602ea9d7f:00000012:3:FEN_SeanceEnCours.IMG_PAUSE:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours$GWDIMG_PAUSE:Clic sur IMG_PAUSE
if(vWD_gbPause.opEgal(true))
{
// 	IMG_PAUSE=IMG_play_tmp
//MAP:2fc248f602ea9d7f:00000012:4:FEN_SeanceEnCours.IMG_PAUSE:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours$GWDIMG_PAUSE:Clic sur IMG_PAUSE
this.setValeur(mWD_IMG_play_tmp);

}
else
{
// 	IMG_PAUSE=IMG_pause_tmp
//MAP:2fc248f602ea9d7f:00000012:6:FEN_SeanceEnCours.IMG_PAUSE:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours$GWDIMG_PAUSE:Clic sur IMG_PAUSE
this.setValeur(mWD_IMG_pause_tmp);

// 	Multitâche((gnTemps-1)*100) //on relance un multitâche pour que le TimerSys continu de bien fonctionner sur toute la durée de l'exercice
//MAP:2fc248f602ea9d7f:00000012:7:FEN_SeanceEnCours.IMG_PAUSE:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours$GWDIMG_PAUSE:Clic sur IMG_PAUSE
WDAPIVM.multitache(vWD_gnTemps.opMoins(1).opMult(100));

}

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurClic();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDIMG_PAUSE mWD_IMG_PAUSE;

/**
 * JAUGE_Time
 */
class GWDJAUGE_Time extends WDJaugeCirculaire
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°7 de FEN_SeanceEnCours.JAUGE_Time
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setRectCompPrincipal(0,0,236,218);
super.setQuid(3441396361928476607l);

super.setChecksum("869524817");

super.setNom("JAUGE_Time");

super.setType(10);

super.setBulle("");

super.setLibelle("Jauge");

super.setNote("", "");

super.setCurseurSouris(0);

super.setNavigable(false);

super.setEtatInitial(0);

super.setPositionInitiale(320, 15);

super.setTailleInitiale(236, 218);

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(134217727, 134217727);

super.setVisibleInitial(true);

super.setAltitude(7);

super.setAncrageInitial(6, 1000, 1000, 1000, 500, 0);

super.setNumTab(-1);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x98FF, creerPolice_GEN("Roboto", -8.000000, 0), -1, 0, 0x98FF);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN(1, 0xB4B4B4, 0x343434, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1));

super.setParamJauge(0, 100, 100, true);

super.setStyleJauge(500.000000, 500.000000, -225.000000, 45.000000);

super.setStyleAiguille(1, 0.150000, 0.850000, 0x222222, 5);

super.setStyleFond("", "", 0xFFFFFFFF);

super.setStyleGraduations(0.960000, creerPolice_GEN("Roboto", 0.000000, 0), 0x222222, 0x222222, 0x222222, 0x30);

super.setStyleSupportAiguille(0.100000, 0x222222, "");

super.setStyleLCD(0.200000, WDDegradeFactory.creerDegrade_GEN(new int[]{0x0, 0x0}, 0, null));

activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDJAUGE_Time mWD_JAUGE_Time;

/**
 * LIB_Timer
 */
class GWDLIB_Timer extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°8 de FEN_SeanceEnCours.LIB_Timer
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3441397079190083161l);

super.setChecksum("871589914");

super.setNom("LIB_Timer");

super.setType(3);

super.setBulle("");

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("Timer");

super.setNote("", "");

super.setCurseurSouris(0);

super.setEtatInitial(0);

super.setPositionInitiale(171, 223);

super.setTailleInitiale(226, 71);

super.setPlan(0);

super.setCadrageHorizontal(1);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(8);

super.setAncrageInitial(5, 1000, 1000, 500, 1000, 0);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, 0xFFFFFFFF, creerPolice_GEN("Roboto", -35.000000, 0), 3, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_Timer mWD_LIB_Timer;

/**
 * JAUGE_Prog
 */
class GWDJAUGE_Prog extends WDJauge
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°9 de FEN_SeanceEnCours.JAUGE_Prog
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setRectCompPrincipal(1,1,551,11);
super.setQuid(3442572010502243736l);

super.setChecksum("930533481");

super.setNom("JAUGE_Prog");

super.setType(10);

super.setBulle("");

super.setLibelle("Jauge");

super.setNote("", "");

super.setCurseurSouris(0);

super.setNavigable(false);

super.setEtatInitial(0);

super.setPositionInitiale(8, 301);

super.setTailleInitiale(553, 13);

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(9);

super.setAncrageInitial(9, 1000, 1000, 1000, 1000, 0);

super.setNumTab(-1);

super.setParamJauge(0, 100, 0, false, false);

super.setImage("", 1, 0, 1);

super.setImageFond("", 1, 0, 1);

super.setStyleJauge(0x98FF, 0x212121, 0x98FF, 0xFFFFFF, false);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x212121, creerPolice_GEN("Roboto", -11.000000, 0), -1, 0, 0x808080);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(2, 0x222222, 0x0, 0xFFFFFFFF, 2.000000, 2.000000, 1, 1), 0, 0, 0, 0);

super.setCadreInterne(WDCadreFactory.creerCadre_GEN(1, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFF, 2.000000, 2.000000, 1, 1));

super.setParamAnimationChamp(21, 1202, 300);

activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDJAUGE_Prog mWD_JAUGE_Prog;

/**
 * IMG_pause_tmp
 */
class GWDIMG_pause_tmp extends WDChampImage
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°10 de FEN_SeanceEnCours.IMG_pause_tmp
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3443270689953315138l);

super.setChecksum("871857141");

super.setNom("IMG_pause_tmp");

super.setType(30001);

super.setBulle("");

super.setLibelle("");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setNavigable(false);

super.setEtatInitial(0);

super.setPositionInitiale(-572, -70);

super.setTailleInitiale(85, 38);

super.setValeurInitiale("C:\\Users\\axelm\\Desktop\\COURS IUT INFO\\Semestre 3\\M3302 - Projet tutore - Mise en situation professionnelle\\ptut-home-trainer\\Home_Biking\\Bouton pause.png");

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(10);

super.setAncrageInitial(0, 1000, 1000, 1000, 1000, 0);

super.setTransparence(1);

super.setParamImage(2097158, 0, true, 100);

super.setSymetrie(0);

super.setZoneClicage(true);

super.setPCodeMultitouch(false);

super.setChargementEnTacheDeFond(false);

super.setOrientationExif(false);

super.setParamAnimation(1, 1, false, 300, true, false);

super.setAnimationInitiale(false);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -1, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDIMG_pause_tmp mWD_IMG_pause_tmp;

/**
 * IMG_play_tmp
 */
class GWDIMG_play_tmp extends WDChampImage
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°11 de FEN_SeanceEnCours.IMG_play_tmp
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3443270896112448081l);

super.setChecksum("872559924");

super.setNom("IMG_play_tmp");

super.setType(30001);

super.setBulle("");

super.setLibelle("");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setNavigable(false);

super.setEtatInitial(0);

super.setPositionInitiale(-578, 41);

super.setTailleInitiale(84, 39);

super.setValeurInitiale("C:\\Users\\axelm\\Desktop\\COURS IUT INFO\\Semestre 3\\M3302 - Projet tutore - Mise en situation professionnelle\\ptut-home-trainer\\Home_Biking\\Bouton play.png");

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(11);

super.setAncrageInitial(0, 1000, 1000, 1000, 1000, 0);

super.setTransparence(1);

super.setParamImage(2097158, 0, true, 100);

super.setSymetrie(0);

super.setZoneClicage(true);

super.setPCodeMultitouch(false);

super.setChargementEnTacheDeFond(false);

super.setOrientationExif(false);

super.setParamAnimation(1, 1, false, 300, true, false);

super.setAnimationInitiale(false);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -1, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDIMG_play_tmp mWD_IMG_play_tmp;

/**
 * LIB_Repos_BPM
 */
class GWDLIB_Repos_BPM extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°12 de FEN_SeanceEnCours.LIB_Repos_BPM
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3449151479398242992l);

super.setChecksum("837384455");

super.setNom("LIB_Repos_BPM");

super.setType(3);

super.setBulle("");

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("REPOS");

super.setNote("", "");

super.setCurseurSouris(0);

super.setEtatInitial(0);

super.setPositionInitiale(374, 74);

super.setTailleInitiale(128, 93);

super.setPlan(0);

super.setCadrageHorizontal(1);

super.setCadrageVertical(1);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(12);

super.setAncrageInitial(6, 1000, 1000, 1000, 1000, 0);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, 0xFFFFFFFF, creerPolice_GEN("Roboto", -19.000000, 1), 3, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_Repos_BPM mWD_LIB_Repos_BPM;

/**
 * BTN_BOUTON
 */
class GWDBTN_BOUTON extends WDBouton
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°13 de FEN_SeanceEnCours.BTN_BOUTON
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(3449227994231755099l);

super.setChecksum("828536593");

super.setNom("BTN_BOUTON");

super.setType(4);

super.setBulle("");

super.setLibelle("Bouton");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setNavigable(true);

super.setEtatInitial(0);

super.setPositionInitiale(-501, 100);

super.setTailleInitiale(162, 48);

super.setPlan(0);

super.setImageEtat(1);

super.setImageFondEtat(5);

super.setTailleMin(0, 0);

super.setTailleMax(134217727, 134217727);

super.setVisibleInitial(true);

super.setAltitude(13);

super.setAncrageInitial(0, 1000, 1000, 1000, 1000, 0);

super.setNumTab(1);

super.setLettreAppel(65535);

super.setTypeBouton(3);

super.setTypeActionPredefinie(0);

super.setBoutonOnOff(false);

super.setTauxParallaxe(0, 0);

super.setLibelleVAlign(1);

super.setLibelleHAlign(5);

super.setPresenceLibelle(true);

super.setImage("", 0, 1, 1, null, null, null);

super.setStyleLibelleRepos(0xFFFFFF, creerPolice_GEN("Roboto", -8.000000, 0), 0, 0x222222);

super.setStyleLibelleSurvol(0xFFFFFF, creerPolice_GEN("Roboto", -8.000000, 0), 0, 0x222222);

super.setStyleLibelleEnfonce(0xFFFFFF, creerPolice_GEN("Roboto", -8.000000, 0), 0, 0x222222);

super.setCadreRepos(WDCadreFactory.creerCadre_GEN(2, 0x98FF, 0x187F, 0x98FF, 2.000000, 2.000000, 1, 1));

super.setCadreSurvol(WDCadreFactory.creerCadre_GEN(2, 0x98FF, 0x187F, 0x98FF, 2.000000, 2.000000, 1, 1));

super.setCadreEnfonce(WDCadreFactory.creerCadre_GEN(2, 0x98FF, 0x187F, 0x98FF, 2.000000, 2.000000, 1, 1));

super.setImageFond9Images(new int[] {1,2,1,2,2,2,1,2,1}, 10, 10, 10, 10);

super.setImageFond("C:\\Users\\axelm\\Desktop\\COURS IUT INFO\\Semestre 3\\M3302 - Projet tutore - Mise en situation professionnelle\\ptut-home-trainer\\Home_Biking\\Gabarits\\WM\\210 Material Design Orange\\Material Design Orange_Btn_Std@dpi1x.png?E5_3NP_10_10_10_10", 1, 0, 1, 1);

activerEcoute();
super.terminerInitialisation();
}

/**
 * Traitement: Clic sur BTN_BOUTON
 */
public void clicSurBoutonGauche()
{
super.clicSurBoutonGauche();

// ExécuteTraitement(IMG_ARRET,trtClic)
//MAP:2fde1e960184515b:00000012:1:FEN_SeanceEnCours.BTN_BOUTON:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours$GWDBTN_BOUTON:Clic sur BTN_BOUTON
// ExécuteTraitement(IMG_ARRET,trtClic)
//MAP:2fde1e960184515b:00000012:1:FEN_SeanceEnCours.BTN_BOUTON:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours$GWDBTN_BOUTON:Clic sur BTN_BOUTON
WDAPIVM.executeTraitement(mWD_IMG_ARRET,18);

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurClic();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDBTN_BOUTON mWD_BTN_BOUTON;

/**
 * LIB_Fin_Prog
 */
class GWDLIB_Fin_Prog extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°14 de FEN_SeanceEnCours.LIB_Fin_Prog
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetre( getWDFenetreThis() );
super.setQuid(2321055422643348246l);

super.setChecksum("584889354");

super.setNom("LIB_Fin_Prog");

super.setType(3);

super.setBulle("");

super.setTypeSaisie(0);

super.setMasqueSaisie(new WDChaineU("0"));

super.setLibelle("Fin dans X min");

super.setNote("", "");

super.setCurseurSouris(0);

super.setEtatInitial(0);

super.setPositionInitiale(441, 275);

super.setTailleInitiale(120, 19);

super.setPlan(0);

super.setCadrageHorizontal(0);

super.setCadrageVertical(0);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(14);

super.setAncrageInitial(5, 0, 1000, 1000, 1000, 0);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, 0xFFFFFFFF, creerPolice_GEN("Roboto", -8.000000, 0), 3, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_Fin_Prog mWD_LIB_Fin_Prog;

/**
 * Traitement: Déclarations globales de FEN_SeanceEnCours
 */
public void declarerGlobale(WDObjet[] WD_tabParam)
{
// PROCEDURE MaFenêtre(gParamIdProgramme)
//MAP:2fc246d3012a43ad:00000000:1:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:Déclarations globales de FEN_SeanceEnCours
super.declarerGlobale(WD_tabParam, 1, 1);
int WD_ntabParamLen = 0;
if(WD_tabParam!=null) WD_ntabParamLen = WD_tabParam.length;

// Traitement du paramètre n°0
if(0<WD_ntabParamLen) 
{
vWD_gParamIdProgramme = WD_tabParam[0];
}
else { vWD_gParamIdProgramme = null; }
super.ajouterVariableGlobale("gParamIdProgramme",vWD_gParamIdProgramme);


// gbPause est un booléen, utile="Permet de mettre en pause le timer";
//MAP:2fc246d3012a43ad:00000000:4:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:Déclarations globales de FEN_SeanceEnCours
vWD_gbPause = new WDBooleen();

super.ajouterVariableGlobale("gbPause",vWD_gbPause);



// gnTemps est un entier, utile="Contient le temps du timer de l'exercice actuel"
//MAP:2fc246d3012a43ad:00000000:5:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:Déclarations globales de FEN_SeanceEnCours
vWD_gnTemps = new WDEntier4();

super.ajouterVariableGlobale("gnTemps",vWD_gnTemps);



// gnTempsMax est un entier, utile="Contient le temps du timer de l'exercice constant"
//MAP:2fc246d3012a43ad:00000000:6:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:Déclarations globales de FEN_SeanceEnCours
vWD_gnTempsMax = new WDEntier4();

super.ajouterVariableGlobale("gnTempsMax",vWD_gnTempsMax);



// gnTempsProg est un entier, utile="Contient le temps du timer du programme"
//MAP:2fc246d3012a43ad:00000000:7:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:Déclarations globales de FEN_SeanceEnCours
vWD_gnTempsProg = new WDEntier4();

super.ajouterVariableGlobale("gnTempsProg",vWD_gnTempsProg);



// gnTempsP est un entier, utile="Contient le temps du timer du programme actuel"
//MAP:2fc246d3012a43ad:00000000:8:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:Déclarations globales de FEN_SeanceEnCours
vWD_gnTempsP = new WDEntier4();

super.ajouterVariableGlobale("gnTempsP",vWD_gnTempsP);



// gnRep est un entier, utile="Les répétitions restantes à effectuer"
//MAP:2fc246d3012a43ad:00000000:9:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:Déclarations globales de FEN_SeanceEnCours
vWD_gnRep = new WDEntier4();

super.ajouterVariableGlobale("gnRep",vWD_gnRep);



// grJTValFloat est un réel, utile="garde la valeur réel de la jauge de l'exercice"
//MAP:2fc246d3012a43ad:00000000:b:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:Déclarations globales de FEN_SeanceEnCours
vWD_grJTValFloat = new WDReel();

super.ajouterVariableGlobale("grJTValFloat",vWD_grJTValFloat);



// grJPValFloat est un réel, utile="garde la valeur réel de la jauge du programme"
//MAP:2fc246d3012a43ad:00000000:c:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:Déclarations globales de FEN_SeanceEnCours
vWD_grJPValFloat = new WDReel();

super.ajouterVariableGlobale("grJPValFloat",vWD_grJPValFloat);



}




/**
 * Traitement: A chaque modification de FEN_SeanceEnCours
 */
public void modification()
{
super.modification();

// gbPause=Faux; //initialisation de la pause sur Faux en début de programme
//MAP:2fc246d3012a43ad:00000011:1:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
final WDProcedureInterne []fWDI_aFFICHAGE_TIMER = new WDProcedureInterne[1];
final WDProcedureInterne []fWDI_timeExo = new WDProcedureInterne[1];
fWDI_aFFICHAGE_TIMER[0] = new WDProcedureInterne()
{
@Override
public String getNom()
{
return "AFFICHAGE_TIMER";
}
@Override
public void executer_void(WDObjet... parametres)
{
verifNbParametres(parametres.length, 0);
fWD_aFFICHAGE_TIMER();
}
public void fWD_aFFICHAGE_TIMER()
{
// 	procédure interne AFFICHAGE_TIMER()
//MAP:2fc246d3012a43ad:00000011:27:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:AFFICHAGE_TIMER
initExecProcInterne();

try
{
// 		SI modulo(gnTemps,60)<10 ALORS //si il y a moins de 10 sec, affiche un 0 devant le nombre de secondes
//MAP:2fc246d3012a43ad:00000011:28:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:AFFICHAGE_TIMER
if(vWD_gnTemps.opMod(60).opInf(10))
{
// 			LIB_Timer			=PartieEntière(gnTemps/60)+":0"+modulo(gnTemps,60)
//MAP:2fc246d3012a43ad:00000011:29:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:AFFICHAGE_TIMER
mWD_LIB_Timer.setValeur(WDAPIMath.partieEntiere(vWD_gnTemps.opDiv(60)).opPlus(":0").opPlus(vWD_gnTemps.opMod(60)));

}
else
{
// 			LIB_Timer			=PartieEntière(gnTemps/60)+":"+modulo(gnTemps,60)
//MAP:2fc246d3012a43ad:00000011:2b:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:AFFICHAGE_TIMER
mWD_LIB_Timer.setValeur(WDAPIMath.partieEntiere(vWD_gnTemps.opDiv(60)).opPlus(":").opPlus(vWD_gnTemps.opMod(60)));

}

// 		SI ArrondiInférieur(gnTempsP/60)=0 ALORS
//MAP:2fc246d3012a43ad:00000011:2e:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:AFFICHAGE_TIMER
if(WDAPIMath.arrondiInferieur(vWD_gnTempsP.opDiv(60)).opEgal(0))
{
// 			LIB_Fin_Prog			="Fin dans "+modulo(gnTempsP,60)+" sec"
//MAP:2fc246d3012a43ad:00000011:2f:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:AFFICHAGE_TIMER
mWD_LIB_Fin_Prog.setValeur(new WDChaineU("Fin dans ").opPlus(vWD_gnTempsP.opMod(60)).opPlus(" sec"));

}
else
{
// 			LIB_Fin_Prog			="Fin dans "+PartieEntière(gnTempsP/60)+" min"
//MAP:2fc246d3012a43ad:00000011:31:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:AFFICHAGE_TIMER
mWD_LIB_Fin_Prog.setValeur(new WDChaineU("Fin dans ").opPlus(WDAPIMath.partieEntiere(vWD_gnTempsP.opDiv(60))).opPlus(" min"));

}

}
finally
{
finExecProcInterne();
}

}


};
WDAppelContexte.getContexte().declarerProcedureInterne(fWDI_aFFICHAGE_TIMER[0]);
fWDI_timeExo[0] = new WDProcedureInterne()
{
@Override
public String getNom()
{
return "TimeExo";
}
@Override
public void executer_void(WDObjet... parametres)
{
verifNbParametres(parametres.length, 0);
fWD_timeExo();
}
public void fWD_timeExo()
{
// 	PROCÉDURE INTERNE TimeExo()
//MAP:2fc246d3012a43ad:00000011:36:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:TimeExo
initExecProcInterne();

try
{
// 		SI gbPause = Faux ALORS
//MAP:2fc246d3012a43ad:00000011:37:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:TimeExo
if(vWD_gbPause.opEgal(false))
{
// 			gnTemps-- //une seconde en moins sur le temps de l'exo
//MAP:2fc246d3012a43ad:00000011:39:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:TimeExo
vWD_gnTemps.opDec();

// 			gnTempsP-- //une seconde en moins sur le temps du programme
//MAP:2fc246d3012a43ad:00000011:3a:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:TimeExo
vWD_gnTempsP.opDec();

// 			AFFICHAGE_TIMER()
//MAP:2fc246d3012a43ad:00000011:3c:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:TimeExo
fWDI_aFFICHAGE_TIMER[0].executer_void();

// 			grJTValFloat=grJTValFloat-(100/gnTempsMax) //on garde la valeur exacte dans une variable float
//MAP:2fc246d3012a43ad:00000011:3e:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:TimeExo
vWD_grJTValFloat.setValeur(vWD_grJTValFloat.opMoins(new WDEntier4(100).opDiv(vWD_gnTempsMax)));

// 			JAUGE_Time..Valeur	=Arrondi(grJTValFloat)//on fourni un arrondi entier à la valeur de la jauge
//MAP:2fc246d3012a43ad:00000011:3f:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:TimeExo
mWD_JAUGE_Time.setProp(EWDPropriete.PROP_VALEUR,WDAPIMath.arrondi(vWD_grJTValFloat));

// 			grJPValFloat=grJPValFloat+(100/gnTempsProg) //même chose que pour la jauge précédente
//MAP:2fc246d3012a43ad:00000011:40:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:TimeExo
vWD_grJPValFloat.setValeur(vWD_grJPValFloat.opPlus(new WDEntier4(100).opDiv(vWD_gnTempsProg)));

// 			JAUGE_Prog..Valeur	=Arrondi(grJPValFloat)
//MAP:2fc246d3012a43ad:00000011:41:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:TimeExo
mWD_JAUGE_Prog.setProp(EWDPropriete.PROP_VALEUR,WDAPIMath.arrondi(vWD_grJPValFloat));

}

}
finally
{
finExecProcInterne();
}

}


};
WDAppelContexte.getContexte().declarerProcedureInterne(fWDI_timeExo[0]);
// gbPause=Faux; //initialisation de la pause sur Faux en début de programme
//MAP:2fc246d3012a43ad:00000011:1:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
vWD_gbPause.setValeur(false);

// HExécuteRequête(REQ_InfoProgramme,hRequêteDéfaut,gParamIdProgramme)
//MAP:2fc246d3012a43ad:00000011:4:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
WDAPIHF.hExecuteRequete(WDAPIHF.getRequeteSansCasseNiAccent("req_infoprogramme"),new WDEntier4(0),new WDObjet[] {vWD_gParamIdProgramme} );

// gnTempsProg=REQ_InfoProgramme.Temps_Total
//MAP:2fc246d3012a43ad:00000011:5:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
vWD_gnTempsProg.setValeur(WDAPIHF.getFichierSansCasseNiAccent("req_infoprogramme").getRubriqueSansCasseNiAccent("temps_total"));

// HLitRecherche(Programme,Id_Programme,gParamIdProgramme)
//MAP:2fc246d3012a43ad:00000011:8:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
WDAPIHF.hLitRecherche(WDAPIHF.getFichierSansCasseNiAccent("programme"),WDAPIHF.getRubriqueSansCasseNiAccent("id_programme"),vWD_gParamIdProgramme);

// SI PAS EnModeSimulateur() ALORS //Gestion de la mise en veille automatique de l'écran de l'utilisateur, ici désactivation de la mise en veille de l'écran
//MAP:2fc246d3012a43ad:00000011:a:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
if((!WDAPIVM.enModeSimulateur().getBoolean()))
{
// 	SysMiseEnVeille(sysSecteur,sysVeilleInactive)
//MAP:2fc246d3012a43ad:00000011:b:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
WDAPIBatterie.sysMiseEnVeille(2,0);

// 	SysMiseEnVeille(sysBatterie,sysVeilleInactive)
//MAP:2fc246d3012a43ad:00000011:c:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
WDAPIBatterie.sysMiseEnVeille(1,0);

}

// HExécuteRequête(REQ_Exo_Prog,hRequêteDéfaut,gParamIdProgramme)
//MAP:2fc246d3012a43ad:00000011:10:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
WDAPIHF.hExecuteRequete(WDAPIHF.getRequeteSansCasseNiAccent("req_exo_prog"),new WDEntier4(0),new WDObjet[] {vWD_gParamIdProgramme} );

// JAUGE_Prog..Valeur=0
//MAP:2fc246d3012a43ad:00000011:13:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
mWD_JAUGE_Prog.setProp(EWDPropriete.PROP_VALEUR,0);

// grJPValFloat=0
//MAP:2fc246d3012a43ad:00000011:14:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
vWD_grJPValFloat.setValeur(0);

// gnTempsP=gnTempsProg
//MAP:2fc246d3012a43ad:00000011:15:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
vWD_gnTempsP.setValeur(vWD_gnTempsProg);

// SynthèseVocaleLitTexte("Début du programme!")
//MAP:2fc246d3012a43ad:00000011:19:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
WDAPISyntheseVocale.syntheseVocaleLitTexte("Début du programme!");

// BOUCLE
//MAP:2fc246d3012a43ad:00000011:1a:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
while(true)
{
// 	LIB_Exercice	= REQ_Exo_Prog.Nom
//MAP:2fc246d3012a43ad:00000011:21:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
mWD_LIB_Exercice.setValeur(WDAPIHF.getFichierSansCasseNiAccent("req_exo_prog").getRubriqueSansCasseNiAccent("nom"));

// 	LIB_trparmin	= REQ_Exo_Prog.cadence + " tr/min"
//MAP:2fc246d3012a43ad:00000011:22:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
mWD_LIB_trparmin.setValeur(WDAPIHF.getFichierSansCasseNiAccent("req_exo_prog").getRubriqueSansCasseNiAccent("cadence").opPlus(" tr/min"));

// 	LIB_FCM			= REQ_Exo_Prog.pourcentage_fcm + "% FCM"
//MAP:2fc246d3012a43ad:00000011:23:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
mWD_LIB_FCM.setValeur(WDAPIHF.getFichierSansCasseNiAccent("req_exo_prog").getRubriqueSansCasseNiAccent("pourcentage_fcm").opPlus("% FCM"));

// 	gnRep			= REQ_Exo_Prog.nb_rep //Nombre répétitions
//MAP:2fc246d3012a43ad:00000011:24:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
vWD_gnRep.setValeur(WDAPIHF.getFichierSansCasseNiAccent("req_exo_prog").getRubriqueSansCasseNiAccent("nb_rep"));

// 	procédure interne AFFICHAGE_TIMER()
//MAP:2fc246d3012a43ad:00000011:27:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours

// 	PROCÉDURE INTERNE TimeExo()
//MAP:2fc246d3012a43ad:00000011:36:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours

// 	BOUCLE
//MAP:2fc246d3012a43ad:00000011:46:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
{
do
{
// 		SynthèseVocaleLitTexte("Début d'exercice!") //audio disant "Début d'exercice"
//MAP:2fc246d3012a43ad:00000011:47:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
WDAPISyntheseVocale.syntheseVocaleLitTexte("Début d'exercice!");

// 		LIB_Rep				= REQ_Exo_Prog.nb_rep-(gnRep-1) + " /"+REQ_Exo_Prog.nb_rep //affichage du numéro de la répétition en cours sur le nombre de répétitions totales
//MAP:2fc246d3012a43ad:00000011:48:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
mWD_LIB_Rep.setValeur(WDAPIHF.getFichierSansCasseNiAccent("req_exo_prog").getRubriqueSansCasseNiAccent("nb_rep").opMoins(vWD_gnRep.opMoins(1)).opPlus(" /").opPlus(WDAPIHF.getFichierSansCasseNiAccent("req_exo_prog").getRubriqueSansCasseNiAccent("nb_rep")));

// 		JAUGE_Time..Valeur	=100 //initialisation de la jauge
//MAP:2fc246d3012a43ad:00000011:4b:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
mWD_JAUGE_Time.setProp(EWDPropriete.PROP_VALEUR,100);

// 		grJTValFloat		=100 //initialisation de la valeur réel de la jauge 
//MAP:2fc246d3012a43ad:00000011:4c:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
vWD_grJTValFloat.setValeur(100);

// 		gnTempsMax			=REQ_Exo_Prog.temps	//récupération temps programme
//MAP:2fc246d3012a43ad:00000011:4d:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
vWD_gnTempsMax.setValeur(WDAPIHF.getFichierSansCasseNiAccent("req_exo_prog").getRubriqueSansCasseNiAccent("temps"));

// 		gnTemps				=REQ_Exo_Prog.temps
//MAP:2fc246d3012a43ad:00000011:4e:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
vWD_gnTemps.setValeur(WDAPIHF.getFichierSansCasseNiAccent("req_exo_prog").getRubriqueSansCasseNiAccent("temps"));

// 		AFFICHAGE_TIMER() //affichage du temps de l'exercice
//MAP:2fc246d3012a43ad:00000011:4f:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
fWDI_aFFICHAGE_TIMER[0].executer_void();

// 		HLitPremier(Compte_Client)//récupération info client
//MAP:2fc246d3012a43ad:00000011:51:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
WDAPIHF.hLitPremier(WDAPIHF.getFichierSansCasseNiAccent("compte_client"));

// 		LIB_Repos_BPM..PoliceTaille	=20
//MAP:2fc246d3012a43ad:00000011:52:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
mWD_LIB_Repos_BPM.setProp(EWDPropriete.PROP_POLICETAILLE,20);

// 		LIB_Repos_BPM..Couleur		= CouleurDifficulte(DifficulteFCM(REQ_Exo_Prog.pourcentage_fcm))
//MAP:2fc246d3012a43ad:00000011:53:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
mWD_LIB_Repos_BPM.setProp(EWDPropriete.PROP_COULEUR,GWDCPCouleur_Difficulte.fWD_couleurDifficulte(GWDCPCouleur_Difficulte.fWD_difficulteFCM(WDAPIHF.getFichierSansCasseNiAccent("req_exo_prog").getRubriqueSansCasseNiAccent("pourcentage_fcm"))));

// 		LIB_Repos_BPM				=Arrondi(Compte_Client.fcm*(REQ_Exo_Prog.pourcentage_fcm/100)) //calcul de la fréquence cible
//MAP:2fc246d3012a43ad:00000011:54:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
mWD_LIB_Repos_BPM.setValeur(WDAPIMath.arrondi(WDAPIHF.getFichierSansCasseNiAccent("compte_client").getRubriqueSansCasseNiAccent("fcm").opMult(WDAPIHF.getFichierSansCasseNiAccent("req_exo_prog").getRubriqueSansCasseNiAccent("pourcentage_fcm").opDiv(100))));

// 		TimerSys(TimeExo,100,1) //exécution de la procedure qui fait avancer le timer de l'exercice, s'exécute toutes les secondes
//MAP:2fc246d3012a43ad:00000011:56:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
WDAPITimer.timerSys(fWDI_timeExo[0],new WDEntier4(100),(long)1);

// 		Multitâche((gnTemps)*100) //multitache qui dur le temps de l'exercice qui permet à TimerSys de s'exécuter sur la totalité du Timer
//MAP:2fc246d3012a43ad:00000011:57:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
WDAPIVM.multitache(vWD_gnTemps.opMult(100));

// 		BOUCLE //Tant que le programme est en pause exécuter un multitache de 1 seconde pour éviter que le TimerSys ne s'arrête
//MAP:2fc246d3012a43ad:00000011:59:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
{
do
{
// 			Multitâche(100)
//MAP:2fc246d3012a43ad:00000011:5a:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
WDAPIVM.multitache(new WDEntier4(100));

}
while(vWD_gbPause.opEgal(true));
}

// 		FinTimerSys(1) //Quand les multitâches sont terminés et que le programme est arrivé au bout, arrêt de la boucle sur la procedure du Timer
//MAP:2fc246d3012a43ad:00000011:5d:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
WDAPITimer.finTimerSys(1);

// 		SI REQ_Exo_Prog.repos <>0 ALORS //si il y a un repos pour l'exercice
//MAP:2fc246d3012a43ad:00000011:60:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
if(WDAPIHF.getFichierSansCasseNiAccent("req_exo_prog").getRubriqueSansCasseNiAccent("repos").opDiff(0))
{
// 			JAUGE_Time..Valeur	=100
//MAP:2fc246d3012a43ad:00000011:63:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
mWD_JAUGE_Time.setProp(EWDPropriete.PROP_VALEUR,100);

// 			grJTValFloat		=100
//MAP:2fc246d3012a43ad:00000011:64:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
vWD_grJTValFloat.setValeur(100);

// 			SynthèseVocaleLitTexte("Repos!") //audio disant "Repos"
//MAP:2fc246d3012a43ad:00000011:66:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
WDAPISyntheseVocale.syntheseVocaleLitTexte("Repos!");

// 			gnTempsMax			=REQ_Exo_Prog.repos //récupération du temps du repos
//MAP:2fc246d3012a43ad:00000011:67:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
vWD_gnTempsMax.setValeur(WDAPIHF.getFichierSansCasseNiAccent("req_exo_prog").getRubriqueSansCasseNiAccent("repos"));

// 			gnTemps				=REQ_Exo_Prog.repos
//MAP:2fc246d3012a43ad:00000011:68:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
vWD_gnTemps.setValeur(WDAPIHF.getFichierSansCasseNiAccent("req_exo_prog").getRubriqueSansCasseNiAccent("repos"));

// 			AFFICHAGE_TIMER()
//MAP:2fc246d3012a43ad:00000011:6a:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
fWDI_aFFICHAGE_TIMER[0].executer_void();

// 			LIB_Repos_BPM..Couleur		=Noir
//MAP:2fc246d3012a43ad:00000011:6d:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
mWD_LIB_Repos_BPM.setProp(EWDPropriete.PROP_COULEUR,0);

// 			LIB_Repos_BPM..PoliceTaille	=14
//MAP:2fc246d3012a43ad:00000011:6e:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
mWD_LIB_Repos_BPM.setProp(EWDPropriete.PROP_POLICETAILLE,14);

// 			LIB_Repos_BPM				="REPOS"
//MAP:2fc246d3012a43ad:00000011:6f:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
mWD_LIB_Repos_BPM.setValeur("REPOS");

// 			TimerSys(TimeExo,100,1)
//MAP:2fc246d3012a43ad:00000011:70:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
WDAPITimer.timerSys(fWDI_timeExo[0],new WDEntier4(100),(long)1);

// 			Multitâche((gnTemps)*100)
//MAP:2fc246d3012a43ad:00000011:71:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
WDAPIVM.multitache(vWD_gnTemps.opMult(100));

// 			BOUCLE //Tant que le programme est en pause exécuter un multitache de 1 seconde pour éviter que le TimerSys ne s'arrête
//MAP:2fc246d3012a43ad:00000011:73:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
{
do
{
// 				Multitâche(100)
//MAP:2fc246d3012a43ad:00000011:74:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
WDAPIVM.multitache(new WDEntier4(100));

}
while(vWD_gbPause.opEgal(true));
}

// 			FinTimerSys(1)
//MAP:2fc246d3012a43ad:00000011:77:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
WDAPITimer.finTimerSys(1);

}

// 		gnRep--
//MAP:2fc246d3012a43ad:00000011:7a:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
vWD_gnRep.opDec();

}
while(vWD_gnRep.opSup(0));
}

// 	SynthèseVocaleLitTexte("Fin d'exercice!")//audio disant que l'exercice est terminé
//MAP:2fc246d3012a43ad:00000011:7f:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
WDAPISyntheseVocale.syntheseVocaleLitTexte("Fin d'exercice!");

// 	HLitSuivant(REQ_Exo_Prog) //passage à l'exercice de la place suivante
//MAP:2fc246d3012a43ad:00000011:81:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
WDAPIHF.hLitSuivant(WDAPIHF.getRequeteSansCasseNiAccent("req_exo_prog"));

// 	SI HTrouve()=Faux ALORS //si aucun n'est trouvé, c'est que le programme est terminé
//MAP:2fc246d3012a43ad:00000011:82:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
if(WDAPIHF.hTrouve().opEgal(false))
{
// 		SORTIR //sortie de la boucle du programme
//MAP:2fc246d3012a43ad:00000011:83:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
break;

//////////////////////////////////////////////////////////
// Code Inaccessible
// 
}

}


// SynthèseVocaleLitTexte("Fin du programme!")//audio disant que le programme est terminé
//MAP:2fc246d3012a43ad:00000011:88:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
WDAPISyntheseVocale.syntheseVocaleLitTexte("Fin du programme!");

// SI PAS EnModeSimulateur() ALORS 
//MAP:2fc246d3012a43ad:00000011:89:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
if((!WDAPIVM.enModeSimulateur().getBoolean()))
{
// 	SysMiseEnVeille(sysSecteur,sysVeilleactive)
//MAP:2fc246d3012a43ad:00000011:8a:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
WDAPIBatterie.sysMiseEnVeille(2,1);

// 	SysMiseEnVeille(sysBatterie,sysVeilleactive)
//MAP:2fc246d3012a43ad:00000011:8b:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
WDAPIBatterie.sysMiseEnVeille(1,1);

}

// Multitâche(3s)
//MAP:2fc246d3012a43ad:00000011:8d:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
WDAPIVM.multitache((new WDDuree("0000003000")));

// Ferme(FEN_Preseance) //retour à la dernière fenêtre ouverte avant la préséance
//MAP:2fc246d3012a43ad:00000011:8e:FEN_SeanceEnCours:com.logicorp.home_biking.wdgen.GWDFFEN_SeanceEnCours:A chaque modification de FEN_SeanceEnCours
WDAPIFenetre.ferme(GWDPHome_Biking.getInstance().mWD_FEN_Preseance);

}




// Activation des écouteurs: 
public void activerEcoute()
{
super.activerEcouteurModification();
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
 public WDObjet vWD_gParamIdProgramme = WDVarNonAllouee.ref;
 public WDObjet vWD_gbPause = WDVarNonAllouee.ref;
 public WDObjet vWD_gnTemps = WDVarNonAllouee.ref;
 public WDObjet vWD_gnTempsMax = WDVarNonAllouee.ref;
 public WDObjet vWD_gnTempsProg = WDVarNonAllouee.ref;
 public WDObjet vWD_gnTempsP = WDVarNonAllouee.ref;
 public WDObjet vWD_gnRep = WDVarNonAllouee.ref;
 public WDObjet vWD_grJTValFloat = WDVarNonAllouee.ref;
 public WDObjet vWD_grJPValFloat = WDVarNonAllouee.ref;
////////////////////////////////////////////////////////////////////////////
// Création des champs de la fenêtre FEN_SeanceEnCours
////////////////////////////////////////////////////////////////////////////
protected void creerChamps()
{
mWD_LIB_Exercice = new GWDLIB_Exercice();
mWD_LIB_Rep = new GWDLIB_Rep();
mWD_LIB_FCM = new GWDLIB_FCM();
mWD_LIB_trparmin = new GWDLIB_trparmin();
mWD_IMG_ARRET = new GWDIMG_ARRET();
mWD_IMG_PAUSE = new GWDIMG_PAUSE();
mWD_JAUGE_Time = new GWDJAUGE_Time();
mWD_LIB_Timer = new GWDLIB_Timer();
mWD_JAUGE_Prog = new GWDJAUGE_Prog();
mWD_IMG_pause_tmp = new GWDIMG_pause_tmp();
mWD_IMG_play_tmp = new GWDIMG_play_tmp();
mWD_LIB_Repos_BPM = new GWDLIB_Repos_BPM();
mWD_BTN_BOUTON = new GWDBTN_BOUTON();
mWD_LIB_Fin_Prog = new GWDLIB_Fin_Prog();

}
////////////////////////////////////////////////////////////////////////////
// Initialisation de la fenêtre FEN_SeanceEnCours
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.setQuid(3441390937336071085l);

super.setChecksum("824575329");

super.setNom("FEN_SeanceEnCours");

super.setType(1);

super.setBulle("");

super.setMenuContextuelSysteme();

super.setCurseurSouris(0);

super.setNote("", "");

super.setCouleur(0x0);

super.setCouleurFond(0xFFFFFF);

super.setPositionInitiale(0, 0);

super.setTailleInitiale(567, 320);

super.setTitre("SeanceEnCours");

super.setTailleMin(-1, -1);

super.setTailleMax(20000, 20000);

super.setVisibleInitial(true);

super.setPositionFenetre(3);

super.setPersistant(true);

super.setGFI(true);

super.setAnimationFenetre(0);

super.setImageFond("", 1, 0, 1);

super.setCouleurTexteAutomatique(0x303030);

super.setCouleurBarreSysteme(0x80FF);


activerEcoute();

////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FEN_SeanceEnCours
////////////////////////////////////////////////////////////////////////////
mWD_LIB_Exercice.initialiserObjet();
super.ajouter("LIB_Exercice", mWD_LIB_Exercice);
mWD_LIB_Rep.initialiserObjet();
super.ajouter("LIB_Rep", mWD_LIB_Rep);
mWD_LIB_FCM.initialiserObjet();
super.ajouter("LIB_FCM", mWD_LIB_FCM);
mWD_LIB_trparmin.initialiserObjet();
super.ajouter("LIB_trparmin", mWD_LIB_trparmin);
mWD_IMG_ARRET.initialiserObjet();
super.ajouter("IMG_ARRET", mWD_IMG_ARRET);
mWD_IMG_PAUSE.initialiserObjet();
super.ajouter("IMG_PAUSE", mWD_IMG_PAUSE);
mWD_JAUGE_Time.initialiserObjet();
super.ajouter("JAUGE_Time", mWD_JAUGE_Time);
mWD_LIB_Timer.initialiserObjet();
super.ajouter("LIB_Timer", mWD_LIB_Timer);
mWD_JAUGE_Prog.initialiserObjet();
super.ajouter("JAUGE_Prog", mWD_JAUGE_Prog);
mWD_IMG_pause_tmp.initialiserObjet();
super.ajouter("IMG_pause_tmp", mWD_IMG_pause_tmp);
mWD_IMG_play_tmp.initialiserObjet();
super.ajouter("IMG_play_tmp", mWD_IMG_play_tmp);
mWD_LIB_Repos_BPM.initialiserObjet();
super.ajouter("LIB_Repos_BPM", mWD_LIB_Repos_BPM);
mWD_BTN_BOUTON.initialiserObjet();
super.ajouter("BTN_BOUTON", mWD_BTN_BOUTON);
mWD_LIB_Fin_Prog.initialiserObjet();
super.ajouter("LIB_Fin_Prog", mWD_LIB_Fin_Prog);

super.terminerInitialisation();
}

////////////////////////////////////////////////////////////////////////////
public boolean isUniteAffichageLogique()
{
return false;
}

public WDProjet getProjet()
{
return GWDPHome_Biking.getInstance();
}

public IWDEnsembleElement getEnsemble()
{
return GWDPHome_Biking.getInstance();
}
public int getModeContexteHF()
{
return 1;
}
/**
* Retourne le mode d'affichage de l'ActionBar de la fenêtre.
*/
public int getModeActionBar()
{
return 0;
}
/**
* Retourne vrai si la fenêtre est maximisée, faux sinon.
*/
public boolean isMaximisee()
{
return true;
}
/**
* Retourne vrai si la fenêtre a une barre de titre, faux sinon.
*/
public boolean isAvecBarreDeTitre()
{
return false;
}
/**
* Retourne le mode d'affichage de la barre système de la fenêtre.
*/
public int getModeBarreSysteme()
{
return 0;
}
/**
* Retourne vrai si la fenêtre est munie d'ascenseurs automatique, faux sinon.
*/
public boolean isAvecAscenseurAuto()
{
return false;
}
/**
* Retourne Vrai si on doit appliquer un theme "dark" (sombre) ou Faux si on doit appliquer "light" (clair) à la fenêtre.
* Ce choix se base sur la couleur du libellé par défaut dans le gabarit de la fenêtre.
*/
public boolean isThemeDark()
{
return false;
}
/**
* Retourne vrai si l'option de masquage automatique de l'ActionBar lorsqu'on scrolle dans un champ de la fenêtre a été activée.
*/
public boolean isMasquageAutomatiqueActionBar()
{
return false;
}
public static class WDActiviteFenetre extends WDActivite
{
protected WDFenetre getFenetre()
{
return GWDPHome_Biking.getInstance().mWD_FEN_SeanceEnCours;
}
}
/**
* Retourne le nom du gabarit associée à la fenêtre.
*/
public String getNomGabarit()
{
return "210 MATERIAL DESIGN ORANGE#WM";
}
}
