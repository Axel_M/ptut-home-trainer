/**
 * Code généré par WINDEV Mobile - NE PAS MODIFIER !
 * Objet WINDEV Mobile : Collection
 * Classe Android : Couleur_Difficulte
 * Date : 06/03/2021 17:18:02
 * Version de wdjava.dll  : 25.0.221.6
 */


package com.logicorp.home_biking.wdgen;


import com.logicorp.home_biking.*;
import fr.pcsoft.wdjava.core.types.*;
import fr.pcsoft.wdjava.core.*;
import fr.pcsoft.wdjava.core.application.*;
import fr.pcsoft.wdjava.api.*;
/*Imports trouvés dans le code WL*/
/*Fin Imports trouvés dans le code WL*/



public class GWDCPCouleur_Difficulte extends WDCollProcAndroid
{

public WDProjet getProjet()
{
return GWDPHome_Biking.getInstance();
}

public IWDEnsembleElement getEnsemble()
{
return GWDPHome_Biking.getInstance();
}

protected String getNomCollection()
{
return "Couleur_Difficulte";
}
private final static GWDCPCouleur_Difficulte ms_instance = new GWDCPCouleur_Difficulte();
public final static GWDCPCouleur_Difficulte getInstance()
{
return ms_instance;
}

// Code de déclaration de Couleur_Difficulte
static public void init()
{
// 
//MAP:1fc666db000763b9:000b0000:1:Couleur_Difficulte:com.logicorp.home_biking.wdgen.GWDCPCouleur_Difficulte:Déclaration de Couleur_Difficulte
ms_instance.initDeclarationCollection();

try
{
}
finally
{
finDeclarationCollection();
}

}




// Code de terminaison de Couleur_Difficulte
static public void term()
{
// 
//MAP:1fc666db000763b9:000b0002:1:Couleur_Difficulte:com.logicorp.home_biking.wdgen.GWDCPCouleur_Difficulte:Terminaison de Couleur_Difficulte
ms_instance.initTerminaisonCollection();

try
{
}
finally
{
finTerminaisonCollection();
}

}



// Nombre de Procédures : 2
static public WDObjet fWD_couleurDifficulte( WDObjet vWD_ParamDifficulte )
{
// FONCTION CouleurDifficulte(ParamDifficulte)
//MAP:1fc666f10008b61e:00070000:1:Couleur_Difficulte.CouleurDifficulte:com.logicorp.home_biking.wdgen.GWDCPCouleur_Difficulte:CouleurDifficulte
ms_instance.initExecProcGlobale("CouleurDifficulte");

try
{
// 	SI ParamDifficulte="Très facile" ALORS
//MAP:1fc666f10008b61e:00070000:2:Couleur_Difficulte.CouleurDifficulte:com.logicorp.home_biking.wdgen.GWDCPCouleur_Difficulte:CouleurDifficulte
if(vWD_ParamDifficulte.opEgal("Très facile"))
{
// 		RENVOYER RVB(144,220,0)
//MAP:1fc666f10008b61e:00070000:3:Couleur_Difficulte.CouleurDifficulte:com.logicorp.home_biking.wdgen.GWDCPCouleur_Difficulte:CouleurDifficulte
return WDAPIDessin.rvb(144,220,0);

}

// 	SI ParamDifficulte="Facile" ALORS
//MAP:1fc666f10008b61e:00070000:6:Couleur_Difficulte.CouleurDifficulte:com.logicorp.home_biking.wdgen.GWDCPCouleur_Difficulte:CouleurDifficulte
if(vWD_ParamDifficulte.opEgal("Facile"))
{
// 		RENVOYER RVB(109,166,0)
//MAP:1fc666f10008b61e:00070000:7:Couleur_Difficulte.CouleurDifficulte:com.logicorp.home_biking.wdgen.GWDCPCouleur_Difficulte:CouleurDifficulte
return WDAPIDessin.rvb(109,166,0);

}

// 	SI ParamDifficulte="Moyen" ALORS
//MAP:1fc666f10008b61e:00070000:a:Couleur_Difficulte.CouleurDifficulte:com.logicorp.home_biking.wdgen.GWDCPCouleur_Difficulte:CouleurDifficulte
if(vWD_ParamDifficulte.opEgal("Moyen"))
{
// 		RENVOYER RVB(255,219,54)
//MAP:1fc666f10008b61e:00070000:b:Couleur_Difficulte.CouleurDifficulte:com.logicorp.home_biking.wdgen.GWDCPCouleur_Difficulte:CouleurDifficulte
return WDAPIDessin.rvb(255,219,54);

}

// 	SI ParamDifficulte="Difficile" ALORS
//MAP:1fc666f10008b61e:00070000:e:Couleur_Difficulte.CouleurDifficulte:com.logicorp.home_biking.wdgen.GWDCPCouleur_Difficulte:CouleurDifficulte
if(vWD_ParamDifficulte.opEgal("Difficile"))
{
// 		RENVOYER RVB(255,128,0)
//MAP:1fc666f10008b61e:00070000:f:Couleur_Difficulte.CouleurDifficulte:com.logicorp.home_biking.wdgen.GWDCPCouleur_Difficulte:CouleurDifficulte
return WDAPIDessin.rvb(255,128,0);

}

// 	SI ParamDifficulte="Très difficile" ALORS
//MAP:1fc666f10008b61e:00070000:12:Couleur_Difficulte.CouleurDifficulte:com.logicorp.home_biking.wdgen.GWDCPCouleur_Difficulte:CouleurDifficulte
if(vWD_ParamDifficulte.opEgal("Très difficile"))
{
// 		RENVOYER RVB(192,0,0)
//MAP:1fc666f10008b61e:00070000:13:Couleur_Difficulte.CouleurDifficulte:com.logicorp.home_biking.wdgen.GWDCPCouleur_Difficulte:CouleurDifficulte
return WDAPIDessin.rvb(192,0,0);

}
else
{
// 		RENVOYER Noir
//MAP:1fc666f10008b61e:00070000:15:Couleur_Difficulte.CouleurDifficulte:com.logicorp.home_biking.wdgen.GWDCPCouleur_Difficulte:CouleurDifficulte
return new WDEntier4(0);

}

}
finally
{
finExecProcGlobale();
}

}


static public WDObjet fWD_difficulteFCM( WDObjet vWD_ParamFCM )
{
// FONCTION DifficulteFCM(ParamFCM)
//MAP:1fc66824000b6789:00070000:1:Couleur_Difficulte.DifficulteFCM:com.logicorp.home_biking.wdgen.GWDCPCouleur_Difficulte:DifficulteFCM
ms_instance.initExecProcGlobale("DifficulteFCM");

try
{
// 	SI ParamFCM>=50 ET ParamFCM<60 ALORS
//MAP:1fc66824000b6789:00070000:2:Couleur_Difficulte.DifficulteFCM:com.logicorp.home_biking.wdgen.GWDCPCouleur_Difficulte:DifficulteFCM
if((vWD_ParamFCM.opSupEgal(50) & vWD_ParamFCM.opInf(60)))
{
// 		RENVOYER "Très facile"
//MAP:1fc66824000b6789:00070000:3:Couleur_Difficulte.DifficulteFCM:com.logicorp.home_biking.wdgen.GWDCPCouleur_Difficulte:DifficulteFCM
return new WDChaineU("Très facile");

}

// 	SI ParamFCM>=60 ET ParamFCM<70 ALORS
//MAP:1fc66824000b6789:00070000:6:Couleur_Difficulte.DifficulteFCM:com.logicorp.home_biking.wdgen.GWDCPCouleur_Difficulte:DifficulteFCM
if((vWD_ParamFCM.opSupEgal(60) & vWD_ParamFCM.opInf(70)))
{
// 		RENVOYER "Facile"
//MAP:1fc66824000b6789:00070000:7:Couleur_Difficulte.DifficulteFCM:com.logicorp.home_biking.wdgen.GWDCPCouleur_Difficulte:DifficulteFCM
return new WDChaineU("Facile");

}

// 	SI ParamFCM>=70 ET ParamFCM<80 ALORS
//MAP:1fc66824000b6789:00070000:a:Couleur_Difficulte.DifficulteFCM:com.logicorp.home_biking.wdgen.GWDCPCouleur_Difficulte:DifficulteFCM
if((vWD_ParamFCM.opSupEgal(70) & vWD_ParamFCM.opInf(80)))
{
// 		RENVOYER "Moyen"
//MAP:1fc66824000b6789:00070000:b:Couleur_Difficulte.DifficulteFCM:com.logicorp.home_biking.wdgen.GWDCPCouleur_Difficulte:DifficulteFCM
return new WDChaineU("Moyen");

}

// 	SI ParamFCM>=80 ET ParamFCM<90 ALORS
//MAP:1fc66824000b6789:00070000:e:Couleur_Difficulte.DifficulteFCM:com.logicorp.home_biking.wdgen.GWDCPCouleur_Difficulte:DifficulteFCM
if((vWD_ParamFCM.opSupEgal(80) & vWD_ParamFCM.opInf(90)))
{
// 		RENVOYER "Difficile"
//MAP:1fc66824000b6789:00070000:f:Couleur_Difficulte.DifficulteFCM:com.logicorp.home_biking.wdgen.GWDCPCouleur_Difficulte:DifficulteFCM
return new WDChaineU("Difficile");

}

// 	SI ParamFCM>=90 ALORS
//MAP:1fc66824000b6789:00070000:12:Couleur_Difficulte.DifficulteFCM:com.logicorp.home_biking.wdgen.GWDCPCouleur_Difficulte:DifficulteFCM
if(vWD_ParamFCM.opSupEgal(90))
{
// 		RENVOYER "Très difficile"
//MAP:1fc66824000b6789:00070000:13:Couleur_Difficulte.DifficulteFCM:com.logicorp.home_biking.wdgen.GWDCPCouleur_Difficulte:DifficulteFCM
return new WDChaineU("Très difficile");

}

return new WDVoid("fWD_difficulteFCM");
}
finally
{
finExecProcGlobale();
}

}



////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
