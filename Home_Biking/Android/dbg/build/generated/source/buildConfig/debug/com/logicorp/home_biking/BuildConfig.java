/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.logicorp.home_biking;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.logicorp.home_biking.go";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 100;
  public static final String VERSION_NAME = "0.0.97.0";
}
