/**
 * Code généré par WINDEV Mobile - NE PAS MODIFIER !
 * Objet WINDEV Mobile : Requête
 * Classe Android : FEN_Creation_Prog_1$Requête
 * Date : 03/04/2021 16:20:01
 * Version de wdjava.dll  : 25.0.221.6
 */


package com.logicorp.home_biking.wdgen;


import com.logicorp.home_biking.*;
import fr.pcsoft.wdjava.core.types.*;
import fr.pcsoft.wdjava.core.*;
import fr.pcsoft.wdjava.database.hf.requete.parsing.*;
/*Imports trouvés dans le code WL*/
/*Fin Imports trouvés dans le code WL*/



public class GWDRFEN_Creation_Prog_1SRequete extends WDDescRequeteWDR
{
public String getNomLogique()
{
return "FEN_Creation_Prog_1$Requête";
}
public String getCodeSQLOriginal()
{
return " SELECT  Creation_contenir.place AS place,\t Creation_contenir.Id_Exercice AS Id_Exercice,\t Exercice.Nom AS Nom  FROM  Exercice,\t Creation_contenir  WHERE   Exercice.Id_Exercice = Creation_contenir.Id_Exercice   ORDER BY  place ASC";
}
public Requete initArbre() throws WDInvalidSQLException
{
Select varSelect = new Select();
varSelect.setType(1);
Rubrique rub_place = new Rubrique();
rub_place.setNom("place");
rub_place.setAlias("place");
rub_place.setNomFichier("Creation_contenir");
rub_place.setAliasFichier("Creation_contenir");
varSelect.ajouterElement(rub_place);
Rubrique rub_Id_Exercice = new Rubrique();
rub_Id_Exercice.setNom("Id_Exercice");
rub_Id_Exercice.setAlias("Id_Exercice");
rub_Id_Exercice.setNomFichier("Creation_contenir");
rub_Id_Exercice.setAliasFichier("Creation_contenir");
varSelect.ajouterElement(rub_Id_Exercice);
Rubrique rub_Nom = new Rubrique();
rub_Nom.setNom("Nom");
rub_Nom.setAlias("Nom");
rub_Nom.setNomFichier("Exercice");
rub_Nom.setAliasFichier("Exercice");
varSelect.ajouterElement(rub_Nom);
From varFrom = new From();
Fichier fic_Exercice = new Fichier();
fic_Exercice.setNom("Exercice");
fic_Exercice.setAlias("Exercice");
varFrom.ajouterElement(fic_Exercice);
Fichier fic_Creation_contenir = new Fichier();
fic_Creation_contenir.setNom("Creation_contenir");
fic_Creation_contenir.setAlias("Creation_contenir");
varFrom.ajouterElement(fic_Creation_contenir);
Requete varReqSelect = new Requete(1);
varReqSelect.ajouterClause(varSelect);
varReqSelect.ajouterClause(varFrom);
Expression expr__ = new Expression(9, "=", "Exercice.Id_Exercice = Creation_contenir.Id_Exercice");
Rubrique rub_Id_Exercice_1 = new Rubrique();
rub_Id_Exercice_1.setNom("Exercice.Id_Exercice");
rub_Id_Exercice_1.setAlias("Id_Exercice");
rub_Id_Exercice_1.setNomFichier("Exercice");
rub_Id_Exercice_1.setAliasFichier("Exercice");
expr__.ajouterElement(rub_Id_Exercice_1);
Rubrique rub_Id_Exercice_2 = new Rubrique();
rub_Id_Exercice_2.setNom("Creation_contenir.Id_Exercice");
rub_Id_Exercice_2.setAlias("Id_Exercice");
rub_Id_Exercice_2.setNomFichier("Creation_contenir");
rub_Id_Exercice_2.setAliasFichier("Creation_contenir");
expr__.ajouterElement(rub_Id_Exercice_2);
Where varWhere = new Where();
varWhere.ajouterElement(expr__);
varReqSelect.ajouterClause(varWhere);
OrderBy varOrderBy = new OrderBy();
Rubrique rub_place_1 = new Rubrique();
rub_place_1.setNom("place");
rub_place_1.setAlias("place");
rub_place_1.setNomFichier("Creation_contenir");
rub_place_1.setAliasFichier("Creation_contenir");
rub_place_1.ajouterOption(EWDOptionRequete.TRI, "0");
rub_place_1.ajouterOption(EWDOptionRequete.INDEX_RUB, "0");
varOrderBy.ajouterElement(rub_place_1);
varReqSelect.ajouterClause(varOrderBy);
Limit varLimit = new Limit();
varLimit.setType(0);
varLimit.setNbEnregs(0);
varLimit.setOffset(0);
varReqSelect.ajouterClause(varLimit);
return varReqSelect;
}
public String getNomFichier(int nIndex)
{
switch(nIndex)
{
case 0 : return "Exercice";
case 1 : return "Creation_contenir";
default: return null;
}
}
public String getAliasFichier(int nIndex)
{
switch(nIndex)
{
case 0 : return "Exercice";
case 1 : return "Creation_contenir";
default: return null;
}
}


}
