/**
 * Code généré par WINDEV Mobile - NE PAS MODIFIER !
 * Objet WINDEV Mobile : Requête
 * Classe Android : REQ_Creation_Prog
 * Date : 06/03/2021 17:25:16
 * Version de wdjava.dll  : 25.0.221.6
 */


package com.logicorp.home_biking.wdgen;


import com.logicorp.home_biking.*;
import fr.pcsoft.wdjava.core.types.*;
import fr.pcsoft.wdjava.core.*;
import fr.pcsoft.wdjava.database.hf.requete.parsing.*;
/*Imports trouvés dans le code WL*/
/*Fin Imports trouvés dans le code WL*/



public class GWDRREQ_Creation_Prog extends WDDescRequeteWDR
{
public String getNomLogique()
{
return "REQ_Creation_Prog";
}
public String getCodeSQLOriginal()
{
return "";
}
public Requete initArbre() throws WDInvalidSQLException
{
throw new WDInvalidSQLException("", true);
}
public String getNomFichier(int nIndex)
{
switch(nIndex)
{
default: return null;
}
}
public String getAliasFichier(int nIndex)
{
switch(nIndex)
{
default: return null;
}
}


public int getIdWDR()
{
return com.logicorp.home_biking.R.raw.req_creation_prog;
}
public String getNomFichierWDR()
{
return "req_creation_prog";
}
}
