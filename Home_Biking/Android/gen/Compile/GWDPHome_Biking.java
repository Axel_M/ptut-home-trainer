/**
 * Code généré par WINDEV Mobile - NE PAS MODIFIER !
 * Objet WINDEV Mobile : Projet
 * Classe Android : Home_Biking
 * Date : 03/04/2021 16:20:02
 * Version de wdjava.dll  : 25.0.221.6
 */


package com.logicorp.home_biking.wdgen;


import com.logicorp.home_biking.*;
import fr.pcsoft.wdjava.core.types.*;
import fr.pcsoft.wdjava.core.*;
import fr.pcsoft.wdjava.core.application.*;
import fr.pcsoft.wdjava.core.context.*;
import fr.pcsoft.wdjava.api.*;
/*Imports trouvés dans le code WL*/
/*Fin Imports trouvés dans le code WL*/





public class GWDPHome_Biking extends WDProjet
{
private static GWDPHome_Biking ms_instance = null;
/**
 * Accès au projet: Home_Biking
 * Pour accéder au projet à partir de n'importe où: 
 * GWDPHome_Biking.getInstance()
 */
public static GWDPHome_Biking getInstance()
{
return (GWDPHome_Biking) ms_instance;
}

 // FEN_Accueil
public GWDFFEN_Accueil mWD_FEN_Accueil = new GWDFFEN_Accueil();
 // accesseur de FEN_Accueil
public GWDFFEN_Accueil getFEN_Accueil()
{
mWD_FEN_Accueil.checkOuverture();
return mWD_FEN_Accueil;
}

 // FEN_BaseSolide
public GWDFFEN_BaseSolide mWD_FEN_BaseSolide = new GWDFFEN_BaseSolide();
 // accesseur de FEN_BaseSolide
public GWDFFEN_BaseSolide getFEN_BaseSolide()
{
mWD_FEN_BaseSolide.checkOuverture();
return mWD_FEN_BaseSolide;
}

 // FEN_User_Profile
public GWDFFEN_User_Profile mWD_FEN_User_Profile = new GWDFFEN_User_Profile();
 // accesseur de FEN_User_Profile
public GWDFFEN_User_Profile getFEN_User_Profile()
{
mWD_FEN_User_Profile.checkOuverture();
return mWD_FEN_User_Profile;
}

 // FEN_Splash_Screen
public GWDFFEN_Splash_Screen mWD_FEN_Splash_Screen = new GWDFFEN_Splash_Screen();
 // accesseur de FEN_Splash_Screen
public GWDFFEN_Splash_Screen getFEN_Splash_Screen()
{
mWD_FEN_Splash_Screen.checkOuverture();
return mWD_FEN_Splash_Screen;
}

 // FEN_Preseance
public GWDFFEN_Preseance mWD_FEN_Preseance = new GWDFFEN_Preseance();
 // accesseur de FEN_Preseance
public GWDFFEN_Preseance getFEN_Preseance()
{
mWD_FEN_Preseance.checkOuverture();
return mWD_FEN_Preseance;
}

 // FEN_SeanceEnCours
public GWDFFEN_SeanceEnCours mWD_FEN_SeanceEnCours = new GWDFFEN_SeanceEnCours();
 // accesseur de FEN_SeanceEnCours
public GWDFFEN_SeanceEnCours getFEN_SeanceEnCours()
{
mWD_FEN_SeanceEnCours.checkOuverture();
return mWD_FEN_SeanceEnCours;
}

 // FEN_Creation_Prog
public GWDFFEN_Creation_Prog mWD_FEN_Creation_Prog = new GWDFFEN_Creation_Prog();
 // accesseur de FEN_Creation_Prog
public GWDFFEN_Creation_Prog getFEN_Creation_Prog()
{
mWD_FEN_Creation_Prog.checkOuverture();
return mWD_FEN_Creation_Prog;
}

 // FEN_Exercices
public GWDFFEN_Exercices mWD_FEN_Exercices = new GWDFFEN_Exercices();
 // accesseur de FEN_Exercices
public GWDFFEN_Exercices getFEN_Exercices()
{
mWD_FEN_Exercices.checkOuverture();
return mWD_FEN_Exercices;
}

 // FEN_Programm_Perso
public GWDFFEN_Programm_Perso mWD_FEN_Programm_Perso = new GWDFFEN_Programm_Perso();
 // accesseur de FEN_Programm_Perso
public GWDFFEN_Programm_Perso getFEN_Programm_Perso()
{
mWD_FEN_Programm_Perso.checkOuverture();
return mWD_FEN_Programm_Perso;
}

 // FEN_CreationTables
public GWDFFEN_CreationTables mWD_FEN_CreationTables = new GWDFFEN_CreationTables();
 // accesseur de FEN_CreationTables
public GWDFFEN_CreationTables getFEN_CreationTables()
{
mWD_FEN_CreationTables.checkOuverture();
return mWD_FEN_CreationTables;
}

 // FEN_Programm_Preregister3
public GWDFFEN_Programm_Preregister3 mWD_FEN_Programm_Preregister3 = new GWDFFEN_Programm_Preregister3();
 // accesseur de FEN_Programm_Preregister3
public GWDFFEN_Programm_Preregister3 getFEN_Programm_Preregister3()
{
mWD_FEN_Programm_Preregister3.checkOuverture();
return mWD_FEN_Programm_Preregister3;
}

 // FEN_Description_Exo
public GWDFFEN_Description_Exo mWD_FEN_Description_Exo = new GWDFFEN_Description_Exo();
 // accesseur de FEN_Description_Exo
public GWDFFEN_Description_Exo getFEN_Description_Exo()
{
mWD_FEN_Description_Exo.checkOuverture();
return mWD_FEN_Description_Exo;
}

 // FEN_Exercice_Prog_Detail
public GWDFFEN_Exercice_Prog_Detail mWD_FEN_Exercice_Prog_Detail = new GWDFFEN_Exercice_Prog_Detail();
 // accesseur de FEN_Exercice_Prog_Detail
public GWDFFEN_Exercice_Prog_Detail getFEN_Exercice_Prog_Detail()
{
mWD_FEN_Exercice_Prog_Detail.checkOuverture();
return mWD_FEN_Exercice_Prog_Detail;
}


 // FI_Mobile_Compteur_Jauge
public GWDFIFI_Mobile_Compteur_Jauge mWD_FI_Mobile_Compteur_Jauge = new GWDFIFI_Mobile_Compteur_Jauge();
 // accesseur de FI_Mobile_Compteur_Jauge
public GWDFIFI_Mobile_Compteur_Jauge getFI_Mobile_Compteur_Jauge()
{
GWDFIFI_Mobile_Compteur_Jauge fiCtx = (GWDFIFI_Mobile_Compteur_Jauge)WDAppelContexte.getContexte().getFenetreInterne("FI_Mobile_Compteur_Jauge");
return fiCtx != null ? fiCtx  : mWD_FI_Mobile_Compteur_Jauge;
}

 // Constructeur de la classe GWDPHome_Biking
public GWDPHome_Biking()
{
ms_instance = this;
// Définition des langues du projet
setLangueProjet(new int[] {1}, new int[] {0}, 1, false);
ajouterCollectionProcedures(GWDCPCOL_iSPerso.getInstance());
ajouterCollectionProcedures(GWDCPCouleur_Difficulte.getInstance());
ajouterCollectionProcedures(GWDCPCOL_Suppr_Creation.getInstance());

// Palette des couleurs
setPaletteCouleurGabarit(new int[] {0x394AE1, 0x26A0FA, 0x6DBC61, 0x85A800, 0xD2AC54, 0xC9822B, 0xA95CF8, 0xB86592, 0xB74A5E, 0x654E44, 0xA5A595});
ajouterFenetre("FEN_Accueil", mWD_FEN_Accueil);
ajouterFenetre("FEN_BaseSolide", mWD_FEN_BaseSolide);
ajouterFenetre("FEN_User_Profile", mWD_FEN_User_Profile);
ajouterFenetre("FEN_Splash_Screen", mWD_FEN_Splash_Screen);
ajouterFenetre("FEN_Preseance", mWD_FEN_Preseance);
ajouterFenetre("FEN_SeanceEnCours", mWD_FEN_SeanceEnCours);
ajouterFenetre("FEN_Creation_Prog", mWD_FEN_Creation_Prog);
ajouterFenetre("FEN_Exercices", mWD_FEN_Exercices);
ajouterFenetre("FEN_Programm_Perso", mWD_FEN_Programm_Perso);
ajouterFenetre("FEN_CreationTables", mWD_FEN_CreationTables);
ajouterFenetre("FEN_Programm_Preregister3", mWD_FEN_Programm_Preregister3);
ajouterFenetre("FEN_Description_Exo", mWD_FEN_Description_Exo);
ajouterFenetre("FEN_Exercice_Prog_Detail", mWD_FEN_Exercice_Prog_Detail);
ajouterFenetreInterne("FI_Mobile_Compteur_Jauge");

ajouterRequeteWDR( new GWDRFEN_Accueil_1SRequete() );
ajouterRequeteWDR( new GWDRFEN_Creation_Prog_1SRequete() );
ajouterRequeteWDR( new GWDRFEN_Exercices_1SRequete() );
ajouterRequeteWDR( new GWDRFEN_Preseance_1SRequete() );
ajouterRequeteWDR( new GWDRFEN_Programm_Preregister3_1SRequete() );
ajouterRequeteWDR( new GWDRREQ_Recherche_ProgrammePerso() );
ajouterRequeteWDR( new GWDRREQ_Recherche_ProgrammePreenregistre() );
ajouterRequeteWDR( new GWDRREQ_InfoProgramme() );
ajouterRequeteWDR( new GWDRREQ_Info_Utilisateur() );
ajouterRequeteWDR( new GWDRREQ_Select_Utiliser() );
ajouterRequeteWDR( new GWDRREQ_Exo_Prog() );
ajouterRequeteWDR( new GWDRREQ_Creation_Prog() );
ajouterRequeteWDR( new GWDRREQ_SUPPR_CONTENU_PROG() );


}


////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
public String getVersionApplication(){ return "0.0.125.0";}
public String getNomSociete(){ return "";}
public String getNomAPK(){ return "Home_Biking";}
public int getIdNomApplication(){return com.logicorp.home_biking.R.string.app_name;}
public boolean isModeAnsi(){ return false;}
public boolean isAffectationTableauParCopie(){ return true;}
public boolean isAssistanceAutoHFActive(){ return true;}
public String getPackageRacine(){ return "com.logicorp.home_biking";}
public int getIdIconeApplication(){ return com.logicorp.home_biking.R.drawable.i_c_o_n_e________2;}
public int getInfoPlateforme(EWDInfoPlateforme info)
{
switch(info)
{
case DPI_ECRAN : return 160;
case HAUTEUR_BARRE_SYSTEME : return 25;
case HAUTEUR_BARRE_TITRE : return 25;
case HAUTEUR_ACTION_BAR : return 48;
case HAUTEUR_BARRE_BAS : return 0;
case HAUTEUR_ECRAN : return 568;
case LARGEUR_ECRAN : return 320;
default : return 0;
}
}
public boolean isActiveThemeMaterialDesign()
{
return true;
}
////////////////////////////////////////////////////////////////////////////
public String getAdresseEmail() 
{
return "";
}
public boolean isIgnoreErreurCertificatHTTPS()
{
return false;
}
////////////////////////////////////////////////////////////////////////////
public boolean isUniteAffichageLogique()
{
return false;
}
public String getNomProjet()
{
return "Home_Biking";
}
public String getNomConfiguration()
{
return "Application Android";
}
public String getNomAnalyse()
{
return "home_biking_bdd";
}
public String getMotDePasseAnalyse()
{
return "";
}
public boolean isModeGestionFichierMultiUtilisateur()
{
return true;
}
public boolean isCreationAutoFichierDonnees()
{
return true;
}

////////////////////////////////////////////////////////////////////////////
// Formats des masques du projet
////////////////////////////////////////////////////////////////////////////
public String getFichierWDM()
{
return null;
}
protected void declarerRessources()
{
super.ajouterFichierAssocie("C:\\Users\\axelm\\Desktop\\COURS IUT INFO\\Semestre 3\\M3302 - Projet tutore - Mise en situation professionnelle\\ptut-home-trainer\\Home_Biking\\HOMEBIKING.PNG",com.logicorp.home_biking.R.drawable.homebiking_43, "");
super.ajouterFichierAssocie("C:\\USERS\\AXELM\\DESKTOP\\COURS IUT INFO\\SEMESTRE 3\\M3302 - PROJET TUTORE - MISE EN SITUATION PROFESSIONNELLE\\PTUT-HOME-TRAINER\\HOME_BIKING\\GABARITS\\WM\\210 MATERIAL DESIGN ORANGE\\MATERIAL DESIGN ORANGE_PROGBAR_BG.PNG",com.logicorp.home_biking.R.drawable.material_design_orange_progbar_bg_42, "");
super.ajouterFichierAssocie("C:\\USERS\\AXELM\\DESKTOP\\COURS IUT INFO\\SEMESTRE 3\\M3302 - PROJET TUTORE - MISE EN SITUATION PROFESSIONNELLE\\PTUT-HOME-TRAINER\\HOME_BIKING\\GABARITS\\WM\\210 MATERIAL DESIGN ORANGE\\MATERIAL DESIGN ORANGE_PROGBAR_ACTIV.PNG",com.logicorp.home_biking.R.drawable.material_design_orange_progbar_activ_41, "");
super.ajouterFichierAssocie("C:\\USERS\\AXELM\\DESKTOP\\COURS IUT INFO\\SEMESTRE 3\\M3302 - PROJET TUTORE - MISE EN SITUATION PROFESSIONNELLE\\PTUT-HOME-TRAINER\\HOME_BIKING\\BOUTON STOP.PNG",com.logicorp.home_biking.R.drawable.bouton_stop_40, "");
super.ajouterFichierAssocie("C:\\USERS\\AXELM\\DESKTOP\\COURS IUT INFO\\SEMESTRE 3\\M3302 - PROJET TUTORE - MISE EN SITUATION PROFESSIONNELLE\\PTUT-HOME-TRAINER\\HOME_BIKING\\BOUTON PLAY.PNG",com.logicorp.home_biking.R.drawable.bouton_play_39, "");
super.ajouterFichierAssocie("C:\\USERS\\AXELM\\DESKTOP\\COURS IUT INFO\\SEMESTRE 3\\M3302 - PROJET TUTORE - MISE EN SITUATION PROFESSIONNELLE\\PTUT-HOME-TRAINER\\HOME_BIKING\\BOUTON PAUSE.PNG",com.logicorp.home_biking.R.drawable.bouton_pause_38, "");
super.ajouterFichierAssocie("C:\\USERS\\AXELM\\DESKTOP\\COURS IUT INFO\\SEMESTRE 3\\M3302 - PROJET TUTORE - MISE EN SITUATION PROFESSIONNELLE\\PTUT-HOME-TRAINER\\HOME_BIKING\\BUTTON ADD PROGRAMM.PNG",com.logicorp.home_biking.R.drawable.button_add_programm_37, "");
super.ajouterFichierAssocie("C:\\USERS\\AXELM\\DESKTOP\\COURS IUT INFO\\SEMESTRE 3\\M3302 - PROJET TUTORE - MISE EN SITUATION PROFESSIONNELLE\\PTUT-HOME-TRAINER\\HOME_BIKING\\GABARITS\\WM\\210 MATERIAL DESIGN ORANGE\\MATERIAL DESIGN ORANGE_TABLE_COLPICT.PNG",com.logicorp.home_biking.R.drawable.material_design_orange_table_colpict_35, "");
super.ajouterFichierAssocie("C:\\USERS\\AXELM\\DESKTOP\\COURS IUT INFO\\SEMESTRE 3\\M3302 - PROJET TUTORE - MISE EN SITUATION PROFESSIONNELLE\\PTUT-HOME-TRAINER\\HOME_BIKING\\GABARITS\\WM\\210 MATERIAL DESIGN ORANGE\\MATERIAL DESIGN ORANGE_CBOX_TABLE.PNG",com.logicorp.home_biking.R.drawable.material_design_orange_cbox_table_34, "");
super.ajouterFichierAssocie("C:\\USERS\\AXELM\\DESKTOP\\COURS IUT INFO\\SEMESTRE 3\\M3302 - PROJET TUTORE - MISE EN SITUATION PROFESSIONNELLE\\PTUT-HOME-TRAINER\\HOME_BIKING\\BUTTON SUPPR.PNG",com.logicorp.home_biking.R.drawable.button_suppr_33, "");
super.ajouterFichierAssocie("C:\\USERS\\AXELM\\DESKTOP\\COURS IUT INFO\\SEMESTRE 3\\M3302 - PROJET TUTORE - MISE EN SITUATION PROFESSIONNELLE\\PTUT-HOME-TRAINER\\HOME_BIKING\\BUTTON START.PNG",com.logicorp.home_biking.R.drawable.button_start_32, "");
super.ajouterFichierAssocie("C:\\USERS\\AXELM\\DESKTOP\\COURS IUT INFO\\SEMESTRE 3\\M3302 - PROJET TUTORE - MISE EN SITUATION PROFESSIONNELLE\\PTUT-HOME-TRAINER\\HOME_BIKING\\BUTTON MODIF.PNG",com.logicorp.home_biking.R.drawable.button_modif_31, "");
super.ajouterFichierAssocie("C:\\USERS\\AXELM\\DESKTOP\\COURS IUT INFO\\SEMESTRE 3\\M3302 - PROJET TUTORE - MISE EN SITUATION PROFESSIONNELLE\\PTUT-HOME-TRAINER\\HOME_BIKING\\GABARITS\\WM\\210 MATERIAL DESIGN ORANGE\\MATERIAL DESIGN ORANGE_PICT_DELETE_16_5.PNG",com.logicorp.home_biking.R.drawable.material_design_orange_pict_delete_16_5_29, "");
super.ajouterFichierAssocie("C:\\USERS\\AXELM\\DESKTOP\\COURS IUT INFO\\SEMESTRE 3\\M3302 - PROJET TUTORE - MISE EN SITUATION PROFESSIONNELLE\\PTUT-HOME-TRAINER\\HOME_BIKING\\GABARITS\\WM\\210 MATERIAL DESIGN ORANGE\\MATERIAL DESIGN ORANGE_BTN_CANCEL.PNG?E5_3NP_8_8_8_8",com.logicorp.home_biking.R.drawable.material_design_orange_btn_cancel_28_np3_8_8_8_8_selector, "");
super.ajouterFichierAssocie("C:\\USERS\\AXELM\\DESKTOP\\COURS IUT INFO\\SEMESTRE 3\\M3302 - PROJET TUTORE - MISE EN SITUATION PROFESSIONNELLE\\PTUT-HOME-TRAINER\\HOME_BIKING\\DELETE EXERCISE INITIAL STATE.PNG",com.logicorp.home_biking.R.drawable.delete_exercise_initial_state_27, "");
super.ajouterFichierAssocie("C:\\USERS\\AXELM\\DESKTOP\\COURS IUT INFO\\SEMESTRE 3\\M3302 - PROJET TUTORE - MISE EN SITUATION PROFESSIONNELLE\\PTUT-HOME-TRAINER\\HOME_BIKING\\DELETE EXERCISE DETELTING STATE.PNG",com.logicorp.home_biking.R.drawable.delete_exercise_detelting_state_26, "");
super.ajouterFichierAssocie("C:\\USERS\\AXELM\\DESKTOP\\COURS IUT INFO\\SEMESTRE 3\\M3302 - PROJET TUTORE - MISE EN SITUATION PROFESSIONNELLE\\PTUT-HOME-TRAINER\\HOME_BIKING\\BUTTON VALIDATE.PNG",com.logicorp.home_biking.R.drawable.button_validate_25, "");
super.ajouterFichierAssocie("C:\\USERS\\AXELM\\DESKTOP\\COURS IUT INFO\\SEMESTRE 3\\M3302 - PROJET TUTORE - MISE EN SITUATION PROFESSIONNELLE\\PTUT-HOME-TRAINER\\HOME_BIKING\\BUTTON ADD EXERCISE.PNG",com.logicorp.home_biking.R.drawable.button_add_exercise_24, "");
super.ajouterFichierAssocie("C:\\USERS\\AXELM\\DESKTOP\\COURS IUT INFO\\SEMESTRE 3\\M3302 - PROJET TUTORE - MISE EN SITUATION PROFESSIONNELLE\\PTUT-HOME-TRAINER\\HOME_BIKING\\BOUTON_PREVIOUS_PAGE.PNG",com.logicorp.home_biking.R.drawable.bouton_previous_page_23, "");
super.ajouterFichierAssocie("C:\\USERS\\AXELM\\DESKTOP\\COURS IUT INFO\\SEMESTRE 3\\M3302 - PROJET TUTORE - MISE EN SITUATION PROFESSIONNELLE\\PTUT-HOME-TRAINER\\HOME_BIKING\\GABARITS\\WM\\210 MATERIAL DESIGN ORANGE\\MATERIAL DESIGN ORANGE_ROLLOVER.PNG",com.logicorp.home_biking.R.drawable.material_design_orange_rollover_22, "");
super.ajouterFichierAssocie("C:\\USERS\\AXELM\\DESKTOP\\COURS IUT INFO\\SEMESTRE 3\\M3302 - PROJET TUTORE - MISE EN SITUATION PROFESSIONNELLE\\PTUT-HOME-TRAINER\\HOME_BIKING\\GABARITS\\WM\\210 MATERIAL DESIGN ORANGE\\MATERIAL DESIGN ORANGE_EDT.PNG?E5_3NP_8_8_8_8",com.logicorp.home_biking.R.drawable.material_design_orange_edt_21_np3_8_8_8_8_selector, "");
super.ajouterFichierAssocie("C:\\USERS\\AXELM\\DESKTOP\\COURS IUT INFO\\SEMESTRE 3\\M3302 - PROJET TUTORE - MISE EN SITUATION PROFESSIONNELLE\\PTUT-HOME-TRAINER\\HOME_BIKING\\GABARITS\\WM\\210 MATERIAL DESIGN ORANGE\\MATERIAL DESIGN ORANGE_BTN_STD.PNG?E5_3NP_10_10_10_10",com.logicorp.home_biking.R.drawable.material_design_orange_btn_std_20_np3_10_10_10_10_selector, "");
super.ajouterFichierAssocie("C:\\USERS\\AXELM\\DESKTOP\\COURS IUT INFO\\SEMESTRE 3\\M3302 - PROJET TUTORE - MISE EN SITUATION PROFESSIONNELLE\\PTUT-HOME-TRAINER\\HOME_BIKING\\GABARITS\\WM\\210 MATERIAL DESIGN ORANGE\\MATERIAL DESIGN ORANGE_BREAK_PICT.PNG?E2_4O",com.logicorp.home_biking.R.drawable.material_design_orange_break_pict_19_selector, "");
super.ajouterFichierAssocie("C:\\USERS\\AXELM\\DESKTOP\\COURS IUT INFO\\SEMESTRE 3\\M3302 - PROJET TUTORE - MISE EN SITUATION PROFESSIONNELLE\\PTUT-HOME-TRAINER\\HOME_BIKING\\GABARITS\\WM\\210 MATERIAL DESIGN ORANGE\\MATERIAL DESIGN ORANGE_BREAK.PNG",com.logicorp.home_biking.R.drawable.material_design_orange_break_18, "");
super.ajouterFichierAssocie("C:\\USERS\\AXELM\\DESKTOP\\COURS IUT INFO\\SEMESTRE 3\\M3302 - PROJET TUTORE - MISE EN SITUATION PROFESSIONNELLE\\PTUT-HOME-TRAINER\\HOME_BIKING\\BOUTON SETTINGS.PNG",com.logicorp.home_biking.R.drawable.bouton_settings_17, "");
super.ajouterFichierAssocie("C:\\USERS\\AXELM\\DESKTOP\\COURS IUT INFO\\SEMESTRE 3\\M3302 - PROJET TUTORE - MISE EN SITUATION PROFESSIONNELLE\\PTUT-HOME-TRAINER\\HOME_BIKING\\BOUTON PROFILE.PNG",com.logicorp.home_biking.R.drawable.bouton_profile_16, "");
super.ajouterFichierAssocie("utiliser.FIC",com.logicorp.home_biking.R.raw.utiliser_15, "##BDD##/utiliser.fic");
super.ajouterFichierAssocie("Programme.ndx",com.logicorp.home_biking.R.raw.programme_14, "##BDD##/programme.ndx");
super.ajouterFichierAssocie("Compte_Client.FIC",com.logicorp.home_biking.R.raw.compte_client_13, "##BDD##/compte_client.fic");
super.ajouterFichierAssocie("Compte_Client.ndx",com.logicorp.home_biking.R.raw.compte_client_12, "##BDD##/compte_client.ndx");
super.ajouterFichierAssocie("Exercice.FIC",com.logicorp.home_biking.R.raw.exercice_11, "##BDD##/exercice.fic");
super.ajouterFichierAssocie("Exercice.ndx",com.logicorp.home_biking.R.raw.exercice_10, "##BDD##/exercice.ndx");
super.ajouterFichierAssocie("CreateProg.fic",com.logicorp.home_biking.R.raw.createprog_9, "##BDD##/createprog.fic");
super.ajouterFichierAssocie("Programme.FIC",com.logicorp.home_biking.R.raw.programme_8, "##BDD##/programme.fic");
super.ajouterFichierAssocie("contenir.FIC",com.logicorp.home_biking.R.raw.contenir_7, "##BDD##/contenir.fic");
super.ajouterFichierAssocie("utiliser.ndx",com.logicorp.home_biking.R.raw.utiliser_6, "##BDD##/utiliser.ndx");
super.ajouterFichierAssocie("CreateProg.ndx",com.logicorp.home_biking.R.raw.createprog_5, "##BDD##/createprog.ndx");
super.ajouterFichierAssocie("contenir.ndx",com.logicorp.home_biking.R.raw.contenir_4, "##BDD##/contenir.ndx");
super.ajouterFichierAssocie("Exercice.mmo",com.logicorp.home_biking.R.raw.exercice_3, "##BDD##/exercice.mmo");
}

////////////////////////////////////////////////////////////////////////////
// Dialogues avec Question
////////////////////////////////////////////////////////////////////////////
public WDObjet afficherDialogue(int nIdQuestion, String... params)
{
switch(nIdQuestion)
{
case 0 : return WDAPIDialogue.dialogue("Voulez-vous arrêtez la création de votre programme ?", new String[] {"Oui", "Non"} , new int[] {1, 2} , 0, 1, 0, "", 1, com.logicorp.home_biking.R.raw.question_2290762593068780372_1_30, params);
case 1 : return WDAPIDialogue.dialogue("Voulez-vous vraiment supprimer ce programme ?", new String[] {"Oui", "Non"} , new int[] {1, 2} , 0, 1, 3, "", 1, com.logicorp.home_biking.R.raw.question_1182273482912085284_1_36, params);

default: return super.afficherDialogue(nIdQuestion, params);
}
}

////////////////////////////////////////////////////////////////////////////
// Saisies avec Question
////////////////////////////////////////////////////////////////////////////
public WDObjet afficherSaisie(int nIdQuestion, WDObjet variable, String... params)
{
switch(nIdQuestion)
{
case 0 : return WDAPIDialogue.saisie("Voulez-vous arrêtez la création de votre programme ?", new String[] {"Oui", "Non"} , new int[] {1, 2} , 0, 1, 0, "", 1, com.logicorp.home_biking.R.raw.question_2290762593068780372_1_30, variable, params);
case 1 : return WDAPIDialogue.saisie("Voulez-vous vraiment supprimer ce programme ?", new String[] {"Oui", "Non"} , new int[] {1, 2} , 0, 1, 3, "", 1, com.logicorp.home_biking.R.raw.question_1182273482912085284_1_36, variable, params);

default: return super.afficherSaisie(nIdQuestion, variable, params);
}
}
// Initialisation des collections de procédures
public void initCollections()
{
GWDCPCOL_iSPerso.init();
GWDCPCouleur_Difficulte.init();
GWDCPCOL_Suppr_Creation.init();

}


// Terminaison des collections de procédures
public void terminaisonCollections()
{
GWDCPCOL_Suppr_Creation.term();
GWDCPCouleur_Difficulte.term();
GWDCPCOL_iSPerso.term();

}

/**
 * Lancer de l'application Android
 */
public static class WDLanceur extends WDAbstractLanceur
{
public Class<? extends WDProjet> getClasseProjet()
{
return GWDPHome_Biking.class;
}
}
}
