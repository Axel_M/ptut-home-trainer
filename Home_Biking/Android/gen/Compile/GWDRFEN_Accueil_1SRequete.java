/**
 * Code généré par WINDEV Mobile - NE PAS MODIFIER !
 * Objet WINDEV Mobile : Requête
 * Classe Android : FEN_Accueil_1$Requête
 * Date : 03/04/2021 16:20:01
 * Version de wdjava.dll  : 25.0.221.6
 */


package com.logicorp.home_biking.wdgen;


import com.logicorp.home_biking.*;
import fr.pcsoft.wdjava.core.types.*;
import fr.pcsoft.wdjava.core.*;
import fr.pcsoft.wdjava.database.hf.requete.parsing.*;
/*Imports trouvés dans le code WL*/
/*Fin Imports trouvés dans le code WL*/



public class GWDRFEN_Accueil_1SRequete extends WDDescRequeteWDR
{
public String getNomLogique()
{
return "FEN_Accueil_1$Requête";
}
public String getCodeSQLOriginal()
{
return " SELECT TOP 10  Programme.Nom AS Nom,\t Programme.Difficulte AS Difficulte,\t utiliser.Id_Programme AS Id_Programme,\t utiliser.DateUtilisation AS DateUtilisation  FROM  Programme,\t utiliser  WHERE   Programme.Id_Programme = utiliser.Id_Programme   ORDER BY  DateUtilisation DESC";
}
public Requete initArbre() throws WDInvalidSQLException
{
Select varSelect = new Select();
varSelect.setType(1);
Rubrique rub_Nom = new Rubrique();
rub_Nom.setNom("Nom");
rub_Nom.setAlias("Nom");
rub_Nom.setNomFichier("Programme");
rub_Nom.setAliasFichier("Programme");
varSelect.ajouterElement(rub_Nom);
Rubrique rub_Difficulte = new Rubrique();
rub_Difficulte.setNom("Difficulte");
rub_Difficulte.setAlias("Difficulte");
rub_Difficulte.setNomFichier("Programme");
rub_Difficulte.setAliasFichier("Programme");
varSelect.ajouterElement(rub_Difficulte);
Rubrique rub_Id_Programme = new Rubrique();
rub_Id_Programme.setNom("Id_Programme");
rub_Id_Programme.setAlias("Id_Programme");
rub_Id_Programme.setNomFichier("utiliser");
rub_Id_Programme.setAliasFichier("utiliser");
varSelect.ajouterElement(rub_Id_Programme);
Rubrique rub_DateUtilisation = new Rubrique();
rub_DateUtilisation.setNom("DateUtilisation");
rub_DateUtilisation.setAlias("DateUtilisation");
rub_DateUtilisation.setNomFichier("utiliser");
rub_DateUtilisation.setAliasFichier("utiliser");
varSelect.ajouterElement(rub_DateUtilisation);
From varFrom = new From();
Fichier fic_Programme = new Fichier();
fic_Programme.setNom("Programme");
fic_Programme.setAlias("Programme");
varFrom.ajouterElement(fic_Programme);
Fichier fic_utiliser = new Fichier();
fic_utiliser.setNom("utiliser");
fic_utiliser.setAlias("utiliser");
varFrom.ajouterElement(fic_utiliser);
Requete varReqSelect = new Requete(1);
varReqSelect.ajouterClause(varSelect);
varReqSelect.ajouterClause(varFrom);
Expression expr__ = new Expression(9, "=", "Programme.Id_Programme = utiliser.Id_Programme");
Rubrique rub_Id_Programme_1 = new Rubrique();
rub_Id_Programme_1.setNom("Programme.Id_Programme");
rub_Id_Programme_1.setAlias("Id_Programme");
rub_Id_Programme_1.setNomFichier("Programme");
rub_Id_Programme_1.setAliasFichier("Programme");
expr__.ajouterElement(rub_Id_Programme_1);
Rubrique rub_Id_Programme_2 = new Rubrique();
rub_Id_Programme_2.setNom("utiliser.Id_Programme");
rub_Id_Programme_2.setAlias("Id_Programme");
rub_Id_Programme_2.setNomFichier("utiliser");
rub_Id_Programme_2.setAliasFichier("utiliser");
expr__.ajouterElement(rub_Id_Programme_2);
Where varWhere = new Where();
varWhere.ajouterElement(expr__);
varReqSelect.ajouterClause(varWhere);
OrderBy varOrderBy = new OrderBy();
Rubrique rub_DateUtilisation_1 = new Rubrique();
rub_DateUtilisation_1.setNom("DateUtilisation");
rub_DateUtilisation_1.setAlias("DateUtilisation");
rub_DateUtilisation_1.setNomFichier("utiliser");
rub_DateUtilisation_1.setAliasFichier("utiliser");
rub_DateUtilisation_1.ajouterOption(EWDOptionRequete.TRI, "1");
rub_DateUtilisation_1.ajouterOption(EWDOptionRequete.INDEX_RUB, "3");
varOrderBy.ajouterElement(rub_DateUtilisation_1);
varReqSelect.ajouterClause(varOrderBy);
Limit varLimit = new Limit();
varLimit.setType(1);
varLimit.setNbEnregs(10);
varLimit.setOffset(0);
varReqSelect.ajouterClause(varLimit);
return varReqSelect;
}
public String getNomFichier(int nIndex)
{
switch(nIndex)
{
case 0 : return "Programme";
case 1 : return "utiliser";
default: return null;
}
}
public String getAliasFichier(int nIndex)
{
switch(nIndex)
{
case 0 : return "Programme";
case 1 : return "utiliser";
default: return null;
}
}


}
