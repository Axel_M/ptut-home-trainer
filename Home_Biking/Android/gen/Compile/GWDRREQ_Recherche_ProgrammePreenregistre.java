/**
 * Code généré par WINDEV Mobile - NE PAS MODIFIER !
 * Objet WINDEV Mobile : Requête
 * Classe Android : REQ_Recherche_ProgrammePreenregistre
 * Date : 03/04/2021 11:37:53
 * Version de wdjava.dll  : 25.0.221.6
 */


package com.logicorp.home_biking.wdgen;


import com.logicorp.home_biking.*;
import fr.pcsoft.wdjava.core.types.*;
import fr.pcsoft.wdjava.core.*;
import fr.pcsoft.wdjava.database.hf.requete.parsing.*;
/*Imports trouvés dans le code WL*/
/*Fin Imports trouvés dans le code WL*/



public class GWDRREQ_Recherche_ProgrammePreenregistre extends WDDescRequeteWDR
{
public String getNomLogique()
{
return "REQ_Recherche_ProgrammePreenregistre";
}
public String getCodeSQLOriginal()
{
return "SELECT \r\n\tProgramme.Id_Programme AS Id_Programme,\t\r\n\tProgramme.Nom AS Nom,\t\r\n\tProgramme.Difficulte AS Difficulte,\t\r\n\tProgramme.Id_Compte_Client AS Id_Compte_Client\r\nFROM \r\n\tProgramme\r\nWHERE \r\n\tProgramme.Nom LIKE %{ParamNom#0}%\r\n\tAND \r\n\tProgramme.Id_Compte_Client = 0";
}
public Requete initArbre() throws WDInvalidSQLException
{
Select varSelect = new Select();
varSelect.setType(1);
Rubrique rub_Id_Programme = new Rubrique();
rub_Id_Programme.setNom("Id_Programme");
rub_Id_Programme.setAlias("Id_Programme");
rub_Id_Programme.setNomFichier("Programme");
rub_Id_Programme.setAliasFichier("Programme");
varSelect.ajouterElement(rub_Id_Programme);
Rubrique rub_Nom = new Rubrique();
rub_Nom.setNom("Nom");
rub_Nom.setAlias("Nom");
rub_Nom.setNomFichier("Programme");
rub_Nom.setAliasFichier("Programme");
varSelect.ajouterElement(rub_Nom);
Rubrique rub_Difficulte = new Rubrique();
rub_Difficulte.setNom("Difficulte");
rub_Difficulte.setAlias("Difficulte");
rub_Difficulte.setNomFichier("Programme");
rub_Difficulte.setAliasFichier("Programme");
varSelect.ajouterElement(rub_Difficulte);
Rubrique rub_Id_Compte_Client = new Rubrique();
rub_Id_Compte_Client.setNom("Id_Compte_Client");
rub_Id_Compte_Client.setAlias("Id_Compte_Client");
rub_Id_Compte_Client.setNomFichier("Programme");
rub_Id_Compte_Client.setAliasFichier("Programme");
varSelect.ajouterElement(rub_Id_Compte_Client);
From varFrom = new From();
Fichier fic_Programme = new Fichier();
fic_Programme.setNom("Programme");
fic_Programme.setAlias("Programme");
varFrom.ajouterElement(fic_Programme);
Requete varReqSelect = new Requete(1);
varReqSelect.ajouterClause(varSelect);
varReqSelect.ajouterClause(varFrom);
Expression expr_AND = new Expression(24, "AND", "Programme.Nom LIKE %{ParamNom}%\r\n\tAND \r\n\tProgramme.Id_Compte_Client = 0");
Expression expr_LIKE = new Expression(32, "LIKE", "Programme.Nom LIKE %{ParamNom}%");
expr_LIKE.ajouterOption(EWDOptionRequete.LIKE_CASE_SENSITIVE, "0");
expr_LIKE.ajouterOption(EWDOptionRequete.LIKE_COMMENCE_PAR, "0");
expr_LIKE.ajouterOption(EWDOptionRequete.LIKE_CONTIENT, "1");
expr_LIKE.ajouterOption(EWDOptionRequete.LIKE_FINI_PAR, "0");
expr_LIKE.ajouterOption(EWDOptionRequete.NOT_LIKE, "0");
expr_LIKE.ajouterOption(EWDOptionRequete.LIKE_CARACT_ECHAP, "92");
Rubrique rub_Nom_1 = new Rubrique();
rub_Nom_1.setNom("Programme.Nom");
rub_Nom_1.setAlias("Nom");
rub_Nom_1.setNomFichier("Programme");
rub_Nom_1.setAliasFichier("Programme");
expr_LIKE.ajouterElement(rub_Nom_1);
Parametre param_ParamNom = new Parametre();
param_ParamNom.setNom("ParamNom");
expr_LIKE.ajouterElement(param_ParamNom);
expr_AND.ajouterElement(expr_LIKE);
Expression expr__ = new Expression(9, "=", "Programme.Id_Compte_Client = 0");
Rubrique rub_Id_Compte_Client_1 = new Rubrique();
rub_Id_Compte_Client_1.setNom("Programme.Id_Compte_Client");
rub_Id_Compte_Client_1.setAlias("Id_Compte_Client");
rub_Id_Compte_Client_1.setNomFichier("Programme");
rub_Id_Compte_Client_1.setAliasFichier("Programme");
expr__.ajouterElement(rub_Id_Compte_Client_1);
Literal varLiteral = new Literal();
varLiteral.setValeur("0");
varLiteral.setTypeWL(8);
expr__.ajouterElement(varLiteral);
expr_AND.ajouterElement(expr__);
Where varWhere = new Where();
varWhere.ajouterElement(expr_AND);
varReqSelect.ajouterClause(varWhere);
Limit varLimit = new Limit();
varLimit.setType(0);
varLimit.setNbEnregs(0);
varLimit.setOffset(0);
varReqSelect.ajouterClause(varLimit);
return varReqSelect;
}
public String getNomFichier(int nIndex)
{
switch(nIndex)
{
case 0 : return "Programme";
default: return null;
}
}
public String getAliasFichier(int nIndex)
{
switch(nIndex)
{
case 0 : return "Programme";
default: return null;
}
}


public int getIdWDR()
{
return com.logicorp.home_biking.R.raw.req_recherche_programmepreenregistre;
}
public String getNomFichierWDR()
{
return "req_recherche_programmepreenregistre";
}
}
