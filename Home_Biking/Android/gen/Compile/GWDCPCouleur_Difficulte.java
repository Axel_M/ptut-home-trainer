/**
 * Code généré par WINDEV Mobile - NE PAS MODIFIER !
 * Objet WINDEV Mobile : Collection
 * Classe Android : Couleur_Difficulte
 * Date : 03/04/2021 11:37:53
 * Version de wdjava.dll  : 25.0.221.6
 */


package com.logicorp.home_biking.wdgen;


import com.logicorp.home_biking.*;
import fr.pcsoft.wdjava.core.types.*;
import fr.pcsoft.wdjava.core.*;
import fr.pcsoft.wdjava.core.application.*;
import fr.pcsoft.wdjava.api.*;
/*Imports trouvés dans le code WL*/
/*Fin Imports trouvés dans le code WL*/



public class GWDCPCouleur_Difficulte extends WDCollProcAndroid
{

public WDProjet getProjet()
{
return GWDPHome_Biking.getInstance();
}

public IWDEnsembleElement getEnsemble()
{
return GWDPHome_Biking.getInstance();
}

protected String getNomCollection()
{
return "Couleur_Difficulte";
}
private final static GWDCPCouleur_Difficulte ms_instance = new GWDCPCouleur_Difficulte();
public final static GWDCPCouleur_Difficulte getInstance()
{
return ms_instance;
}

// Code de déclaration de Couleur_Difficulte
static public void init()
{
// 
ms_instance.initDeclarationCollection();

try
{
}
finally
{
finDeclarationCollection();
}

}




// Code de terminaison de Couleur_Difficulte
static public void term()
{
// 
ms_instance.initTerminaisonCollection();

try
{
}
finally
{
finTerminaisonCollection();
}

}



// Nombre de Procédures : 3
static public WDObjet fWD_couleurDifficulte( WDObjet vWD_ParamDifficulte )
{
// FONCTION CouleurDifficulte(ParamDifficulte)
ms_instance.initExecProcGlobale("CouleurDifficulte");

try
{
// 	SI ParamDifficulte="Très facile" ALORS
if(vWD_ParamDifficulte.opEgal("Très facile"))
{
// 		RENVOYER RVB(144,220,0)
return WDAPIDessin.rvb(144,220,0);

}

// 	SI ParamDifficulte="Facile" ALORS
if(vWD_ParamDifficulte.opEgal("Facile"))
{
// 		RENVOYER RVB(109,166,0)
return WDAPIDessin.rvb(109,166,0);

}

// 	SI ParamDifficulte="Moyen" ALORS
if(vWD_ParamDifficulte.opEgal("Moyen"))
{
// 		RENVOYER RVB(255,219,54)
return WDAPIDessin.rvb(255,219,54);

}

// 	SI ParamDifficulte="Difficile" ALORS
if(vWD_ParamDifficulte.opEgal("Difficile"))
{
// 		RENVOYER RVB(255,128,0)
return WDAPIDessin.rvb(255,128,0);

}

// 	SI ParamDifficulte="Très difficile" ALORS
if(vWD_ParamDifficulte.opEgal("Très difficile"))
{
// 		RENVOYER RVB(192,0,0)
return WDAPIDessin.rvb(192,0,0);

}
else
{
// 		RENVOYER Noir
return new WDEntier4(0);

}

}
finally
{
finExecProcGlobale();
}

}


static public WDObjet fWD_difficulteFCM( WDObjet vWD_ParamFCM )
{
// FONCTION DifficulteFCM(ParamFCM)
ms_instance.initExecProcGlobale("DifficulteFCM");

try
{
// 	SI ParamFCM>=50 ET ParamFCM<60 ALORS
if((vWD_ParamFCM.opSupEgal(50) & vWD_ParamFCM.opInf(60)))
{
// 		RENVOYER "Très facile"
return new WDChaineU("Très facile");

}

// 	SI ParamFCM>=60 ET ParamFCM<70 ALORS
if((vWD_ParamFCM.opSupEgal(60) & vWD_ParamFCM.opInf(70)))
{
// 		RENVOYER "Facile"
return new WDChaineU("Facile");

}

// 	SI ParamFCM>=70 ET ParamFCM<80 ALORS
if((vWD_ParamFCM.opSupEgal(70) & vWD_ParamFCM.opInf(80)))
{
// 		RENVOYER "Moyen"
return new WDChaineU("Moyen");

}

// 	SI ParamFCM>=80 ET ParamFCM<90 ALORS
if((vWD_ParamFCM.opSupEgal(80) & vWD_ParamFCM.opInf(90)))
{
// 		RENVOYER "Difficile"
return new WDChaineU("Difficile");

}

// 	SI ParamFCM>=90 ALORS
if(vWD_ParamFCM.opSupEgal(90))
{
// 		RENVOYER "Très difficile"
return new WDChaineU("Très difficile");

}

return new WDVoid("fWD_difficulteFCM");
}
finally
{
finExecProcGlobale();
}

}


static public WDObjet fWD_difficulteVersEntier( WDObjet vWD_ParamDifficulte )
{
// PROCEDURE DifficulteVersEntier(ParamDifficulte)
ms_instance.initExecProcGlobale("DifficulteVersEntier");

try
{
// SI ParamDifficulte="Très facile" ALORS
if(vWD_ParamDifficulte.opEgal("Très facile"))
{
// 	RENVOYER 0
return new WDEntier4(0);

}

// SI ParamDifficulte="Facile" ALORS
if(vWD_ParamDifficulte.opEgal("Facile"))
{
// 	RENVOYER 1
return new WDEntier4(1);

}

// SI ParamDifficulte="Moyen" ALORS
if(vWD_ParamDifficulte.opEgal("Moyen"))
{
// 	RENVOYER 2
return new WDEntier4(2);

}

// SI ParamDifficulte="Difficile" ALORS
if(vWD_ParamDifficulte.opEgal("Difficile"))
{
// 	RENVOYER 3
return new WDEntier4(3);

}

// SI ParamDifficulte="Très difficile" ALORS
if(vWD_ParamDifficulte.opEgal("Très difficile"))
{
// 	RENVOYER 4
return new WDEntier4(4);

}
else
{
// 	RENVOYER -1
return new WDEntier4(-1);

}

}
finally
{
finExecProcGlobale();
}

}



////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
