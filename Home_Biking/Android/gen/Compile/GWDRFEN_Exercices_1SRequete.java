/**
 * Code généré par WINDEV Mobile - NE PAS MODIFIER !
 * Objet WINDEV Mobile : Requête
 * Classe Android : FEN_Exercices_1$Requête
 * Date : 03/04/2021 16:20:01
 * Version de wdjava.dll  : 25.0.221.6
 */


package com.logicorp.home_biking.wdgen;


import com.logicorp.home_biking.*;
import fr.pcsoft.wdjava.core.types.*;
import fr.pcsoft.wdjava.core.*;
import fr.pcsoft.wdjava.database.hf.requete.parsing.*;
/*Imports trouvés dans le code WL*/
/*Fin Imports trouvés dans le code WL*/



public class GWDRFEN_Exercices_1SRequete extends WDDescRequeteWDR
{
public String getNomLogique()
{
return "FEN_Exercices_1$Requête";
}
public String getCodeSQLOriginal()
{
return " SELECT  Exercice.Nom AS Nom,\t Exercice.Description AS Description,\t Exercice.Id_Exercice AS Id_Exercice  FROM  Exercice  WHERE   Exercice.Nom LIKE %{ParamNom#0}%  ORDER BY  Nom ASC";
}
public Requete initArbre() throws WDInvalidSQLException
{
Select varSelect = new Select();
varSelect.setType(1);
Rubrique rub_Nom = new Rubrique();
rub_Nom.setNom("Nom");
rub_Nom.setAlias("Nom");
rub_Nom.setNomFichier("Exercice");
rub_Nom.setAliasFichier("Exercice");
varSelect.ajouterElement(rub_Nom);
Rubrique rub_Description = new Rubrique();
rub_Description.setNom("Description");
rub_Description.setAlias("Description");
rub_Description.setNomFichier("Exercice");
rub_Description.setAliasFichier("Exercice");
varSelect.ajouterElement(rub_Description);
Rubrique rub_Id_Exercice = new Rubrique();
rub_Id_Exercice.setNom("Id_Exercice");
rub_Id_Exercice.setAlias("Id_Exercice");
rub_Id_Exercice.setNomFichier("Exercice");
rub_Id_Exercice.setAliasFichier("Exercice");
varSelect.ajouterElement(rub_Id_Exercice);
From varFrom = new From();
Fichier fic_Exercice = new Fichier();
fic_Exercice.setNom("Exercice");
fic_Exercice.setAlias("Exercice");
varFrom.ajouterElement(fic_Exercice);
Requete varReqSelect = new Requete(1);
varReqSelect.ajouterClause(varSelect);
varReqSelect.ajouterClause(varFrom);
Expression expr_LIKE = new Expression(32, "LIKE", "Exercice.Nom LIKE %{ParamNom}%");
expr_LIKE.ajouterOption(EWDOptionRequete.LIKE_CASE_SENSITIVE, "0");
expr_LIKE.ajouterOption(EWDOptionRequete.LIKE_COMMENCE_PAR, "0");
expr_LIKE.ajouterOption(EWDOptionRequete.LIKE_CONTIENT, "1");
expr_LIKE.ajouterOption(EWDOptionRequete.LIKE_FINI_PAR, "0");
expr_LIKE.ajouterOption(EWDOptionRequete.NOT_LIKE, "0");
expr_LIKE.ajouterOption(EWDOptionRequete.LIKE_CARACT_ECHAP, "92");
Rubrique rub_Nom_1 = new Rubrique();
rub_Nom_1.setNom("Exercice.Nom");
rub_Nom_1.setAlias("Nom");
rub_Nom_1.setNomFichier("Exercice");
rub_Nom_1.setAliasFichier("Exercice");
expr_LIKE.ajouterElement(rub_Nom_1);
Parametre param_ParamNom = new Parametre();
param_ParamNom.setNom("ParamNom");
expr_LIKE.ajouterElement(param_ParamNom);
Where varWhere = new Where();
varWhere.ajouterElement(expr_LIKE);
varReqSelect.ajouterClause(varWhere);
OrderBy varOrderBy = new OrderBy();
Rubrique rub_Nom_2 = new Rubrique();
rub_Nom_2.setNom("Nom");
rub_Nom_2.setAlias("Nom");
rub_Nom_2.setNomFichier("Exercice");
rub_Nom_2.setAliasFichier("Exercice");
rub_Nom_2.ajouterOption(EWDOptionRequete.TRI, "0");
rub_Nom_2.ajouterOption(EWDOptionRequete.INDEX_RUB, "0");
varOrderBy.ajouterElement(rub_Nom_2);
varReqSelect.ajouterClause(varOrderBy);
Limit varLimit = new Limit();
varLimit.setType(0);
varLimit.setNbEnregs(0);
varLimit.setOffset(0);
varReqSelect.ajouterClause(varLimit);
return varReqSelect;
}
public String getNomFichier(int nIndex)
{
switch(nIndex)
{
case 0 : return "Exercice";
default: return null;
}
}
public String getAliasFichier(int nIndex)
{
switch(nIndex)
{
case 0 : return "Exercice";
default: return null;
}
}


}
