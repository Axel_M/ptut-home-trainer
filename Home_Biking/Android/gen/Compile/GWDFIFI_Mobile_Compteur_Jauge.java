/**
 * Code généré par WINDEV Mobile - NE PAS MODIFIER !
 * Objet WINDEV Mobile : Fenêtre
 * Classe Android : FI_Mobile_Compteur_Jauge
 * Date : 16/12/2020 08:51:30
 * Version de wdjava.dll  : 25.0.221.6
 */


package com.logicorp.home_biking.wdgen;


import com.logicorp.home_biking.*;
import fr.pcsoft.wdjava.core.types.*;
import fr.pcsoft.wdjava.core.*;
import fr.pcsoft.wdjava.ui.champs.fenetreinterne.*;
import fr.pcsoft.wdjava.ui.champs.image.*;
import fr.pcsoft.wdjava.ui.cadre.*;
import fr.pcsoft.wdjava.ui.champs.libelle.*;
import fr.pcsoft.wdjava.core.application.*;
import fr.pcsoft.wdjava.ui.dessin.*;
import fr.pcsoft.wdjava.core.poo.*;
import fr.pcsoft.wdjava.api.*;
/*Imports trouvés dans le code WL*/
/*Fin Imports trouvés dans le code WL*/



public class GWDFIFI_Mobile_Compteur_Jauge extends WDFenetreInterne
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs de FI_Mobile_Compteur_Jauge
////////////////////////////////////////////////////////////////////////////

/**
 * IMG_Dessin
 */
class GWDIMG_Dessin extends WDChampImage
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°1 de FI_Mobile_Compteur_Jauge.FI_Mobile_Compteur_Jauge.IMG_Dessin
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setQuid(3300695115197941282l);

super.setChecksum("1527513370");

super.setNom("IMG_Dessin");

super.setType(30001);

super.setBulle("");

super.setLibelle("");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setCurseurSouris(0);

super.setNavigable(false);

super.setEtatInitial(0);

super.setPositionInitiale(0, 0);

super.setTailleInitiale(250, 250);

super.setValeurInitiale("");

super.setPlan(0);

super.setTailleMin(0, 0);

super.setTailleMax(134217727, 134217727);

super.setVisibleInitial(true);

super.setAltitude(1);

super.setAncrageInitial(10, 1000, 1000, 1000, 1000, 0);

super.setTransparence(1);

super.setParamImage(1, 0, true, 2500);

super.setSymetrie(0);

super.setZoneClicage(true);

super.setPCodeMultitouch(false);

super.setChargementEnTacheDeFond(false);

super.setOrientationExif(false);

super.setParamAnimation(1, 1, false, 10, true, false);

super.setAnimationInitiale(false);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(false);

super.setStyleLibelle(0x222222, creerPolice_GEN("Roboto", -8.000000, 0), -1, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDIMG_Dessin mWD_IMG_Dessin;

/**
 * LIB_Total
 */
class GWDLIB_Total extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°2 de FI_Mobile_Compteur_Jauge.FI_Mobile_Compteur_Jauge.LIB_Total
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setQuid(3300695115198006818l);

super.setChecksum("1527576626");

super.setNom("LIB_Total");

super.setType(3);

super.setBulle("");

super.setTypeSaisie(1);

super.setMasqueSaisie(new WDChaineU("999 999"));

super.setLibelle("9999");

super.setNote("", "");

super.setCurseurSouris(0);

super.setEtatInitial(0);

super.setPositionInitiale(34, 135);

super.setTailleInitiale(182, 33);

super.setPlan(0);

super.setCadrageHorizontal(1);

super.setCadrageVertical(1);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(2);

super.setAncrageInitial(4, 1000, 1000, 500, 1000, 0);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x8E8E8F, 0xFFFFFFFF, creerPolice_GEN("Roboto", -22.000000, 0), 3, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_Total mWD_LIB_Total;

/**
 * LIB_Valeur
 */
class GWDLIB_Valeur extends WDLibelle
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des champs du fils n°3 de FI_Mobile_Compteur_Jauge.FI_Mobile_Compteur_Jauge.LIB_Valeur
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setFenetreInterne( getWDFenetreInterneThis() );
super.setQuid(3300695115198072354l);

super.setChecksum("1527642162");

super.setNom("LIB_Valeur");

super.setType(3);

super.setBulle("");

super.setTypeSaisie(1);

super.setMasqueSaisie(new WDChaineU("999 999"));

super.setLibelle("9999");

super.setNote("", "");

super.setCurseurSouris(0);

super.setEtatInitial(0);

super.setPositionInitiale(34, 72);

super.setTailleInitiale(182, 56);

super.setPlan(0);

super.setCadrageHorizontal(1);

super.setCadrageVertical(1);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setVisibleInitial(true);

super.setAltitude(3);

super.setAncrageInitial(4, 1000, 1000, 500, 1000, 0);

super.setEllipse(0);

super.setTauxParallaxe(0, 0);

super.setPresenceLibelle(true);

super.setStyleLibelle(0x222222, 0xFFFFFFFF, creerPolice_GEN("Roboto", -40.000000, 0), 3, 0, 0x222222);

super.setCadreExterieur(WDCadreFactory.creerCadre_GEN(1, 0xE1E1E1, 0x616161, 0xFFFFFFFF, 2.000000, 2.000000, 0, 1), 0, 0, 0, 0);


activerEcoute();
super.terminerInitialisation();
}

// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
public GWDLIB_Valeur mWD_LIB_Valeur;


////////////////////////////////////////////////////////////////////////////
// Procédures utilisateur de FI_Mobile_Compteur_Jauge
////////////////////////////////////////////////////////////////////////////
public void fWD__DessineJaugeAnneau( WDObjet vWD_nPourcent )
{
// PROCÉDURE PRIVÉE _DessineJaugeAnneau(LOCAL nPourcent est un entier)
initExecProcLocale("_DessineJaugeAnneau");

try
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables locales au traitement
// (En WLangage les variables sont encore visibles après la fin du bloc dans lequel elles sont déclarées)
////////////////////////////////////////////////////////////////////////////
WDObjet vWD_nDiametreCercle = new WDEntier4();

WDObjet vWD_nCouleurJauge = new WDEntier4();

WDObjet vWD_MonImage = WDVarNonAllouee.ref;
WDObjet vWD_nX1 = new WDEntier4();

WDObjet vWD_nX2 = new WDEntier4();

WDObjet vWD_nY1 = new WDEntier4();

WDObjet vWD_nY2 = new WDEntier4();

WDObjet vWD_MonImageRef = WDVarNonAllouee.ref;
WDObjet vWD_nFacteur = new WDEntier4();

WDObjet vWD_nLargeur = new WDEntier4();

WDObjet vWD_nHauteur = new WDEntier4();

WDObjet vWD_nCentreX = new WDEntier4();

WDObjet vWD_nCentreY = new WDEntier4();

WDObjet vWD_nEpaisseur = new WDEntier4();

WDObjet vWD_nAngleFin = new WDEntier4();



vWD_nPourcent = WDParametre.traiterParametre(vWD_nPourcent, 1, true, 8);


// nDiametreCercle est un entier 


// nCouleurJauge est un entier = gnCouleurJauge

vWD_nCouleurJauge.setValeur(vWD_gnCouleurJauge);


// MonImage est une Image
vWD_MonImage = new WDInstance( new WDImage() );


// nX1, nX2, nY1, nY2 sont des entiers





// MonImageRef est Image = IMG_Dessin
vWD_MonImageRef = new WDInstance( new WDImage() );

vWD_MonImageRef.setValeur(mWD_IMG_Dessin);


// nFacteur est entier = MonImageRef..EchelleDessin

vWD_nFacteur.setValeur(vWD_MonImageRef.getProp(EWDPropriete.PROP_ECHELLEDESSIN));


// nLargeur est entier = IMG_Dessin..Largeur 

vWD_nLargeur.setValeur(mWD_IMG_Dessin.getProp(EWDPropriete.PROP_LARGEUR));


// nHauteur est entier = IMG_Dessin..Hauteur 

vWD_nHauteur.setValeur(mWD_IMG_Dessin.getProp(EWDPropriete.PROP_HAUTEUR));


// MonImage..Largeur = nLargeur*nFacteur
vWD_MonImage.setProp(EWDPropriete.PROP_LARGEUR,vWD_nLargeur.opMult(vWD_nFacteur));

// MonImage..Hauteur = nHauteur*nFacteur
vWD_MonImage.setProp(EWDPropriete.PROP_HAUTEUR,vWD_nHauteur.opMult(vWD_nFacteur));

// MonImage..EchelleDessin = nFacteur
vWD_MonImage.setProp(EWDPropriete.PROP_ECHELLEDESSIN,vWD_nFacteur);

// nCentreX est un entier = nLargeur/2

vWD_nCentreX.setValeur(vWD_nLargeur.opDiv(2));


// nCentreY est un entier = nHauteur/2

vWD_nCentreY.setValeur(vWD_nHauteur.opDiv(2));


// dDébutDessin(MonImage)
WDAPIDessin.dDebutDessin(vWD_MonImage);

// SI nFacteur <= 1 ALORS
if(vWD_nFacteur.opInfEgal(1))
{
// 	dChangeMode(dessinAntiAliasing)
WDAPIDessin.dChangeMode(256);

}

// nDiametreCercle = MonImage..Largeur*0.9 
vWD_nDiametreCercle.setValeur(vWD_MonImage.getProp(EWDPropriete.PROP_LARGEUR).opMult(0.9));

// nEpaisseur est un entier = MonImage..Largeur*10/200

vWD_nEpaisseur.setValeur(vWD_MonImage.getProp(EWDPropriete.PROP_LARGEUR).opMult(10).opDiv(200));


// nAngleFin est un entier = modulo((nPourcent * 360 / 100)+gnAngleDépart,360)

vWD_nAngleFin.setValeur(vWD_nPourcent.opMult(360).opDiv(100).opPlus(vWD_gnAngleDepart).opMod(360));


// _PositionsSelonAngle(gnAngleDépart, nCentreX, nCentreY, nDiametreCercle, nX1, nY1)
fWD__PositionsSelonAngle(vWD_gnAngleDepart,vWD_nCentreX,vWD_nCentreY,vWD_nDiametreCercle,vWD_nX1,vWD_nY1);

// _PositionsSelonAngle(nAngleFin, nCentreX, nCentreY, nDiametreCercle, nX2, nY2)
fWD__PositionsSelonAngle(vWD_nAngleFin,vWD_nCentreX,vWD_nCentreY,vWD_nDiametreCercle,vWD_nX2,vWD_nY2);

// dArc((nCentreX - nDiametreCercle/2)/nFacteur, (nCentreY - nDiametreCercle/2)/nFacteur, (nCentreX + nDiametreCercle/2)/nFacteur, (nCentreY + nDiametreCercle/2)/nFacteur, nX2/nFacteur, nY2/nFacteur,nX1/nFacteur, nY1/nFacteur, gnCouleurDécor,nEpaisseur/nFacteur)
WDAPIDessin.dArc(vWD_nCentreX.opMoins(vWD_nDiametreCercle.opDiv(2)).opDiv(vWD_nFacteur),vWD_nCentreY.opMoins(vWD_nDiametreCercle.opDiv(2)).opDiv(vWD_nFacteur).getInt(),vWD_nCentreX.opPlus(vWD_nDiametreCercle.opDiv(2)).opDiv(vWD_nFacteur).getInt(),vWD_nCentreY.opPlus(vWD_nDiametreCercle.opDiv(2)).opDiv(vWD_nFacteur).getInt(),vWD_nX2.opDiv(vWD_nFacteur).getInt(),vWD_nY2.opDiv(vWD_nFacteur).getInt(),vWD_nX1.opDiv(vWD_nFacteur).getInt(),vWD_nY1.opDiv(vWD_nFacteur).getInt(),vWD_gnCouleurDecor,vWD_nEpaisseur.opDiv(vWD_nFacteur));

// SI nPourcent > 0 ALORS
if(vWD_nPourcent.opSup(0))
{
// 	dArc(((MonImage..Largeur-nDiametreCercle)/2)/nFacteur, ((MonImage..Largeur-nDiametreCercle)/2)/nFacteur, (MonImage..Hauteur-(MonImage..Largeur-nDiametreCercle)/2)/nFacteur, (MonImage..Largeur-(MonImage..Largeur-nDiametreCercle)/2)/nFacteur, nX1/nFacteur, nY1/nFacteur, nX2/nFacteur, nY2/nFacteur, nCouleurJauge,nEpaisseur/nFacteur)	
WDAPIDessin.dArc(vWD_MonImage.getProp(EWDPropriete.PROP_LARGEUR).opMoins(vWD_nDiametreCercle).opDiv(2).opDiv(vWD_nFacteur),vWD_MonImage.getProp(EWDPropriete.PROP_LARGEUR).opMoins(vWD_nDiametreCercle).opDiv(2).opDiv(vWD_nFacteur).getInt(),vWD_MonImage.getProp(EWDPropriete.PROP_HAUTEUR).opMoins(vWD_MonImage.getProp(EWDPropriete.PROP_LARGEUR).opMoins(vWD_nDiametreCercle).opDiv(2)).opDiv(vWD_nFacteur).getInt(),vWD_MonImage.getProp(EWDPropriete.PROP_LARGEUR).opMoins(vWD_MonImage.getProp(EWDPropriete.PROP_LARGEUR).opMoins(vWD_nDiametreCercle).opDiv(2)).opDiv(vWD_nFacteur).getInt(),vWD_nX1.opDiv(vWD_nFacteur).getInt(),vWD_nY1.opDiv(vWD_nFacteur).getInt(),vWD_nX2.opDiv(vWD_nFacteur).getInt(),vWD_nY2.opDiv(vWD_nFacteur).getInt(),vWD_nCouleurJauge,vWD_nEpaisseur.opDiv(vWD_nFacteur));

}

// SI gbSensHoraire ALORS
if(vWD_gbSensHoraire.getBoolean())
{
// 	dSymétrieVerticale(MonImage)
WDAPIDessin.dSymetrieVerticale(vWD_MonImage);

}

// IMG_Dessin = MonImage
mWD_IMG_Dessin.setValeur(vWD_MonImage);

}
finally
{
finExecProcLocale();
}

}



public void fWD__DessineJaugeRond( WDObjet vWD_nPourcent )
{
// PROCÉDURE PRIVÉ _DessineJaugeRond(LOCAL nPourcent est un entier)
initExecProcLocale("_DessineJaugeRond");

try
{

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables locales au traitement
// (En WLangage les variables sont encore visibles après la fin du bloc dans lequel elles sont déclarées)
////////////////////////////////////////////////////////////////////////////
WDObjet vWD_nDiametreCercle = new WDEntier4();

WDObjet vWD_nCouleurJauge = new WDEntier4();

WDObjet vWD_nX1 = new WDEntier4();

WDObjet vWD_nX2 = new WDEntier4();

WDObjet vWD_nY1 = new WDEntier4();

WDObjet vWD_nY2 = new WDEntier4();

WDObjet vWD_nDifference = new WDEntier4();

WDObjet vWD_MonImage = WDVarNonAllouee.ref;
WDObjet vWD_MonImageRef = WDVarNonAllouee.ref;
WDObjet vWD_nFacteur = new WDEntier4();

WDObjet vWD_nLargeur = new WDEntier4();

WDObjet vWD_nHauteur = new WDEntier4();

WDObjet vWD_nCentreX = new WDEntier4();

WDObjet vWD_nCentreY = new WDEntier4();



vWD_nPourcent = WDParametre.traiterParametre(vWD_nPourcent, 1, true, 8);


// nDiametreCercle est un entier 


// nCouleurJauge est un entier = gnCouleurJauge

vWD_nCouleurJauge.setValeur(vWD_gnCouleurJauge);


// nX1, nX2, nY1, nY2 sont des entiers





// nDifférence est un entier 


// MonImage est une Image
vWD_MonImage = new WDInstance( new WDImage() );


// MonImageRef est Image = IMG_Dessin
vWD_MonImageRef = new WDInstance( new WDImage() );

vWD_MonImageRef.setValeur(mWD_IMG_Dessin);


// nFacteur est entier = MonImageRef..EchelleDessin

vWD_nFacteur.setValeur(vWD_MonImageRef.getProp(EWDPropriete.PROP_ECHELLEDESSIN));


// nLargeur est entier = IMG_Dessin..Largeur 

vWD_nLargeur.setValeur(mWD_IMG_Dessin.getProp(EWDPropriete.PROP_LARGEUR));


// nHauteur est entier = IMG_Dessin..Hauteur 

vWD_nHauteur.setValeur(mWD_IMG_Dessin.getProp(EWDPropriete.PROP_HAUTEUR));


// MonImage..Largeur = nLargeur*nFacteur
vWD_MonImage.setProp(EWDPropriete.PROP_LARGEUR,vWD_nLargeur.opMult(vWD_nFacteur));

// MonImage..Hauteur = nHauteur*nFacteur
vWD_MonImage.setProp(EWDPropriete.PROP_HAUTEUR,vWD_nHauteur.opMult(vWD_nFacteur));

// MonImage..EchelleDessin = nFacteur
vWD_MonImage.setProp(EWDPropriete.PROP_ECHELLEDESSIN,vWD_nFacteur);

// nCentreX est un entier = nLargeur/2

vWD_nCentreX.setValeur(vWD_nLargeur.opDiv(2));


// nCentreY est un entier = nHauteur/2

vWD_nCentreY.setValeur(vWD_nHauteur.opDiv(2));


// dDébutDessin(MonImage)
WDAPIDessin.dDebutDessin(vWD_MonImage);

// SI nFacteur <= 1 ALORS
if(vWD_nFacteur.opInfEgal(1))
{
// 	dChangeMode(dessinAntiAliasing)
WDAPIDessin.dChangeMode(256);

}

// nDiametreCercle = MonImage..Largeur * 95/100
vWD_nDiametreCercle.setValeur(vWD_MonImage.getProp(EWDPropriete.PROP_LARGEUR).opMult(95).opDiv(100));

// nDifférence = MonImage..Largeur - nDiametreCercle
vWD_nDifference.setValeur(vWD_MonImage.getProp(EWDPropriete.PROP_LARGEUR).opMoins(vWD_nDiametreCercle));

// SI nPourcent > 0 ALORS
if(vWD_nPourcent.opSup(0))
{
// 	nAngleDébut est un entier = gnAngleDépart
WDObjet vWD_nAngleDebut = new WDEntier4();


vWD_nAngleDebut.setValeur(vWD_gnAngleDepart);


// 	nAngleFin est un entier = modulo((nPourcent * 360 / 100)+gnAngleDépart,360)
WDObjet vWD_nAngleFin = new WDEntier4();


vWD_nAngleFin.setValeur(vWD_nPourcent.opMult(360).opDiv(100).opPlus(vWD_gnAngleDepart).opMod(360));


// 	_PositionsSelonAngle(gnAngleDépart, nCentreX, nCentreY, nDiametreCercle, nX1, nY1)
fWD__PositionsSelonAngle(vWD_gnAngleDepart,vWD_nCentreX,vWD_nCentreY,vWD_nDiametreCercle,vWD_nX1,vWD_nY1);

// 	_PositionsSelonAngle(nAngleFin, nCentreX, nCentreY, nDiametreCercle, nX2, nY2)
fWD__PositionsSelonAngle(vWD_nAngleFin,vWD_nCentreX,vWD_nCentreY,vWD_nDiametreCercle,vWD_nX2,vWD_nY2);

// 	dPortion(nDifférence/nFacteur, nDifférence/nFacteur, (MonImage..Hauteur-nDifférence)/nFacteur, (MonImage..Largeur-nDifférence)/nFacteur, nX1/nFacteur, nY1/nFacteur, nX2/nFacteur, nY2/nFacteur, nCouleurJauge, nCouleurJauge)
WDAPIDessin.dPortion(vWD_nDifference.opDiv(vWD_nFacteur),vWD_nDifference.opDiv(vWD_nFacteur).getInt(),vWD_MonImage.getProp(EWDPropriete.PROP_HAUTEUR).opMoins(vWD_nDifference).opDiv(vWD_nFacteur).getInt(),vWD_MonImage.getProp(EWDPropriete.PROP_LARGEUR).opMoins(vWD_nDifference).opDiv(vWD_nFacteur).getInt(),vWD_nX1.opDiv(vWD_nFacteur).getInt(),vWD_nY1.opDiv(vWD_nFacteur).getInt(),vWD_nX2.opDiv(vWD_nFacteur).getInt(),vWD_nY2.opDiv(vWD_nFacteur).getInt(),vWD_nCouleurJauge,vWD_nCouleurJauge);

}

// nDiametreCercle = MonImage..Largeur * 50/100
vWD_nDiametreCercle.setValeur(vWD_MonImage.getProp(EWDPropriete.PROP_LARGEUR).opMult(50).opDiv(100));

// dCercle((nCentreX - nDiametreCercle/2)/nFacteur, (nCentreY - nDiametreCercle/2)/nFacteur, (nCentreX + nDiametreCercle/2)/nFacteur, (nCentreY + nDiametreCercle/2)/nFacteur, gnCouleurDécor)
WDAPIDessin.dCercle(vWD_nCentreX.opMoins(vWD_nDiametreCercle.opDiv(2)).opDiv(vWD_nFacteur),vWD_nCentreY.opMoins(vWD_nDiametreCercle.opDiv(2)).opDiv(vWD_nFacteur).getInt(),vWD_nCentreX.opPlus(vWD_nDiametreCercle.opDiv(2)).opDiv(vWD_nFacteur).getInt(),vWD_nCentreY.opPlus(vWD_nDiametreCercle.opDiv(2)).opDiv(vWD_nFacteur).getInt(),vWD_gnCouleurDecor);

// SI gbSensHoraire ALORS
if(vWD_gbSensHoraire.getBoolean())
{
// 	dSymétrieVerticale(MonImage)
WDAPIDessin.dSymetrieVerticale(vWD_MonImage);

}

// IMG_Dessin = MonImage
mWD_IMG_Dessin.setValeur(vWD_MonImage);

}
finally
{
finExecProcLocale();
}

}



//  Résumé : Récupère la position X et Y d'un point du cercle selon l'angle
//  Syntaxe :
// _PositionsSelonAngle (<nAngle> est entier, <nCentreX> est entier, <nCentreY> est entier, <nDiametreCercle> est entier, <nX> est entier, <nY> est entier)
// 
//  Paramètres :
// 	nAngle (entier) : Angle
// 	nCentreX (entier) : X du centre
// 	nCentreY (entier) : Y du centre
// 	nDiametreCercle (entier) : Diamètre
// 	nX (entier) : Position X déterminée
// 	nY (entier) : Position Y déterminée
//  Valeur de retour :
//  	Aucune
// 
public void fWD__PositionsSelonAngle( WDObjet vWD_nAngle , WDObjet vWD_nCentreX , WDObjet vWD_nCentreY , WDObjet vWD_nDiametreCercle , WDObjet vWD_nX , WDObjet vWD_nY )
{
// PROCEDURE PRIVÉE _PositionsSelonAngle(LOCAL nAngle est un entier, nCentreX est un entier, nCentreY est un entier, nDiametreCercle est un entier, nX est un entier, nY est un entier)
initExecProcLocale("_PositionsSelonAngle");

try
{
vWD_nAngle = WDParametre.traiterParametre(vWD_nAngle, 1, true, 8);

vWD_nCentreX = WDParametre.traiterParametre(vWD_nCentreX, 2, false, 8);

vWD_nCentreY = WDParametre.traiterParametre(vWD_nCentreY, 3, false, 8);

vWD_nDiametreCercle = WDParametre.traiterParametre(vWD_nDiametreCercle, 4, false, 8);

vWD_nX = WDParametre.traiterParametre(vWD_nX, 5, false, 8);

vWD_nY = WDParametre.traiterParametre(vWD_nY, 6, false, 8);


// nX = nCentreX + (nDiametreCercle/2) * Cos(nAngle)
vWD_nX.setValeur(vWD_nCentreX.opPlus(vWD_nDiametreCercle.opDiv(2).opMult(WDAPIMath.cosinus(vWD_nAngle.getDouble()))));

// nY = nCentreY + (nDiametreCercle/2) * -Sin(nAngle)
vWD_nY.setValeur(vWD_nCentreY.opPlus(vWD_nDiametreCercle.opDiv(2).opMult(WDAPIMath.sinus(vWD_nAngle.getDouble()).opMoinsUnaire())));

}
finally
{
finExecProcLocale();
}

}



public void fWD_dessineJauge( WDObjet vWD_nValeur , WDObjet vWD_nValeurMax )
{
// PROCÉDURE DessineJauge(LOCAL nValeur est un entier, LOCAL nValeurMax est un entier)
initExecProcLocale("DessineJauge");

try
{
vWD_nValeur = WDParametre.traiterParametre(vWD_nValeur, 1, true, 8);

vWD_nValeurMax = WDParametre.traiterParametre(vWD_nValeurMax, 2, true, 8);


// nValeur = Min(nValeur, nValeurMax)
vWD_nValeur.setValeur(WDAPINum.min(new WDObjet[] {vWD_nValeur,vWD_nValeurMax} ));

// SELON gnType
// Délimiteur de visibilité pour ne pas étendre la visibilité de la variable temporaire _WDExpSelon
{
// SELON gnType
WDObjet _WDExpSelon0 = vWD_gnType;
if(_WDExpSelon0.opEgal(1))
{
// 	CAS TYPE_ROND : _DessineJaugeRond(100*nValeur/nValeurMax)
fWD__DessineJaugeRond(new WDEntier4(100).opMult(vWD_nValeur).opDiv(vWD_nValeurMax));

}
else if(_WDExpSelon0.opEgal(2))
{
// 	CAS TYPE_ANNEAU : _DessineJaugeAnneau(100*nValeur/nValeurMax)		
fWD__DessineJaugeAnneau(new WDEntier4(100).opMult(vWD_nValeur).opDiv(vWD_nValeurMax));

}

}

// IMG_Dessin..Visible=Vrai
mWD_IMG_Dessin.setProp(EWDPropriete.PROP_VISIBLE,true);

// SI gbAvecTexte ALORS
if(vWD_gbAvecTexte.getBoolean())
{
// 	LIB_Valeur = nValeur
mWD_LIB_Valeur.setValeur(vWD_nValeur);

// 	LIB_Total = nValeurMax
mWD_LIB_Total.setValeur(vWD_nValeurMax);

// 	LIB_Valeur..Visible = Vrai
mWD_LIB_Valeur.setProp(EWDPropriete.PROP_VISIBLE,true);

// 	LIB_Total..Visible = Vrai
mWD_LIB_Total.setProp(EWDPropriete.PROP_VISIBLE,true);

// 	LIB_Valeur..Couleur = gnCouleurTexte
mWD_LIB_Valeur.setProp(EWDPropriete.PROP_COULEUR,vWD_gnCouleurTexte);

// 	LIB_Total..Couleur = gnCouleurTexte
mWD_LIB_Total.setProp(EWDPropriete.PROP_COULEUR,vWD_gnCouleurTexte);

}
else
{
// 	LIB_Valeur..Visible = Faux
mWD_LIB_Valeur.setProp(EWDPropriete.PROP_VISIBLE,false);

// 	LIB_Total..Visible = Faux
mWD_LIB_Total.setProp(EWDPropriete.PROP_VISIBLE,false);

}

}
finally
{
finExecProcLocale();
}

}




/**
 * Traitement: Déclarations globales de FI_Mobile_Compteur_Jauge
 */
public void declarerGlobale(WDObjet[] WD_tabParam)
{
// PROCEDURE MaFenêtre(gnValeur est un entier = 0, gnValeurMaximale est un entier = 10 000)
super.declarerGlobale(WD_tabParam, 0, 2);
int WD_ntabParamLen = 0;
if(WD_tabParam!=null) WD_ntabParamLen = WD_tabParam.length;

// Traitement du paramètre n°0
if(0<WD_ntabParamLen) 
{
vWD_gnValeur = WD_tabParam[0];
}
else { vWD_gnValeur = new WDEntier4(0); }
super.ajouterVariableGlobale("gnValeur",vWD_gnValeur);

// Traitement du paramètre n°1
if(1<WD_ntabParamLen) 
{
vWD_gnValeurMaximale = WD_tabParam[1];
}
else { vWD_gnValeurMaximale = new WDEntier4(10000); }
super.ajouterVariableGlobale("gnValeurMaximale",vWD_gnValeurMaximale);


vWD_gnValeur = WDParametre.traiterParametre(vWD_gnValeur, 1, false, 8);

vWD_gnValeurMaximale = WDParametre.traiterParametre(vWD_gnValeurMaximale, 2, false, 8);


// CONSTANTE

// gnAngleDépart est un entier = 90 
vWD_gnAngleDepart = new WDEntier4();

vWD_gnAngleDepart.setValeur(90);

super.ajouterVariableGlobale("gnAngleDépart",vWD_gnAngleDepart);



// gnType est un entier = TYPE_ANNEAU
vWD_gnType = new WDEntier4();

vWD_gnType.setValeur(2);

super.ajouterVariableGlobale("gnType",vWD_gnType);



// gnCouleurJauge est un entier = RVB(41, 128, 185) 
vWD_gnCouleurJauge = new WDEntier4();

vWD_gnCouleurJauge.setValeur(WDAPIDessin.rvb(41,128,185));

super.ajouterVariableGlobale("gnCouleurJauge",vWD_gnCouleurJauge);



// gnCouleurDécor est un entier =  gnType=TYPE_ROND ? RVB(52, 73, 94) 	SINON  RVB(239, 246, 255) 
vWD_gnCouleurDecor = new WDEntier4();

vWD_gnCouleurDecor.setValeur((vWD_gnType.opEgal(1) ? (WDObjet)WDAPIDessin.rvb(52,73,94) : (WDObjet)WDAPIDessin.rvb(239,246,255)));

super.ajouterVariableGlobale("gnCouleurDécor",vWD_gnCouleurDecor);



// gnCouleurTexte est un entier =  gnType=TYPE_ROND ? RVB(226, 232, 233) SINON  RVB(52, 73, 94) 
vWD_gnCouleurTexte = new WDEntier4();

vWD_gnCouleurTexte.setValeur((vWD_gnType.opEgal(1) ? (WDObjet)WDAPIDessin.rvb(226,232,233) : (WDObjet)WDAPIDessin.rvb(52,73,94)));

super.ajouterVariableGlobale("gnCouleurTexte",vWD_gnCouleurTexte);



// gbAvecTexte est un booléen = Vrai 
vWD_gbAvecTexte = new WDBooleen();

vWD_gbAvecTexte.setValeur(true);

super.ajouterVariableGlobale("gbAvecTexte",vWD_gbAvecTexte);



// gbSensHoraire est un booléen = Vrai
vWD_gbSensHoraire = new WDBooleen();

vWD_gbSensHoraire.setValeur(true);

super.ajouterVariableGlobale("gbSensHoraire",vWD_gbSensHoraire);



// DessineJauge(gnValeur,gnValeurMaximale)
fWD_dessineJauge(vWD_gnValeur,vWD_gnValeurMaximale);

}




// Activation des écouteurs: 
public void activerEcoute()
{
}

////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
 public WDObjet vWD_gnValeur = WDVarNonAllouee.ref;
 public WDObjet vWD_gnValeurMaximale = WDVarNonAllouee.ref;
 public WDObjet vWD_gnAngleDepart = WDVarNonAllouee.ref;
 public WDObjet vWD_gnType = WDVarNonAllouee.ref;
 public WDObjet vWD_gnCouleurJauge = WDVarNonAllouee.ref;
 public WDObjet vWD_gnCouleurDecor = WDVarNonAllouee.ref;
 public WDObjet vWD_gnCouleurTexte = WDVarNonAllouee.ref;
 public WDObjet vWD_gbAvecTexte = WDVarNonAllouee.ref;
 public WDObjet vWD_gbSensHoraire = WDVarNonAllouee.ref;
////////////////////////////////////////////////////////////////////////////
// Création des champs de la fenêtre FI_Mobile_Compteur_Jauge
////////////////////////////////////////////////////////////////////////////
protected void creerChamps()
{
mWD_IMG_Dessin = new GWDIMG_Dessin();
mWD_LIB_Total = new GWDLIB_Total();
mWD_LIB_Valeur = new GWDLIB_Valeur();

}
////////////////////////////////////////////////////////////////////////////
// Initialisation de la fenêtre FI_Mobile_Compteur_Jauge
////////////////////////////////////////////////////////////////////////////
public  void initialiserObjet()
{
super.initialiserObjet();
super.setQuid(3300694573850729180l);

super.setChecksum("1346191382");

super.setNom("FI_Mobile_Compteur_Jauge");

super.setMenuContextuelSysteme();

super.setNote("", "");

super.setTailleInitiale(250, 250);

super.setTailleMin(0, 0);

super.setTailleMax(2147483647, 2147483647);

super.setCouleurFond(0xFFFFFFFF);


activerEcoute();

////////////////////////////////////////////////////////////////////////////
// Initialisation des champs de FI_Mobile_Compteur_Jauge
////////////////////////////////////////////////////////////////////////////
mWD_IMG_Dessin.initialiserObjet();
super.ajouter("IMG_Dessin", mWD_IMG_Dessin);
mWD_LIB_Total.initialiserObjet();
super.ajouter("LIB_Total", mWD_LIB_Total);
mWD_LIB_Valeur.initialiserObjet();
super.ajouter("LIB_Valeur", mWD_LIB_Valeur);

super.terminerInitialisation();
}

////////////////////////////////////////////////////////////////////////////
public boolean isUniteAffichageLogique()
{
return true;
}

public WDProjet getProjet()
{
return GWDPHome_Biking.getInstance();
}

public IWDEnsembleElement getEnsemble()
{
return GWDPHome_Biking.getInstance();
}
public int getModeContexteHF()
{
return 1;
}
}
