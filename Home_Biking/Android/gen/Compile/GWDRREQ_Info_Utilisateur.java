/**
 * Code généré par WINDEV Mobile - NE PAS MODIFIER !
 * Objet WINDEV Mobile : Requête
 * Classe Android : REQ_Info_Utilisateur
 * Date : 16/12/2020 08:51:30
 * Version de wdjava.dll  : 25.0.221.6
 */


package com.logicorp.home_biking.wdgen;


import com.logicorp.home_biking.*;
import fr.pcsoft.wdjava.core.types.*;
import fr.pcsoft.wdjava.core.*;
import fr.pcsoft.wdjava.database.hf.requete.parsing.*;
/*Imports trouvés dans le code WL*/
/*Fin Imports trouvés dans le code WL*/



public class GWDRREQ_Info_Utilisateur extends WDDescRequeteWDR
{
public String getNomLogique()
{
return "REQ_Info_Utilisateur";
}
public String getCodeSQLOriginal()
{
return " SELECT  Compte_Client.nom AS nom,\t Compte_Client.prenom AS prenom,\t Compte_Client.pma AS pma,\t Compte_Client.fcm AS fcm  FROM  Compte_Client  WHERE   Compte_Client.Id_Compte_Client = 1";
}
public Requete initArbre() throws WDInvalidSQLException
{
Select varSelect = new Select();
varSelect.setType(1);
Rubrique rub_nom = new Rubrique();
rub_nom.setNom("nom");
rub_nom.setAlias("nom");
rub_nom.setNomFichier("Compte_Client");
rub_nom.setAliasFichier("Compte_Client");
varSelect.ajouterElement(rub_nom);
Rubrique rub_prenom = new Rubrique();
rub_prenom.setNom("prenom");
rub_prenom.setAlias("prenom");
rub_prenom.setNomFichier("Compte_Client");
rub_prenom.setAliasFichier("Compte_Client");
varSelect.ajouterElement(rub_prenom);
Rubrique rub_pma = new Rubrique();
rub_pma.setNom("pma");
rub_pma.setAlias("pma");
rub_pma.setNomFichier("Compte_Client");
rub_pma.setAliasFichier("Compte_Client");
varSelect.ajouterElement(rub_pma);
Rubrique rub_fcm = new Rubrique();
rub_fcm.setNom("fcm");
rub_fcm.setAlias("fcm");
rub_fcm.setNomFichier("Compte_Client");
rub_fcm.setAliasFichier("Compte_Client");
varSelect.ajouterElement(rub_fcm);
From varFrom = new From();
Fichier fic_Compte_Client = new Fichier();
fic_Compte_Client.setNom("Compte_Client");
fic_Compte_Client.setAlias("Compte_Client");
varFrom.ajouterElement(fic_Compte_Client);
Requete varReqSelect = new Requete(1);
varReqSelect.ajouterClause(varSelect);
varReqSelect.ajouterClause(varFrom);
Expression expr__ = new Expression(9, "=", "Compte_Client.Id_Compte_Client = 1");
Rubrique rub_Id_Compte_Client = new Rubrique();
rub_Id_Compte_Client.setNom("Compte_Client.Id_Compte_Client");
rub_Id_Compte_Client.setAlias("Id_Compte_Client");
rub_Id_Compte_Client.setNomFichier("Compte_Client");
rub_Id_Compte_Client.setAliasFichier("Compte_Client");
expr__.ajouterElement(rub_Id_Compte_Client);
Literal varLiteral = new Literal();
varLiteral.setValeur("1");
varLiteral.setTypeWL(8);
expr__.ajouterElement(varLiteral);
Where varWhere = new Where();
varWhere.ajouterElement(expr__);
varReqSelect.ajouterClause(varWhere);
Limit varLimit = new Limit();
varLimit.setType(0);
varLimit.setNbEnregs(0);
varLimit.setOffset(0);
varReqSelect.ajouterClause(varLimit);
return varReqSelect;
}
public String getNomFichier(int nIndex)
{
switch(nIndex)
{
case 0 : return "Compte_Client";
default: return null;
}
}
public String getAliasFichier(int nIndex)
{
switch(nIndex)
{
case 0 : return "Compte_Client";
default: return null;
}
}


public int getIdWDR()
{
return com.logicorp.home_biking.R.raw.req_info_utilisateur;
}
public String getNomFichierWDR()
{
return "req_info_utilisateur";
}
}
