/**
 * Code généré par WINDEV Mobile - NE PAS MODIFIER !
 * Objet WINDEV Mobile : Collection
 * Classe Android : COL_Suppr_Creation
 * Date : 06/03/2021 17:25:16
 * Version de wdjava.dll  : 25.0.221.6
 */


package com.logicorp.home_biking.wdgen;


import com.logicorp.home_biking.*;
import fr.pcsoft.wdjava.core.types.*;
import fr.pcsoft.wdjava.core.*;
import fr.pcsoft.wdjava.core.application.*;
import fr.pcsoft.wdjava.api.*;
/*Imports trouvés dans le code WL*/
/*Fin Imports trouvés dans le code WL*/



public class GWDCPCOL_Suppr_Creation extends WDCollProcAndroid
{

public WDProjet getProjet()
{
return GWDPHome_Biking.getInstance();
}

public IWDEnsembleElement getEnsemble()
{
return GWDPHome_Biking.getInstance();
}

protected String getNomCollection()
{
return "COL_Suppr_Creation";
}
private final static GWDCPCOL_Suppr_Creation ms_instance = new GWDCPCOL_Suppr_Creation();
public final static GWDCPCOL_Suppr_Creation getInstance()
{
return ms_instance;
}

// Code de déclaration de COL_Suppr_Creation
static public void init()
{
// 
ms_instance.initDeclarationCollection();

try
{
}
finally
{
finDeclarationCollection();
}

}




// Code de terminaison de COL_Suppr_Creation
static public void term()
{
// 
ms_instance.initTerminaisonCollection();

try
{
}
finally
{
finTerminaisonCollection();
}

}



// Nombre de Procédures : 1
static public void fWD_supprCrea()
{
// PROCEDURE SupprCrea()
ms_instance.initExecProcGlobale("SupprCrea");

try
{
// SI HNbEnr(Creation_Programme)>0 ALORS
if(WDAPIHF.hNbEnr(WDAPIHF.getFichierSansCasseNiAccent("creation_programme")).opSup(0))
{
// 	HLitPremier(Creation_Programme)
WDAPIHF.hLitPremier(WDAPIHF.getFichierSansCasseNiAccent("creation_programme"));

// 	BOUCLE
{
do
{
// 		HSupprime(Creation_Programme,hNumEnrEnCours)
WDAPIHF.hSupprime(WDAPIHF.getFichierSansCasseNiAccent("creation_programme"),(long)0);

// 		HModifie(Creation_Programme,hNumEnrEnCours)
WDAPIHF.hModifie(WDAPIHF.getFichierSansCasseNiAccent("creation_programme"),(long)0);

// 		HLitSuivant(Creation_Programme)
WDAPIHF.hLitSuivant(WDAPIHF.getFichierSansCasseNiAccent("creation_programme"));

}
while(WDAPIHF.hTrouve().getBoolean());
}

}

// SI HNbEnr(Creation_contenir)>0 ALORS
if(WDAPIHF.hNbEnr(WDAPIHF.getFichierSansCasseNiAccent("creation_contenir")).opSup(0))
{
// 	HLitPremier(Creation_contenir)
WDAPIHF.hLitPremier(WDAPIHF.getFichierSansCasseNiAccent("creation_contenir"));

// 	BOUCLE
{
do
{
// 		HSupprime(Creation_contenir,hNumEnrEnCours)
WDAPIHF.hSupprime(WDAPIHF.getFichierSansCasseNiAccent("creation_contenir"),(long)0);

// 		HLitSuivant(Creation_contenir)
WDAPIHF.hLitSuivant(WDAPIHF.getFichierSansCasseNiAccent("creation_contenir"));

}
while(WDAPIHF.hTrouve().getBoolean());
}

}

}
finally
{
finExecProcGlobale();
}

}



////////////////////////////////////////////////////////////////////////////
// Déclaration des variables globales
////////////////////////////////////////////////////////////////////////////
}
