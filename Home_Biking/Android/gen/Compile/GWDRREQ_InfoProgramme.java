/**
 * Code généré par WINDEV Mobile - NE PAS MODIFIER !
 * Objet WINDEV Mobile : Requête
 * Classe Android : REQ_InfoProgramme
 * Date : 16/12/2020 08:51:30
 * Version de wdjava.dll  : 25.0.221.6
 */


package com.logicorp.home_biking.wdgen;


import com.logicorp.home_biking.*;
import fr.pcsoft.wdjava.core.types.*;
import fr.pcsoft.wdjava.core.*;
import fr.pcsoft.wdjava.database.hf.requete.parsing.*;
/*Imports trouvés dans le code WL*/
/*Fin Imports trouvés dans le code WL*/



public class GWDRREQ_InfoProgramme extends WDDescRequeteWDR
{
public String getNomLogique()
{
return "REQ_InfoProgramme";
}
public String getCodeSQLOriginal()
{
return "SELECT \r\n\tCOUNT(contenir.Id_Exercice) AS Nb_Exercices,\t\r\n\tSUM((contenir.temps+contenir.repos)*contenir.nb_rep) AS Temps_Total\r\nFROM \r\n\tcontenir\r\nWHERE \r\n\tcontenir.Id_Programme = {ParamId_Programme#0}\r\n";
}
public Requete initArbre() throws WDInvalidSQLException
{
Select varSelect = new Select();
varSelect.setType(1);
Expression varExprAgregat = new Expression(31, "COUNT", "COUNT(contenir.Id_Exercice)");
varExprAgregat.ajouterOption(EWDOptionRequete.SELECT, "1");
Rubrique rub_Id_Exercice = new Rubrique();
rub_Id_Exercice.setNom("contenir.Id_Exercice");
rub_Id_Exercice.setAlias("Id_Exercice");
rub_Id_Exercice.setNomFichier("contenir");
rub_Id_Exercice.setAliasFichier("contenir");
varExprAgregat.setAlias("Nb_Exercices");
varExprAgregat.ajouterElement(rub_Id_Exercice);
varSelect.ajouterElement(varExprAgregat);
Expression varExprAgregat_1 = new Expression(28, "SUM", "SUM((contenir.temps+contenir.repos)*contenir.nb_rep)");
varExprAgregat_1.ajouterOption(EWDOptionRequete.SELECT, "1");
Expression expr__ = new Expression(4, "*", "(contenir.temps+contenir.repos)*contenir.nb_rep");
Expression expr___1 = new Expression(0, "+", "(contenir.temps+contenir.repos)");
Rubrique rub_temps = new Rubrique();
rub_temps.setNom("contenir.temps");
rub_temps.setAlias("temps");
rub_temps.setNomFichier("contenir");
rub_temps.setAliasFichier("contenir");
expr___1.ajouterElement(rub_temps);
Rubrique rub_repos = new Rubrique();
rub_repos.setNom("contenir.repos");
rub_repos.setAlias("repos");
rub_repos.setNomFichier("contenir");
rub_repos.setAliasFichier("contenir");
expr___1.ajouterElement(rub_repos);
expr__.ajouterElement(expr___1);
Rubrique rub_nb_rep = new Rubrique();
rub_nb_rep.setNom("contenir.nb_rep");
rub_nb_rep.setAlias("nb_rep");
rub_nb_rep.setNomFichier("contenir");
rub_nb_rep.setAliasFichier("contenir");
expr__.ajouterElement(rub_nb_rep);
varExprAgregat_1.setAlias("Temps_Total");
varExprAgregat_1.ajouterElement(expr__);
varSelect.ajouterElement(varExprAgregat_1);
From varFrom = new From();
Fichier fic_contenir = new Fichier();
fic_contenir.setNom("contenir");
fic_contenir.setAlias("contenir");
varFrom.ajouterElement(fic_contenir);
Requete varReqSelect = new Requete(1);
varReqSelect.ajouterClause(varSelect);
varReqSelect.ajouterClause(varFrom);
Expression expr___2 = new Expression(9, "=", "contenir.Id_Programme = {ParamId_Programme}");
Rubrique rub_Id_Programme = new Rubrique();
rub_Id_Programme.setNom("contenir.Id_Programme");
rub_Id_Programme.setAlias("Id_Programme");
rub_Id_Programme.setNomFichier("contenir");
rub_Id_Programme.setAliasFichier("contenir");
expr___2.ajouterElement(rub_Id_Programme);
Parametre param_ParamId_Programme = new Parametre();
param_ParamId_Programme.setNom("ParamId_Programme");
expr___2.ajouterElement(param_ParamId_Programme);
Where varWhere = new Where();
varWhere.ajouterElement(expr___2);
varReqSelect.ajouterClause(varWhere);
Limit varLimit = new Limit();
varLimit.setType(0);
varLimit.setNbEnregs(0);
varLimit.setOffset(0);
varReqSelect.ajouterClause(varLimit);
return varReqSelect;
}
public String getNomFichier(int nIndex)
{
switch(nIndex)
{
case 0 : return "contenir";
default: return null;
}
}
public String getAliasFichier(int nIndex)
{
switch(nIndex)
{
case 0 : return "contenir";
default: return null;
}
}


public int getIdWDR()
{
return com.logicorp.home_biking.R.raw.req_infoprogramme;
}
public String getNomFichierWDR()
{
return "req_infoprogramme";
}
}
