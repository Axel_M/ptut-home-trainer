/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.logicorp.home_biking;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.logicorp.home_biking";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 128;
  public static final String VERSION_NAME = "0.0.125.0";
}
